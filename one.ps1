Andrey.Isakov@waveaccess.ru
8378Fz2vM
username: myquiz
pass: myquizVMazure200$

Set-ExecutionPolicy RemoteSigned  # if error in PowerShell


Get-AzVM -ResourceGroupName myquiz-move -Name foovm -Status
Get-AzVM -ResourceGroupName myquiz-move -Name foovm 

Get-AzStorageAccount -ResourceGroupName myquiz-move

Get-AzVMExtension -ResourceGroupName myquiz-move -VMName foovm [-Name customScript]



New-AzVm `
    -ResourceGroupName "myquiz-move" `
    -Name "foovm3" `
    -Location "East US" `
    -Image "UbuntuLTS" `
    -VirtualNetworkName "myVnet" `
    -SubnetName "mySubnet" `
    -SecurityGroupName "myNetworkSecurityGroup" `
    -PublicIpAddressName "myPublicIpAddress" `
    -OpenPorts 80, 22, 3389
    
Get-AzVMExtension -ResourceGroupName myquiz-move -VMName foovm3 -Status    
$VMCustomScriptExtension = Get-AzVMExtension -ResourceGroupName myquiz-move -VMName foovm -Name customScript
$ext = Get-AzVMExtension -ResourceGroupName myquiz-move -VMName foovm3 
Get-AzVMExtension -ResourceGroupName myquiz-move -VMName foovm3 -Name CustomScriptForLinux
Remove-AzVMExtension -ResourceGroupName myquiz-move -Name customScript -VMName foovm -Force  

# Set-AzureRmVMCustomScriptExtension -ResourceGroupName "myquiz-move" -Location "Central US" -VMName "foovm3" -Name "ContosoTest" -TypeHandlerVersion "1.1" -StorageAccountName "Contoso" -StorageAccountKey <StorageKey> -FileName "script.sh" -ContainerName "Scripts"
Set-AzureRmVMCustomScriptExtension -ResourceGroupName "myquiz-move" -Location "Central US" -VMName "foovm3" -Name "ContosoTest" -TypeHandlerVersion "1.1" -StorageAccountName "Contoso" -StorageAccountKey StorageKey> -FileName "script.sh" -ContainerName "Scripts"

$stokey = Get-AzStorageAccountKey -ResourceGroupName myquiz-move -AccountName myquizmovediag
$stokey = Get-AzStorageAccountKey -ResourceGroupName myquiz-move -AccountName $storage.StorageAccountName
$Settings = @{"fileUris" = "[]"; "commandToExecute" = ""};
$foo = ConvertFrom-Json $ext.PublicSettings
$Settings = @{"fileUris" = $foo.fileUris; "commandToExecute" = $foo.commandToExecute};
$VMCustomScriptExtension.VMName='foovm3'
$ProtectedSettings = @{"storageAccountName" = $storage.StorageAccountName; "storageAccountKey" = $stokey};
Set-AzVMExtension -ResourceGroupName myquiz-move -Location "East US" -VMName foovm3 -Name "ContosoTest" -Publisher "Microsoft.Azure.Extensions" -Type "Microsoft.Azure.Extensions.customScript" -TypeHandlerVersion "2.0" -Settings $Settings -ProtectedSettings $ProtectedSettings;
Set-AzVMExtension -ResourceGroupName $VMCustomScriptExtension.ResourceGroupName -Location $VMCustomScriptExtension.Location -VMName $VMCustomScriptExtension.VMName -Name $VMCustomScriptExtension.Name -Publisher $VMCustomScriptExtension.Publisher -Type $VMCustomScriptExtension.ExtensionType -TypeHandlerVersion $VMCustomScriptExtension.TypeHandlerVersion -Settings $Settings -ProtectedSettings $ProtectedSettings;


#------------WHOLE VMSS-------------
$Vmss =  Get-AzVmss -ResourceGroupName myquiz-move -VMScaleSetName foo
$SourceVaultId =  $Vmss.VirtualMachineProfile.OsProfile.Secrets[0].SourceVault.Id 
$VaultCertificate = $Vmss.VirtualMachineProfile.OsProfile.Secrets[0].VaultCertificates[0]
Remove-AzVmss -ResourceGroupName myquiz-move -VMScaleSetName foo -Force
$Vmss.VirtualMachineProfile.OsProfile.Secrets=$null  
# $Vault = Get-AzKeyVault -VaultName bar
# $CertConfig = New-AzVmssVaultCertificateConfig -CertificateUrl "https://bar.vault.azure.net/secrets/bar/81515254786a44d7803357459df53a18"
New-AzVmss -ResourceGroupName myquiz-move -VMScaleSetName foo -VirtualMachineScaleSet $Vmss
# Add-AzVmssSecret -VirtualMachineScaleSet $Vmss -SourceVaultId $Vault.ResourceId -VaultCertificate $CertConfig
Add-AzVmssSecret -VirtualMachineScaleSet $Vmss -SourceVaultId $SourceVaultId -VaultCertificate $VaultCertificate
New-AzVmss -ResourceGroupName myquiz-move -VMScaleSetName foo -VirtualMachineScaleSet $Vmss
#--------------------------------
$Vmss =  Get-AzVmss -ResourceGroupName myquiz -VMScaleSetName myquizperfss

Add-AzVmssExtension -VirtualMachineScaleSet $Vmss -Name customScript -Publisher Microsoft.Compute -ForceUpdateTag 2d516fcf-fc50-4a18-9e28-046b3431dfaf -Type CustomScriptExtension -TypeHandlerVersion 1.9 -AutoUpgradeMinorVersion $False -Setting '{"fileUris":["https://myquizyad.blob.core.windows.net/scripts/myqg-perf_deploy_ssl.ps1","https://myquizyad.blob.core.windows.net/scripts/ConfigureRemotingForAnsible.ps1"],"commandToExecute":"powershell -ExecutionPolicy Unrestricted -File myqg-perf_deploy_ssl.ps1"}'

$Ext0 = $Vmss.VirtualMachineProfile.ExtensionProfile.Extensions[0]
$Ext0 = (Get-AzVmss -ResourceGroupName myquiz -VMScaleSetName myquizperfss).VirtualMachineProfile.ExtensionProfile.Extensions[0]
Add-AzVmssExtension -VirtualMachineScaleSet $Vmss -Name $Ext0.Name -Publisher $Ext0.Publisher -ForceUpdateTag $Ext0.ForceUpdateTag -Type $Ext0.Type -TypeHandlerVersion $Ext0.TypeHandlerVersion -AutoUpgradeMinorVersion $Ext0.AutoUpgradeMinorVersion -Setting $Ext0.Settings

Update-AzVmss -ResourceGroupName myquiz -VMScaleSetName myquizperfss -AutomaticOSUpgrade $false

$Vmss.VirtualMachineProfile.StorageProfile.ImageReference=$null
Set-AzVmssStorageProfile -VirtualMachineScaleSet $Vmss -ImageReferenceId '/subscriptions/8e293158-b459-4a99-be65-872e94944d4e/resourceGroups/myquiz/providers/Microsoft.Compute/images/myqgssvm-image-20180709120300'
Remove-AzVmss -ResourceGroupName myquiz -VMScaleSetName myquizperfss -Force
Set-AzVmssOSProfile -VirtualMachineScaleSet $Vmss   -AdminPassword "myquizVMazure200$"
New-AzVmss -ResourceGroupName myquiz -VMScaleSetName myquizperfss -VirtualMachineScaleSet $Vmss 

Update-AzVmss -ResourceGroupName myquiz -VMScaleSetName myquizperfss -VirtualMachineScaleSet $Vmss -ManagedDiskStorageAccountType "StandardLRS"


$CertConfig = New-AzVmssVaultCertificateConfig -CertificateUrl "https://myquiz.vault.azure.net/secrets/myqg-ru/93074e68c2c84737a1da1636aa05139c" -CertificateStore "WebHosting"
Add-AzVmssSecret -VirtualMachineScaleSet $Vmss -SourceVaultId "/subscriptions/8e293158-b459-4a99-be65-872e94944d4e/resourceGroups/myquiz/providers/Microsoft.KeyVault/vaults/myquiz" -VaultCertificate $CertConfig

$IPCfg = New-AzVmssIPConfig -Name "myquizperfssIpConfig3" -LoadBalancerBackendAddressPoolsId "/subscriptions/8e293158-b459-4a99-be65-872e94944d4e/resourceGroups/myquiz/providers/Microsoft.Network/loadBalancers/myquizperfsslb/backendAddressPools/bepool" -SubnetId "/subscriptions/8e293158-b459-4a99-be65-872e94944d4e/resourceGroups/myquiz/providers/Microsoft.Network/virtualNetworks/myquizperfssVnet/subnets/default"
Add-AzVmssNetworkInterfaceConfiguration -VirtualMachineScaleSet $Vmss -Name "myquizperfssNic3" -Primary $False -IPConfiguration $IPCfg

$IPCfg = New-AzVmssIPConfig -Name "myquizperfssIpConfig4" -LoadBalancerBackendAddressPoolsId "/subscriptions/8e293158-b459-4a99-be65-872e94944d4e/resourceGroups/myquiz/providers/Microsoft.Network/loadBalancers/myquizperfsslb/backendAddressPools/bepool" -SubnetId "/subscriptions/8e293158-b459-4a99-be65-872e94944d4e/resourceGroups/myquiz/providers/Microsoft.Network/virtualNetworks/myquizperfssVnet/subnets/default"
Add-AzVmssNetworkInterfaceConfiguration -VirtualMachineScaleSet $Vmss -Name "myquizperfssNic4" -Primary $False -IPConfiguration $IPCfg

Remove-AzVmssNetworkInterfaceConfiguration -VirtualMachineScaleSet $Vmss -Name myquiaa4bNic
$LBBAPI='/subscriptions/8e293158-b459-4a99-be65-872e94944d4e/resourceGroups/myquiz/providers/Microsoft.Network/loadBalancers/myquizperfsslb/backendAddressPools/bepool'
$SBI='/subscriptions/8e293158-b459-4a99-be65-872e94944d4e/resourceGroups/myquiz/providers/Microsoft.Network/virtualNetworks/myquizperfssVnet/subnets/default'
$LBINPI0='/subscriptions/8e293158-b459-4a99-be65-872e94944d4e/resourceGroups/myquiz/providers/Microsoft.Network/loadBalancers/myquizperfsslb/inboundNatPools/natpool'
$LBINPI1='/subscriptions/8e293158-b459-4a99-be65-872e94944d4e/resourceGroups/myquiz/providers/Microsoft.Network/loadBalancers/myquizperfsslb/inboundNatPools/WinRM_natpool'
$IPCfg = New-AzVmssIPConfig -Name "myquizperfssIpConfig" -LoadBalancerBackendAddressPoolsId $LBBAPI -LoadBalancerInboundNatPoolsId $LBINPI0, $LBINPI1  -SubnetId $SBI
Add-AzVmssNetworkInterfaceConfiguration -VirtualMachineScaleSet $Vmss -Name "myquizperfssNic" -Primary $True -IPConfiguration $IPCfg
Remove-AzVmss -ResourceGroupName myquiz -VMScaleSetName myquizperfss -Force
Set-AzVmssOSProfile -VirtualMachineScaleSet $Vmss   -AdminPassword "myquizVMazure200$"
New-AzVmss -ResourceGroupName myquiz -VMScaleSetName myquizperfss -VirtualMachineScaleSet $Vmss

$Nic0 = $Vmss.VirtualMachineProfile.NetworkProfile.NetworkInterfaceConfigurations[0]
$Id='/subscriptions/8e293158-b459-4a99-be65-872e94944d4e/resourceGroups/myquiz/providers/Microsoft.Network/loadBalancers/myquizperfsslb/inboundNatPools/WinRM_natpool'
$Nic0.IpConfigurations[0].LoadBalancerInboundNatPools.Add($Id)

$RGName = 'myquiz'
$AdminUsername = "myquiz";
$AdminPassword = "myquizVMazure200$" + $RGName;
# Set-AzVmssOSProfile -VirtualMachineScaleSet $Vmss  -AdminUsername $AdminUsername -AdminPassword $AdminPassword
Set-AzVmssOSProfile -VirtualMachineScaleSet $Vmss   -AdminPassword $AdminPassword

Remove-AzVmssExtension -VirtualMachineScaleSet $Vmss -Name customScript
Add-AzVmssExtension -VirtualMachineScaleSet $Vmss -Name customScript -Publisher Microsoft.Compute -ForceUpdateTag 2d516fcf-fc50-4a18-9e28-046b3431dfaf -Type CustomScriptExtension -TypeHandlerVersion 1.9 -AutoUpgradeMinorVersion $False -Setting '{"fileUris":["https://myquizyad.blob.core.windows.net/scripts/myqg-perf_deploy_ssl.ps1","https://myquizyad.blob.core.windows.net/scripts/ConfigureRemotingForAnsible.ps1"],"commandToExecute":"powershell -ExecutionPolicy Unrestricted -File myqg-perf_deploy_ssl.ps1"}'


$IPCfg = New-AzVmssIPConfig -Name "myquizperfssNic3" `
    -LoadBalancerInboundNatPoolsId $ExpectedLb.InboundNatPools[0].Id `
    -LoadBalancerBackendAddressPoolsId $ExpectedLb.BackendAddressPools[0].Id `
    -SubnetId $SubNetId;
# https://clck.ru/Foxjc    

.\foo.ps1 foo myquiz-move bar fooSubnet
.\foo.ps1 myquizperfss myquiz myqg-vnet myqgperf-subnet
.\foo.ps1 myQuizQAImg myquiz myqg-vnet myqgperf-subnet
.\foo.ps1 perftest myquiz myqg-vnet myqgperf-subnet



$Vmss.VirtualMachineProfile.NetworkProfile.NetworkInterfaceConfigurations[0].IpConfigurations[0].LoadBalancerInboundNatPools[0].Id
$Vmss.VirtualMachineProfile.NetworkProfile.NetworkInterfaceConfigurations[0].IpConfigurations[0].LoadBalancerBackendAddressPools[0].Id
$Vmss.VirtualMachineProfile.NetworkProfile.NetworkInterfaceConfigurations[0].IpConfigurations[0].Subnet.Id

$IPCfg = New-AzVmssIPConfig -Name "fooSecondIpConfig" -LoadBalancerInboundNatPoolsId $Vmss.VirtualMachineProfile.NetworkProfile.NetworkInterfaceConfigurations[0].IpConfigurations[0].LoadBalancerInboundNatPools[0].Id -LoadBalancerBackendAddressPoolsId $Vmss.VirtualMachineProfile.NetworkProfile.NetworkInterfaceConfigurations[0].IpConfigurations[0].LoadBalancerBackendAddressPools[0].Id -SubnetId $Vmss.VirtualMachineProfile.NetworkProfile.NetworkInterfaceConfigurations[0].IpConfigurations[0].Subnet.Id;

# Deallocate VMSS
Add-AzVmssNetworkInterfaceConfiguration -VirtualMachineScaleSet $Vmss -Name "fooSecondIpConfig" -Primary $False -IPConfiguration $IPCfg

$tmp = $Vmss.VirtualMachineProfile.NetworkProfile.NetworkInterfaceConfigurations
foreach ($t in $tmp) { $t.IpConfigurations[0].Subnet.Id="aaaaaaaaaaaaaaaa"}
foreach ($t in $Vmss.VirtualMachineProfile.NetworkProfile.NetworkInterfaceConfigurations) { $t.IpConfigurations[0].Subnet.Id="bbbbbbbbbbbbbbb"}

$Vnet = Get-AzVirtualNetwork -Name foo -ResourceGroupName myquiz-move
$Vnet.Subnets[0].Id

New-AzVmss -ResourceGroupName myquiz-move -VMScaleSetName foo2 -VirtualNetworkName foo -VirtualMachineScaleSet $Vmss 

(Get-AzVirtualNetworkSubnetConfig -Name secondbar -VirtualNetwork $Vnet).Id

-SourceVaultId $Vmss.VirtualMachineProfile.OsProfile.Secrets[0].SourceVault.Id 
-VaultCertificate $Vmss.VirtualMachineProfile.OsProfile.Secrets[0].VaultCertificates[0]

$Vmss.VirtualMachineProfile.ExtensionProfile
$Vmss.VirtualMachineProfile.OsProfile.Secrets


Get-Member -InputObject  $vmss2

########
$obj1 = [pscustomobject]@{
    'Name1' = 'Data1'
    'Name2' = 'Data2'
    'Name3' = 'Data3'
    'Name4' = 'Data4'
}
$obj1.PsObject.Members.Remove('Name2')
============UPDATE CERT===============

# $Cert = Get-AzKeyVaultSecret -VaultName bar -Name myqgru
# $Vault = Get-AzKeyVault -VaultName bar
$Vmss =  Get-AzVmss -ResourceGroupName myquiz-move -VMScaleSetName foo

# Add-AzVmssSecret -VirtualMachineScaleSet $Vmss -SourceVaultId $Vault.ResourceId -VaultCertificate $Cert
# Update-AzVmss -ResourceGroupName myquiz-move -VMScaleSetName foo -VirtualMachineScaleSet $Vmss

# $CertConfig = New-AzVmssVaultCertificateConfig -CertificateUrl "https://bar.vault.azure.net/secrets/myquizru/b670091e8fa4445698d62168d619b5c8"

# $Cert = Get-AzKeyVaultSecret -VaultName bar -Name myqgru
$Cert = Get-AzKeyVaultSecret -VaultName bar -Name myqgorg
# $CertConfig = New-AzVmssVaultCertificateConfig -CertificateUrl "https://bar.vault.azure.net/secrets/myqgru/95d298d73d3a45f5b11ff972e7d40b14"
$CertConfig = New-AzVmssVaultCertificateConfig -CertificateUrl $Cert.Id

# Add-AzVmssSecret -VirtualMachineScaleSet $VMSS -SourceVaultId $Vault.ResourceId -VaultCertificate $CertConfig

$Vmss.VirtualMachineProfile.OsProfile.Secrets[0].VaultCertificates[0]=$CertConfig
Update-AzVmss -ResourceGroupName myquiz-move -VMScaleSetName foo -VirtualMachineScaleSet $Vmss
# -----------------
$Cert = Get-AzKeyVaultSecret -VaultName bar -Name bar
$Vault = Get-AzKeyVault -VaultName bar
$Vmss =  Get-AzVmss -ResourceGroupName myquiz-move -VMScaleSetName foo2
Add-AzVmssSecret -VirtualMachineScaleSet $Vmss -SourceVaultId $Vault.ResourceId -VaultCertificate $Cert

Remove-Variable -Name vmss2
Get-Variable | Out-String

New-AzVmss -ResourceGroupName -ImageName UbuntuLTS 

PSVirtualMachineScaleSet

$Vmss =  Get-AzVmss -ResourceGroupName myquiz-move -VMScaleSetName foo
$Vmss  | ConvertTo-Json -Depth 3 > vmss3.json  # lose data

$Vmss | Export-Clixml Vmss
$vmss = Import-Clixml Vmss

Remove-Variable * -ErrorAction SilentlyContinue  # Remove variables https://clck.ru/FqkV8
Get-Variable -Exclude PWD,*Preference | Remove-Variable -EA 0 
