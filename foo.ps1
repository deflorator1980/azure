Set-PSDebug -Trace 1

$VmssName=$Args[0]
$Rg=$Args[1]
$VnetName=$Args[2]
$SubnetName=$Args[3]
Write-Output $VmssName
Write-Output $Rg
Write-Output $VnetName
Write-Output $SubnetName

$Vmss =  Get-AzVmss -ResourceGroupName $Rg -VMScaleSetName $VmssName
$Ext0 = (Get-AzVmss -ResourceGroupName $Rg -VMScaleSetName $VmssName).VirtualMachineProfile.ExtensionProfile.Extensions[0]
$SourceVaultId =  $Vmss.VirtualMachineProfile.OsProfile.Secrets[0].SourceVault.Id 
$VaultCertificate = $Vmss.VirtualMachineProfile.OsProfile.Secrets[0].VaultCertificates[0]
$Vnet = Get-AzVirtualNetwork -Name $VnetName -ResourceGroupName $Rg
$Vmss > vmss.txt
Remove-AzVmss -ResourceGroupName $Rg -VMScaleSetName $VmssName -Force
$Vmss.VirtualMachineProfile.OsProfile.Secrets=$null 
Remove-AzVmssExtension -VirtualMachineScaleSet $Vmss -Name $Ext0.Name
$AdminPassword = "myquizVMazure200$" + $RGName;
Set-AzVmssOSProfile -VirtualMachineScaleSet $Vmss   -AdminPassword $AdminPassword
# $Vmss.VirtualMachineProfile.NetworkProfile.NetworkInterfaceConfigurations[0].IpConfigurations[0].Subnet.Id = $Vnet.Subnets[0].Id
# $Vmss.VirtualMachineProfile.NetworkProfile.NetworkInterfaceConfigurations[0].IpConfigurations[0].Subnet.Id = (Get-AzVirtualNetworkSubnetConfig -Name $SubnetName -VirtualNetwork $Vnet).Id
foreach ($t in $Vmss.VirtualMachineProfile.NetworkProfile.NetworkInterfaceConfigurations) { $t.IpConfigurations[0].Subnet.Id = (Get-AzVirtualNetworkSubnetConfig -Name $SubnetName -VirtualNetwork $Vnet).Id}

New-AzVmss -ResourceGroupName $Rg -VMScaleSetName $VmssName -VirtualMachineScaleSet $Vmss
Add-AzVmssSecret -VirtualMachineScaleSet $Vmss -SourceVaultId $SourceVaultId -VaultCertificate $VaultCertificate
Update-AzVmss -ResourceGroupName $Rg -VMScaleSetName $VmssName -VirtualMachineScaleSet $Vmss
# Add-AzVmssExtension -VirtualMachineScaleSet $Vmss -Name customScript -Publisher Microsoft.Compute -ForceUpdateTag 2d516fcf-fc50-4a18-9e28-046b3431dfaf -Type CustomScriptExtension -TypeHandlerVersion 1.9 -AutoUpgradeMinorVersion $False -Setting '{"fileUris":["https://myquizyad.blob.core.windows.net/scripts/myqg-perf_deploy_ssl.ps1","https://myquizyad.blob.core.windows.net/scripts/ConfigureRemotingForAnsible.ps1"],"commandToExecute":"powershell -ExecutionPolicy Unrestricted -File myqg-perf_deploy_ssl.ps1"}'
Add-AzVmssExtension -VirtualMachineScaleSet $Vmss -Name $Ext0.Name -Publisher $Ext0.Publisher -ForceUpdateTag $Ext0.ForceUpdateTag -Type $Ext0.Type -TypeHandlerVersion $Ext0.TypeHandlerVersion -AutoUpgradeMinorVersion $Ext0.AutoUpgradeMinorVersion -Setting $Ext0.Settings
Update-AzVmss -ResourceGroupName $Rg -VMScaleSetName $VmssName -VirtualMachineScaleSet $Vmss