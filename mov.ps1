Set-PSDebug -Trace 1

$VmssName=$Args[0]
$Rg=$Args[1]
$VnetName=$Args[2]
$SubnetName=$Args[3]
Write-Output $VmssName
Write-Output $Rg
Write-Output $VnetName
Write-Output $SubnetName

$Vmss =  Get-AzVmss -ResourceGroupName $Rg -VMScaleSetName $VmssName
$Ext = (Get-AzVmss -ResourceGroupName $Rg -VMScaleSetName $VmssName).VirtualMachineProfile.ExtensionProfile.Extensions
$SourceVaultId =  $Vmss.VirtualMachineProfile.OsProfile.Secrets[0].SourceVault.Id 
$VaultCertificate = $Vmss.VirtualMachineProfile.OsProfile.Secrets[0].VaultCertificates
$Vnet = Get-AzVirtualNetwork -Name $VnetName -ResourceGroupName $Rg
$Vmss > vmss.txt
Remove-AzVmss -ResourceGroupName $Rg -VMScaleSetName $VmssName -Force
$Vmss.VirtualMachineProfile.OsProfile.Secrets=$null 
foreach ($e in $Ext) {
  Remove-AzVmssExtension -VirtualMachineScaleSet $Vmss -Name $e.Name 
}
$AdminPassword = "myquizVMazure200$"
Set-AzVmssOSProfile -VirtualMachineScaleSet $Vmss   -AdminPassword $AdminPassword
foreach ($t in $Vmss.VirtualMachineProfile.NetworkProfile.NetworkInterfaceConfigurations) { 
  $t.IpConfigurations[0].Subnet.Id = (Get-AzVirtualNetworkSubnetConfig -Name $SubnetName -VirtualNetwork $Vnet).Id
}
New-AzVmss -ResourceGroupName $Rg -VMScaleSetName $VmssName -VirtualMachineScaleSet $Vmss
if ($VaultCertificate) {
  Add-AzVmssSecret -VirtualMachineScaleSet $Vmss -SourceVaultId $SourceVaultId -VaultCertificate $VaultCertificate[0]
}
if ($Ext.Count) {
  foreach ($e in $Ext) { 
    Add-AzVmssExtension -VirtualMachineScaleSet $Vmss -Name $e.Name -Publisher $e.Publisher -ForceUpdateTag $e.ForceUpdateTag -Type $e.Type -TypeHandlerVersion $e.TypeHandlerVersion -AutoUpgradeMinorVersion $e.AutoUpgradeMinorVersion -Setting $e.Settings
  }
  Update-AzVmss -ResourceGroupName $Rg -VMScaleSetName $VmssName -VirtualMachineScaleSet $Vmss
}
