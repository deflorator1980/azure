# echo "Enter the Resource Group name:" &&
# read resourceGroupName &&
export resourceGroupName='myquiz-move' 
# echo "Enter the location (i.e. centralus):" &&
# read location &&
export location='eastus'
# echo "Enter the project name (used for generating resource names):" &&
# read projectName &&
export projectName='tmpfoo'
# echo "Enter the administrator username:" &&
# read username &&
export username='h'
# echo "Enter the SSH public key:" &&
# read key &&
export key='ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQCwrnMAo5Pr0bvPxmnbsAi0Er+eERmq7kFuhQ5DmQ0NKBNLPchgRpqztomJ0jTrb/Lbf4Ii6DAiJYgDECE89qq0huLAnAjcSrVkHQhz3W6KPnvWlyIUQp/5Na+k32MvuSBhBbN74Fg8+vryMX80IM9tROh77Nr3UWEdSCyUUUgLwS3wkel3WJNZk/XxVNVZTrDo0jIF5ISeEn4YhRb4MODLZy50ZN63feSbwtRew8uJ9a+S1eeZVjx9sgsSU2yeetP20DItmjQCu+kACvPoBqj/X+WbRMPAouPKY3V6KxQRAm+JK/duRyKL2U/mReggP78b1OqRExJPdzj6vRxlNIs1 a@a'

echo $projectName
# az group create --name $resourceGroupName --location "$location" &&
# az group deployment create --resource-group $resourceGroupName --template-uri https://raw.githubusercontent.com/azure/azure-quickstart-templates/master/101-vm-sshkey/azuredeploy.json --parameters projectName=$projectName adminUsername=$username adminPublicKey="$key" &&
az group deployment create --resource-group $resourceGroupName --template-file azuredeploy.json
# az vm show --resource-group $resourceGroupName --name "$projectName-vm" --show-details --query publicIps --output tsv