# az vmss create -g myquiz-move -n foo --image UbuntuLTS --admin-username h --generate-ssh-keys --lb foo --vnet-name foo
$SBI='/subscriptions/8e293158-b459-4a99-be65-872e94944d4e/resourceGroups/myquiz-move/providers/Microsoft.Network/virtualNetworks/bar/subnets/fooSubnet'
$lb = Get-AzLoadBalancer -ResourceGroupName myquiz-move -Name foo
$vmssConfig = New-AzVmssConfig -Location "eastus" -SkuName "Standard_DS2" -UpgradePolicyMode "Manual"
$IPCfg = New-AzVmssIPConfig -Name "fooIpConfig" -LoadBalancerBackendAddressPoolsId $lb.BackendAddressPools[0].Id -LoadBalancerInboundNatPoolsId $lb.InboundNatPools[0].Id  -SubnetId $SBI
Add-AzVmssNetworkInterfaceConfiguration -VirtualMachineScaleSet $vmssConfig -Name "fooNic" -Primary $True -IPConfiguration $IPCfg
New-AzVmss -ResourceGroupName myquiz-move -VMScaleSetName foo -VirtualMachineScaleSet $vmssConfig
