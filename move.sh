#!/bin/bash
# set -x

VMSS=$1
RG=$2
VNET=$3

secrets=$(az vmss show -g $RG -n $VMSS -o yaml --query virtualMachineProfile.osProfile.secrets[0].vaultCertificates[0].certificateUrl -o tsv)
vm_secrets=$(az vm secret format -s "$secrets")
az vmss delete -g $RG -n $VMSS
az vmss create -g $RG -n $VMSS --image UbuntuLTS --admin-username h --generate-ssh-keys --lb foo --vnet-name $VNET 
az vmss create -g $RG -n $VMSS --image UbuntuLTS --admin-username h --generate-ssh-keys --lb foo --vnet-name $VNET --secrets "$vm_secrets" 