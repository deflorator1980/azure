#Variables
$website = "myqg"
$path = "C:\MyQuizzes\" + $website
$zipBlob = "https://myquizyad.blob.core.windows.net/apps/myqg-perf_webui.zip"
$zipTmp = "c:\tmp\" + $website + ".zip"

$certName = "myqg.ru"
$certStore = "Cert:\LocalMachine\WebHosting\"

# --- Turn on Powershell remote access on HTTPS
Write-Output "Enable WinRM on HTTPS"
.\ConfigureRemotingForAnsible.ps1 -ForceNewSSLCert -DisableBasicAuth
Write-Output "Remove HTTP listener"
Get-ChildItem -Path WSMan:\localhost\Listener | Where-Object { $_.Keys -contains "Transport=HTTP" } | Remove-Item -Recurse -Force
# ---

# --- Add HTTP to HTTPS redirect rule
$redirectRuleName = 'Redirect to HTTPS'
$siteWithRedirectRule = 'Default Web Site'
$iisPath = "iis:\sites\$siteWithRedirectRule"
$rootIISConfigSection = '/system.webServer/rewrite/rules'
$IISConfigSection = "$rootIISConfigSection/rule[@name='$redirectRuleName']"

if (Get-WebConfiguration -pspath $iisPath  -filter $IISConfigSection) {
    Write-Output "rule '$redirectRuleName' found, skip create"
} else {
    Add-WebConfigurationProperty -pspath $iisPath `
        -filter $rootIISConfigSection `
        -name "." `
        -value @{name="$redirectRuleName" ;patternSyntax='Regular Expressions';stopProcessing='True'}
    Set-WebConfigurationProperty -pspath $iisPath `
        -filter "$IISConfigSection/action" `
        -name . `
        -value @{ type="Redirect"; url='https://{SERVER_NAME}:443/{R:1}'; redirectType="permanent" } 
    Set-WebConfigurationProperty -pspath $iisPath `
        -filter "$IISConfigSection/conditions" `
        -name . `
        -value @{ logicalGrouping="MatchAll" }
    Set-WebConfigurationProperty -pspath $iisPath `
        -filter "$IISConfigSection/match" `
        -name . `
        -value @{ url="(.*)" } 
    Add-WebConfigurationProperty -pspath $iisPath `
        -filter "$IISConfigSection/conditions" `
        -name "." `
        -value @{ input='{HTTPS}'; matchType='Pattern'; pattern='^OFF$'}
    Write-Output "rule '$redirectRuleName' created"
}
# ---

$cert = (Get-ChildItem $certStore | where-object {$_.Subject -like "*$certName*"} | Select-Object -First 1)
$certThumbprint = $cert.Thumbprint
$certPath = "{0}{1}" -f $certStore, $certThumbprint

Stop-WebSite -Name $website

#Remove all files from path
#Get-ChildItem $path -Recurse | Remove-Item -Force

# Download and extract app
Invoke-WebRequest $zipBlob -OutFile $zipTmp
Expand-Archive -LiteralPath $zipTmp -DestinationPath $path -Force

##SSL binding
$bindingsHTTPS = Get-WebBinding -Name $website  -Protocol "https"
if ($bindingsHTTPS.count -eq 0) { New-WebBinding -Name $website -IP "*" -Port 443 -Protocol https }

cd "IIS:\SslBindings"
Get-Item $certPath | New-Item 0.0.0.0!443 -Force

Start-WebSite -Name $website

#Cleaning
Remove-Item -Path $zipTmp