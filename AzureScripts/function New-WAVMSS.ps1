﻿function New-WAVMSS {
  [CmdletBinding()]

  Param(
    [Parameter(Mandatory=$true)][string] $ResourceGroupName,
    [Parameter(Mandatory=$true)][string] $Location,
    [Parameter(Mandatory=$true)][string] $Name,
    [Parameter(Mandatory=$true)][string] $AdminUsername,
    [Parameter(Mandatory=$true)][string] $AdminPassword,
    [Parameter(Mandatory=$true)][string] $SkuName,
    [Parameter(Mandatory=$true)][string] $SkuCapacity,
    [Parameter(Mandatory=$true)][string] $UpgradePolicyMode,
    [Parameter(Mandatory=$true)][string] $SubnetId,
    [Parameter(Mandatory=$true)][string] $NetworkSecurityGroupId,
    [Parameter(Mandatory=$true)][string] $ImageReferenceId,
    [Parameter(Mandatory=$true)][Microsoft.Azure.Commands.Network.Models.PSLoadBalancer] $LoadBalancer,
    [Parameter(Mandatory=$true)][string] $ScriptExtFileName,
    [Parameter(Mandatory=$true)][string] $ScriptExtPath,
    [Parameter(Mandatory=$false)][string] $SourceVaultId,
    [Parameter(Mandatory=$false)][Object] $VaultCertificate
    )


  #Step 1: Create a IP configuration for the NIC
  $vmssIpCfg = New-AzureRmVmssIPConfig `
    -Name ("ipconfig" + $Name) `
    -LoadBalancerInboundNatPoolsId $LoadBalancer.InboundNatPools[0].Id `
    -LoadBalancerBackendAddressPoolsId $LoadBalancer.BackendAddressPools[0].Id `
    -SubnetId $SubnetId

  #Step 3: Create a custom script extension config
  $f = $ScriptExtPath + $ScriptExtFileName

  $customScriptExtensionConfig = @{
    "fileUris" = (,  $f);
    "commandToExecute" = "powershell -ExecutionPolicy Unrestricted -File " + $ScriptExtFileName
  }


  #Step 4: Create a configurable local VMSS object
  $vmss = New-AzureRmVmssConfig `
    -Location $Location `
    -SkuCapacity $SkuCapacity `
    -SkuName $SkuName `
    -SkuTier Standard `
    -UpgradePolicyMode $UpgradePolicyMode `
    -Priority "Regular" `
   |Add-AzureRmVmssNetworkInterfaceConfiguration `
          -Name ($Name + "-nic") `
          -Primary $True `
          -IPConfiguration $vmssIpCfg `
          -NetworkSecurityGroupId $NetworkSecurityGroupId `
          -EnableAcceleratedNetworking `
      | Set-AzureRmVmssOSProfile `
          -ComputerNamePrefix "mqgprodru" `
          -AdminUsername $AdminUsername `
          -AdminPassword $AdminPassword `
      | Set-AzureRmVmssStorageProfile `
          -ImageReferenceId $ImageReferenceId `
          -OsDiskCaching ReadWrite `
          -OsDiskCreateOption "FromImage" `
      | Add-AzureRmVmssExtension `
          -Name "customScript" `
          -Publisher "Microsoft.Compute" `
          -Type "CustomScriptExtension" `
          -TypeHandlerVersion 1.9 `
          -Setting $customScriptExtensionConfig `
          -AutoUpgradeMinorVersion $True

  if (![string]::IsNullOrEmpty($SourceVaultId)) {
      Add-AzureRmVmssSecret `
      -VirtualMachineScaleSet $vmss `
      -SourceVaultId $SourceVaultId `
      -VaultCertificate $VaultCertificate | Out-Null
  }



  #Step 5: Create the VMSS
  New-AzureRmVmss -ResourceGroupName $ResourceGroupName -VMScaleSetName $Name -VirtualMachineScaleSet $vmss | Out-Null

  Get-AzureRmVmss -ResourceGroupName $ResourceGroupName -VMScaleSetName $Name

}