﻿### Creating Public Load Balancer + Virtual Maichine Scale Set in subnet

# Parameters
$resourceGroupName = "myquiz"
$location = "East US"
$vnetName = "myqg-vnet"

# New VMSS Parameters
$vmssName = "myqgperf2"
$adminUsername = "myquiz"
$adminPassword = "myquizVMazure200$"
#$vmssIpCfgName = "ipConfig1"
$vmssNSG = "/subscriptions/8e293158-b459-4a99-be65-872e94944d4e/resourceGroups/myquiz/providers/Microsoft.Network/networkSecurityGroups/myqg-vnet-nsg"
$vmssSkuCapacity = 0
$vmssSKUName = "Standard_F4s"
$vmssUpgradePolicyMode = "Manual"
$vmssImageResourceId = "/subscriptions/8e293158-b459-4a99-be65-872e94944d4e/resourceGroups/myquiz/providers/Microsoft.Compute/images/myqgssvm-image-20180709120300"

# New subnet Parameters
$subnetName = $vmssName + "-subnet"
$subnetAddressPrefix = "10.3.7.0/24"

# New ILB Parameters
$lbIp = "10.3.7.4"
$lbFrontEndPort = 80
$lbBackEndPort = 8088
$ScriptExtFileName = "myqg-perf_deploy.ps1"
$ScriptExtPath = "https://myquizyad.blob.core.windows.net/scripts/"


### Creating subnet of existing virtual network

$subnet = New-WASubnet `
  -ResourceGroupName $resourceGroupName `
  -VnetName $vnetName `
  -SubnetName $subnetName `
  -SubnetAddressPrefix $subnetAddressPrefix


$lb = New-WAInternalLoadBalancer `
    -ResourceGroupName $resourceGroupName `
    -Location $location `
    -Name $vmssName `
    -SubnetId $subnet.Id `
    -PrivateIpAddress $lbIp `
    -FrontendPort $lbFrontEndPort `
    -BackendPort $lbBackEndPort `
    -SKU Basic


New-WAVMSS `
-ResourceGroupName $resourceGroupName `
-Location $location `
-Name $vmssName `
-AdminUsername $adminUsername `
-AdminPassword $adminPassword `
-SkuName $vmssSKUName `
-SkuCapacity $vmssSkuCapacity `
-UpgradePolicyMode $vmssUpgradePolicyMode `
-SubnetId $subnet.Id `
-NetworkSecurityGroupId $vmssNSG `
-ImageReferenceId $vmssImageResourceId `
-LoadBalancer $lb `
-ScriptExtFileName $ScriptExtFileName `
-ScriptExtPath $ScriptExtPath
