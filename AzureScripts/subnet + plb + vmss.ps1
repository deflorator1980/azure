﻿### Creating Public Load Balancer + Virtual Maichine Scale Set in subnet

# Parameters
$resourceGroupName = "myquiz"
$location = "East US"
$vnetName = "myqg-vnet"

# New VMSS Parameters
$vmssName = "myQuizProdRu"
$adminUsername = "myquiz"
$adminPassword = "myquizVMazure200$"
#$vmssIpCfgName = "ipConfig1"
$vmssNSG = "/subscriptions/8e293158-b459-4a99-be65-872e94944d4e/resourceGroups/myquiz/providers/Microsoft.Network/networkSecurityGroups/myqg-vnet-nsg"
$vmssSkuCapacity = 0
$vmssSKUName = "Standard_B2ms"
$vmssUpgradePolicyMode = "Manual"
$vmssImageResourceId = "/subscriptions/8e293158-b459-4a99-be65-872e94944d4e/resourceGroups/myquiz/providers/Microsoft.Compute/images/myqgssvm-image-20180709120300"
#$vmssExtensionName = "customScript"

$keyVaultName = "myquiz"
$certUrl = "https://myquiz.vault.azure.net/secrets/myqg-ru/cac5962bbe7244bba8ecfc8964bb575b"
$certStore = "WebHosting"


# New subnet Parameters
$subnetName = $vmssName + "-subnet"
$subnetAddressPrefix = "10.3.10.0/24"

# New PLB Parameters
#$lbName = $vmssName + "-plb"
$ipDomainLabel = "myquizprodru"
$lbFrontEndPort = 443
$lbBackEndPort = 443

$ScriptExtFileName = "myqg-prodru_deploy_ssl.ps1"
$ScriptExtPath = "https://myquizyad.blob.core.windows.net/scripts/"



## Creating subnet of existing virtual network

$subnet = New-WASubnet `
  -ResourceGroupName $resourceGroupName `
  -VnetName $vnetName `
  -SubnetName $subnetName `
  -SubnetAddressPrefix $subnetAddressPrefix

# $vnet = Get-AzureRmVirtualNetwork -Name $VnetName -ResourceGroupName $ResourceGroupName
# $subnet =  Get-AzureRmVirtualNetworkSubnetConfig -VirtualNetwork $vnet -Name $SubnetName

$lb = New-WAPublicLoadBalancer `
    -ResourceGroupName $resourceGroupName `
    -Location $location `
    -Name $vmssName `
    -FrontendPort $lbFrontEndPort `
    -BackendPort $lbBackEndPort `
    -IpDomainLabel $ipDomainLabel

# $lb = Get-AzureRmLoadBalancer -Name $vmssName -ResourceGroupName $ResourceGroupName


$keyVault = Get-AzureRmKeyVault -VaultName $keyVaultName

$certConfig = New-AzureRmVmssVaultCertificateConfig -CertificateUrl $certUrl -CertificateStore $certStore


New-WAVMSS `
-ResourceGroupName $resourceGroupName `
-Location $location `
-Name $vmssName `
-AdminUsername $adminUsername `
-AdminPassword $adminPassword `
-SkuName $vmssSKUName `
-SkuCapacity $vmssSkuCapacity `
-UpgradePolicyMode $vmssUpgradePolicyMode `
-SubnetId $subnet.Id `
-NetworkSecurityGroupId $vmssNSG `
-ImageReferenceId $vmssImageResourceId `
-LoadBalancer $lb `
-ScriptExtFileName $ScriptExtFileName `
-ScriptExtPath $ScriptExtPath `
-SourceVaultId $keyVault.ResourceId `
-VaultCertificate $certConfig