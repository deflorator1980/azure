﻿function New-WASubnet {
  [CmdletBinding()] 
  
  Param( 
    [Parameter(Mandatory=$true)][string] $ResourceGroupName, 
    [Parameter(Mandatory=$true)][string] $VnetName, 
    [Parameter(Mandatory=$true)][string] $SubnetName, 
    [Parameter(Mandatory=$true)][string] $SubnetAddressPrefix)    
   
   
      $vnet = Get-AzureRmVirtualNetwork -Name $VnetName -ResourceGroupName $ResourceGroupName   
    
      Add-AzureRmVirtualNetworkSubnetConfig -Name $SubnetName -VirtualNetwork $vnet  -AddressPrefix $SubnetAddressPrefix | Out-Null
            
      Set-AzureRmVirtualNetwork -VirtualNetwork $vnet | Out-Null          
   
      
   $vnet = Get-AzureRmVirtualNetwork -Name $VnetName -ResourceGroupName $ResourceGroupName
   
   return Get-AzureRmVirtualNetworkSubnetConfig -VirtualNetwork $vnet -Name $SubnetName
}