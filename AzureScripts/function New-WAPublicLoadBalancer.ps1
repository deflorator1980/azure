﻿function New-WAPublicLoadBalancer {
  [CmdletBinding()] 
  
  Param( 
    [Parameter(Mandatory=$true)][string] $ResourceGroupName, 
    [Parameter(Mandatory=$true)][string] $Location, 
    [Parameter(Mandatory=$true)][string] $Name, 
    [Parameter(Mandatory=$true)][int] $FrontendPort, 
    [Parameter(Mandatory=$true)][int] $BackendPort, 
    [Parameter(Mandatory=$true)][string] $IpDomainLabel)    

    #Step 1: Creating public IP address
    $pubIP = New-AzureRmPublicIpAddress `
      -Name ($Name + "-ip") `
      -ResourceGroupName $ResourceGroupName `
      -Location $Location `
      -AllocationMethod Static `
      -DomainNameLabel $ipDomainLabel `
      -Sku Standard `
      -Force
  
    $pubIP = Get-AzureRmPublicIpAddress -Name ($Name + "-ip") -ResourceGroupName $ResourceGroupName

    #Step 2: Create a front-end IP pool
    $frontendIP = New-AzureRmLoadBalancerFrontendIpConfig `
      -Name ("fe" + $Name) `
      -PublicIpAddress $pubIP

    #Step 3: Create a back-end address pool
    $beAddressPool = New-AzureRmLoadBalancerBackendAddressPoolConfig -Name ("be" + $Name)

    #Step 4: Create a health probe
    $healthProbe = New-AzureRmLoadBalancerProbeConfig `
      -Name ("hp" + $Name)  `
      -Protocol http `
      -RequestPath "/Home/Index" `
      -Port 8088 `
      -IntervalInSeconds 5 `
      -ProbeCount 2

    #Step 5: Create a inbound NAT pool
    $inboundNATPool = New-AzureRmLoadBalancerInboundNatPoolConfig `
      -Name $("inat-" + $Name)  `
      -FrontendIPConfigurationId $frontendIP.Id `
      -Protocol Tcp `
      -FrontendPortRangeStart 50000 `
      -FrontendPortRangeEnd 50119 `
      -BackendPort 3389

    #Step 6: Create a load balancing rule
      $lbrule = New-AzureRmLoadBalancerRuleConfig `
      -Name ("rule" + $Name) `
      -FrontendIpConfiguration $frontendIP `
      -BackendAddressPool $beAddressPool `
      -Probe $healthProbe `
      -Protocol Tcp `
      -FrontendPort $FrontendPort `
      -BackendPort $BackendPort

    #Step 7: Create a load balancer
    New-AzureRmLoadBalancer `
      -ResourceGroupName $ResourceGroupName `
      -Name $Name `
      -Location $Location `
      -FrontendIpConfiguration $frontendIP `
      -LoadBalancingRule $lbrule `
      -BackendAddressPool $beAddressPool `
      -InboundNatPool  $inboundNATPool `
      -Probe $healthProbe `
      -Sku Standard `
      | Out-Null
  
    Get-AzureRmLoadBalancer -Name $Name -ResourceGroupName $ResourceGroupName  
}