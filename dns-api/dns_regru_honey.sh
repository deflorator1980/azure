#!/bin/bash

#Here is a sample custom api script.
#This file name is "dns_myapi.sh"
#So, here must be a method   dns_myapi_add()
#Which will be called by acme.sh to add the txt record to your api system.
#returns 0 means success, otherwise error.
#
#Author: Neilpang
#Report Bugs here: https://github.com/Neilpang/acme.sh
#
REGRU_API_URL="https://api.reg.ru/api/regru2"

REGRU_API_Username="av_deflorator1980"
REGRU_API_Password="pelotka9"

########  Public functions #####################

#Usage: dns_myapi_add   _acme-challenge.www.domain.com   "XKrxpRBosdIKFzxW_CT3KLZNf6q0HG9i01zxXp5CPBs"
dns_regru_add() {
  fulldomain=$1
  txtvalue=$2
  echo 'ONE '$1
  echo 'TWO '$2
#  response="$(_get "https://api.reg.ru/api/regru2/zone/add_txt?input_data={%22username%22:%22av_deflorator1980%22,%22password%22:%22pelotka9%22,%22domains%22:[{%22dname%22:%22tverhoney.ru%22}],%22subdomain%22:%22_acme-challenge%22,%22text%22:%22${txtvalue}%22,%22output_content_type%22:%22plain%22}&input_format=json")"

  # response="$(_get "$REGRU_API_URL/zone/add_txt?input_data={%22username%22:%22av_deflorator1980%22,%22password%22:%22pelotka9%22,%22domains%22:[{%22dname%22:%22tverhoney.ru%22}],%22subdomain%22:%22_acme-challenge%22,%22text%22:%22${txtvalue}%22,%22output_content_type%22:%22plain%22}&input_format=json")"

#  response="$(_get "$REGRU_API_URL/zone/add_txt?input_data={%22username%22:%22$REGRU_API_Username%22,%22password%22:%22$REGRU_API_Password%22,%22domains%22:[{%22dname%22:%22tverhoney.ru%22}],%22subdomain%22:%22_acme-challenge%22,%22text%22:%22${txtvalue}%22,%22output_content_type%22:%22plain%22}&input_format=json")"

  response="$(_get "$REGRU_API_URL/zone/add_txt?input_data={%22username%22:%22${REGRU_API_Username}%22,%22password%22:%22${REGRU_API_Password}%22,%22domains%22:[{%22dname%22:%22tverhoney.ru%22}],%22subdomain%22:%22_acme-challenge%22,%22text%22:%22${txtvalue}%22,%22output_content_type%22:%22plain%22}&input_format=json")"


  #_info "Using myapi"
  #_debug fulldomain "$fulldomain"
  #_debug txtvalue "$txtvalue"
  #_err "Not implemented!"
  #return 1
  return 0
}

#Usage: fulldomain txtvalue
#Remove the txt record after validation.
dns_regru_rm() {
  fulldomain=$1
  txtvalue=$2
  echo 'RM-ONE '$1
  echo 'RM-TWO '$2
  _info "Using myapi"
  _debug fulldomain "$fulldomain"
  _debug txtvalue "$txtvalue"
}
