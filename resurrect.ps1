Set-PSDebug -Trace 1

Update-AzVmss -ResourceGroupName myquiz -VMScaleSetName myquizperfss -AutomaticOSUpgrade $false
$Vmss =  Get-AzVmss -ResourceGroupName myquiz -VMScaleSetName myquizperfss

# Extension
Add-AzVmssExtension -VirtualMachineScaleSet $Vmss -Name customScript -Publisher Microsoft.Compute -ForceUpdateTag 2d516fcf-fc50-4a18-9e28-046b3431dfaf -Type CustomScriptExtension -TypeHandlerVersion 1.9 -AutoUpgradeMinorVersion $False -Setting '{"fileUris":["https://myquizyad.blob.core.windows.net/scripts/myqg-perf_deploy_ssl.ps1","https://myquizyad.blob.core.windows.net/scripts/ConfigureRemotingForAnsible.ps1"],"commandToExecute":"powershell -ExecutionPolicy Unrestricted -File myqg-perf_deploy_ssl.ps1"}'

# Secrets
$CertConfig = New-AzVmssVaultCertificateConfig -CertificateUrl "https://myquiz.vault.azure.net/secrets/myqg-ru/93074e68c2c84737a1da1636aa05139c" -CertificateStore "WebHosting"
Add-AzVmssSecret -VirtualMachineScaleSet $Vmss -SourceVaultId "/subscriptions/8e293158-b459-4a99-be65-872e94944d4e/resourceGroups/myquiz/providers/Microsoft.KeyVault/vaults/myquiz" -VaultCertificate $CertConfig
Update-AzVmss -ResourceGroupName myquiz -VMScaleSetName myquizperfss -VirtualMachineScaleSet $Vmss

# Storage Profile
$Vmss.VirtualMachineProfile.StorageProfile.ImageReference=$null
Set-AzVmssStorageProfile -VirtualMachineScaleSet $Vmss -ImageReferenceId '/subscriptions/8e293158-b459-4a99-be65-872e94944d4e/resourceGroups/myquiz/providers/Microsoft.Compute/images/myqgssvm-image-20180709120300'
Remove-AzVmss -ResourceGroupName myquiz -VMScaleSetName myquizperfss -Force
Set-AzVmssOSProfile -VirtualMachineScaleSet $Vmss   -AdminPassword "myquizVMazure200$"
New-AzVmss -ResourceGroupName myquiz -VMScaleSetName myquizperfss -VirtualMachineScaleSet $Vmss 


# Network
Remove-AzVmssNetworkInterfaceConfiguration -VirtualMachineScaleSet $Vmss -Name myquiaa4bNic
$LBBAPI='/subscriptions/8e293158-b459-4a99-be65-872e94944d4e/resourceGroups/myquiz/providers/Microsoft.Network/loadBalancers/myquizperfsslb/backendAddressPools/bepool'
$SBI='/subscriptions/8e293158-b459-4a99-be65-872e94944d4e/resourceGroups/myquiz/providers/Microsoft.Network/virtualNetworks/myquizperfssVnet/subnets/default'
$LBINPI0='/subscriptions/8e293158-b459-4a99-be65-872e94944d4e/resourceGroups/myquiz/providers/Microsoft.Network/loadBalancers/myquizperfsslb/inboundNatPools/natpool'
$LBINPI1='/subscriptions/8e293158-b459-4a99-be65-872e94944d4e/resourceGroups/myquiz/providers/Microsoft.Network/loadBalancers/myquizperfsslb/inboundNatPools/WinRM_natpool'
$IPCfg = New-AzVmssIPConfig -Name "myquizperfssIpConfig" -LoadBalancerBackendAddressPoolsId $LBBAPI -LoadBalancerInboundNatPoolsId $LBINPI0, $LBINPI1  -SubnetId $SBI
Add-AzVmssNetworkInterfaceConfiguration -VirtualMachineScaleSet $Vmss -Name "myquizperfssNic" -Primary $True -IPConfiguration $IPCfg

$IPCfg = New-AzVmssIPConfig -Name "myquizperfssIpConfig3" -LoadBalancerBackendAddressPoolsId "/subscriptions/8e293158-b459-4a99-be65-872e94944d4e/resourceGroups/myquiz/providers/Microsoft.Network/loadBalancers/myquizperfsslb/backendAddressPools/bepool" -SubnetId "/subscriptions/8e293158-b459-4a99-be65-872e94944d4e/resourceGroups/myquiz/providers/Microsoft.Network/virtualNetworks/myquizperfssVnet/subnets/default"
Add-AzVmssNetworkInterfaceConfiguration -VirtualMachineScaleSet $Vmss -Name "myquizperfssNic3" -Primary $False -IPConfiguration $IPCfg

$IPCfg = New-AzVmssIPConfig -Name "myquizperfssIpConfig4" -LoadBalancerBackendAddressPoolsId "/subscriptions/8e293158-b459-4a99-be65-872e94944d4e/resourceGroups/myquiz/providers/Microsoft.Network/loadBalancers/myquizperfsslb/backendAddressPools/bepool" -SubnetId "/subscriptions/8e293158-b459-4a99-be65-872e94944d4e/resourceGroups/myquiz/providers/Microsoft.Network/virtualNetworks/myquizperfssVnet/subnets/default"
Add-AzVmssNetworkInterfaceConfiguration -VirtualMachineScaleSet $Vmss -Name "myquizperfssNic4" -Primary $False -IPConfiguration $IPCfg

Remove-AzVmss -ResourceGroupName myquiz -VMScaleSetName myquizperfss -Force
Set-AzVmssOSProfile -VirtualMachineScaleSet $Vmss   -AdminPassword "myquizVMazure200$"
New-AzVmss -ResourceGroupName myquiz -VMScaleSetName myquizperfss -VirtualMachineScaleSet $Vmss

Write-Output 'The end'