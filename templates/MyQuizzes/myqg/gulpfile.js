/// <binding ProjectOpened='watch' />
/// <reference path="node_modules/jquery/dist/jquery.js" />
/// <binding ProjectOpened='watch' />
/*jshint multistr: true */
process.env.DISABLE_NOTIFIER = true;

var gulp = require('gulp');
var plugins = {
    sass: require('gulp-sass'),
    notify: require('gulp-notify'),
    rename: require('gulp-rename'),
    filter: require('gulp-filter'),
    cleanCss: require('gulp-clean-css'),
    uglify: require('gulp-uglify'),
    concat: require('gulp-concat'),
    jshint: require('gulp-jshint'),
    resolveDependencies: require('gulp-resolve-dependencies'),
    autoprefixer: require('gulp-autoprefixer'),
    plumber: require('gulp-plumber'),
    empty: require('gulp-empty'),
    debug: require('gulp-debug')
}

var config = {
    appPath: './app',
    distPath: './app/dist',
    coreAppPath: './app/core',
    panelAppPath: './app/panel',
    landings: {
        home: {
            appPath: {
                org: './app/landings/home/org',
                ru: './app/landings/home/ru',
                shared: './app/landings/home/shared'
            }
        },
        newYear: {
            css: './app/landings/newYear/css',
            js: './app/landings/newYear/js'
        }
    },
    gameAppPath: './app/game',
    iconsPath: './app/icons',
    nodeModulesDir: './node_modules'
};

var allTasksNames = [];

function scssTask(taskName, pathToScss, outputPath) {
    if (outputPath === undefined) {
        outputPath = '/css';
    }

    return gulp
        .src(pathToScss)
        .pipe(plugins.plumber({ errorHandler: plugins.notify.onError("Error: <%= error.message %>") }))
        .pipe(plugins.sass({
            outputStyle: 'expanded',
            includePaths: [
                config.panelAppPath,
                './bootstrap.modified/bootstrap-sass/assets/stylesheets'
            ]
        }).on('error', plugins.sass.logError))
        .pipe(plugins.autoprefixer())
        .pipe(gulp.dest(config.distPath + outputPath))
        .pipe(plugins.rename({ suffix: '.min' }))
        .pipe(plugins.cleanCss())
        .pipe(gulp.dest(config.distPath + outputPath))
        .pipe(plugins.notify({ message: taskName + ' task completed', onLast: true }));
}

function scssTaskRegister(taskName, pathToScss, outputPath) {
    gulp.task(taskName,
        function() {
            return scssTask(
                taskName,
                pathToScss,
                outputPath
            );
        });
    allTasksNames.push(taskName);
}

scssTaskRegister('css-core', [config.coreAppPath + '/core.scss']);
scssTaskRegister('css-icons', [config.iconsPath + '/mq.scss']);
scssTaskRegister('css-panel', [config.panelAppPath + '/panel.scss']);
scssTaskRegister('css-landings-home-org', [config.landings.home.appPath.org + '/home.scss'], '/css/landings/org');
scssTaskRegister('css-landings-home-ru', [config.landings.home.appPath.ru + '/home.scss'], '/css/landings/ru');
scssTaskRegister('css-landings-newYear',
    [
        config.landings.newYear.css + '/animate.scss',
        config.landings.newYear.css + '/responsive.scss',
        config.landings.newYear.css + '/style.scss'
    ],
    '/css/landings/newYear');
scssTaskRegister('css-game', [config.gameAppPath + '/game.scss']);
scssTaskRegister('css-game-landing', [config.gameAppPath + '/game.landing.scss']);
scssTaskRegister('css-lecture', [config.coreAppPath + '/lecture.scss']);


function jsTask(taskName, jsFiles, outputFileName, dependencyResolverConfig, filesFilterConfig, outputPath) {

    gulp.task(taskName,
        function() {
            if (outputPath === undefined) {
                outputPath = '/js';
            }

            return gulp
                .src(jsFiles)
                .pipe(plugins.plumber({ errorHandler: plugins.notify.onError('Error: <%= error.message %>') }))
                .pipe(filesFilterConfig === undefined
                    ? plugins.empty()
                    : plugins.filter(filesFilterConfig.pattern, filesFilterConfig.config))
                .pipe(dependencyResolverConfig === undefined
                    ? plugins.empty()
                    : plugins.resolveDependencies(dependencyResolverConfig))
                .pipe(plugins.jshint({ lookup: false }))
                .pipe(plugins.jshint.reporter('default'))
                .pipe(plugins.concat(outputFileName))
                .pipe(gulp.dest(config.distPath + outputPath))
                .pipe(plugins.rename({ suffix: '.min' }))
                .pipe(plugins.uglify())
                .pipe(gulp.dest(config.distPath + outputPath))
                .pipe(plugins.notify({ message: taskName + ' task completed', onLast: true }));
        });
}

function jsTaskRegister(taskName, jsFiles, outputFileName, dependencyResolverConfig, filesFilterConfig, outputPath) {

    jsTask(taskName, jsFiles, outputFileName, dependencyResolverConfig, filesFilterConfig, outputPath);

    allTasksNames.push(taskName);
}



(function () {
    var jsFilterConfig = { pattern: '**/*.js', config: { restore: true }};
    var jsFiles = [];
    jsFiles.push(config.nodeModulesDir + '/jquery/dist/jquery.js');
    jsFiles.push(config.nodeModulesDir + '/jquery-validation/dist/jquery.validate.js');
    jsFiles.push(config.nodeModulesDir + '/jquery-validation-unobtrusive/jquery.validate.unobtrusive.js');
    jsFiles.push(config.nodeModulesDir + '/jquery-ajax-unobtrusive/jquery.unobtrusive-ajax.js');
    jsFiles.push(config.nodeModulesDir + '/jquery.cookie/jquery.cookie.js');
    jsFiles.push('./bootstrap.modified/bootstrap-sass/assets/javascripts/bootstrap.js');
    jsFiles.push(config.nodeModulesDir + '/cropperjs/dist/cropper.js');
    jsFiles.push(config.nodeModulesDir + '/moment/min/moment.min.js');
    jsFiles.push(config.nodeModulesDir + '/moment/locale/ru.js');
    jsFiles.push(config.nodeModulesDir + '/moment-timezone/builds/moment-timezone-with-data.min.js');
    jsFiles.push(config.nodeModulesDir + '/perfect-scrollbar/dist/perfect-scrollbar.js');
    jsFiles.push('./Scripts/jquery.signalR-2.2.2.js');
    jsFiles.push('./Scripts/bluebird.min.js');
    jsFiles.push('./Scripts/jquery.parseparams.js');
    jsFiles.push('./Scripts/Plugins/imageStorageUploader/imageUrlHelper.js');
    jsFiles.push('./Scripts/Plugins/imageStorageUploader/uploadToStorageStrategy.js');
    jsFiles.push('./Client Scripts/mvcfoolproof.unobtrusive.js');
    jsFiles.push(config.coreAppPath + '/**/*.js');
    jsFiles.push(config.coreAppPath + '/**/**/*.js');

    jsTaskRegister('js-core', jsFiles, 'core.js', undefined, jsFilterConfig);
})();

(function() {
    var jsFiles = [];
    jsFiles.push(config.panelAppPath + '/panel.js');
    var dependencyResolverConfig = {
        pattern: /\/\/\/\s*<reference path=\"(.*?\.js)\"\s*\/>/g,
        log: true
    };

    jsTaskRegister('js-panel', jsFiles, 'panel.js', dependencyResolverConfig);
})();

(function() {
    var jsFiles = [];
    jsFiles.push(config.landings.home.appPath.org + '/home.js');
    var dependencyResolverConfig = {
        pattern: /\/\/\/\s*<reference path=\"(.*?\.js)\"\s*\/>/g,
        log: true
    };

    jsTaskRegister('js-landings-home-org', jsFiles, 'home.js', dependencyResolverConfig, undefined, '/js/landings/org');
})();

(function() {
    var jsFiles = [];
    jsFiles.push(config.landings.home.appPath.ru + '/home.js');
    var dependencyResolverConfig = {
        pattern: /\/\/\/\s*<reference path=\"(.*?\.js)\"\s*\/>/g,
        log: true
    };

    jsTaskRegister('js-landings-home-ru', jsFiles, 'home.js', dependencyResolverConfig, undefined, '/js/landings/ru');
})();

(function() {
    var jsFiles = [];
    //jsFiles.push(config.nodeModulesDir + '/jquery/dist/jquery.js');
    jsFiles.push(config.nodeModulesDir + '/jquery.cookie/jquery.cookie.js');
    jsFiles.push(config.landings.newYear.js + '/evenfly.js');
    jsFiles.push(config.landings.newYear.js + '/jquery.nicescroll.min.js');
    jsFiles.push(config.nodeModulesDir + '/moment/min/moment.min.js');
    jsFiles.push(config.nodeModulesDir + '/moment/locale/ru.js');
    //jsFiles.push(config.nodeModulesDir + '/moment-timezone/builds/moment-timezone.min.js');
    jsFiles.push(config.nodeModulesDir + '/moment-timezone/builds/moment-timezone-with-data.min.js');
    jsFiles.push(config.landings.newYear.js + '/snowstorm-min.js');
    jsFiles.push(config.landings.newYear.js + '/wow.min.js');
    var dependencyResolverConfig = {
        pattern: /\/\/\/\s*<reference path=\"(.*?\.js)\"\s*\/>/g,
        log: true
    };

    jsTaskRegister('js-landings-newYear', jsFiles, 'home.js', dependencyResolverConfig, undefined, '/js/landings/newYear');
})();


(function() {
    var jsFiles = [];
    jsFiles.push(config.gameAppPath + '/game.owner.init.js');
    var dependencyResolverConfig = {
        pattern: /\/\/\/\s*<reference path=\"(.*?(shared|owner)\.js)\"\s*\/>/g,
        log: true
    };

    jsTaskRegister('js-game-owner', jsFiles, 'game.owner.js', dependencyResolverConfig);
})();

(function() {
    var jsFiles = [];
    jsFiles.push(config.gameAppPath + '/game.player.init.js');

    var dependencyResolverConfig = {
        pattern: /\/\/\/\s*<reference path=\"(.*?(shared|player)\.js)\"\s*\/>/g,
        log: true
    };

    jsTaskRegister('js-game-player', jsFiles, 'game.player.js', dependencyResolverConfig);
})();

(function () {
    var jsFiles = [];
    jsFiles.push(config.gameAppPath + '/game.landing.js');

    var dependencyResolverConfig = {
        pattern: /\/\/\/\s*<reference path=\"(.*?\.js)\"\s*\/>/g,
        log: true
    };

    jsTaskRegister('js-game-landing', jsFiles, 'game.landing.js', dependencyResolverConfig);
})();

gulp.task('fonts', function () {
    return gulp
        .src([config.appPath + '/icons/fonts/**/*'])
        .pipe(plugins.plumber({ errorHandler: plugins.notify.onError("Error: <%= error.message %>") }))
        .pipe(gulp.dest(config.distPath + '/fonts'))
        .pipe(plugins.notify({ message: 'fonts task completed', onLast: true }));
});
allTasksNames.push('fonts');

gulp.task('watch',
    allTasksNames,
    function() {
        gulp.watch(config.coreAppPath + '/**/*.scss', ['css-core']);
        gulp.watch(config.coreAppPath + '/**/*.js', ['js-core']);
        gulp.watch(config.coreAppPath + '/**/**/*.js', ['js-core']);
        gulp.watch(config.gameAppPath + '/**/*.scss', ['css-game', 'css-game-landing']);
        gulp.watch(config.gameAppPath + '/**/game.landing.js', ['js-game-landing']);
        gulp.watch(config.gameAppPath + '/**/*.player.js', ['js-game-player']);
        gulp.watch(config.gameAppPath + '/**/*.owner.js', ['js-game-owner']);
        gulp.watch(config.gameAppPath + '/**/*.shared.js', ['js-game-player', 'js-game-owner']);
        gulp.watch(config.panelAppPath + '/**/*.scss', ['css-panel']);
        gulp.watch(config.panelAppPath + '/**/*.scss', ['css-lecture']);
        gulp.watch(config.panelAppPath + '/**/*.js', ['js-panel']);
        gulp.watch(config.landings.home.appPath.org + '/**/*.scss', ['css-landings-home-org']);
        gulp.watch(config.landings.home.appPath.org + '/**/*.js', ['js-landings-home-org']);
        gulp.watch(config.landings.home.appPath.ru + '/**/*.scss', ['css-landings-home-ru']);
        gulp.watch(config.landings.home.appPath.ru + '/**/*.js', ['js-landings-home-ru']);
        gulp.watch(config.landings.home.appPath.shared + '/**/*.scss', ['css-landings-home-org', 'css-landings-home-ru']);
        gulp.watch(config.landings.home.appPath.shared + '/**/*.js', ['js-landings-home-org', 'js-landings-home-ru']);
        gulp.watch(config.iconsPath + '/fonts/**/*', ['fonts']);
        gulp.watch(config.iconsPath + '/*.scss', ['css-icons']);
        gulp.watch(config.landings.newYear.css + '/*.scss', ['css-landings-newYear']);
    }
);

gulp.task('default', allTasksNames);