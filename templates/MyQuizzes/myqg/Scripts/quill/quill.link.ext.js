﻿
/*THE ORIGINAL CODE IN ES6*/

//var Link = Quill.import('formats/link');

//class PromoblockLink extends Link {

//    static create(value) {
//        debugger;
//        let node = super.create(value);
//        value = this.sanitize(value);

//        if (!value.startsWith("http")) {
//            value = "http://" + value;
//        }

//        node.setAttribute('href', value);
//        return node;
//    }

//    static sanitize(url) {
//        debugger;
//        if (!url.startsWith("http")) {
//            url = "http://" + url;
//        }

//        return super.sanitize(url);
//    }
//}

/*CONVERTED TO ES5 BY https:///babeljs.io/repl */

"use strict";

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _get = function get(object, property, receiver) { if (object === null) object = Function.prototype; var desc = Object.getOwnPropertyDescriptor(object, property); if (desc === undefined) { var parent = Object.getPrototypeOf(object); if (parent === null) { return undefined; } else { return get(parent, property, receiver); } } else if ("value" in desc) { return desc.value; } else { var getter = desc.get; if (getter === undefined) { return undefined; } return getter.call(receiver); } };

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var Link = Quill.import('formats/link');

var PromoblockLink = function (_Link) {
    _inherits(PromoblockLink, _Link);

    function PromoblockLink() {
        _classCallCheck(this, PromoblockLink);

        return _possibleConstructorReturn(this, (PromoblockLink.__proto__ || Object.getPrototypeOf(PromoblockLink)).apply(this, arguments));
    }

    _createClass(PromoblockLink, null, [{
        key: "create",
        value: function create(value) {
            var node = _get(PromoblockLink.__proto__ || Object.getPrototypeOf(PromoblockLink), "create", this).call(this, value);
            value = this.sanitize(value);

            if (!value.startsWith("http")) {
                value = "http://" + value;
            }

            node.setAttribute('href', value);
            return node;
        }
    }, {
        key: "sanitize",
        value: function sanitize(url) {
            if (!url.startsWith("http")) {
                url = "http://" + url;
            }

            return _get(PromoblockLink.__proto__ || Object.getPrototypeOf(PromoblockLink), "sanitize", this).call(this, url);
        }
    }]);

    return PromoblockLink;
}(Link);