﻿$.validator.addMethod('requiredif',
    function (value, element, parameters) {
        var id = '#' + parameters['dependentproperty'];

        var targetvalue = parameters['targetvalue'];
        targetvalue =
          (targetvalue == null ? '' : targetvalue).toString();

        var control = $(id);
        var controltype = control.attr('type');
        var actualvalue =
            controltype === 'radio' ?
            control.is(':checked').toString() :
            control.val();

        if (targetvalue === actualvalue)
            return $.validator.methods.required.call(
              this, value, element, parameters);

        return true;
    });

$.validator.addMethod('rangeif',
    function (value, element, parameters) {
        var minValue = parameters['minValue'];
        var maxValue = parameters["maxValue"];
        var input = $('#SecondsForAnswer');

        if (!input.is(':disabled') && (input.val() < minValue || input.val() > maxValue)) {
            return $.validator.methods.range.call(
                 this, value, element, parameters);
        }

        return true;
    });

$.validator.unobtrusive.adapters.add(
    'requiredif',
    ['dependentproperty', 'targetvalue'],
    function (options) {
        options.rules['requiredif'] = {
            dependentproperty: options.params['dependentproperty'],
            targetvalue: options.params['targetvalue']
        };
        options.messages['requiredif'] = options.message;
    });

$.validator.unobtrusive.adapters.add(
    'rangeif',
    ['dependentproperty', 'targetvalue', 'minValue', 'maxValue'],
    function (options) {
        options.rules['rangeif'] = {
            dependentproperty: options.params['dependentproperty'],
            targetvalue: options.params['targetvalue'],
            minValue: options.params["minValue"],
            maxValue: options.params["maxValue"]
        };
        options.messages['rangeif'] = options.message;
    });