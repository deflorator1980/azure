﻿var Controls = {};

Controls.ASPECT_RATIO_QUIZ = 3 / 2;
Controls.ASPECT_RATIO_AVATAR = 1 / 1;
Controls.ASPECT_RATIO_PROMO = 1 / 1;



Controls.onEntityItemRemoveButton = function (button, checkOtherContainersCount) {
    var header = $(button).closest(".header");
    var listItemContainer = header.parent();
    var addButtonContainer = $(listItemContainer).parents('.subentities-list').parent().children('.subentities-list-addbutton-container');
    $(addButtonContainer).find('a').show();
    listItemContainer.remove();

    if (checkOtherContainersCount !== null && checkOtherContainersCount) {
        checkListItemContainersCount();
    }
    return false;

    function checkListItemContainersCount() {
        var $listItemContainers = $('.convergence-goals-container');
        if ($listItemContainers.length <= 2) {
            $.each($listItemContainers, function () {
                var $this = $(this);
                var $deleteButton = $this.find('.entity-item-remove-button');
                $deleteButton.addClass('hide');
            });
        }
    }
};

Controls.onToglingItemClick = function (toglingButton) {
    toglingButton = $(toglingButton);
    var toglContainer = toglingButton.parent().parent();
    var toglingItemTitle = toglContainer.find(".togling-item-title");
    var toglingItemContent = toglContainer.find(".togling-item-content");
    toglingButton.add(toglingItemTitle).toggleClass('colapsed');
    toglingItemContent.toggle();
    return false;
};

Controls.onToggleContainerButton = function (button, titleElementName) {
    var header = $(button).closest(".header");
    var body = header.next(".body");

    if (titleElementName) {
        titleElementName = titleElementName.replace(/\./g, '\\.').replace(/\[/g, '\\[').replace(/\]/g, '\\]');
    }
    var titlesElemNames = titleElementName ? titleElementName.split(',') : '';

    body.toggle(300).promise().done(function () {
        header.find('div.toggleButton-button a').toggleClass('toggleButton-button-opened');
        header.find('div.toggleButton-button a').toggleClass('toggleButton-button-closed');
        header.find('div.entity-item-remove-button a').toggle();

        if (titleElementName) {
            var title = ""
            for (var i = 0; i < titlesElemNames.length; i++) {
                title = title + body.find("[name=" + titlesElemNames[i] + "]").val() + " ";
            }
            $(button).find('div.toggleButton-title').html(title);
        }
    });
    return false;
};

Controls.onAddSubEntitiesClick = function (button, url, prefix, callBack) {
    return Controls.onAddEntitiesClick(button, url, prefix, "sub", callBack);
};

Controls.onAddEntitiesClick = function (button, url, prefix, cssPrefix, callBack, secondsLocalization) {
    if (!cssPrefix) {
        cssPrefix = "";
    }

    var listContainer = $(button).closest('.' + cssPrefix + 'entities-list-container');

    if (listContainer !== null && listContainer !== undefined) {
        var list = listContainer.children('.' + cssPrefix + 'entities-list');
        var loader = listContainer.children('.' + cssPrefix + 'entities-list-loader');
        var type = $('#Type').val();
        var count = listContainer.find('.' + cssPrefix + 'entity-list-item').length;

        $(button).attr('disabled', 'disabled');
        $.get(url,
            { htmlPrefix: prefix, count: count, type: type },
            function (data, textStatus, jqXHR) {
                var $data = $(data);
                list.append($data);
                loader.hide();
                $(button).removeAttr('disabled');
                if (typeof callBack === 'function') {
                    var timeInputs = $('input[timeinput="true"]', $data[0]);
                    callBack.call($data);
                    Controls.onInitTimeInputsDuplcates(timeInputs, secondsLocalization);
                    Controls.onAddEntitiesCommonPart($data);
                }
                Controls.OnCheckCustomSettingsValue();
            });
        loader.show();
        return false;
    }
};

Controls.onNoteLabelClick = function(noteField) {
    if (noteField.attr('state') === "hidden") {
        noteField
            .removeClass('hidden')
            .attr('state', 'visible');
    } else {
        noteField
            .addClass('hidden')
            .attr('state', 'hidden');
        $('span[data-valmsg-for="' + noteField.attr('name') + '"]')
            .removeClass('custom-validation-error')
            .addClass('field-validation-valid');
    }
};


Controls.onAddEmptyEntity = function(link, url, prefix, cssPrefix, callBack, secondsLocalization) {
    if (!cssPrefix) {
        cssPrefix = "";
    }
    var listContainer = $('.' + cssPrefix + 'entities-list-container');
    var list = listContainer.children('.' + cssPrefix + 'entities-list');
    var loader = listContainer.children('.' + cssPrefix + 'entities-list-loader');
    var count = listContainer.find('.' + cssPrefix + 'entity-list-item').length;
    var type = $('#Type').val();
    var dropzonePrefix = new Date().getTime();
    if (count < 1) {
        $.get(url,
            { htmlPrefix: prefix, count: count, type: type },
            function(data, textStatus, jqXHR) {
                var $data = $(data);
                list.append($data);
                var noteLabel = $data.find('.note-label').first();
                var noteField = noteLabel.parents('.subentities-list-add-note').find('textarea').first();
                noteLabel.click(function () {
                    Controls.onNoteLabelClick(noteField);
                });
                var timeInputs = $('input[timeinput="true"]', $data[0]);
                Controls.onInitTimeInputsDuplcates(timeInputs, secondsLocalization);
                Controls.onAddEntitiesCommonPart($data);

                loader.hide();
                if (typeof callBack === 'function') {
                    timeInputs = $('input[timeinput="true"]', $data[0]);
                    callBack.call($data);
                    Controls.onInitTimeInputsDuplcates(timeInputs, secondsLocalization);

                    Controls.onAddEntitiesCommonPart($data);
                }
                Controls.OnCheckCustomSettingsValue();
            });
        loader.show();
        return false;
    }
};

Controls.validateAnswers = function (questionContainer) {
    var quizType = $('#Type').val();
    var errorBlockHide = true;

    if (quizType == 'Quiz' &&
        !$('[name="AnswersContainer"] input[type="checkbox"]:checked', questionContainer).length) {
        $('#ErrorsBlock', questionContainer).show();
        $('[name="ErrorBlockCheckbox"]', questionContainer).show().addClass('custom-validation-error');
        errorBlockHide = false;
    } else {
        $('[name="ErrorBlockCheckbox"]', questionContainer)
            .hide()
            .removeClass('custom-validation-error');
    }
    if (quizType == 'Quiz' &&
        $('[name="AnswersContainer"] input[type="checkbox"]:not(:checked)', questionContainer).length < 1) {
        $('#ErrorsBlock', questionContainer).show();
        $('[name="ErrorMaximumAnswers"]', questionContainer).show().addClass('custom-validation-error');
        errorBlockHide = false;
    } else {
        $('[name="ErrorMaximumAnswers"]', questionContainer).hide().removeClass('custom-validation-error');
    }

    if (errorBlockHide) {
        $('#ErrorsBlock', questionContainer).hide();
    }

    return true;
};

Controls.onAddEntitiesCommonPart = function ($data) {
    $.each($data.find('.dropzone'), function (index, item) {
        if ($(item).parents(".Promo").length) {
            mediaSelectorForPromo.bind(item);
        } else {
            mediaSelectorBase.bind(item);
        };
    });
    /*var dropzonePrefix = new Date().getTime();
    

    var currenDropzoneForm = $('#dropzoneQuestionForm', $data[0]);
    currenDropzoneForm.attr('id', 'dropzoneQuestionForm' + dropzonePrefix);
    Controls.onInitDropZone('dropzoneQuestionForm' + dropzonePrefix);
    var dropzonePromoQuizItem = $('.dropzonePromoQuiz', $data[0]);
    dropzonePromoQuizItem.attr('id', 'dropzonePromoQuiz' + dropzonePrefix);
    Controls.onInitDropZone('dropzonePromoQuiz' + dropzonePrefix);
    $('.dropzonePromoQuestion', $data[0]).each(function () {
        Controls.onInitDropZone($(this).attr('id'));
    });*/


    $('[data-quill-editor-for]', $data[0]).each(function () {
        initQuill($(this));
    });
}

Controls.onSeeMoreClick = function (button, url, containerSelector, loaderSelector, httpParams) {
    if (!Controls.onSeeMoreClick.page) {
        Controls.onSeeMoreClick.page = 1;
    }
    $(loaderSelector).show();
    $(button).attr('disabled', 'disabled');

    //-------------
    var filterSelectElements = $("a.active[data-filter-goup-filter]");
    var filterType = $(filterSelectElements).data("filter-type");
    var isOnlyChoosen;

    switch (filterType) {
        case "allFilterQuestions": isOnlyChoosen = false; break;
        case "onlyFavoriteQuestions": isOnlyChoosen = true; break;
        default: isOnlyChoosen = false;
    }
    //-------------

    $.get(url + "?" + httpParams, { page: ++Controls.onSeeMoreClick.page, isOnlyChosen: isOnlyChoosen }, function (data, textStatus, jqXHR) {

        $(loaderSelector).hide();
        data = $.trim(data);
        if (data === "") {
            $(button).hide();
        } else {
            $(containerSelector).append(data);
            $(button).removeAttr('disabled');
        }
    });
};

Controls.onSeeMoreGameHistoryClick = function (button, url, containerSelector, loaderSelector, httpParams) {
    if (!Controls.onSeeMoreClick.page) {
        Controls.onSeeMoreClick.page = 1;
    }
    $(loaderSelector).show();
    $(button).attr('disabled', 'disabled');

    //-------------
    var filterSelectElements = $("a.active[data-filter-goup-filter]");
    var filterType = $(filterSelectElements).data("filter-type");
    var isOnlyChoosen;

    switch (filterType) {
        case "allFilterQuestions": isOnlyChoosen = false; break;
        case "onlyFavoriteQuestions": isOnlyChoosen = true; break;
        default: isOnlyChoosen = false;
    }
    //-------------

    $.get(url + "?" + httpParams, { page: ++Controls.onSeeMoreClick.page, isOnlyChosen: isOnlyChoosen }, function (data, textStatus, jqXHR) {

        $(loaderSelector).hide();
        data = $.trim(data);
        if (data === "") {
            $(button).hide();
        } else {

            var datas = $("[data-question-group=numbers][data-question-number]", data);
            datas.each(function () {
                var item = $("[data-question-group=numbers][data-question-number=" + $(this).data("question-number") + "]");
                if (item.size() > 0) {
                    item.parents(".question-history-info-item-container").remove();
                }
            });


            $(containerSelector).append(data);
            $(button).removeAttr('disabled');
        }
    });
};

Controls.onImageRotate = function (size, isRight, isCroppedImage) {
    var rootUrl = "/Content/Uploads/";
    var imagePathElement = $('#Image_CroppedInternalPath, #Photo_InternalPath');
    var imageIdElement = $('#Image_Id, #Photo_Id');
    var imageId = imageIdElement.val();
    if (imageId === '0')
        return;
    var imageUrl = $('.the-logo').css('background-image').replace('url("', '').replace('")', '');
    var imageName = imageUrl.split('/').pop();
    var path = size ? rootUrl + size + "/" : rootUrl;

    var secondImageContainer = $('#mobile-profile .logo-container');
    var secondLastDivInContainer = secondImageContainer.find('div').last();
    var secondFirstDivInContainer = secondImageContainer.find('div').first();
    var imageContainer = $('.image-container');
    var lastDivInContainer = imageContainer.find('div').last();
    var firstDivInContainer = imageContainer.find('div').first();
    var removeImgDiv = $('<span class="remove-image"></span>');

    $.ajax({
        url: '/Image/RotateImage',
        type: 'POST',
        data: { imageId: imageId, isRight: isRight, size: size, isCroppedImage: isCroppedImage },
        success: function (data) {
            var rotatedFileName = "";
            if (isCroppedImage) {
                rotatedFileName = data.CroppedInternalPath;
            } else {
                rotatedFileName = data.InternalPath;
            }

            imageContainer.removeClass("no-image");
            firstDivInContainer.attr("style", "background-image:url(" + path + rotatedFileName + ");background-repeat:no-repeat;");
            secondFirstDivInContainer.attr("style", "background-image:url(" + path + rotatedFileName + "); border-radius:150px;");
            imagePathElement.val(rotatedFileName);
        },
        error: function (error) {
            console.log(error);
        }
    });
}

Controls.onMobileImageUpload = function (input, size, modelNamePrefix, multiFiles, localizedProcess, localizedAddImageText) {
    var rootUrl = "/Content/Uploads/";

    var imgSize = 4194304;
    var imgFile = input.files[0];

    var $quizImageForCropp = $('#quiz-image');
    var cropBoxData = "";
    var canvasData = "";

    var form = $('<form action="/Image/Upload" enctype="multipart/form-data" id="imageUploadForm" method="post"></form>');

    var imageContainer = $(input).parents('.image-block.mobile').find('.image-container');
    var textDivContainer = imageContainer.find('div').last();
    var imageLogoContainer = imageContainer.find('.the-logo');
    var emptyImageblock = imageContainer.find('#no-image-bg');

    var $hiddenImageIdElement = $(input).parents('.right-column-big').prev('.left-column-big').find('.dropzone').children('[data-val-id~="ImageIdValue"]');
    var $hiddenInternalPathElement = $(input).parents('.right-column-big').prev('.left-column-big').find('.dropzone').children('[data-val-id~="ImageInternalPathValue"]');
    var $hiddenDisplayNameElement = $(input).parents('.right-column-big').prev('.left-column-big').find('.dropzone').children('[data-val-id~="ImageDisplayNameValue"]');

    var dropzoneId = $(input).parents('[name~="QuestionContainer"]').find('.dropzone').attr('id');

    var cloned = $(input).clone();

    cloned.insertBefore($(input));

    var quizEditorField = imageContainer.parents('.editor-field.editor-field-file');
    var quizErrorBlock = quizEditorField.next();
    var quizFileSizeErrorSpan = quizErrorBlock.find('.file-size-error');
    var quizFileTypeErrorSpan = quizErrorBlock.find('.file-type-error');
    var quizFileRacyErrorSpan = quizErrorBlock.find('.file-racy-error');

    if ($.inArray(imgFile.type, ['image/gif', 'image/png', 'image/jpg', 'image/jpeg']) === -1) {
        console.log("The file type is not accepteble");
        quizFileTypeErrorSpan.addClass('enabled');
        return false;
    }

    if (imgFile.size > imgSize) {
        quizFileTypeErrorSpan.removeClass('enabled');
        quizFileRacyErrorSpan.removeClass('enabled');
        quizFileSizeErrorSpan.addClass('enabled');

    } else {
        quizFileSizeErrorSpan.removeClass('enabled');
        quizFileTypeErrorSpan.removeClass('enabled');
        quizFileRacyErrorSpan.removeClass('enabled');

        textDivContainer.html('');
        textDivContainer.addClass('image-loading');
        textDivContainer.html(localizedProcess);

        form.append(input);

        $(imageLogoContainer).addClass('loader--show');
        $(imageLogoContainer).prev('.cssload-container').addClass('show');

        form.ajaxForm(
            {
                target: imageContainer

            })
            .ajaxSubmit(function (data) {

                textDivContainer.html("");
                textDivContainer.removeClass('image-loading');
                textDivContainer.html(localizedAddImageText);

                if (data.success) {
                    emptyImageblock.removeAttr('id');
                    imageContainer.removeClass("no-image");
                    imageContainer.removeClass('empty');

                    $hiddenImageIdElement.attr("value", data.image.Id);
                    $hiddenInternalPathElement.attr('value', data.image.InternalPath);
                    $hiddenDisplayNameElement.attr('value', data.image.DisplayName);
                    var path = size ? rootUrl + size + "/" : rootUrl;
                    var aspectRatio = Controls.ASPECT_RATIO_QUIZ;
                    if ($('#' + $dropzoneId).is('[aspectRatio]')) {
                        var aspectRatio = eval($('#' + $dropzoneId).attr('aspectRatio'));
                    }

                    Controls.onInitCropper($quizImageForCropp, cropBoxData, canvasData,
                        rootUrl + data.image.InternalPath, data.image.InternalPath, input.id, dropzoneId,
                        false, true, aspectRatio);
                } else if (data.isAdultImage) {

                    $(imageLogoContainer).prev('.cssload-container').removeClass('show');
                    $(imageLogoContainer).removeClass('loader--show');

                    quizFileRacyErrorSpan.addClass('enabled');
                }
            });

        var $imageChanged = true;
        $(input).insertBefore(cloned);
        $('#data-input-changed').val($imageChanged);
        cloned.remove();
        $(input).val('');
    }
};

Controls.onImageSelected = function (input, size, modelNamePrefix, multiFiles, localizedProcess, localizedAddImageText) {
    var rootUrl = "/Content/Uploads/";

    var imgSize = 4194304;
    var imgFile = input.files[0];

    var secondImageContainer = $('#mobile-profile .logo-container');
    var secondLastDivInContainer = secondImageContainer.find('div').last();
    var secondFirstDivInContainer = secondImageContainer.find('div').first();

    var imageContainer = $(input).parent().parent().find('.image-container');
    var lastDivInContainer = imageContainer.find('div').last();
    var firstDivInContainer = imageContainer.find('div').first();
    var imageIdHdnField = $(input).parent().find(".uploaded-image-id");
    var form = $('<form action="/Image/Upload" enctype="multipart/form-data" id="imageUploadForm" method="post"></form>');
    var uniqueIndex = new Date().getTime();
    var hiddenFieldTextIndex = $('<input type="hidden" name="' + modelNamePrefix + '.Index" value="' + uniqueIndex + '" />');

    var mobileQuizImageContainer = $('.image-block.mobile .editor-field-file').children('div');
    var lastMobileQuizDiv = mobileQuizImageContainer.find('div').last();
    var firstMobileQuizDiv = mobileQuizImageContainer.find('div').first();
    var emptyImageblock = mobileQuizImageContainer.find('#no-image-bg');
    var $mobileAddImageText = $('<div style="text-align: center; color: #FFFFFF; font-size: 14px; width: 45%; display: inline-block; height: 100%;vertical-align: middle">' + localizedAddImageText + '</div>');

    if (multiFiles) {
        modelNamePrefix = modelNamePrefix + "[" + uniqueIndex + "]";
    }

    var removeImgDiv = $('<span class="remove-image"></span>');
    var hiddenFieldText = $('<input type="hidden" name="' + modelNamePrefix + '.Id" />');
    var hiddenInternalPath = $('<input type="hidden" name="' + modelNamePrefix + '.InternalPath" />');
    var hiddenDisplayName = $('<input type="hidden" name="' + modelNamePrefix + '.DisplayName" />');
    var hiddenFieldTextIsDeleted = $('<input class="is-deleted" type="hidden" name="' + modelNamePrefix + '.UiIsDeleted" value="false" />');
    var cloned = $(input).clone();
    if (multiFiles) {
        lastDivInContainer = lastDivInContainer.clone().insertAfter(lastDivInContainer);
        firstDivInContainer.hide();
    }

    cloned.insertBefore($(input));

    var registerImageContainer = imageContainer.parents('.sign-in-form-line.logo-container');
    var registerErrorBlock = registerImageContainer.next();
    var registerErrorSpan = registerErrorBlock.find('.file-size-error');

    var quizEditorField = imageContainer.parents('.editor-field.editor-field-file');
    var quizErrorBlock = quizEditorField.next();
    var quizErrorSpan = quizErrorBlock.find('.file-size-error');

    var profileImageContainer = imageContainer.parent().next();
    var profileImageErrorSpan = profileImageContainer.find('.file-size-error');

    var mobileFileError = $('#mobile-profile').next();
    var mobileFileErrorSpan = mobileFileError.find('.file-size-error');

    if (imgFile.size > imgSize) {
        console.log("The file is exceeding the maximum file size (4 Mb)");
        mobileFileErrorSpan.addClass('enabled');
        profileImageErrorSpan.addClass('enabled');
        quizErrorSpan.addClass('enabled');
        registerErrorSpan.addClass('enabled');

    } else {
        mobileFileErrorSpan.removeClass('enabled');
        profileImageErrorSpan.removeClass('enabled');
        quizErrorSpan.removeClass('enabled');
        registerErrorSpan.removeClass('enabled');

        secondLastDivInContainer.html('');
        secondLastDivInContainer.removeAttr('style');
        secondLastDivInContainer.parent().removeAttr('style');
        secondLastDivInContainer.addClass('image-loading');
        secondLastDivInContainer.html(localizedProcess);

        lastDivInContainer.html('');
        //lastDivInContainer.removeAttr("style");
        //lastDivInContainer.parent().removeAttr('style');
        lastDivInContainer.addClass('image-loading');
        lastDivInContainer.html(localizedProcess);

        lastMobileQuizDiv.html('');
        lastMobileQuizDiv.removeAttr("style");
        lastMobileQuizDiv.parent().removeAttr('style');
        lastMobileQuizDiv.addClass('image-loading');
        lastMobileQuizDiv.html(localizedProcess);

        form.append(input);
        form.ajaxForm(
            {
                target: imageContainer

            })
            .ajaxSubmit(function (data) {
                imageContainer.removeClass("no-image");
                //$('.the-logo').children('div').text('');

                lastDivInContainer.html("");
                lastDivInContainer.removeClass('image-loading');
                lastDivInContainer.append(localizedAddImageText);
                firstDivInContainer.append(hiddenFieldText);
                firstDivInContainer.append(hiddenInternalPath);
                firstDivInContainer.append(hiddenDisplayName);

                secondLastDivInContainer.html('');
                secondLastDivInContainer.removeClass('image-loading');
                secondFirstDivInContainer.append(hiddenFieldText);
                secondFirstDivInContainer.append(hiddenInternalPath);
                secondFirstDivInContainer.append(hiddenDisplayName);

                lastMobileQuizDiv.html('');
                emptyImageblock.remove();
                lastMobileQuizDiv.removeClass('image-loading');
                lastMobileQuizDiv.append(hiddenFieldText);
                lastMobileQuizDiv.append(hiddenInternalPath);
                lastMobileQuizDiv.append(hiddenDisplayName);

                if (multiFiles) {
                    lastDivInContainer.append(hiddenFieldTextIndex);
                    lastDivInContainer.append(hiddenFieldTextIsDeleted);
                    lastDivInContainer.data("isDeleted", hiddenFieldTextIsDeleted);
                    lastDivInContainer.append(removeImgDiv);
                }
                hiddenFieldText.attr("value", data.Id);
                hiddenInternalPath.attr('value', data.InternalPath);
                hiddenDisplayName.attr('value', data.DisplayName);
                var path = size ? rootUrl + size + "/" : rootUrl;

                //firstDivInContainer.attr("style","background-image:url(" + path + data.InternalPath + ");background-repeat:no-repeat;position:absolute; left:0;background-size: cover;");
                firstDivInContainer.attr("style", "background-image:url(" + path + data.InternalPath + ");background-repeat:no-repeat;width: 45%;height: 115px;background-size: contain;");

                secondFirstDivInContainer.attr("style",
                    "background-image:url(" + path + data.InternalPath + "); border-radius:150px;");

                firstMobileQuizDiv.attr("style", "background-image:url(" + path + data.InternalPath + "); position:absolute;background-position: center;left:-2px;background-size: cover;");



                if (multiFiles) {
                    lastDivInContainer.click(function () {
                        $(this).data("isDeleted").attr("value", true);
                        $(this).hide();
                        var divCount = $(this).parent().find("div").not(":hidden").length;
                        if (divCount === 0) {
                            $(this).parent().addClass("no-image");
                            firstDivInContainer.show();
                        }
                    });
                }
            });
        var $imageChanged = true;
        $(input).insertBefore(cloned);
        $('#data-input-changed').val($imageChanged);
        cloned.remove();
    }
};

Controls.onQuizImageSelected = function (input, size, modelNamePrefix, multiFiles, localizedProcess, localizedUploadImageText) {
    var rootUrl = "/Content/Uploads/";

    var imgSize = 4194304;
    var imgFile = input.files[0];

    var $quizImageForCropp = $('#quiz-image');
    var cropBoxData = "";
    var canvasData = "";

    var imageContainer = $(input).parent().parent().find('.image-container');
    var form = $('<form action="/Image/Upload" enctype="multipart/form-data" id="imageUploadForm" method="post"></form>');
    var $hiddenImageIdElement = $("#dropzoneForm").children('[data-val-id~="ImageIdValue"]');
    var $hiddenInternalPathElement = $("#dropzoneForm").children('[data-val-id~="ImageInternalPathValue"]');
    var $hiddenCroppedInternalPathElement = $("#dropzoneForm").children('[data-val-id~="ImageCroppedInternalPathValue"]');
    var $hiddenDisplayNameElement = $("#dropzoneForm").children('[data-val-id~="ImageDisplayNameValue"]');
    var $previousHiddenInternalPathElement = $("#dropzoneForm").children('[data-val-id~="PreviousImageInternalPathValue"]'); //May be need image ID

    var uploadImageTextContainer =
        $('<div style="padding-top: 20px;left: 0px;text-align: center;color: #FFFFFF;line-height: 25px;">' +
            localizedUploadImageText +
            '</div>');
    var mobileQuizImageContainer = $('[name~="QuizForm"] .image-block.mobile .editor-field-file').children('div');

    var lastMobileQuizDiv = mobileQuizImageContainer.find('div').last();
    var imageblock = mobileQuizImageContainer.find('#no-image-bg');

    if (imageblock.length === 0) {
        imageblock = mobileQuizImageContainer.find('.the-logo');
    }

    var cloned = $(input).clone();

    cloned.insertBefore($(input));

    var quizEditorField = imageContainer.parents('.editor-field.editor-field-file');
    var quizErrorBlock = quizEditorField.next();
    var quizErrorSpan = quizErrorBlock.find('.file-size-error');
    var quizFileTypeErrorSpan = quizErrorBlock.find('.file-type-error');
    var quizAdultImageErrorSpan = quizErrorBlock.find('.file-racy-error');

    if (imgFile.size > imgSize) {
        quizAdultImageErrorSpan.removeClass('enabled');
        quizFileTypeErrorSpan.removeClass('enabled');
        quizErrorSpan.addClass('enabled');

    } else if (!imgFile.type.match(/image.*/)) {
        quizAdultImageErrorSpan.removeClass('enabled');
        quizErrorSpan.removeClass('enabled');
        quizFileTypeErrorSpan.addClass('enabled');
    } else {
        quizErrorSpan.removeClass('enabled');
        quizAdultImageErrorSpan.removeClass('enabled');
        quizFileTypeErrorSpan.removeClass('enabled');

        lastMobileQuizDiv.html('');
        lastMobileQuizDiv.removeAttr("style");
        lastMobileQuizDiv.parent().removeAttr('style');
        lastMobileQuizDiv.addClass('image-loading');
        lastMobileQuizDiv.html(localizedProcess);

        form.append(input);
        $('.image-loaded').remove();


        $(imageblock).addClass('loader--show');
        $(imageblock).parent().find('.cssload-container').addClass('show');

        form.ajaxForm({
            target: imageContainer,
            success: function () {
                console.log("Image uploaded success");
            }
        })
            .ajaxSubmit(function (data) {
                if (data.success) {
                    imageContainer.removeClass("no-image");

                    lastMobileQuizDiv.html('');
                    lastMobileQuizDiv.removeClass('image-loading').addClass("image-loaded");
                    imageblock.addClass('hidden');

                    $previousHiddenInternalPathElement.attr('value', $hiddenInternalPathElement.val());
                    $hiddenImageIdElement.attr("value", data.image.Id);
                    $hiddenInternalPathElement.attr('value', data.image.InternalPath);
                    $hiddenCroppedInternalPathElement.attr('value', data.image.CroppedInternalPath);
                    $hiddenDisplayNameElement.attr('value', data.image.DisplayName);
                    var path = size ? rootUrl + size + "/" : rootUrl;

                    $(imageblock).removeClass('hidden');
                    $(imageblock).addClass('quiz--image-bg');
                    $(imageblock).next().append(uploadImageTextContainer);
                    var aspectRatio = Controls.ASPECT_RATIO_QUIZ;
                    if ($("#dropzoneForm").is('[aspectRatio]')) {
                        var aspectRatio = eval($("#dropzoneForm").attr('aspectRatio'));
                    }

                    Controls.onInitCropper($quizImageForCropp, cropBoxData, canvasData, rootUrl + data.image.InternalPath,
                        data.image.InternalPath, input.id, "dropzoneForm",
                        false, true, aspectRatio);
                }
                if (data.isAdultImage) {
                    $(imageblock).removeClass('hidden');
                    lastMobileQuizDiv.html('');
                    lastMobileQuizDiv.removeClass('image-loading');
                    $(imageblock).next().append(uploadImageTextContainer);

                    $(imageblock).parent().find('.cssload-container').removeClass('show');
                    $(imageblock).removeClass('loader--show');

                    quizAdultImageErrorSpan.addClass('enabled');
                }
            });

        var $imageChanged = true;
        $(input).insertBefore(cloned);
        $('#data-input-changed').val($imageChanged);
        cloned.remove();
        $(input).val('');
    }
}

Controls.onImageSelectedWithProgressBar = function (input, size, modelNamePrefix, multiFiles, localizedProcess) {
    var rootUrl = "/Content/Uploads/";

    var imgSize = 4194304;
    var imgFile = input.files[0];

    var secondImageContainer = $('#mobile-profile .logo-container');
    var secondLastDivInContainer = secondImageContainer.find('div').last();
    var secondFirstDivInContainer = secondImageContainer.find('div').first();

    var imageContainer = $(input).parent().parent().find('.image-container');
    var lastDivInContainer = imageContainer.find('div').last();
    var firstDivInContainer = imageContainer.find('div').first();
    var form = $('<form action="/Image/Upload" enctype="multipart/form-data" id="imageUploadForm" method="post"></form>');

    var mobileQuizImageContainer = $('.image-block.mobile .editor-field-file').children('div');
    var lastMobileQuizDiv = mobileQuizImageContainer.find('div').last();
    var firstMobileQuizDiv = mobileQuizImageContainer.find('div').first();

    var bar = $('.bar');
    var percent = $(bar).children('.percent');
    var status = $('#status');

    var hiddenFieldText = $('#' + modelNamePrefix + '_Id');
    var hiddenInternalPath = $('#' + modelNamePrefix + '_InternalPath');
    var hiddenDisplayName = $('#' + modelNamePrefix + '_DisplayName');
    var cloned = $(input).clone();

    cloned.insertBefore($(input));

    var registerImageContainer = imageContainer.parents('.sign-in-form-line.logo-container');
    var registerErrorBlock = registerImageContainer.next();
    var registerErrorSpan = registerErrorBlock.find('.file-size-error');
    var registerRacyErrorSpan = registerErrorBlock.find('.file-racy-error');

    var quizEditorField = imageContainer.parents('.editor-field.editor-field-file');
    var quizErrorBlock = quizEditorField.next();
    var quizErrorSpan = quizErrorBlock.find('.file-size-error');

    var profileImageContainer = imageContainer.parents('.left-column').children('.error-block-container');
    var profileImageErrorSpan = profileImageContainer.find('.file-size-error');
    var profileImageRacyErrorSpan = profileImageContainer.find('.file-racy-error');

    var mobileFileError = $('#mobile-profile').next();
    var mobileFileErrorSpan = mobileFileError.find('.file-size-error');
    var mobileFileRacyErrorSpan = mobileFileError.find('.file-racy-error');

    var profileSaveBtn = $('#saveProfile');

    if (imgFile.size > imgSize) {
        console.log("The file is exceeding the maximum file size (4 Mb)");
        mobileFileErrorSpan.addClass('enabled');
        profileImageErrorSpan.addClass('enabled');
        quizErrorSpan.addClass('enabled');
        registerErrorSpan.addClass('enabled');

        profileImageRacyErrorSpan.removeClass('enabled');
        mobileFileRacyErrorSpan.removeClass('enabled');
        registerRacyErrorSpan.removeClass('enabled');

    } else {
        profileSaveBtn.attr('disabled', 'disabled');

        mobileFileErrorSpan.removeClass('enabled');
        profileImageErrorSpan.removeClass('enabled');
        quizErrorSpan.removeClass('enabled');
        registerErrorSpan.removeClass('enabled');

        profileImageRacyErrorSpan.removeClass('enabled');
        mobileFileRacyErrorSpan.removeClass('enabled');
        registerRacyErrorSpan.removeClass('enabled');

        secondLastDivInContainer.addClass('image-loading');
        lastMobileQuizDiv.addClass('image-loading');

        status.empty();
        var percentVal = '0';
        bar.width(percentVal).css('opacity', '1');
        percent.html(percentVal);

        form.append(input);
        form.ajaxForm({
            uploadProgress: function (event, position, total, percentComplete) {
                var percentVal = percentComplete + '%';
                bar.width(percentVal);
                percent.html(percentVal);
            },
            success: function (result, statusText, xhr) {

                if (result.success) {

                    secondLastDivInContainer.removeAttr('style');
                    secondLastDivInContainer.parent().removeAttr('style');

                    lastMobileQuizDiv.removeAttr("style");
                    lastMobileQuizDiv.parent().removeAttr('style');

                    lastDivInContainer.removeAttr("style");
                    lastDivInContainer.parent().removeAttr('style');
                    lastDivInContainer.addClass('image-loading');

                    imageContainer.removeClass("no-image");

                    lastDivInContainer.removeClass('image-loading');
                    secondLastDivInContainer.removeClass('image-loading');
                    lastMobileQuizDiv.removeClass('image-loading');

                    hiddenFieldText.attr("value", result.image.Id);
                    hiddenInternalPath.attr('value', result.image.InternalPath);
                    hiddenDisplayName.attr('value', result.image.DisplayName);
                    var path = size ? rootUrl + size + "/" : rootUrl;

                    firstDivInContainer.attr("style", "background-image:url(" + path + result.image.InternalPath + ");background-repeat:no-repeat;");
                    secondFirstDivInContainer.attr("style", "background-image:url(" + path + result.image.InternalPath + "); border-radius:150px;");
                    firstMobileQuizDiv.attr("style", "background-image:url(" + path + result.image.InternalPath + "); position:absolute;background-position: center;left:-2px;background-size: cover;");

                    profileImageRacyErrorSpan.removeClass('enabled');
                    mobileFileRacyErrorSpan.removeClass('enabled');
                    registerRacyErrorSpan.removeClass('enabled');

                    profileSaveBtn.removeAttr('disabled');

                    if (multiFiles) {
                        lastDivInContainer.click(function () {
                            $(this).data("isDeleted").attr("value", true);
                            $(this).hide();
                            var divCount = $(this).parent().find("div").not(":hidden").length;
                            if (divCount === 0) {
                                $(this).parent().addClass("no-image");
                                firstDivInContainer.show();
                            }
                        });
                    }

                    $('.btnRotate').removeAttr('disabled', 'disabled');
                    var $imageChanged = true;
                    $(input).insertBefore(cloned);
                    $('#data-input-changed').val($imageChanged);
                    cloned.remove();

                    var percentVal = '100%';
                    bar.width(percentVal);
                    percent.html(percentVal);
                    console.log(xhr.responseText);
                } else {
                    bar.width(0).css('opacity', '0');;
                    profileImageRacyErrorSpan.addClass('enabled');
                    mobileFileRacyErrorSpan.addClass('enabled');
                    registerRacyErrorSpan.addClass('enabled');

                    mobileFileErrorSpan.removeClass('enabled');
                    profileImageErrorSpan.removeClass('enabled');
                    quizErrorSpan.removeClass('enabled');
                    registerErrorSpan.removeClass('enabled');

                    profileSaveBtn.removeAttr('disabled');
                }
            },
            error: function (xhr, statusText, err) {
                status.html(err || statusText);
                profileSaveBtn.removeAttr('disabled');
            }
        }).submit();
    }
};

Controls.onImageItemDelete = function (divEl) {
    $(divEl).find(".is-deleted").attr("value", true);
    $(divEl).hide();
    var divCount = $(divEl).parent().find("div").not(":hidden").length;
    if (divCount === 0) {
        $(divEl).parent().addClass("no-image");
        $(divEl).parent().find('div').first().show();
    }
};

Controls.OnImageUpload = function (input, size, modelNamePrefix, multiFiles, localizedProcess) {
    var $quizImageForCropp = $('#quiz-image');
    var cropBoxData;
    var canvasData;

    var rootUrl = "/Content/Uploads/";
    var imgSize = 4194304;
    var imgFile = input.files[0];


    var secondImageContainer = $('#mobile-profile .logo-container');
    var secondLastDivInContainer = secondImageContainer.find('div').last();
    var secondFirstDivInContainer = secondImageContainer.find('div').first();

    var imageContainer = $(input).parent().parent().find('.image-container');
    var lastDivInContainer = imageContainer.find('div').last();
    var firstDivInContainer = imageContainer.find('div').first();
    var imageIdHdnField = $(input).parent().find(".uploaded-image-id");

    var mobileQuizImageContainer = $('.image-block.mobile .editor-field-file').children('div');
    var lastMobileQuizDiv = mobileQuizImageContainer.find('div').last();
    var firstMobileQuizDiv = mobileQuizImageContainer.find('div').first();

    var loader = $(input).prev('p').children('span');

    var removeImgDiv = $('<span class="remove-image"></span>');
    var hiddenFieldText = $('#' + modelNamePrefix + '_Id');
    var hiddenInternalPath = $('#' + modelNamePrefix + '_InternalPath');
    var hiddenCroppedInternalPath = $('#' + modelNamePrefix + '_CroppedInternalPath');
    var hiddenDisplayName = $('#' + modelNamePrefix + '_DisplayName');
    var cloned = $(input).clone();

    cloned.insertBefore($(input));

    var registerImageContainer = imageContainer.parents('.sign-in-form-line.logo-container');
    var registerErrorBlock = registerImageContainer.next();
    var registerErrorSpan = registerErrorBlock.find('.file-size-error');
    var registerRacyErrorSpan = registerErrorBlock.find('.file-racy-error');

    var quizEditorField = imageContainer.parents('.editor-field.editor-field-file');
    var quizErrorBlock = quizEditorField.next();
    var quizErrorSpan = quizErrorBlock.find('.file-size-error');
    var quizRacyErrorSpan = quizErrorBlock.find('.file-racy-error');

    var profileImageContainer = imageContainer.parent().next();
    var profileImageErrorSpan = profileImageContainer.find('.file-size-error');
    var profileImageRacyErrorSpan = profileImageContainer.find('.file-racy-error');

    var mobileFileError = $('#mobile-profile').next();
    var mobileFileErrorSpan = mobileFileError.find('.file-size-error');
    var mobileFileRacyErrorSpan = mobileFileError.find('.file-racy-error');

    if (imgFile.size > imgSize) {
        console.log("The file is exceeding the maximum file size (4 Mb)");
        mobileFileErrorSpan.addClass('enabled');
        profileImageErrorSpan.addClass('enabled');
        quizErrorSpan.addClass('enabled');
        registerErrorSpan.addClass('enabled');

        registerRacyErrorSpan.removeClass('enabled');
        quizRacyErrorSpan.removeClass('enabled');
        profileImageRacyErrorSpan.removeClass('enabled');
        mobileFileRacyErrorSpan.removeClass('enabled');

    } else {
        mobileFileErrorSpan.removeClass('enabled');
        profileImageErrorSpan.removeClass('enabled');
        quizErrorSpan.removeClass('enabled');
        registerErrorSpan.removeClass('enabled');

        var fd = new FormData();
        fd.append('name', imgFile);

        $('#popup-wrap').show();
        $('.cssload-container').addClass('show');

        var xhr = new XMLHttpRequest();
        xhr.open('POST', '/Image/Upload', true);
        xhr.setRequestHeader("X-Requested-With", "XMLHttpRequest");
        xhr.setRequestHeader("pragma", "no-cache");

        xhr.upload.addEventListener("progress", function (e) {
            if (e.lengthComputable) {
                var loaded = Math.ceil((e.loaded / e.total) * 100);
                $(input).prev('p').children('span').css({ 'width': loaded + '%' }).html(loaded + '%');
            }
        }, false);

        xhr.addEventListener("load",
            function (e) {
                var response = JSON.parse(e.target.responseText);
                if (!response.IsEmpty) {
                    if (response.success) {

                        secondLastDivInContainer.removeAttr('style');
                        secondLastDivInContainer.parent().removeAttr('style');
                        secondLastDivInContainer.addClass('image-loading');

                        lastDivInContainer.removeAttr("style");
                        lastDivInContainer.parent().removeAttr('style');
                        lastDivInContainer.addClass('image-loading');

                        lastMobileQuizDiv.removeAttr("style");
                        lastMobileQuizDiv.parent().removeAttr('style');
                        lastMobileQuizDiv.addClass('image-loading');

                        imageContainer.removeClass("no-image");

                        lastDivInContainer.removeClass('image-loading');
                        secondLastDivInContainer.removeClass('image-loading');
                        lastMobileQuizDiv.removeClass('image-loading');

                        hiddenFieldText.attr("value", response.image.Id);
                        hiddenInternalPath.attr("value", response.image.InternalPath);
                        hiddenCroppedInternalPath.attr("value", response.image.CroppedInternalPath);
                        hiddenDisplayName.attr("value", response.image.DisplayName);
                        var path = size ? rootUrl + size + "/" : rootUrl;

                        $('#popup-wrap').hide();
                        $('.cssload-container').removeClass('show');

                        Controls.onInitCropper($quizImageForCropp,
                            cropBoxData, canvasData, rootUrl + response.image.InternalPath,
                            response.image.InternalPath,
                            input.id, null, true, false, Controls.ASPECT_RATIO_AVATAR);

                        registerRacyErrorSpan.removeClass('enabled');
                        quizRacyErrorSpan.removeClass('enabled');
                        profileImageRacyErrorSpan.removeClass('enabled');
                        mobileFileRacyErrorSpan.removeClass('enabled');

                    } else {
                        $('#popup-wrap').hide();
                        $('.cssload-container').removeClass('show');

                        mobileFileErrorSpan.removeClass('enabled');
                        profileImageErrorSpan.removeClass('enabled');
                        quizErrorSpan.removeClass('enabled');
                        registerErrorSpan.removeClass('enabled');

                        registerRacyErrorSpan.addClass('enabled');
                        quizRacyErrorSpan.addClass('enabled');
                        profileImageRacyErrorSpan.addClass('enabled');
                        mobileFileRacyErrorSpan.addClass('enabled');
                    }
                }
            },
            false);

        xhr.send(fd);
        $('.btnRotate').removeAttr('disabled', 'disabled');
        var $imageChanged = true;
        $(input).insertBefore(cloned);
        $('#data-input-changed').val($imageChanged);
        cloned.remove();
    }
}

Controls.setFileInputValidationRules = function () {
    var inputs = $("input[type=file]");
    if ($.validator && inputs.length > 0) {
        inputs.each(function (index, input) {
            $(input).rules("add", {
                accept: "jpg|jpeg|png|ico|bmp|gif"
            });
        });
    }
};

Controls.refreshValidation = function () {
    $("form").each(function (ind, form) {
        form = $(form);
        form.removeData("validator").removeData("unobtrusiveValidation");
        $.validator.unobtrusive.parse(form);
    });
    //Controls.setFileInputValidationRules();
};

Controls.togglePageLocker = function (label) {
    var locker = $(".blue-overlay");
    if (!label) {
        label = '';
    }
    if (locker.length === 0) {
        $('body').prepend('<div class="blue-overlay">' + label + '</div>');
    } else {
        locker.remove();
    }
};

Controls.hidePageLocker = function () {
    $(".blue-overlay").remove();
};

Controls.onSignInButton = function (button) {
    $(button).next('.sign-in-form').css('display', 'block');

    $('body').prepend('<div class="blue-overlay">');
    $('.blue-overlay').on('click', function () {
        $('.sign-in-form').css('display', 'none');
        $(this).remove();
    });
    $('.closePopup').on('click', function () {
        $('.sign-in-form').css('display', 'none');
        $('.blue-overlay').remove();
    });
    return false;
};

Controls.showBlue = function (callback) {
    $('body').prepend('<div class="blue-overlay">');
    $('.blue-overlay').on('click', function () {
        $(this).remove();
        if (callback) {
            callback();
        }
    });

}

Controls.showMessage = function (title, text) {

    $('body').prepend('<div class="the-message">');
    $('.the-message').append('<h3>' + title + '</h3>');
    $('.the-message').append('<a href="#" class="message_close"></a>');
    $('.the-message').append('<p>' + text + '</p>');
    $('.the-message').append('<ul class="controls"><li><a class="save-button" href="#">Save</a></li><li><a  class="cancel-button" href="#">Cancel</a></li></ul>');

    $('.message_close').on('click', function () {
        $('.the-message').remove();
        $('.blue-overlay').remove();
    });

    $('.cancel-button').on('click', function () {
        $('.the-message').remove();
        $('.blue-overlay').remove();
    });

    this.showBlue(function () {
        $('.the-message').remove();
    });


};

Controls.onChangeLocalization = function (lang) {
    $.cookie("lang", lang, { expires: 100, path: '/' });
    location.reload();
}

Controls.onDetectBrowserLanguage = function (lang) {
    lang = lang.contains('en') ? 'en' : 'ru';
    $.cookie("lang", lang, { expires: 100, path: '/' });
}

Controls.onShowLanguageMenu = function (button) {
    var heightBeforeBottom = $(document).height() - ($("#language").offset().top + $("#language").height());
    var $languageMenu = $(button).parent('#language').children('ul');

    var languageLinkWidth = 130;

    if ($languageMenu.hasClass('disabled')) {
        if (heightBeforeBottom < 70) {
            $languageMenu.removeClass('disabled').addClass('active up').css({ 'width': languageLinkWidth });
        } else {
            $languageMenu.removeClass('disabled').addClass('active down').css({ 'width': languageLinkWidth });
        }
    } else {
        $languageMenu.removeClass('active').removeClass('up').removeClass('down').addClass('disabled');
    }

    $(document).on('click', function (e) {
        if (!$('#localization').hasClass('disabled') && !$(e.target).is('a.lang')) {
            $('#localization').removeClass('active down').addClass('disabled');
        }
    });

    if ($("#language a.lang.mobile").is(':visible')) {
        var rightPosition = $("#language a.lang.mobile").offset().left;
        $("header #menu-container ul #language .active#localization").css("left", rightPosition - 55);
    }
}

Controls.onShowDropDownMenu = function (button) {
    var mainElement = $(button).parent('.dropDownButton');
    var dropDownButton = mainElement.children('.dropDownButton__button');
    var dropDownMenu = mainElement.children('.dropDownButton__menu');

    if (mainElement[0].CloseDropDownMenuIfOpen === undefined) {
        mainElement[0].CloseDropDownMenuIfOpen = function (e) {
            if (e.target !== button && !$.contains(dropDownButton[0], e.target) && dropDownMenu.is(':visible')) {
                dropDownMenu.hide();
                $(document).unbind('click', mainElement[0].CloseDropDownMenuIfOpen);
            }
        }
    }

    if (dropDownMenu.is(':visible')) {
        dropDownMenu.hide()
        $(document).unbind('click', mainElement[0].CloseDropDownMenuIfOpen);
    }
    else {
        dropDownMenu.show();
        $(document).on('click', mainElement[0].CloseDropDownMenuIfOpen);
    }

}

Controls.onShowFooter = function () {
    var $form = $('#form');
    var $sectionBody = $('#body');

    if ($form.length === 0) {
        $form = $('#lectureCreateForm');
    }

    if ($form.length === 0) {
        $form = $('#lectureEditForm').parent();
    }
    if ($form.length === 0) {
        $form = $('.register-page-inner');
    }

    if (($(window).height() - $('header').height()) <= ($sectionBody.height() + $('.footer-bottom').height())) {
        $('.footer-bottom').show();
        $('.footer-bottom').css({ 'position': 'relative' });
    } else {
        $('.footer-bottom').show();
        $('.footer-bottom').css({ 'position': 'absolute' });
    }
}

Controls.onCkeckDeviceModel = function () {
    var deviceInfo = [
        {
            "index": navigator.userAgent.toLowerCase().indexOf("ipad"),
            "deviceName": "iOS"
        },
        {
            "index": navigator.userAgent.toLowerCase().indexOf("iphone"),
            "deviceName": "iOS"
        },
        {
            "index": navigator.userAgent.toLowerCase().indexOf("ipod"),
            "deviceName": "iOS"
        },
        {
            "index": navigator.userAgent.toLowerCase().indexOf("android"),
            "deviceName": "Android"
        },
        {
            "index": navigator.userAgent.toLowerCase().indexOf("blackberry"),
            "deviceName": "Blackberry"
        },
        {
            "index": navigator.userAgent.toLowerCase().indexOf("webos"),
            "deviceName": "WebOs"
        },
        {
            "index": navigator.userAgent.toLowerCase().indexOf("windows phone"),
            "deviceName": "WindowsPhone"
        },
        {
            "index": navigator.userAgent.toLowerCase().indexOf("iemobile"),
            "deviceName": "WindowsPhone"
        },
        {
            "index": navigator.userAgent.toLowerCase().indexOf("wpdesktop"),
            "deviceName": "WindowsPhone"
        },
        {
            "index": navigator.userAgent.toLowerCase().indexOf("windows"),
            "deviceName": "Windows"
        },
        {
            "index": navigator.userAgent.toLowerCase().indexOf("mac"),
            "deviceName": "Mac"
        }];

    var firstBracketIndex = navigator.userAgent.toLowerCase().indexOf("(");
    var minIndex = 99999999;
    var arrayIndex;
    var userDevice = 'Сan not identify mobile device';
    for (var counter = 0; counter < deviceInfo.length; counter++) {
        if (deviceInfo[counter].index < minIndex && deviceInfo[counter].index > firstBracketIndex) {
            minIndex = deviceInfo[counter].index;
            userDevice = deviceInfo[counter].deviceName;
        }
    }

    if ($.cookie("UserDevice") !== null) {
        $.removeCookie("UserDevice");
    }
    $.cookie("UserDevice", userDevice, { expires: 1, path: '/' });
}

Controls.onCheckInternetConnectionSpeed = function (imageUrl, imageSize) {
    var duration, endTime, bitsLoaded, speedMbps, speedKbps, speedBps;

    var image = new Image();
    var startTime = (new Date()).getTime();
    console.log("startTime = " + startTime);
    var cacheBuster = "?spdt=" + startTime;

    image.onload = (function () {
        endTime = (new Date()).getTime();
        console.log("endTime = " + endTime);
        duration = (endTime - startTime) / 1000;
        console.log("duration = " + duration);
        bitsLoaded = imageSize * 8;
        speedBps = (bitsLoaded / duration).toFixed(2);
        console.log("speedBps = " + speedBps);
        speedKbps = (speedBps / 1024).toFixed(2);
        console.log("speedKbps = " + speedKbps);
        speedMbps = (speedKbps / 1024).toFixed(2);
        console.log("speedMbps = " + speedMbps);
        if (speedKbps <= 195) {
            var warningMessageOpened = $('#warningWrap, #warningWrapSpeaker').children("#connectionSpeed").hasClass('show');
            if (!warningMessageOpened) {
                $('#connectionSpeed').removeClass('hideContent').removeClass('hide').addClass('show');
                $('.mobile-game #connectionSpeed').removeClass('hideContent').removeClass('hide').addClass('show');
                $('#connectionOffline').removeClass('show').addClass('hideContent');
                $('.mobile-game #connectionOffline').removeClass('show').addClass('hideContent');

                resize();
            }
        } else {
            var warningMessageClosed = $('#warningWrap, #warningWrapSpeaker').children("#connectionSpeed").hasClass('hideContent');
            var offlineMessageOpened = $('#warningWrap, #warningWrapSpeaker').children("#connectionOffline").hasClass('hideContent');
            if (!warningMessageClosed || !offlineMessageOpened) {
                $('#connectionSpeed').removeClass('show').addClass('hideContent');
                $('.mobile-game #connectionSpeed').removeClass('show').addClass('hideContent');
                $('#connectionOffline').removeClass('show').addClass('hideContent');
                $('.mobile-game #connectionOffline').removeClass('show').addClass('hideContent');

                resize();
            }
        }
    });

    image.onerror = (function () {
        var offlineMessageOpened = $('#warningWrap, #warningWrapSpeaker').children("#connectionOffline").hasClass('show');
        if (!offlineMessageOpened) {
            $('#connectionSpeed').removeClass('show').addClass('hideContent');
            $('.mobile-game #connectionSpeed').removeClass('show').addClass('hideContent');
            $('#connectionOffline').removeClass('hideContent').removeClass('hide').addClass('show');
            $('.mobile-game #connectionOffline').removeClass('hideContent').removeClass('hide').addClass('show');

            resize();
        }
    });

    image.src = imageUrl + cacheBuster;
}

Controls.OnCheckCustomSettingsValue = function () {
    var form = $('#lectureCreateForm') || $('#lectureEditForm');

    var customSettingsContainer = $('.custom-settings-block');
    var customPointsContainer = $('.question-score-container');
    var customSecondsContainer = $('.question-time-container');
    customPointsContainer.addClass('hide');
    customSecondsContainer.addClass('hide');
    customSettingsContainer.addClass('hide');

    if ($('#radio-points-yes').is(':checked') || $('#radio-time-yes').is(':checked')) {
        customSettingsContainer.removeClass('hide').addClass('show');
    } else {
        customSettingsContainer.removeClass('show').addClass('hide');
    }

    if ($('#radio-points-yes').is(':checked')) {
        customPointsContainer.removeClass('hide').addClass('show');
    } else {
        customPointsContainer.removeClass('show').addClass('hide');
    }

    if ($('#radio-time-yes').is(':checked')) {
        customSecondsContainer.removeClass('hide').addClass('show');
        $('#SecondsForAnswer').next('div').addClass('disabled');
        $('#RestingTime').next('div').addClass('disabled');
        $('#SecondsForAnswer').attr('disabled', 'disabled');
        $('#RestingTime').attr('disabled', 'disabled');
        $('#SecondsForAnswer').removeClass('input-validation-error');
        $('#RestingTime').removeClass('input-validation-error');
        $('#SecondsForAnswer').prev('span').removeClass('field-validation-error').addClass('field-validation-valid');
        $('#RestingTime').prev('span').removeClass('field-validation-error').addClass('field-validation-valid');
    } else {
        customSecondsContainer.removeClass('show').addClass('hide');
        $('#SecondsForAnswer').next('div').removeClass('disabled');
        $('#RestingTime').next('div').removeClass('disabled');
        $('#SecondsForAnswer').removeAttr('disabled');
        $('#RestingTime').removeAttr('disabled');
    }
}

Controls.OnCustomSettingsValidate = function (form, isOnStep) {
    var isFormValid = true;

    if (!isOnStep) {
        var isCustomPointsEnabled = $('#radio-points-yes').is(':checked');
        var isCustomTimeEnabled = $('#radio-time-yes').is(':checked');
        var questionContainer = $('[name="QuestionContainer"]', form);

        if (isCustomPointsEnabled && form !== null && form.attr('id') === 'questionsForm') {
            for (var i = 0; i < questionContainer.length; i++) {
                var customPointInput = $('.question-score-field', questionContainer[i]).children('input');
                var customPointError = $(customPointInput).prev('span');

                if (customPointInput !== null && customPointInput.val().length === 0) {
                    $(customPointError, questionContainer[i]).removeClass('field-validation-valid').addClass('field-validation-error');
                    customPointInput.addClass('input-validation-error');
                    isFormValid = false;
                } else {
                    customPointInput.removeClass('input-validation-error');
                    $(customPointError, questionContainer[i]).addClass('field-validation-valid').removeClass('field-validation-error');
                }
            }

            $('.field-validation-error').first().parents('[name="QuestionContainer"]').find('.question-score-field').first().children('input').focus();
        }

        if (isCustomTimeEnabled && form !== null && form.attr('id') === 'questionsForm') {
            for (var q = 0; q < questionContainer.length; q++) {
                var customTimeInput = $('.question-time-field', questionContainer[q]).children('input');

                $.each($(customTimeInput), function () {
                    var $this = $(this);
                    var customTimeError = $this.prev('span');

                    if ($this.val().length === 0) {
                        $(customTimeError, questionContainer[q]).removeClass('field-validation-valid').addClass('field-validation-error');
                        $this.addClass('input-validation-error');
                        isFormValid = false;
                    } else {
                        $this.removeClass('input-validation-error');
                        $(customTimeError, questionContainer[q]).addClass('field-validation-valid').removeClass('field-validation-error');
                    }
                });
            }

            //$('.field-validation-error').first().parents('[name="QuestionContainer"]').find('.field-validation-error').first().next('input').focus();
            var $duplicatedDev = $('.field-validation-error').first().parents('[name="QuestionContainer"]').find('.field-validation-error').first().parent().children('.content-for-input');
            $duplicatedDev.trigger('click', function () {
                var $this = $(this);
                $this.prev('input').focus();
            });

        } else {
            var $secondsFormAnswer = $('#SecondsForAnswer');
            var $secondsForAnswerReview = $('#RestingTime');
            var controlsArray = [$secondsFormAnswer, $secondsForAnswerReview];

            if (($secondsFormAnswer.val() === "" && !$secondsFormAnswer.attr('disabled')) || ($secondsForAnswerReview.val() === "" && !$secondsForAnswerReview.attr('disabled'))) {
                $.each(controlsArray, function () {
                    var $this = $(this);
                    var customTimeError = $this.prev('span');

                    if ($this.val().length <= 0) {
                        $this.addClass('input-validation-error');
                        $(customTimeError).removeClass('field-validation-valid').addClass('field-validation-error');
                        isFormValid = false;
                    } else {
                        $this.removeClass('input-validation-error');
                        $(customTimeError).addClass('field-validation-valid').removeClass('field-validation-error');
                        isFormValid = true;
                    }
                });
            }

            $.each(controlsArray, function () {
                var $this = $(this);
                var customTimeError = $this.prev('span');
                $this.on('keypress keyup change', function () {
                    if ($this.val().length <= 0) {
                        $this.addClass('input-validation-error');
                        $(customTimeError).removeClass('field-validation-valid').addClass('field-validation-error');
                        isFormValid = false;
                    } else {
                        $this.removeClass('input-validation-error');
                        $(customTimeError).addClass('field-validation-valid').removeClass('field-validation-error');
                        isFormValid = true;
                    }

                    var maxlength = $this.attr('maxlength');

                    if ($this.val().length > maxlength) {
                        $this.val(function (index, value) {
                            return value.substr(0, maxlength);
                        });
                    }
                });
            });
        }

        return isFormValid;
    }

    return isFormValid;
}

Controls.onInitDropZone = function (dropzoneId) {
    var rootUrl = "/Content/Uploads/";
    var $quizImageForCropp = $('#quiz-image');
    var cropBoxData;
    var canvasData;

    $("#" + dropzoneId).dropzone({
        url: "/Image/Upload",
        method: "POST",
        clickable: true,
        thumbnailWidth: null,
        thumbnailHeight: null,
        acceptedFiles: 'image/*',
        maxFileSize: 4194304,
        maxFiles: 1,
        init: function () {
            var $dropzoneForm = $("#" + dropzoneId);

            var $hiddenImageIdElement = $dropzoneForm.children('[data-val-id~="ImageIdValue"]');
            var $hiddenInternalPathElement = $dropzoneForm.children('[data-val-id~="ImageInternalPathValue"]');
            var $hiddenDisplayNameElement = $dropzoneForm.children('[data-val-id~="ImageDisplayNameValue"]');

            this.on("addedfile", function (file) {
                var $fileSizeErrorElement = $(this.element).parent().next().find('.file-size-error');
                var $fileTypeErrorElement = $(this.element).parent().next().find('.file-type-error');
                var $fileRacyErrorElement = $(this.element).parent().next().find('.file-racy-error');

                $fileSizeErrorElement.hide();
                $fileTypeErrorElement.hide();
                $fileRacyErrorElement.hide();

                if (file.size > this.options.maxFileSize) {
                    $fileSizeErrorElement.show();
                    $fileTypeErrorElement.hide();
                    $('#saveButton').removeAttr('disabled');
                    this.removeFile(file);

                } else if (!file.type.match(/image.*/)) {
                    $fileSizeErrorElement.hide();
                    $fileTypeErrorElement.show();
                    $('#saveButton').removeAttr('disabled');
                    this.removeFile(file);

                } else {
                    $(this.element).find('.dz-preview').hide();

                    $(this.element).addClass('loader--show');
                    $(this.element).find('.cssload-container').addClass('show');
                    $dropzoneForm.next('.toggle--popup').addClass('hide');

                    $('#saveButton').attr('disabled', 'disabled');

                    $(this.element).children('.dz-preview').last().addClass('dz-processing').addClass('dz-progress').css({ 'position': 'absolute', 'left': '0' });
                    console.log("File added");

                    file.previewElement.addEventListener('click', function () {
                        console.log('preview container click triggered');
                        file.previewElement.remove();
                        $dropzoneForm.click();
                    });
                }

            }),
                this.on("success", function (response) {

                    $(this.element).removeClass('loader--show');
                    $(this.element).find('.cssload-container').removeClass('show');

                    $('.dz-image').css({ "width": "100%", "height": "100%" });

                    var result = $.parseJSON(response.xhr.responseText);
                    if (result.success) {

                        //$previousHiddenImageIdElement.attr('value', $hiddenImageIdElement.val());
                        //$previousHiddenInternalPathElement.attr('value', $hiddenInternalPathElement.val());
                        //$previousHiddenDisplayNameElement.attr('value', $hiddenDisplayNameElement.val());

                        //$hiddenImageIdElement.attr("value", result.image.Id);
                        //$hiddenInternalPathElement.attr('value', result.image.InternalPath);
                        //$hiddenDisplayNameElement.attr('value', result.image.DisplayName);

                        $(this.element).find('.dz-message').addClass('hide');
                        var aspectRatio = Controls.ASPECT_RATIO_QUIZ;
                        if ($($dropzoneForm).is('[aspectRatio]')) {
                            aspectRatio = eval($($dropzoneForm).attr('aspectRatio'));
                        }

                        Controls.onInitCropper($quizImageForCropp, cropBoxData, canvasData, rootUrl + result.image.InternalPath,
                            result.image.InternalPath, null, this.element.id,
                            false, false, aspectRatio);
                    } else {

                        $(this.element).removeClass('loader--show');
                        $(this.element).find('.cssload-container').removeClass('show');

                        console.log(result.message);
                    }
                }),
                this.on("complete", function (data) {
                    $('#saveButton').removeAttr('disabled');

                    $('.dz-details').remove();
                    $(this).children('.dz-preview').first().remove();
                    this.removeAllFiles();

                    console.log("transaction completed");
                }),
                this.on("error", function (file) {

                    $(this.element).removeClass('loader--show');
                    $(this.element).find('.cssload-container').removeClass('show');

                    $('#saveButton').removeAttr('disabled');
                    console.log(file);
                });
        }
    });
}

Controls.onInitMultipleDropzones = function () {
    var rootUrl = "/Content/Uploads/";
    var $quizImageForCropp = $('#quiz-image');
    var cropBoxData;
    var canvasData;

    $.each($('.dropzone'), function () {

        var $this = $(this);
        var $dropzoneId = $($this).attr('id');

        if ($dropzoneId === 'dropzoneQuestionForm') {
            var $questionId = $this.parents('div.body').children('input[type="hidden"]').val();
            $dropzoneId = $dropzoneId + $questionId;
            $this.attr('id', $dropzoneId);
        }

        Dropzone.autoDiscover = false;
        $this.dropzone({
            url: "/Image/Upload",
            method: "POST",
            clickable: true,
            thumbnailWidth: null,
            thumbnailHeight: null,
            acceptedFiles: 'image/*',
            maxFileSize: 4194304,
            maxFiles: 1,
            init: function () {
                var $hiddenImageIdElement = $('#' + $dropzoneId).children('[data-val-id~="ImageIdValue"]');
                var $hiddenInternalPathElement = $('#' + $dropzoneId).children('[data-val-id~="ImageInternalPathValue"]');
                var $hiddenCroppedInternalPathElement = $('#' + $dropzoneId).children('[data-val-id~="ImageCroppedInternalPathValue"]');
                var $hiddenDisplayNameElement = $('#' + $dropzoneId).children('[data-val-id~="ImageDisplayNameValue"]');

                var $previousHiddenImageIdElement = $('#' + $dropzoneId).children('[data-val-id~="PreviousImageIdValue"]');
                var $previousHiddenInternalPathElement = $('#' + $dropzoneId).children('[data-val-id~="PreviousImageInternalPathValue"]');
                var $previousHiddenCroppedInternalPathElement = $('#' + $dropzoneId).children('[data-val-id~="PreviousImageCroppedInternalPathValue"]');
                var $previousHiddenDisplayNameElement = $('#' + $dropzoneId).children('[data-val-id~="PreviousImageDisplayNameValue"]');

                $previousHiddenImageIdElement.attr('value', $hiddenImageIdElement.val());
                $previousHiddenInternalPathElement.attr('value', $hiddenInternalPathElement.val());
                $previousHiddenCroppedInternalPathElement.attr('value', $hiddenCroppedInternalPathElement.val());
                $previousHiddenDisplayNameElement.attr('value', $hiddenDisplayNameElement.val());

                var internalPath = rootUrl + $hiddenInternalPathElement.val();
                var internalPathEmpty = $hiddenInternalPathElement.val().length === 0;
                var croppedInternalPath = rootUrl + $hiddenCroppedInternalPathElement.val();
                var croppedInternalPathEmpty = $hiddenCroppedInternalPathElement.val().length === 0;

                var $fileSizeErrorElement = $(this.element).parent().next().find('.file-size-error');
                var $fileTypeErrorElement = $(this.element).parent().next().find('.file-type-error');
                var $fileRacyErrorElement = $(this.element).parent().next().find('.file-racy-error');

                var imageFilePath = null;

                if (!croppedInternalPathEmpty) {
                    imageFilePath = croppedInternalPath;
                } else if (!internalPathEmpty) {
                    imageFilePath = internalPath;
                }

                if (imageFilePath !== null) {

                    $(this.element).find('.dz-message').addClass('hide');
                    $(this.element).find('img').hide();

                    $(this.element).css({
                        'background-image': 'url("' + imageFilePath + '")',
                        'background-size': 'cover',
                        'background-position': 'center top',
                        'background-repeat': 'no-repeat',
                        'background-color': 'transparent'
                    });

                    $('#' + $dropzoneId).next('.toggle--popup').removeClass('hide');
                }

                this.on("addedfile", function (file, done) {
                    $fileSizeErrorElement.hide();
                    $fileTypeErrorElement.hide();
                    $fileRacyErrorElement.hide();

                    var isFileSize = file.size < this.options.maxFileSize;
                    var isFileType = file.type.match(/image.*/);

                    if (this.files.length > 1) {
                        this.removeFile(this.files[0]);
                    }

                    if (!isFileSize) {
                        $fileSizeErrorElement.show();
                        $fileTypeErrorElement.hide();
                        $('#saveButton').removeAttr('disabled');
                        this.removeFile(file);
                    } else if (!isFileType) {
                        $fileSizeErrorElement.hide();
                        $fileTypeErrorElement.show();
                        $('#saveButton').removeAttr('disabled');
                        this.removeFile(file);
                    } else {
                        $(this.element).find('.dz-preview').hide();
                        $(this.element).addClass('loader--show');
                        $(this.element).find('.cssload-container').addClass('show');
                        $('#' + $dropzoneId).next('.toggle--popup').addClass('hide');

                        $('#saveButton').attr('disabled', 'disabled');

                        $(this.element).children('.dz-preview').last().addClass('dz-processing').addClass('dz-progress').css({ 'position': 'absolute', 'left': '0' });
                    }

                }),
                    this.on("success", function (response) {
                        var result = $.parseJSON(response.xhr.responseText);
                        if (result.success) {

                            $(this.element).children('.dz-preview').css({ 'position': 'relative' });
                            $(this.element).find('.dz-message').addClass('hide');
                            var aspectRatio = Controls.ASPECT_RATIO_QUIZ;
                            if ($('#' + $dropzoneId).is('[aspectRatio]')) {
                                var aspectRatio = eval($('#' + $dropzoneId).attr('aspectRatio'));
                            }

                            Controls.onInitCropper($quizImageForCropp, cropBoxData, canvasData, rootUrl + result.image.InternalPath,
                                result.image.InternalPath, null, this.element.id,
                                false, false, aspectRatio);
                        } else {

                            if (result.isAdultImage) {
                                $fileRacyErrorElement.show();
                            }

                            $(this.element).removeClass('loader--show');
                            $(this.element).find('.cssload-container').removeClass('show');
                            $(this.element).find('.dz-message').show();

                            console.log(result.message);
                        }
                    }),
                    this.on("complete", function (data) {

                        $('#saveButton').removeAttr('disabled');
                        $('.dz-details').remove();

                        if ($('#' + $dropzoneId).children('.dz-preview').length > 1) {
                            $('#' + $dropzoneId).children('.dz-preview').first().remove();
                        }
                    }),
                    this.on("error", function (file, message) {

                        $(this.element).removeClass('loader--show');
                        $(this.element).find('.cssload-container').removeClass('show');

                        $('#saveButton').removeAttr('disabled');

                        console.log(file);
                        console.log(message);
                    });
            }
        });
    });
}

Controls.onInitTimeInputsDuplcates = function (timeInputs, secondsLocalization) {
    $(timeInputs).each(function () {
        var $this = $(this);
        var $div = $this.next('div.content-for-input');
        $this.addClass('hide');

        $this.on('focusout blur', function () {

            $div.text($this.val() + ' ' + secondsLocalization);
            $div.removeClass('hide');
            $this.addClass('hide');
        });

        $this.on('keyup keypress', function () {
            var maxlength = $this.attr('maxlength');

            if ($this.val().length > maxlength) {
                $this.val(function (index, value) {
                    return value.substr(0, maxlength);
                });
            }
        });
    });

    $(timeInputs).next('div.content-for-input').each(function () {
        var $this = $(this);
        var $input = $this.parent().find('input');

        $this.ready(function () {
            $this.text($input.val() + ' ' + secondsLocalization);
        });

        $this.on('click', function () {
            $input.val(parseInt($this.text()));
            $input.removeClass('hide').focus();
            $this.addClass('hide');

        });
    });
}

Controls.onAddEmptyQuizWinnerPlace = function (button, url, prefix, maxPlacesCount) {
    var listContainer = $(button).parents('.subentities-list-addbutton-container').prev('.quiz-winners-place--container');

    if (listContainer !== null && listContainer !== undefined) {
        var placeItems = listContainer.children('.quiz-winners-place--item');
        var speakerContactsInfoContainer = $('.quiz-speaker-contact-info');
        var loader = listContainer.children('.entities-list-loader');
        var count = $(placeItems).length;

        $(button).css('pointer-events', 'none');
        $.get(url,
            { placeCount: count, htmlPrefix: prefix },
            function (data, textStatus, jqXHR) {
                var $data = $(data);
                $(speakerContactsInfoContainer).before($data);
                loader.hide();
                $(button).css('pointer-events', 'all');

                if (++count >= maxPlacesCount && !$(button).hasClass('hide')) {
                    $(button).parents('.subentities-list-addbutton-container').addClass('hide');
                }

                if (typeof callBack === 'function') {
                    callBack.call($data);
                }
            });
        loader.show();
        return false;
    }
};

Controls.onRemoveQuizWinnerPlace = function (button, checkOtherContainersCount) {
    var listItemContainer = $(button).closest(".quiz-winners-place--item");
    var addButtonContainer = $(listItemContainer).parents('.quiz-winners-place--container').next('.subentities-list-addbutton-container');
    $(addButtonContainer).removeClass('hide');

    var $listItemContainers = $('.quiz-winners-place--item');
    if ($listItemContainers.length == 3 && listItemContainer[0] == $listItemContainers[1]) {
        var $listItemContainer = $(listItemContainer);
        var value = $($listItemContainers[2]).find('.small-block--code input')[0].value;
        $($listItemContainer[0]).find('.small-block--code input')[0].value = value;
        $listItemContainers[2].remove();
    } else {
        listItemContainer.remove();
    }

    if (checkOtherContainersCount !== null && checkOtherContainersCount) {
        checkListItemContainersCount();
    }

    return false;

    function checkListItemContainersCount() {
        var $listItemContainers = $('.quiz-winners-place--item');
        if ($listItemContainers.length <= 1) {
            $.each($listItemContainers, function () {
                var $this = $(this);
                var $deleteButton = $this.find('.entity-item-remove-button');
                $deleteButton.attr('disabled', 'disabled');
            });
        } 
    }
};

Controls.onCheckWinnerCodeValue = function () {
    var $quizWinnerCodesBlock = $('.quiz-winners-place--container');

    if ($('#radio-place-yes').is(':checked')) {
        $($quizWinnerCodesBlock).removeClass('hide');
        $quizWinnerCodesBlock.next('.subentities-list-addbutton-container').removeClass('hide');
    } else {
        $($quizWinnerCodesBlock).addClass('hide');
        $quizWinnerCodesBlock.next('.subentities-list-addbutton-container').addClass('hide');
    }
}

Controls.OnYoutubeLiveUrlValidate = function() {
    var isFormValid = true;

    var isYoutubeLiveVideoEnabled = $('#radio-youtube-yes').is(':checked');
    var $youtubeLiveVideoURL = $('#YoutubeLiveVideoURL');

    if (isYoutubeLiveVideoEnabled) {
        var regEx = /(https?\:\/\/|www\.)((youtube\.com\/embed\/[^/]+)|(youtube\.com\/watch[^/]+)|(youtu\.be\/[^/]+))/i;
        var result = Controls.onUrlValidate($youtubeLiveVideoURL, regEx);
        if (result !== null && result !== undefined) {
            isFormValid = result;
        }

        $('.field-validation-error').first().parents('#YoutubeLiveStreamingBlock').find('.editor-field').first().children('input').focus();

    }

    return isFormValid;
}

Controls.OnQuizRequirementsValidate = function () {
    var isFormValid = true;

    var $lectureProviders = $('.lectureProviders');
    $.each($lectureProviders, function () {
        var $this = $(this);
        var checked = $this.find('.switch input').is(':checked');

        if (checked) {
            var $quizRequirements = $this.parent().find('.requirement-link');
            var regEx = /(^(http|https):\/\/.+)/i;
            $.each($quizRequirements, function () {
                var $this = $(this);
                var result = Controls.onUrlValidate($this, regEx);
                if (result !== null && result !== undefined) {
                    isFormValid = result;
                }

            });
        }
    });

    return isFormValid;
}

Controls.OnWinnerCodeValidate = function () {
    var isFormValid = true;

    var isQuizWinnersCodesEnabled = $('#radio-place-yes').is(':checked');
    var $quizWinnerCodesContainer = $('.quiz-winners-place--container');
    var $speakerContactsContainer = $('.quiz-speaker-contact-info');
    var $quizWinnersCodes = $('.quiz-winners-place--item', $quizWinnerCodesContainer);
    var $speakerContactsInputs = $('input', $speakerContactsContainer);

    if (isQuizWinnersCodesEnabled) {

        $.each($quizWinnersCodes, function () {
            var $this = $(this);
            var $winnerCodeInput = $($this).children('.small-block--code').children('.editor-field').children('.text-box.single-line');
            var result = Controls.onValidateInputIsNotEmpty($winnerCodeInput);

            if (result !== null && result!== undefined) {
                isFormValid = result;
            }
        });

        var $email = $($('#ContactEmail', $speakerContactsContainer)[0]);
        var $phone = $($('#ContactPhone', $speakerContactsContainer)[0]);

        if ($email.val().length > 0 || $phone.val().length > 0) {
            Controls.ClearErrorBlockForInput($email);
            Controls.ClearErrorBlockForInput($phone);
            isFormValid = true;
        } else {
            $.each($speakerContactsInputs, function () {
                var $this = $(this);
                var result = Controls.onValidateInputIsNotEmpty($this);

                if (result !== null) {
                    isFormValid = result;
                }
            });
        }

        $('.field-validation-error').first().parents('.quiz-winners-place--item').find('.editor-field').first().children('input').focus();

    }

    return isFormValid;
}

Controls.ClearErrorBlockForInput = function (input) {

    var $customPointError = $(input).prev('span');
    if (input != null) {
        $(input).removeClass('input-validation-error');
        $($customPointError).addClass('field-validation-valid').removeClass('field-validation-error');
    }
};

Controls.onValidateInputIsNotEmpty = function (input) {
    var $customPointError = $(input).prev('span');

    if (input !== null) {
        if ($(input).val().length === 0) {
            $($customPointError).removeClass('field-validation-valid').addClass('field-validation-error');
            $(input).addClass('input-validation-error');
            return false;
        } else {
            $(input).removeClass('input-validation-error');
            $($customPointError).addClass('field-validation-valid').removeClass('field-validation-error');
            return true;
        }
    }

    return null;
};

Controls.onUrlValidate = function (input, regEx) {
    var $customPointError = $(input).prev('span');

    if (input !== null && ($(input).val().length === 0 || !regEx.test($(input).val()))) {
        $($customPointError).removeClass('field-validation-valid').addClass('field-validation-error');
        $(input).addClass('input-validation-error');
        return false;
    } else if (input !== null) {
        $(input).removeClass('input-validation-error');
        $($customPointError).addClass('field-validation-valid').removeClass('field-validation-error');
    }
};

Controls.OnQuizTrackerCodeValidate = function () {
    var isFormValid = true;

    var isQuizTrackersEnabled = $('#radio-trackers-yes').is(':checked');
    var $quizTrackersRows = $('#QuizTrackers__List .QuizTrackers__Row');

    if (isQuizTrackersEnabled) {

        $.each($quizTrackersRows, function () {
            var $this = $(this);
            var $quizTrackerInput = $($this).children('.small-block--trackerCode').children('.editor-field').children('.text-box.single-line');
            var $quizTrackerError = $($quizTrackerInput).next('span');

            if ($this.is(':visible') && $quizTrackerInput.val().length === 0) {
                $($quizTrackerError, $this).removeClass('field-validation-valid').addClass('field-validation-error');
                $quizTrackerInput.addClass('input-validation-error');
                isFormValid = false;
            }
            else {
                $quizTrackerInput.removeClass('input-validation-error');
                $($quizTrackerError, $this).addClass('field-validation-valid').removeClass('field-validation-error');
            }
        });
    }

    return isFormValid;
}

Controls.onSaveCroppedImage = function () {
    var $quizImageForCropp = $('#quiz-image');
    var originalImageName = $($quizImageForCropp).attr('data-val-originalinternalpath');
    var fileInputId = $($quizImageForCropp).attr('data-val-fileInputId');
    var dropzoneId = $($quizImageForCropp).attr('data-val-dropzoneId');
    var updateCount = $($quizImageForCropp).attr('data-val-updatesCount');
    var isMobile = $($quizImageForCropp).attr('data-val-ismobile') === "true" ? true : false;
    var cropBoxData = $quizImageForCropp[0].cropper.getData();
    var imageData = $quizImageForCropp[0].cropper.getImageData();
    var rootUrl = "/Content/Uploads/";

    $('.croppQuizModal__buttonPlaceholder').children('a').addClass('disabled');
    $('#btnCropp').children('span').hide();
    $('#btnCropp').children('#cropper-loader').show();

    cropBoxData.left = cropBoxData.x;
    cropBoxData.top = cropBoxData.y;

    var cropBoxWidth = cropBoxData.width;
    var cropBoxHeight = cropBoxData.height;
    var cropBoxLeft = cropBoxData.left;
    var cropBoxTop = cropBoxData.top;
    var rotationAngle = imageData.rotate;

    var isUpdate = $quizImageForCropp.parents('#croppQuizModal').find('#btnCropp').attr('data-val-update');

    if (isUpdate === null || isUpdate === undefined) {
        isUpdate = false;
    }

    $.ajax({
        url: "/Image/CroppImage",
        method: "POST",
        data: { cropBoxWidth: cropBoxWidth, cropBoxHeight: cropBoxHeight, cropBoxLeft: cropBoxLeft, cropBoxTop: cropBoxTop, originalImageName: originalImageName, isUpdate: isUpdate, rotationAngle: rotationAngle },
        success: function (result) {
            closeCropperModalDialog();
            updateQuizImage(result);
            resetCropper();
        },
        error: function () {
            console.log("Cropping failed");
        }
    });

    function closeCropperModalDialog() {
        $('.croppQuizModal__buttonPlaceholder').children('a').removeClass('disabled');

        $('#btnCropp').children('span').show();
        $('#btnCropp').children('#cropper-loader').hide();

        $('#croppQuizModal').toggleClass('modal--close');
    }

    function updateQuizImage(result) {

        var $hiddenImageIdElement = $('#' + dropzoneId).children('[data-val-id~="ImageIdValue"]');
        var $hiddenInternalPathElement = $('#' + dropzoneId).children('[data-val-id~="ImageInternalPathValue"]');
        var $hiddenCroppedInternalPathElement = $('#' + dropzoneId).children('[data-val-id~="ImageCroppedInternalPathValue"]');
        var $hiddenDisplayNameElement = $('#' + dropzoneId).children('[data-val-id~="ImageDisplayNameValue"]');

        var $previousHiddenImageIdElement = $('#' + dropzoneId).children('[data-val-id~="PreviousImageIdValue"]');
        var $previousHiddenInternalPathElement = $('#' + dropzoneId).children('[data-val-id~="PreviousImageInternalPathValue"]');
        var $previousHiddenCroppedInternalPathElement = $('#' + dropzoneId).children('[data-val-id~="PreviousImageCroppedInternalPathValue"]');
        var $previousHiddenDisplayNameElement = $('#' + dropzoneId).children('[data-val-id~="PreviousImageDisplayNameValue"]');

        $hiddenImageIdElement.attr("value", result.Id);
        $hiddenInternalPathElement.attr('value', result.InternalPath);
        $hiddenDisplayNameElement.attr('value', result.DisplayName);

        $previousHiddenImageIdElement.attr('value', $hiddenImageIdElement.val());
        $previousHiddenInternalPathElement.attr('value', $hiddenInternalPathElement.val());
        $previousHiddenCroppedInternalPathElement.attr('value', $hiddenCroppedInternalPathElement.val());
        $previousHiddenDisplayNameElement.attr('value', $hiddenDisplayNameElement.val());

        if (isMobile !== null && isMobile !== undefined && !isMobile) {

            var quizDropZoneForm = dropzoneId;
            var $hiddenCroppedImageInternalPath = $('#' + quizDropZoneForm).children('[data-val-id~="ImageCroppedInternalPathValue"]');
            $hiddenCroppedImageInternalPath.val(result.CroppedInternalPath);

            var dropzone = $('#' + quizDropZoneForm).get(0).dropzone;

            $('.cssload-container').removeClass('show');
            $(dropzone.element).removeClass('loader--show');

            $(dropzone.element).find('img').hide();
            $(dropzone.element).find('img').css('src', rootUrl + result.CroppedInternalPath);
            $(dropzone.element).css('background-image', 'none');
            $(dropzone.element).css({
                'background-image': 'url(' + rootUrl + result.CroppedInternalPath + '?' + updateCount + ')',
                'background-position': 'center top',
                'background-size': 'cover'
            });
        } else {

            var imageContainer = $('#' + fileInputId).parent().find('.image-container');

            $(imageContainer).find('.cssload-container').removeClass('show');
            $(imageContainer).find('.loader--show').removeClass('loader--show');

            $(imageContainer).find('.quiz--image-bg').css({ 'background-image': 'url(' + rootUrl + result.CroppedInternalPath + ')', 'background-position': 'center', 'left': '-2px', 'background-size': 'cover', 'width': '45%', 'height': '80px' });
            $(imageContainer).find('.the-logo').css({ 'background-image': 'url(' + rootUrl + result.CroppedInternalPath + ')', 'background-position': 'center', 'left': '-2px', 'background-size': 'cover', 'width': '45%', 'height': '80px' });
            $(imageContainer).children('.image-bg').css('background-image', 'url(' + rootUrl + result.CroppedInternalPath + ')');
        }

        $('#' + dropzoneId).next('.toggle--popup').removeClass('hide');
    }

    function resetCropper() {
        $quizImageForCropp[0].cropper.destroy();
        $('#quiz-image').removeAttr('src');
    }
};

Controls.onUpdateCroppedImage = function (fileInputId) {
    var quizDropZoneForm = $('#' + fileInputId);
    var $hiddenCroppedImageInternalPath = $(quizDropZoneForm).children('[data-val-id~="ImageCroppedInternalPathValue"]');
    var $hiddenInternalPathElement = $(quizDropZoneForm).children('[data-val-id~="ImageInternalPathValue"]');
    var internalPath = $hiddenCroppedImageInternalPath.val() !== '' ? $hiddenCroppedImageInternalPath.val() : $hiddenInternalPathElement.val();
    var $quizImageForCropp = $('#quiz-image');
    var updatesCount = $($quizImageForCropp).attr('data-val-updatesCount');

    var rootUrl = "/Content/Uploads/";
    var originalImageInternalPath = rootUrl + $hiddenInternalPathElement.val();
    $quizImageForCropp.parents('#croppQuizModal').find('#btnCropp').attr('data-val-update', true);

    $($quizImageForCropp).attr('data-val-originalInternalPath', $hiddenInternalPathElement.val());
    $($quizImageForCropp).attr('data-val-dropzoneId', fileInputId);
    $($quizImageForCropp).attr('data-val-updatesCount', updatesCount !== undefined ? ++updatesCount : 1);

    $.ajax({
        url: "/Image/GetImageCropBox",
        method: 'POST',
        data: { internalPath: internalPath },
        success: function (result) {
            console.log(result.AdditionalInfo);

            $quizImageForCropp.parents('#croppQuizModal').toggleClass('modal--close');
            var quizImageSourceForCropp = $($quizImageForCropp).attr('src');

            if (quizImageSourceForCropp === "" || quizImageSourceForCropp === undefined) {
                $($quizImageForCropp).attr('src', originalImageInternalPath);
            }
            var aspectRatio = Controls.ASPECT_RATIO_QUIZ;
            if (quizDropZoneForm.is('[aspectRatio]')) {
                var aspectRatio = eval(quizDropZoneForm.attr('aspectRatio'));
            }

            var cropper = new Cropper($quizImageForCropp[0], {
                aspectRatio: aspectRatio,
                viewMode: 3,
                dragMode: 'move',
                autoCropArea: 1,
                restore: false,
                guides: false,
                highlight: true,
                cropBoxMovable: true,
                cropBoxResizable: false,
                zoomOnWheel: false,
                zoomOnTouch: false,
                ready: function () {
                    $quizImageForCropp[0].cropper.setCropBoxData(result.AdditionalInfo);
                }
            });
        }
    });
};

Controls.onSaveCroppedAvatar = function () {
    var $avatarImageForCropp = $('#quiz-image');
    var originalImageName = $($avatarImageForCropp).attr('data-val-originalinternalpath');
    var fileInputId = $($avatarImageForCropp).attr('data-val-fileInputId');
    var cropBoxData = $avatarImageForCropp[0].cropper.getData();
    var imageData = $avatarImageForCropp[0].cropper.getImageData();
    var rootUrl = "/Content/Uploads/";

    $('.croppAvatarModal__buttonPlaceholder').children('a').addClass('disabled');
    $('#btnCropp').children('span').hide();
    $('#btnCropp').children('#cropper-loader').show();

    cropBoxData.left = cropBoxData.x;
    cropBoxData.top = cropBoxData.y;

    var cropBoxWidth = cropBoxData.width;
    var cropBoxHeight = cropBoxData.height;
    var cropBoxLeft = cropBoxData.left;
    var cropBoxTop = cropBoxData.top;
    var rotationAngle = imageData.rotate;

    var isUpdate = $avatarImageForCropp.parents('#croppAvatarModal').find('#btnCropp').attr('data-val-update');

    if (isUpdate === null || isUpdate === undefined) {
        isUpdate = false;
    }

    $.ajax({
        url: "/Image/CroppImage",
        method: "POST",
        data: { cropBoxWidth: cropBoxWidth, cropBoxHeight: cropBoxHeight, cropBoxLeft: cropBoxLeft, cropBoxTop: cropBoxTop, originalImageName: originalImageName, isUpdate: isUpdate, rotationAngle: rotationAngle },
        success: function (result) {
            closeCropperModalDialog();
            updateAvatarImage(result);
            resetCropper();
        },
        error: function () {
            console.log("Cropping failed");
        }
    });

    function closeCropperModalDialog() {
        $('.croppAvatarModal__buttonPlaceholder').children('a').removeClass('disabled');

        $('#btnCropp').children('span').show();
        $('#btnCropp').children('#cropper-loader').hide();

        $('#croppAvatarModal').toggleClass('modal--close');
    }

    function updateAvatarImage(result) {
        var avatarContainer = $('.image-container');
        var avatar = $(avatarContainer).children('.the-logo');
        var $hiddenCroppedImageInternalPath = $(avatarContainer).parents('form').children('[data-val-id~="ImageCroppedInternalPathValue"]');
        $hiddenCroppedImageInternalPath.val(result.CroppedInternalPath);

        $(avatar).css('background-image', 'url(' + rootUrl + result.CroppedInternalPath + ')');
    }

    function resetCropper() {
        $avatarImageForCropp[0].cropper.destroy();
    }
}

Controls.onCancelCropp = function () {
    var rootUrl = "/Content/Uploads/";

    var $quizImageForCropp = $('#quiz-image');
    var originalImageName = $($quizImageForCropp).attr('data-val-originalinternalpath');
    var fileInputId = $($quizImageForCropp).attr('data-val-fileInputId');
    var dropZoneId = $($quizImageForCropp).attr('data-val-dropzoneId');
    var isMobile = $($quizImageForCropp).attr('data-val-isMobile') === 'true' ? true : false;
    var isUpdate = $quizImageForCropp.parents('#croppQuizModal').find('#btnCropp').attr('data-val-update');

    var dropzone = $('#' + dropZoneId).get(0).dropzone;

    var previousImageInternalPath = $(dropzone.element).find('[data-val-id~="PreviousImageInternalPathValue"]').val();

    var $hiddenImageIdElement = $(dropzone.element).children('[data-val-id~="ImageIdValue"]');
    var $hiddenInternalPathElement = $(dropzone.element).children('[data-val-id~="ImageInternalPathValue"]');
    var $hiddenCroppedInternalPathElement = $(dropzone.element).children('[data-val-id~="ImageCroppedInternalPathValue"]');
    var $hiddenDisplayNameElement = $(dropzone.element).children('[data-val-id~="ImageDisplayNameValue"]');

    if (!isUpdate) {
        $.ajax({
            url: '/Image/Delete',
            method: 'POST',
            data: { internalPath: originalImageName, previousImageInternalPath: previousImageInternalPath },
            success: function (result) {
                if (result.success) {
                    if (!isMobile && (previousImageInternalPath === undefined || previousImageInternalPath === null || previousImageInternalPath === "")) {
                        $(dropzone.element).find('.dz-message').removeClass('hide');
                        $(dropzone.element).removeAttr('style');
                        $(dropzone.element).removeClass('dz-started');
                        hideCroppQuizModal();
                    } else if (result.image !== null && result.image !== undefined) {
                        var image = result.image;

                        $hiddenImageIdElement.val(image.Id);
                        $hiddenInternalPathElement.val(image.InternalPath);
                        $hiddenCroppedInternalPathElement.val(image.CroppedInternalPath);
                        $hiddenDisplayNameElement.val(image.DisplayName);

                        $(dropzone.element).removeAttr('style');
                        $(dropzone.element).removeClass('dz-started');
                        $(dropzone.element).find('img').hide();
                        $('#' + dropZoneId).next('.toggle--popup').removeClass('hide');


                        $(dropzone.element).find('img').css('src', rootUrl + result.image.CroppedInternalPath);
                        $(dropzone.element).css('background-image', 'none');
                        $(dropzone.element).css({
                            'background-image': 'url(' + rootUrl + result.image.CroppedInternalPath + ')',
                            'background-position': 'center top',
                            'background-size': 'cover'
                        });

                        hideCroppQuizModal();
                    }

                    if (isMobile) {
                        $('.image-container').find('.loader--show').removeClass('loader--show').removeClass('quiz--image-bg');

                        hideCroppQuizModal();
                    }
                }
            },
            error: function (error) {
                hideCroppQuizModal();
            }
        });
    } else {
        hideCroppQuizModal();
    }


    function hideCroppQuizModal() {
        $('#croppQuizModal').addClass('modal--close');

        $('.dropzone').removeClass('loader--show');
        $('.cssload-container').removeClass('show');

        $(document.body).removeClass('fixed');

        resetCropper();

        return false;
    }

    function resetCropper() {
        $quizImageForCropp[0].cropper.destroy();
        $('#quiz-image').attr('src', '');
    }
};

Controls.onInitCropper = function (quizImageForCropp, cropBoxData, canvasData, src, internalPath,
    fileInputId, dropzoneId, isAvatar, isMobile, aspectRatio) {
    var croppModal = isAvatar !== undefined && isAvatar ? '#croppAvatarModal' : '#croppQuizModal';
    $(quizImageForCropp).parents(croppModal).toggleClass('modal--close');
    $(quizImageForCropp).attr('src', src);
    $(quizImageForCropp).attr('data-val-originalInternalPath', internalPath);
    $(quizImageForCropp).attr('data-val-fileInputId', fileInputId);
    $(quizImageForCropp).attr('data-val-dropzoneId', dropzoneId);
    $(quizImageForCropp).attr('data-val-isMobile', isMobile);
    $(quizImageForCropp).attr('data-val-updatesCount', 0);

    var cropper = new Cropper($(quizImageForCropp)[0], {
        aspectRatio: aspectRatio,
        viewMode: 3,
        responsive: true,
        dragMode: 'move',
        autoCropArea: 1,
        restore: false,
        guides: false,
        highlight: true,
        cropBoxMovable: true,
        cropBoxResizable: false,
        zoomOnWheel: false,
        zoomOnTouch: false,
        movable: false,
        zoomable: false,
        rotatable: true,
        scalable: true
    });
}

Controls.onUploadNewImageUsingdropzone = function () {
    var $imageForCropp = $('#quiz-image');
    var modalCropper = $($imageForCropp).parents('.modal');
    var isMobile = $($imageForCropp).attr('data-val-isMobile') === 'true' ? true : false;
    $(modalCropper).toggleClass('modal--close');

    $imageForCropp[0].cropper.destroy();
    $($imageForCropp).attr('src', '');

    if (!isMobile) {
        var dropzoneId = $($imageForCropp).attr('data-val-dropzoneId');
        var dropzone = $('#' + dropzoneId).get(0).dropzone;
        dropzone.removeAllFiles(true);
        $('#' + dropzoneId).click();
    } else {
        var fileInputId = $($imageForCropp).attr('data-val-fileInputId');
        $('#' + fileInputId).click();
    }
}

Controls.onEvaluateImage = function (input) {
    var file = input.files[0];
    var formData = new FormData();
    formData.append('image', file);

    var responseJson = "";

    var xhr = new XMLHttpRequest();
    xhr.onreadystatechange = function () {
        if (xhr.readyState === XMLHttpRequest.DONE) {
            responseJson = JSON.parse(xhr.responseText);
        }
    }

    xhr.open("POST", "/Image/Checkimage", false);
    xhr.send(formData);

    return responseJson;
}

Controls.onChangeYesNoRadioValue = function (yesRadioId, noRadioId, blockId) {
    var $block = $('#' + blockId);

    if ($('#' + yesRadioId).is(':checked')) {
        $block.removeClass('hide');
    } else {
        $block.addClass('hide');
    }
}
