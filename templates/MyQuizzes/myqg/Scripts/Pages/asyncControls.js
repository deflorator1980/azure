﻿window.AsyncControls = window.AsyncControl || {};
window.TimeControls = window.TimeControls || {};

window.AsyncControls.ActionSender = (function ($) {
    var _privat = {};
    var _public = {};

    _public.LoadAction = function (url, typeResponceData, sendData, successCallBack, errorCallback) {
        $.ajax({
            url: url,
            contentType: "application/json; charset=utf-8",
            dataType: typeResponceData,
            data: sendData,
            type: "POST",
            success: successCallBack,
            error: errorCallback
        });
    };

    return _public;
})(jQuery);


window.AsyncControls.ContentManager = (function ($) {
    var _privat = {};
    _privat.AjaxContentUpdater = function ($htmlContainer, ajaxUrl) {
        $.ajax({
            url: ajaxUrl,
            contentType: "application/json; charset=utf-8",
            dataType: "html",
            type: "POST",
            success: function (content) {
                $htmlContainer.empty();
                $htmlContainer.append(content);
            },
            error: function (err) {
                //alert("error");
            }
        });
    }


    //------------------------------------------
    var _public = {};

    _public.UpdateHistory = function () {
        //$("#filter-or-update-answers").trigger("click");
        $("a.active[data-filter-goup-filter]").trigger("click", { isfullHistoryUpdate: false });

    };

    _public.UpdateProfileInHeader = function () {
        var dataProfileContainer = $("[data-profile-wrap-ajax-updater=true]");
        var dataProfileUrlToUpadateContent;

        if (dataProfileContainer[0] && (dataProfileUrlToUpadateContent = dataProfileContainer.data("profile-wrap-ajax-url"))) {
            _privat.AjaxContentUpdater(dataProfileContainer, dataProfileUrlToUpadateContent);
        }

    }

    _public.UpdateLeaderBoardInGame = function () {
        var dataLeadersBoardContainer = $("[data-game-leaders-board-ajax-updater=true]");
        var dataLeadersBoardUrlToUpadateContent;

        if (dataLeadersBoardContainer[0] && (dataLeadersBoardUrlToUpadateContent = dataLeadersBoardContainer.data("game-leaders-board-ajax-url"))) {
            _privat.AjaxContentUpdater(dataLeadersBoardContainer, dataLeadersBoardUrlToUpadateContent);
        }
    }


    return _public;
})(jQuery);

window.TimeControls.ContentManager = (function ($) {
    var _privat = {};
    var _public = {};

    _public.StartTimerToNextGame = function ($timeViewElement, leftHours, leftMinutes, leftSeconds) {
        $timeViewElement.text((leftHours < 10 ? ("0" + leftHours) : leftHours) + ":" +
                              (leftMinutes < 10 ? ("0" + leftMinutes) : leftMinutes) + ":" +
                              (leftSeconds < 10 ? ("0" + leftSeconds) : leftSeconds));

        var intervel = window.setInterval(function () {
            --leftSeconds;

            if (leftSeconds < 0) {
                leftSeconds = 59;
                --leftMinutes;
            }

            if (leftMinutes < 0) {
                leftMinutes = 59;
                --leftHours;
            }

            if (leftHours <= 0 && leftMinutes <= 0 && leftSeconds <= 0) {
                clearInterval(intervel);
                window.location.reload();
            }

            $timeViewElement.text((leftHours < 10 ? ("0" + leftHours) : leftHours) + ":" +
                              (leftMinutes < 10 ? ("0" + leftMinutes) : leftMinutes) + ":" +
                              (leftSeconds < 10 ? ("0" + leftSeconds) : leftSeconds));

        }, 1000);
        var tineNow = new Date();
    }

    return _public;
})(jQuery);

Date.prototype.subTime = function (h, m, s) {
    this.setHours(this.getHours() - h);
    this.setMinutes(this.getMinutes() - m);
    this.setSeconds(this.getSeconds() - s);
    return this;
}

Date.prototype.addTime = function (h, m, s) {
    this.setHours(this.getHours() + h);
    this.setMinutes(this.getMinutes() + m);
    this.setSeconds(this.getSeconds() + s);
    return this;
}




Array.isArray = Array.isArray || function (arr) { return arr instanceof Array; }
String.prototype.contains = String.prototype.contains || function (str) { return this.indexOf(str) != -1; }


function functionsRunner(functions, responseText) {
    if (Array.isArray(functions)) {
        for (var i = 0; i < functions.length; i++) {
            if (typeof (functions[i]) == "function") {
                functions[i](responseText);
            }
        }
    }
    if (typeof (functions) == "function") {
        functions(responseText);
    }
    if (typeof (functions) == "string" && typeof (window[functions]) == "function") {
        window[functions](responseText);
    }
}