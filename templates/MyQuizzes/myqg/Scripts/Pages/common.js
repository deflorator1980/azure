﻿/*
options:
entityItemContainerCssClass = '.@entityItemContainerCssClass';
addButton = '#@addbtnId';
removeButton = '#@removebtnId';
loader = '#@loaderId';
formUrl = '@Url.Action("CreateFeature")'
htmlPrefix = null
addStyle = append|after
*/

var Common = {};

Common.removeListsSuffix = function (form) {

    $(".__multiselect-hidden-field__").remove();
    if (!form) {
        form = $("form");
    } else {
        form = $(form);
    }
        
    form.find('select.dropDown').not("#imageUploadForm").each(function (ind, el) {
        var select = $(el);     
        var ids = select.val();            
        var selName = select.attr('name').replace(/__selectList/g, "");
       
        if (ids) {

            if($.isArray(ids)){

                for (var i = 0; i < ids.length; i++) {

                    var name = select.attr('multiple') ?
                        selName + "[" + i + "].Id" : selName + ".Id";
                    
                    // console.log("name = ", name);
                    $('<input>').attr({
                        type: 'hidden',
                        name: name,
                        value: ids[i],
                        "class": "__multiselect-hidden-field__"
                    }).appendTo(form);
                }

            } else {

                var name = select.attr('multiple') ?
                        selName + "[" + i + "].Id" : selName + ".Id";

                // console.log("name = ", name);
                $('<input>').attr({
                    type: 'hidden',
                    name: name,
                    value: ids,
                    "class": "__multiselect-hidden-field__"
                }).appendTo(form);
            }
        }
    });
};

var SubEntityItemLoader = null;
$(function () {   
    
    SubEntityItemLoader = function (options) {
  
        var entityItemContainer = $(options.entityItemContainerCssClass);
        var addButton = $(options.addButton);
        var removeButton = $(options.removeButton);
        var loader = $(options.loader);
        var formUrl = options.formUrl;
        var addstyle = null;

        if (options.addStyle) {
            addstyle = options.addStyle
        } else {
            addstyle = "append";
        }

        $(addButton).click(function () {
            $(loader).show();
            $(addButton).attr('disabled', 'disabled');
            var url = formUrl + "?itemsCount=" + $(entityItemContainer).length;

            if(options.htmlPrefix){
                url = url + "&htmlPrefix=" + options.htmlPrefix;
            }

            $.get(url, function (result) {
                $(loader).hide();
                if (addstyle == "append") {
                    $(entityItemContainer).last().append($(result));
                } else if (addstyle == "after") {
                    $(entityItemContainer).last().after($(result));
                }
                $(removeButton).show();
                $(addButton).removeAttr('disabled');
            });
        });

        $(removeButton).click(function () {
            $(entityItemContainer).last().remove();
            if ($(entityItemContainer).length == 1) {
                $(removeButton).hide();
            }
        });
    };
});

$(function () {


    $.widget("ui.combobox", {
        _create: function () {
            var self = this;

            //console.log(self.options);

            var select = this.element.hide();
            var selected = select.children(":selected");
            var value = "";
            if (!self.options.multiselect) {            
                value = selected.val() ? selected.text() : "";
            }

            self.select = select;

            var input = $("<input />")
              .insertAfter(select)
              .val(value)
              .autocomplete({
                  delay: 0,
                  minLength: 0,
                  source: function (request, response) {
                      var matcher = new RegExp($.ui.autocomplete.escapeRegex(request.term), "i");
                      response(select.children("option").map(function () {
                          var text = $(this).text();
                          if (this.value && (!request.term || matcher.test(text)))
                              return {
                                  label: text.replace(
                                    new RegExp(
                                      "(?![^&;]+;)(?!<[^<>]*)(" +
                                      $.ui.autocomplete.escapeRegex(request.term) +
                                      ")(?![^<>]*>)(?![^&;]+;)", "gi"),
                                    "<strong>$1</strong>"),
                                  value: text,
                                  option: this
                              };
                      }));
                  },
                  select: function (event, ui) {
                      ui.item.option.selected = true;
                      self._trigger("selected", event, {
                          item: ui.item.option
                      });
                      select.trigger("change");
                  },
                  change: function (event, ui) {
                      if (!self.options.insertValues) {
                          if (!ui.item) {
                              var matcher = new RegExp("^" + $.ui.autocomplete.escapeRegex($(this).val()) + "$", "i"),
                              valid = false;
                              select.children("option").each(function () {
                                  if (this.value.match(matcher)) {
                                      this.selected = valid = true;
                                      return false;
                                  }
                              });
                              if (!valid) {
                                  // remove invalid value, as it didn't match anything
                                  $(this).val("");
                                  select.val("");
                                  return false;
                              }
                          }
                      }
                  }
              })
              .addClass("ui-widget ui-widget-content ui-corner-left");

            input.data("autocomplete")._renderItem = function (ul, item) {
                return $("<li></li>")
                  .data("item.autocomplete", item)
                  .append("<a>" + item.label + "</a>")
                  .appendTo(ul);
            };

            

            var showAllBtn = $("<button type='button'> </button>");
            showAllBtn.attr("tabIndex", -1)
            .attr("title", "Show All Items")            
            .insertAfter(input)
            .button({
                icons: {
                    primary: "ui-icon-triangle-1-s"
                },
                text: false
            })
            .removeClass("ui-corner-all")
            .addClass("ui-corner-right ui-button-icon")
            .click(function () {
                // close if already visible
                if (input.autocomplete("widget").is(":visible")) {
                    input.autocomplete("close");
                    return;
                }
                // pass empty string as value to search for, displaying all results
                input.autocomplete("search", "");
                input.focus();
            });

            if (self.options.multiselect) {
                var selectedValuesDiv = $('<div></div>')
                    .addClass('combobox-selected-values-container');
                var addBtn = $("<button type='button'>Add</button>");                               

                selectedValuesDiv.insertBefore(select);
                addBtn.insertAfter(showAllBtn);

                selected.each(function (ind, el) {
                    self.addValue($(el).text(), select, selectedValuesDiv);
                });

                addBtn.click(function () {
                    var v = input.val();
                    if (v != "" && v != undefined && v != null) {
                        self.addValue(input.val(), select, selectedValuesDiv);
                    }
                });
            }//end of if (self.options.multiselect) {

        },
        addValue: function (value, select, selectedValuesDiv) {
            var newValue = true;
            select.find("option").each(function (ind, el) {
                if (value == $(el).text()) {
                    newValue = false;
                    if (!$(el).data("added")) {
                        $(el).attr("selected", "selected");
                        $(el).data("added", true);
                        var selectedItemDiv = $("<div>" + value + "</div>");
                        selectedItemDiv.addClass("combobox-selected-item");
                        selectedItemDiv.data("option", el);
                        selectedValuesDiv.append(selectedItemDiv)
                        selectedItemDiv.click(function () {
                            var elOpt = $($(this).data("option"));
                            elOpt.removeAttr("selected");
                            elOpt.data("added", false);
                            $(this).remove();
                        });
                    }
                }
            });

            if (newValue) { //new value                        
                var optEl = $("<option>" + value + "</option>");
                optEl.attr("selected", "selected");
                optEl.attr("value", 0);
                optEl.data("added", true);
                select.append(optEl);
                var selectedItemDiv = $("<div>" + value + "</div>");
                selectedItemDiv.addClass("combobox-selected-item");
                selectedItemDiv.data("option", optEl);
                selectedValuesDiv.append(selectedItemDiv)
                selectedItemDiv.click(function () {
                    var elOpt = $($(this).data("option"));
                    elOpt.remove();
                    $(this).remove();
                });
            }


        }
    });


});

$(function () {

    /* jQuery.values: get or set all of the name/value pairs from child input controls   
     * @argument data {array} If included, will populate all child controls.
     * @returns element if data was provided, or array of values if not
    */

    $.fn.values = function (data) {
        var els = $(this).find(':input').get();

        if (typeof data != 'object') {
            // return all data
            data = {};

            $.each(els, function () {
                if (this.name && !this.disabled && (this.checked
                                || /select|textarea/i.test(this.nodeName)
                                || /text|hidden|password/i.test(this.type))) {
                    data[this.name] = $(this).val();
                }
            });
            return data;
        } else {
            $.each(els, function () {
                if (this.name && data[this.name]) {
                    if (this.type == 'checkbox' || this.type == 'radio') {
                        $(this).attr("checked", (data[this.name] == $(this).val()));
                    } else {
                        $(this).val(data[this.name]);
                    }
                }
            });
            return $(this);
        }
    };

});

//Injects multiselect choice to form
$(function () {
    $('form').not("#imageUploadForm").submit(function () {
        Common.removeListsSuffix();
    });
})
