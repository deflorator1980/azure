﻿var page = {
    hqSelect: null,
    otherOficesSelect: null,
    productsSelect: null,
    verticalsSelect: null,
    profileEditor: null,
    createContactUrl: null,
    init: function (createContactUrl, profileContent) {
        page.createContactUrl = createContactUrl;
        page.profileContent = profileContent;
        console.log(profileContent);

        $("form").submit(page.onFormSubmit);

        if ($(".companyContact").length > 1) {
            $('#btnRemoveLastContact').show();
        }

        page.profileEditor = $("#profileRichEditor");
        page.profileEditor.freshereditor({ excludes: ['removeFormat', 'insertheading4'] });
        page.profileEditor.freshereditor("edit", true);
        page.profileEditor.html(unescape(page.profileContent));

        page.hqSelect = $("#HeadQuarterCountryList").multiselect({
            multiple: false,
            header: "Select an option",
            noneSelectedText: "Select an Option",
            selectedList: 1
        });

        page.otherOficesSelect = $("#OtherOfficesCountriesList").multiselect();
        page.productsSelect = $("#ProductsList").multiselect();
        page.verticalsSelect = $("#VerticalsList").multiselect();

        $('#btnAddContact').click(function () {
            $("#contactLoader").show();
            $('#btnAddContact').attr('disabled', 'disabled');
            $.get(page.createContactUrl + "?contactCount=" + $(".companyContact").length, function (result) {
                $("#contactLoader").hide();
                $(".companyContact").last().append($(result));
                $('#btnRemoveLastContact').show();
                $('#btnAddContact').removeAttr('disabled');
            });
        });

        $('#btnRemoveLastContact').click(function () {
            $(".companyContact").last().remove();
            if ($(".companyContact").length == 1) {
                $('#btnRemoveLastContact').hide();
            }
        });

    },


    onFormSubmit: function () {
        $("#HeadquarterCountryNameId").val(page.hqSelect.val());
        $("#OtherOfficesCountriesIds").val(page.otherOficesSelect.val());
        $("#ProductsIds").val(page.productsSelect.val());
        $("#VerticalsIds").val(page.verticalsSelect.val());
        $("#ProfileHtmlContent").val(escape(page.profileEditor.html()));        
    }
};

