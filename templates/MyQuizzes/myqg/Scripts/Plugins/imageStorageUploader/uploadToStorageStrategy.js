﻿
if (!HTMLCanvasElement.prototype.toBlob) {
    Object.defineProperty(HTMLCanvasElement.prototype, 'toBlob', {
        value: function (callback, type, quality) {
            var canvas = this;
            setTimeout(function () {
                var binStr = atob(canvas.toDataURL(type, quality).split(',')[1]),
                    len = binStr.length,
                    arr = new Uint8Array(len);

                for (var i = 0; i < len; i++) {
                    arr[i] = binStr.charCodeAt(i);
                }

                callback(new Blob([arr], { type: type || 'image/png' }));
            });
        }
    });
}

//photoImageElement and isDownloadedHiddenElement are optional parameters
function uploadToStorageStrategy(storageUrl,
    token,
    storageName,
    storageFileId,
    photoImageElement,
    isDownloadedHiddenElement) {
    var $this = this;
    $this.photoImageElement = photoImageElement;
    $this.isDownloadedHiddenElement = isDownloadedHiddenElement;
    $this.storageUrl = storageUrl;
    $this.token = token;
    $this.storageFileId = storageFileId;
    $this.storageName = storageName;
    var previousErrorDelay = 3;
    var uploadErrorDelay = 5;
    var errorCount = 0;
    var maximumErrorCount = 3;

    function resetDelayParameters() {
        previousErrorDelay = 3;
        uploadErrorDelay = 5;
        errorCount = 0;
    };

    function updateSecurityParameters() {
        return new window.Promise(function(updateResolve, updateReject) {
            $.ajax({
                url: '/StorageAccount/NewSecurityToken?storageName=' + $this.storageName,
                type: 'GET'
            }).done(function(data) {
                $this.token = data.SecureToken;
                updateResolve();
            }).fail(function(error) {
                updateReject(error);
            });
        });
    }

    function uploadInternal(cropper, size, maxSize, firstResolveFunction) {
        var fileUrl = imageUrlHelper.getProfilePhotoUrl($this.storageUrl, size, $this.storageFileId);
        var croppedCanvas = cropper.getCroppedCanvas({
            width: size,
            height: size
        });

        var secureFileUrl = fileUrl + $this.token;
        var promise = new window.Promise(function(resolve, reject) {
            croppedCanvas.toBlob(function(blob) {
                var fileReader = new FileReader();
                fileReader.addEventListener('loadend',
                    function (e) {
                        var dataResult;
                        if (e.srcElement == undefined) {
                            dataResult = e.target.result;
                        } else {
                            dataResult = e.srcElement.result;
                        }
                        $.ajax({
                                url: secureFileUrl,
                                type: 'PUT',
                                headers: {
                                    'x-ms-blob-type': 'BlockBlob'
                                },
                                data: dataResult,
                                processData: false,
                                contentType: false
                            }).done(function(result) {
                                resetDelayParameters();
                                if (firstResolveFunction) {
                                    firstResolveFunction(fileUrl);
                                } else {
                                    resolve(fileUrl);
                                }
                            })
                            .fail(function(error) {
                                var failPromise;
                                if (error.status === 403) {
                                    failPromise = updateSecurityParameters();
                                } else {
                                    failPromise = window.Promise.resolve();
                                }
                                failPromise.then(function() {
                                    if (errorCount < maximumErrorCount) {
                                        setTimeout(function() {
                                                uploadInternal(cropper, size, maxSize, firstResolveFunction || resolve);
                                            },
                                            uploadErrorDelay * 1000);
                                        var nextDelay = previousErrorDelay + uploadErrorDelay;
                                        previousErrorDelay = uploadErrorDelay;
                                        uploadErrorDelay = nextDelay;
                                    } else {
                                        reject(error);
                                        resetDelayParameters();
                                    }
                                });
                            });
                    });

                fileReader.readAsArrayBuffer(blob);
            });
        });
        return promise;
    };

    $this.uploadProfilePhoto = function(cropper, success, fail) {
        var resultUrl = imageUrlHelper.getProfilePhotoUrl($this.storageUrl,
            imageUrlHelper.profilePhotoMaxWidth,
            $this.storageFileId);

        uploadInternal(cropper, imageUrlHelper.profilePhotoMaxWidth)
            .then(function(smallImageFileUrl) {
                uploadInternal(cropper, imageUrlHelper.profilePhotoSize).then(function(smallImageFileUrl) {
                    if (photoImageElement !== undefined) {
                        var d = new Date();
                        photoImageElement.attr('src', smallImageFileUrl + '?' + d.getTime());
                    }
                    if (isDownloadedHiddenElement !== undefined) {
                        isDownloadedHiddenElement.val(true);
                    }
                    success(resultUrl);
                });
            })
            .catch(function(error) {
                fail(error);
            });
    };

    $this.uploadQuizImage = function(cropper, success, fail) {
        var resultUrl =
            imageUrlHelper.getProfilePhotoUrl($this.storageUrl, imageUrlHelper.quizImageSizes[0], $this.storageFileId);
        var promise = uploadInternal(cropper, imageUrlHelper.quizImageSizes[0]);

        imageUrlHelper.quizImageSizes.reduce(function(p, c) {
            promise = promise.then(function() {
                return uploadInternal(cropper, c);
            });
        });

        promise.then(function(smallImageFileUrl) {
                if ($this.photoImageElement !== undefined) {
                    var d = new Date();
                    $this.photoImageElement.attr('src', smallImageFileUrl + '?' + d.getTime());
                }
                if ($this.isDownloadedHiddenElement !== undefined) {
                    $this.isDownloadedHiddenElement.val(true);
                }
                success(resultUrl);
            })
            .catch(function(error) {
                fail(error);
            });

    };
};