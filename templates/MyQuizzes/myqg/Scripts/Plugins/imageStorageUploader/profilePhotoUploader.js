﻿
(function () {
    function ProfilePhotoStorageUploader(displayImageElements, idHiddenElement, isDownloadedHiddenElement, isPhotoChangedElement, uploadStrategy) {
        var $this = this;
        $this.uploadStrategy = uploadStrategy;
        $this.isDownloadedHiddenElement = isDownloadedHiddenElement;
        $this.displayImageElements = displayImageElements;
        $this.isPhotoChangedElement = isPhotoChangedElement;
        resetOperationImages();

        function toDataUrl(url, callback) {
            var xhr = new XMLHttpRequest();
            xhr.onload = function () {
                var reader = new FileReader();
                reader.onloadend = function () {
                    callback(reader.result);
                };
                reader.readAsDataURL(xhr.response);
            };
            xhr.open('GET', url);
            xhr.responseType = 'blob';
            xhr.send();
        }

        function canvasRotate(angle) {
            var canvas = document.createElement('canvas');
            var image = $this.cropperImage.get(0);

            canvas.width = image.width;
            canvas.height = image.height;
            //canvas.getContext('2d').drawImage(image, 0, 0, image.width, image.height);
            var ctx = canvas.getContext('2d');
            ctx.clearRect(0, 0, canvas.width, canvas.height);
            var newWidth = canvas.height;
            var newHeight = canvas.width;
            canvas.width = newWidth;
            canvas.height = newHeight;
            console.log([canvas.width, canvas.height]);
            ctx.translate(newWidth / 2, newHeight / 2);
            ctx.rotate(angle * Math.PI / 180);
            ctx.drawImage(
                $this.cropperImage.get(0),
                -image.width / 2,
                -image.height / 2,
                image.width,
                image.height
            );
            $this.rotateImage.attr('src', canvas.toDataURL(
                'image/jpeg',
                1));
        }

        function resetOperationImages() {
            if ($this.cropperImage != undefined) {
                $this.cropperImage.remove();
            }
            if ($this.rotateImage != undefined) {
                $this.cropperImage.remove();
            }
            $this.cropperImage = $('<img style="display:none"/>');
            $this.rotateImage = $('<img style="display:none"/>');
            idHiddenElement.after($this.rotateImage);
            idHiddenElement.after($this.cropperImage);
        }

        function readFile(fileInput) {
            var promise = new Promise(function(resolve, reject) {

                var file = fileInput.files[0];
                var reader = new FileReader();
                reader.onload = function (event) {
                    $this.cropperImage.attr('src', event.target.result);
                    resolve();

                };
                reader.readAsDataURL(file);
            });
            return promise;
        };

        $this.rotate = function (angle) {
            toDataUrl(displayImageElements.first().attr('src'),
                function (data) {
                    var cropper;
                    $this.cropperImage.load(
                        function () {

                            $this.rotateImage.load(function () {
                                $this.rotateImage.get(0).addEventListener('ready',
                                    function () {
                                        $this.uploadStrategy.uploadProfilePhoto(cropper,
                                            function(fileUrl) {
                                                $.each(displayImageElements,
                                                    function (index, image) {
                                                        var d = new Date();
                                                        $(image).attr('src', fileUrl + '?' + d.getTime());
                                                    });
                                                cropper.destroy();
                                                $this.isPhotoChangedElement.val(true);
                                                resetOperationImages();
                                            },
                                            function() { });
                                    });
                                cropper = new Cropper($this.rotateImage.get(0),
                                    {
                                        aspectRatio: 1,
                                        autoCrop: true,
                                        autoCropArea: 1,
                                        viewMode: 2
                                    });
                                $('.cropper-container').css('display', 'none');
                            });
                            canvasRotate(angle);
                        });

                    $this.cropperImage.attr('src', data);
                });
        };

//        function

        $this.upload = function (fileInput) {
            if (!fileInput) {
                return;
            }
                
            var promise = readFile(fileInput);
            
            promise.then(function() {
                var cropper = new Cropper($this.cropperImage.get(0),
                    {
                        aspectRatio: 1,
                        autoCrop: true,
                        viewMode: 2,
                        ready: function() {
                            $this.uploadStrategy.uploadProfilePhoto(cropper,
                                function(fileUrl) {
                                    $.each(displayImageElements,
                                        function(index, image) {
                                            var d = new Date();
                                            $(image).attr('src', fileUrl + '?' + d.getTime());
                                        });
                                    cropper.destroy();
                                    $this.isDownloadedHiddenElement.val(true);
                                    $this.isPhotoChangedElement.val(true);
                                    fileInput.value = null;
                                    fileInput.addEventListener('change',
                                        function() {
                                            $('.the-logo').uploadToStorage(this);
                                        },
                                        false);
                                },
                                function(error) { console.log(error); });
                        }
                    });

                $('.cropper-container').css('display', 'none');
            });
        };

        $this.uploadFromUrl = function(url, cropBoxData, imageData) {

        }
    };
if (typeof jQuery !== "undefined" && jQuery !== null) {
    jQuery.fn.imageStorageUploader = function (idHiddenElement, isDownloadedHiddenElement, isPhotoChangedElement, uploadStrategy) {
        $(this).data('uploader', new ProfilePhotoStorageUploader(this,
            idHiddenElement,
            isDownloadedHiddenElement,
            isPhotoChangedElement,
            uploadStrategy));
    };
    jQuery.fn.uploadToStorage = function(fileInput) {
        $(this).data('uploader').upload(fileInput);
    };
    jQuery.fn.rotateImageLeft = function () {
        $(this).data('uploader').rotate(-90);
        };
    jQuery.fn.rotateImageRight = function () {
        $(this).data('uploader').rotate(90);
    };

    jQuery.fn.deleteProfilePhoto = function () {
        var uploader = $(this).data('uploader');
        uploader.isDownloadedHiddenElement.val(false);
        uploader.isPhotoChangedElement.val(true);
        $.each(uploader.displayImageElements, function () {
            $(this).attr('src', '/Images/Add Photo.png');
        });
    };
}
}).call();