﻿function imageUploader(uploadStrategy) {
    var $this = this;

    function readUrl(url) {
        console.log('123');
        console.log(Promise);
        return new Promise(function(resolve) {
            var xhr = new XMLHttpRequest();
            xhr.onload = function() {
                var reader = new FileReader();
                reader.onloadend = function() {
                    resolve(reader.result);
                };
                reader.readAsDataURL(xhr.response);
            };
            xhr.open('GET', url);
            xhr.responseType = 'blob';
            xhr.send();
        });
    }

    function updateImage(fileData, parameters) {
        return new Promise(function(resolve) {
            var body = $('body');
            var img = $('<img />');
            body.append(img);
            var cropper;

            img.load(function() {
                var cropperParameters = {
                    autoCropp: false,
                    modal: false,
                    rotatable: false,
                    scalable: false,
                    zoomable: false
                };
                img.get(0).addEventListener('ready',
                    function() {
                        cropper.setData({
                            'x': parameters.cropBoxData.x,
                            'y': parameters.cropBoxData.y,
                            'width': parameters.cropBoxData.width,
                            'height': parameters.cropBoxData.height,
                            'rotate': parameters.imageData.rotate
                        });
                        resolve(cropper);
                    });
                var cropper = new Cropper(img.get(0), cropperParameters);
                $('.cropper-container').css('display', 'none');
            });

            img.attr('src', fileData);
        });
    }

    function uploadData(imageInfo, cropper) {
        return new Promise(function(resolve, reject) {
            uploadStrategy.uploadQuizImage(cropper,
                function(fileUrl) {
                    resolve(fileUrl);
                },
                function() {
                    reject(error);
                });
        }).then(function(fileUrl) {
            return new Promise(function(resolve, reject) {
                $.post('/Image/UpdateImageInfo',
                    {
                        'imageId': imageInfo.Id,
                        'storageAccountName': uploadStrategy.storageName
                    },
                    function(imageInfo) {
                        resolve(fileUrl);
                    });
            });
        });
    }

    $this.uploadFromUrl = function(imageInfo, url, parameters) {
        return readUrl(url)
            .then(function(fileData) {
                return updateImage(fileData, parameters).then(
                    function(cropper) {
                        return uploadData(imageInfo, cropper);
                    });
            });
    };

    $this.uploadFromInput = function(imageInfo, base64Data, parameters) {
        return updateImage(base64Data, parameters).then(
            function(cropper) {
                return uploadData(imageInfo, cropper);
            });
    };

}