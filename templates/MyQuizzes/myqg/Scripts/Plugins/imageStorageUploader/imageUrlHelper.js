﻿var imageUrlHelper = (function (imageUrlHelper) {
    var $this = imageUrlHelper;
    // should be filled from server constant
    $this.profilePhotoContainerName = ''; 
    $this.profilePhotoSize = '';
    $this.profilePhotoMaxWidth = '';

    $this.getProfilePhotoUrl = function(containerUrl, size, storageId) {
        return containerUrl.concat('/', size, '/', storageId, '.jpg');
    };
    return imageUrlHelper;

}(imageUrlHelper || {}));