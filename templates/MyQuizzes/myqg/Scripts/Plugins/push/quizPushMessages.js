﻿quiz_push_messages = {
    getOrigin:function(){
        if (!window.location.origin) {
            return window.location.origin = window.location.protocol + "//" + window.location.hostname + (window.location.port ? ':' + window.location.port: '');
        }
        return window.location.origin;
    },
	gameStates: {
		"OnGameStart": "OnGameStart",
		"OnNewGameQuestion": "OnNewGameQuestion",
		"OnGameEnd": "OnGameEnd"
	},
	settings:{
		messages: {
		    "OnGameStart": { "title": "Quiz", "body": "The game has begun" },
			"OnNewGameQuestion": { "title": "Quiz", "body": "New quiz question" },
			"OnGameEnd": { "title": "Quiz", "body": "Game over. Check leaderboard" }
		}
	},
	init: function (gameSettings) {
		if (!!gameSettings) {
			this.settings = gameSettings;
		};
	},
	showGamePushMessage:function(gameState){
		var self = this;
		self.show(self.settings.messages[gameState].title, self.settings.messages[gameState].body);
	},
	tabIsActive: function () {
		console.log("document.hidden =" + !document.hidden)
		return !document.hidden;
	},
	show: function (msgTitle, msgBody) {
		var self = this;
		if (!Push.Permission.has() || self.tabIsActive()) {
			return;
		}

		Push.create(msgTitle, {
		    "body": msgBody,
			"onClick": function () {
				window.focus();
				this.close();
			},
		    "icon": self.getOrigin() + "/images/PushIcon.png"
		});
	}
}
if (!Push.Permission.has())
{
	Push.Permission.request();
}