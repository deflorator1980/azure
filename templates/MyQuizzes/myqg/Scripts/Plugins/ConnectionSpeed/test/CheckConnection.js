﻿define(function (require) {
    'use strict';

    var speed_test = require('/Scripts/Plugins/ConnectionSpeed/src/speed_test.js'),
        profile = require('/Scripts/Plugins/ConnectionSpeed/src/profile.js');
    
    return {
        check: function () {
            speed_test.ready();
            return profile;
        }
    }
});
