﻿mediaSelectorCommon = {
    events: {
        'showLoader': 'showLoader',
        'hideLoader': 'hideLoader',
        'fileAdded': 'fileAdded',
        'dropZoneFileUploaded': 'dropZoneFileUploaded',
        'dropZoneComplete': 'dropZoneComplete',
        'closeModal': 'closeModal',
        'save': 'save',
        'updateElementAttributes': 'updateElementAttributes',
        'setActiveTab': 'setActiveTab'
    },
    mediaTypes: {
        'img': 'Image',
        'video': 'Video',
        'videoSelfHosted': 'VideoSelfHosted'
    },
    elementAttributes: {
        'aspectRatio': 'aspectratio',
        'mediaType': 'mediatype',
        'storedUrl': 'storedurl',
        'imageId': 'imageid',
        'videoAutoPlay': 'videoautoplay',
        'thumbnailUrl': 'thumbnailurl'
    },
    localization: {
        mainTabName: 'Add image or video',
        localImgProcessorHeader: 'Upload image',
        localImgProcessorHeaderMobile: 'Upload',
        dropZoneText: 'Drag and drop image or click for upload',
        pixabayProcessorHeader: 'Find free images',
        pixabayProcessorHeaderMobile: 'Find free images',
        pixabayInfo: 'Over 750,000 free stock photo, vectors and art illustrations. <br/> All images and videos on Pixabay are released free of copyrights under Creative Commons CCO.  <br/> You may download, modify, distribute, and use them royalty free for anything you like, even in <br/> commercial applications. Attribution is not required.',
        pixabaySearchPlaceholder: 'search text',
        pixabaySearchFoundImages: 'Found images',
        pixabaySearchBtnNextPage: 'Next page',
        pixabaySearchBtnPrevPage: 'Back',
        youtubeHeader: 'Youtube video',
        youtubeHeaderMobile: 'Youtube video',
        youtubeSearchPlaceholder: 'youtube link',
        amsVideoProcessorHeader: 'Upload video',
        amsVideoProcessorHeaderMobile: 'Upload',
        amsVideoProcessorDropZoneText: 'Drag and drop file or click for upload',
        amsVideoProcessorUploadingError: 'Error',
        btnSave: 'Save',
        btnCancel: 'Cancel',
        btnBackToSearch: 'Back to search',
        btnBrowse: 'Browse',
        dropzoneMaxFileSizeError: 'max file size',
        dropzoneMaxFileTypeError: 'wrong file type',
        youtubeWrongUrlError: 'wrong link',
        youtubeAutoPlayLabel: 'Auto Play'
    },
    error: {
        getView: function (uniqueClass) {
            var errorView = '';
            errorView += '<div class="row mediaSelectorError hidden ' + uniqueClass + 'Error">';
            errorView += '<div class="col-md-12 col-xs-12 col-sm-12 errorText">'
            errorView += '</div>';
            errorView += '</div>'
            return errorView;
        },
        show: function (context, uniqueSelector, error) {
            var errorContainer = $(context).find(uniqueSelector + "Error");
            $(errorContainer).find(".errorText").html(error);
            $(errorContainer).removeClass("hidden");
        },
        hide: function (context, uniqueSelector) {
            $(context).find(uniqueSelector + "Error").addClass("hidden");
        }
    },
    helpers: {
        getUrlParam: function (url, paramName) {
            var results = new RegExp('[\?&]' + paramName + '=([^&#]*)').exec(url);
            if (results == null) {
                return null;
            }
            else {
                return decodeURI(results[1]) || 0;
            }
        }
    }
};

mediaSelectorCommon.messages = {
    showLoader: {
        data: {},
        event: mediaSelectorCommon.events.showLoader
    },
    hideLoader: {
        data: {},
        event: mediaSelectorCommon.events.hideLoader
    },
    fileAdded: {
        data: { file: {}, dropzone: {} },
        event: mediaSelectorCommon.events.fileAdded
    },
    dropZoneFileUploaded: {
        data: { response: {}, dropzone: {} },
        event: mediaSelectorCommon.events.dropZoneFileUploaded
    },
    dropZoneComplete: {
        data: { data: {}, dropzone: {} },
        event: mediaSelectorCommon.events.dropZoneComplete
    },
    closeModal: {
        data: {},
        event: mediaSelectorCommon.events.closeModal
    },
    save: {
        data: {
            mediaType: '',
            url: ''
        },
        event: mediaSelectorCommon.events.save
    },
    updateElementAttributes: {
        data: {},
        event: mediaSelectorCommon.events.updateElementAttributes
    },
    setActiveTab: {
        data: { "linkedTabId": "" },
        event: mediaSelectorCommon.events.setActiveTab
    }
};