﻿var mediaSelectorImgCompressor = {
    options: {
        /**
       * If set, images will be resized to these dimensions before being **uploaded**.
       * If only one, `resizeWidth` **or** `resizeHeight` is provided, the original aspect
       * ratio of the file will be preserved.
       *
       * The `options.transformFile` function uses these options, so if the `transformFile` function
       * is overridden, these options don't do anything.
       */
        resizeWidth: 750,

        /**
         * See `resizeWidth`.
         */
        resizeHeight: null,

        /**
         * The mime type of the resized image (before it gets uploaded to the server).
         * If `null` the original mime type will be used. To force jpeg, for example, use `image/jpeg`.
         * See `resizeWidth` for more information.
         */
        resizeMimeType: null,

        /**
         * The quality of the resized images. See `resizeWidth`.
         */
        resizeQuality: 0.9,

        /**
         * How the images should be scaled down in case both, `resizeWidth` and `resizeHeight` are provided.
         * Can be either `contain` or `crop`.
         */
        resizeMethod: 'contain',
    },

    /**
       * Gets called to calculate the thumbnail dimensions.
       *
       * It gets `file`, `width` and `height` (both may be `null`) as parameters and must return an object containing:
       *
       *  - `srcWidth` & `srcHeight` (required)
       *  - `trgWidth` & `trgHeight` (required)
       *  - `srcX` & `srcY` (optional, default `0`)
       *  - `trgX` & `trgY` (optional, default `0`)
       *
       * Those values are going to be used by `ctx.drawImage()`.
       */
    resize:function(file, width, height, resizeMethod) {
        var info = {
            srcX: 0,
            srcY: 0,
            srcWidth: file.width,
            srcHeight: file.height
        };

        var srcRatio = file.width / file.height;

        // Automatically calculate dimensions if not specified
        if ((width == null) && (height == null)) {
            width = info.srcWidth;
            height = info.srcHeight;
        } else if ((width == null)) {
            width = height * srcRatio;
        } else if ((height == null)) {
            height = width / srcRatio;
        }

        // Make sure images aren't upscaled
        width = Math.min(width, info.srcWidth);
        height = Math.min(height, info.srcHeight);

        var trgRatio = width / height;

        if ((info.srcWidth > width) || (info.srcHeight > height)) {
            // Image is bigger and needs rescaling
            if (resizeMethod === 'crop') {
                if (srcRatio > trgRatio) {
                    info.srcHeight = file.height;
                    info.srcWidth = info.srcHeight * trgRatio;
                } else {
                    info.srcWidth = file.width;
                    info.srcHeight = info.srcWidth / trgRatio;
                }
            } else if (resizeMethod === 'contain') {
                // Method 'contain'
                if (srcRatio > trgRatio) {
                    height = width / srcRatio;
                } else {
                    width = height * srcRatio;
                }
            } else {
                throw new Error('Unknown resizeMethod '+resizeMethod);
            }
        }

        info.srcX = (file.width - info.srcWidth) / 2;
        info.srcY = (file.height - info.srcHeight) / 2;

        info.trgWidth = width;
        info.trgHeight = height;

        return info;
    },
    /**
       * Can be used to transform the file (for example, resize an image if necessary).
       *
       * The default implementation uses `resizeWidth` and `resizeHeight` (if provided) and resizes
       * images according to those dimensions.
       *
       * Gets the `file` as the first parameter, and a `done()` function as the second, that needs
       * to be invoked with the file when the transformation is done.
       */
    transformFile:function(file, done) {
        if ((mediaSelectorImgCompressor.options.resizeWidth || mediaSelectorImgCompressor.options.resizeHeight) && file.type.match(/image.*/)) {
            return mediaSelectorImgCompressor.resizeImage(file, mediaSelectorImgCompressor.options.resizeWidth, mediaSelectorImgCompressor.options.resizeHeight, mediaSelectorImgCompressor.options.resizeMethod, done);
        } else {
            return done(file);
        }
    },

    // Resizes an image before it gets sent to the server. This function is the default behavior of
    // `options.transformFile` if `resizeWidth` or `resizeHeight` are set. The callback is invoked with
    // the resized blob.
    resizeImage:function(file, width, height, resizeMethod, callback) {
        return mediaSelectorImgCompressor.createThumbnail(file, width, height, resizeMethod, false, function(dataUrl, canvas) {
            if (canvas === null) {
                // The image has not been resized
                return callback(file);
            } else {
                var resizeMimeType = mediaSelectorImgCompressor.options.resizeMimeType;
                if (resizeMimeType == null) {
                    resizeMimeType = file.type;
                }
                var resizedDataURL = canvas.toDataURL(resizeMimeType, mediaSelectorImgCompressor.options.resizeQuality);
                if ((resizeMimeType === 'image/jpeg') || (resizeMimeType === 'image/jpg')) {
                    // Now add the original EXIF information
                    resizedDataURL = ExifRestore.restore(file.dataURL, resizedDataURL);
                }
                return callback(resizedDataURL);
                //return callback(Dropzone.dataURItoBlob(resizedDataURL));
            }
        });
    },
    createThumbnail:function(file, width, height, resizeMethod, fixOrientation, callback) {
        var fileReader = new FileReader;

        fileReader.onload = function() {

            file.dataURL = fileReader.result;

            // Don't bother creating a thumbnail for SVG images since they're vector
            if (file.type === "image/svg+xml") {
                if (callback != null) {
                    callback(fileReader.result);
                }
                return;
            }

            return mediaSelectorImgCompressor.createThumbnailFromUrl(file, width, height, resizeMethod, fixOrientation, callback);
        };

        return fileReader.readAsDataURL(file);
    },
    createThumbnailFromUrl:function(file, width, height, resizeMethod, fixOrientation, callback, crossOrigin) {
        // Not using `new Image` here because of a bug in latest Chrome versions.
        // See https://github.com/enyo/dropzone/pull/226
        var img = document.createElement("img");

        if (crossOrigin) {
            img.crossOrigin = crossOrigin;
        }

        img.onload = function() {
            var loadExif = function (callback) { return callback(1) };
            if ((typeof EXIF !== 'undefined' && EXIF !== null) && fixOrientation) {
                loadExif = function (callback) {
                    EXIF.getData(img, function () {
                        return callback(EXIF.getTag(this, 'Orientation'));
                    })
                };
            }

            return loadExif( function(orientation) {
                file.width = img.width;
                file.height = img.height;

                var resizeInfo = mediaSelectorImgCompressor.resize(file, width, height, resizeMethod);

                var canvas = document.createElement("canvas");
                var ctx = canvas.getContext("2d");

                canvas.width = resizeInfo.trgWidth;
                canvas.height = resizeInfo.trgHeight;

                if (orientation > 4) {
                    canvas.width = resizeInfo.trgHeight;
                    canvas.height = resizeInfo.trgWidth;
                }

                switch (orientation) {
                    case 2:
                        // horizontal flip
                        ctx.translate(canvas.width, 0);
                        ctx.scale(-1, 1);
                        break;
                    case 3:
                        // 180° rotate left
                        ctx.translate(canvas.width, canvas.height);
                        ctx.rotate(Math.PI);
                        break;
                    case 4:
                        // vertical flip
                        ctx.translate(0, canvas.height);
                        ctx.scale(1, -1);
                        break;
                    case 5:
                        // vertical flip + 90 rotate right
                        ctx.rotate(0.5 * Math.PI);
                        ctx.scale(1, -1);
                        break;
                    case 6:
                        // 90° rotate right
                        ctx.rotate(0.5 * Math.PI);
                        ctx.translate(0, -canvas.height);
                        break;
                    case 7:
                        // horizontal flip + 90 rotate right
                        ctx.rotate(0.5 * Math.PI);
                        ctx.translate(canvas.width, -canvas.height);
                        ctx.scale(-1, 1);
                        break;
                    case 8:
                        // 90° rotate left
                        ctx.rotate(-0.5 * Math.PI);
                        ctx.translate(-canvas.width, 0);
                        break;
                }

                // This is a bugfix for iOS' scaling bug.
                drawImageIOSFix(ctx, img, resizeInfo.srcX != null ? resizeInfo.srcX : 0, resizeInfo.srcY != null ? resizeInfo.srcY : 0, resizeInfo.srcWidth, resizeInfo.srcHeight, resizeInfo.trgX != null ? resizeInfo.trgX : 0, resizeInfo.trgY != null ? resizeInfo.trgY : 0, resizeInfo.trgWidth, resizeInfo.trgHeight);

                var thumbnail = canvas.toDataURL("image/png");

                if (callback != null) {
                    return callback(thumbnail, canvas);
                }
            });
        };

        if (callback != null) {
            img.onerror = callback;
        }

        return img.src = file.dataURL;
    }
}

// Detecting vertical squash in loaded image.
// Fixes a bug which squash image vertically while drawing into canvas for some images.
// This is a bug in iOS6 devices. This function from https://github.com/stomita/ios-imagefile-megapixel
function detectVerticalSquash (img) {
    var iw = img.naturalWidth;
    var ih = img.naturalHeight;
    var canvas = document.createElement("canvas");
    canvas.width = 1;
    canvas.height = ih;
    var ctx = canvas.getContext("2d");
    ctx.drawImage(img, 0, 0);
    var data = ctx.getImageData(1, 0, 1, ih);


    // search image edge pixel position in case it is squashed vertically.
    var sy = 0;
    var ey = ih;
    var py = ih;
    while (py > sy) {
        var alpha = data[((py - 1) * 4) + 3];

        if (alpha === 0) {
            ey = py;
        } else {
            sy = py;
        }

        py = (ey + sy) >> 1;
    }
    var ratio = (py / ih);

    if (ratio === 0) {
        return 1;
    } else {
        return ratio;
    }
};


// A replacement for context.drawImage
// (args are for source and destination).
var drawImageIOSFix = function (ctx, img, sx, sy, sw, sh, dx, dy, dw, dh) {
    var vertSquashRatio = detectVerticalSquash(img);
    return ctx.drawImage(img, sx, sy, sw, sh, dx, dy, dw, dh / vertSquashRatio);
};


// Based on MinifyJpeg
// Source: http://www.perry.cz/files/ExifRestorer.js
// http://elicon.blog57.fc2.com/blog-entry-206.html
var ExifRestore =(function(ExifRestore){
    ExifRestore.initClass = function() {
        this.KEY_STR = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=';
    }

    ExifRestore.encode64 = function(input) {
        var output = '';
        var chr1 = undefined;
        var chr2 = undefined;
        var chr3 = '';
        var enc1 = undefined;
        var enc2 = undefined;
        var enc3 = undefined;
        var enc4 = '';
        var i = 0;
        while (true) {
            chr1 = input[i++];
            chr2 = input[i++];
            chr3 = input[i++];
            enc1 = chr1 >> 2;
            enc2 = ((chr1 & 3) << 4) | (chr2 >> 4);
            enc3 = ((chr2 & 15) << 2) | (chr3 >> 6);
            enc4 = chr3 & 63;
            if (isNaN(chr2)) {
                enc3 = (enc4 = 64);
            } else if (isNaN(chr3)) {
                enc4 = 64;
            }
            output = output + this.KEY_STR.charAt(enc1) + this.KEY_STR.charAt(enc2) + this.KEY_STR.charAt(enc3) + this.KEY_STR.charAt(enc4);
            chr1 = (chr2 = (chr3 = ''));
            enc1 = (enc2 = (enc3 = (enc4 = '')));
            if (!(i < input.length)) {
                break;
            }
        }
        return output;
    }

    ExifRestore.restore = function(origFileBase64, resizedFileBase64) {
        if (!origFileBase64.match('data:image/jpeg;base64,')) {
            return resizedFileBase64;
        }
        var rawImage = this.decode64(origFileBase64.replace('data:image/jpeg;base64,', ''));
        var segments = this.slice2Segments(rawImage);
        var image = this.exifManipulation(resizedFileBase64, segments);
        return 'data:image/jpeg;base64,'+this.encode64(image);
    }

    ExifRestore.exifManipulation= function (resizedFileBase64, segments) {
        var exifArray = this.getExifArray(segments);
        var newImageArray = this.insertExif(resizedFileBase64, exifArray);
        var aBuffer = new Uint8Array(newImageArray);
        return aBuffer;
    }

    ExifRestore.getExifArray= function (segments) {
        var seg = undefined;
        var x = 0;
        while (x < segments.length) {
            seg = segments[x];
            if ((seg[0] === 255) & (seg[1] === 225)) {
                return seg;
            }
            x++;
        }
        return [];
    }

    ExifRestore.insertExif= function (resizedFileBase64, exifArray) {
        var imageData = resizedFileBase64.replace('data:image/jpeg;base64,', '');
        var buf = this.decode64(imageData);
        var separatePoint = buf.indexOf(255, 3);
        var mae = buf.slice(0, separatePoint);
        var ato = buf.slice(separatePoint);
        var array = mae;
        array = array.concat(exifArray);
        array = array.concat(ato);
        return array;
    }

    ExifRestore.slice2Segments= function (rawImageArray) {
        var head = 0;
        var segments = [];
        while (true) {
            var length;
            if ((rawImageArray[head] === 255) & (rawImageArray[head + 1] === 218)) {
                break;
            }
            if ((rawImageArray[head] === 255) & (rawImageArray[head + 1] === 216)) {
                head += 2;
            } else {
                length = (rawImageArray[head + 2] * 256) + rawImageArray[head + 3];
                var endPoint = head + length + 2;
                var seg = rawImageArray.slice(head, endPoint);
                segments.push(seg);
                head = endPoint;
            }
            if (head > rawImageArray.length) {
                break;
            }
        }
        return segments;
    }

    ExifRestore.decode64= function (input) {
        var output = '';
        var chr1 = undefined;
        var chr2 = undefined;
        var chr3 = '';
        var enc1 = undefined;
        var enc2 = undefined;
        var enc3 = undefined;
        var enc4 = '';
        var i = 0;
        var buf = [];
        // remove all characters that are not A-Z, a-z, 0-9, +, /, or =
        var base64test = /[^A-Za-z0-9\+\/\=]/g;
        if (base64test.exec(input)) {
            console.warn('There were invalid base64 characters in the input text.\nValid base64 characters are A-Z, a-z, 0-9, \'+\', \'/\',and \'=\'\nExpect errors in decoding.');
        }
        input = input.replace(/[^A-Za-z0-9\+\/\=]/g, '');
        while (true) {
            enc1 = this.KEY_STR.indexOf(input.charAt(i++));
            enc2 = this.KEY_STR.indexOf(input.charAt(i++));
            enc3 = this.KEY_STR.indexOf(input.charAt(i++));
            enc4 = this.KEY_STR.indexOf(input.charAt(i++));
            chr1 = (enc1 << 2) | (enc2 >> 4);
            chr2 = ((enc2 & 15) << 4) | (enc3 >> 2);
            chr3 = ((enc3 & 3) << 6) | enc4;
            buf.push(chr1);
            if (enc3 !== 64) {
                buf.push(chr2);
            }
            if (enc4 !== 64) {
                buf.push(chr3);
            }
            chr1 = (chr2 = (chr3 = ''));
            enc1 = (enc2 = (enc3 = (enc4 = '')));
            if (!(i < input.length)) {
                break;
            }
        }
        return buf;
    }

    return ExifRestore;
}(ExifRestore || {})
);
ExifRestore.initClass();


Dropzone.dataURItoBlob = function (dataURI) {
    // convert base64 to raw binary data held in a string
    // doesn't handle URLEncoded DataURIs - see SO answer #6850276 for code that does this
    var byteString = atob(dataURI.split(',')[1]);

    // separate out the mime component
    var mimeString = dataURI.split(',')[0].split(':')[1].split(';')[0];

    // write the bytes of the string to an ArrayBuffer
    var ab = new ArrayBuffer(byteString.length);
    var ia = new Uint8Array(ab);
    for (var i = 0, end = byteString.length, asc = 0 <= end; asc ? i <= end : i >= end; asc ? i++ : i--) {
        ia[i] = byteString.charCodeAt(i);
    }

    // write the ArrayBuffer to a blob
    return new Blob([ab], {type: mimeString});
};
