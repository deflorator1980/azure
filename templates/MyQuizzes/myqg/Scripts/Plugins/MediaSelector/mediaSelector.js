﻿mediaSelector = {
    getNew:function(){
        var uniquePart = GlobalUtils.uuidv4();
        var mediaSelectorInstanse = {
            caller: null,
            baseSettings: {
                modalSelector: '.mediaSelectorModal_' + uniquePart,
                modalClass: 'mediaSelectorModal_' + uniquePart,
                commonModalClass: 'mediaSelectorModal',
                selectedTabClass: 'selectedTab',
                hiddenClass: 'hidden',
                closeModalClass: 'modal__closeCross',
                modalTabsHeaderClass: 'modal__body__header',
                modalTabsHeaderSelector: '.modal__body__header',
                preventBodyScrollClass: 'preventBodyScroll'
            },
            init: function (settings) {
                mediaSelectorInstanse.settings = settings;
                if ($(mediaSelectorInstanse.baseSettings.modalSelector).length) { return; }
                var modal = mediaSelectorInstanse.getModal(mediaSelectorInstanse.getMediaSelectorTabs());

                $(body).append(modal);
                mediaSelectorInstanse.initModalButtons();
                mediaSelectorInstanse.initTabs();
                mediaSelectorInstanse.addProcessorListeners();
            },
            getModal: function (tabData) {
                var result = '';
                result += '<div class="modal ' + mediaSelectorInstanse.baseSettings.modalClass + ' ' + mediaSelectorInstanse.baseSettings.hiddenClass + ' ' + mediaSelectorInstanse.baseSettings.commonModalClass + '">';
                result += '<div class="modal__table">';
                result += '<div class="modal__tableRow">';
                result += '<div class="modal__tableCell">';
                result += '<div class="modal__header">';
                result += '<span>'+mediaSelectorCommon.localization.mainTabName+'</span>';
                result += '<span class="' + mediaSelectorInstanse.baseSettings.closeModalClass + '"></span>';
                result += '</div>';
                result += '<div class="modal__body ' + mediaSelectorInstanse.baseSettings.modalTabsHeaderClass + ' row">';
                result += tabData.headers;
                result += '</div>';
                result += tabData.tabs;
                result += tabData.footer;
                result += '</div>';
                result += '</div>';
                result += '</div>';
                result += '</div>';
                return result;
            },
            getMediaSelectorTabs: function () {
                var headers = '', tabs = '', footers = '', isFirstTab = true;
                var initMediaSelector = function (index, item) {
                    var w = 12 / mediaSelectorInstanse.settings.mediaProcessors.length;
                    if (item.getHeader && item.getView) {
                        var header = '<div class="col-md-'+w+' col-sm-'+w+' col-xs-'+w+' ' + (isFirstTab ? mediaSelectorInstanse.baseSettings.selectedTabClass : '') + '" linked-tab-id="' + item.baseSettings.uniqueTabId + '">';
                        header += '<div class="hidden-sm hidden-xs">'+item.getHeader().full+'</div>';
                        header += '<div class="hidden-md hidden-lg">'+item.getHeader().mobile+'</div>';
                        header += '</div>';
                        headers += header;

                        var tabData = '';
                        tabData += '<div class="modal__body ' + (isFirstTab ? '' : mediaSelectorInstanse.baseSettings.hiddenClass) + '" tab-id="' + item.baseSettings.uniqueTabId + '">';
                        tabData += item.getView();
                        tabData += '</div>';
                        tabs += tabData;

                        var footer = '';
                        footer += '<div class="modal__footer clearfix ' + (isFirstTab ? '' : mediaSelectorInstanse.baseSettings.hiddenClass) + '" tab-id="' + item.baseSettings.uniqueTabId + '">';
                        footer += item.getFooterView();
                        footer += '</div>';
                        footers += footer;

                        isFirstTab = false;
                    };
                }
                $.each(mediaSelectorInstanse.settings.mediaProcessors, function (index, item) {
                    initMediaSelector(index, item);
                });
                return {
                    'headers': headers,
                    'tabs': tabs,
                    'footer': footers
                };
            },
            initModalButtons: function () {
                $(mediaSelectorInstanse.baseSettings.modalSelector).find("." + mediaSelectorInstanse.baseSettings.closeModalClass).click(function () {
                    mediaSelectorInstanse.closeModal();
                });
            },
            closeModal: function () {
                $(mediaSelectorInstanse.baseSettings.modalSelector).addClass(mediaSelectorInstanse.baseSettings.hiddenClass);
                $("body, html").removeClass(mediaSelectorInstanse.baseSettings.preventBodyScrollClass);
                mediaSelectorInstanse.setActiveTab($(mediaSelectorInstanse.baseSettings.modalSelector).find("[linked-tab-id]").first());
                mediaSelectorInstanse.broadcastMsgToProcessors(mediaSelectorCommon.messages.closeModal);
            },
            bind: function (selector) {
                $.each($(selector), function (index, item) {
                    new Dropzone(item, {
                        url: 'test',
                        clickable: false
                    }).on("addedfile", function (file) {
                        mediaSelectorInstanse.caller = item;
                        $(mediaSelectorInstanse.baseSettings.modalSelector).removeClass(mediaSelectorInstanse.baseSettings.hiddenClass);
                        $("body, html").addClass(mediaSelectorInstanse.baseSettings.preventBodyScrollClass);
                        mediaSelectorInstanse.broadcastMsgToProcessors({
                            data: { 'file': file, 'dropzone': this },
                            event: mediaSelectorCommon.events.fileAdded
                        });
                    }).on("success", function (response) {
                        mediaSelectorInstanse.broadcastMsgToProcessors({
                            data: { 'response': response, 'dropzone': this },
                            event: mediaSelectorCommon.events.dropZoneFileUploaded
                        });
                    }).on("complete", function (data) {
                        mediaSelectorInstanse.broadcastMsgToProcessors({
                            data: { 'response': data, 'dropzone': this },
                            event: mediaSelectorCommon.events.dropZoneComplete
                        });
                    });
                });

                $(selector).on('click', function () {
                    var msg = mediaSelectorCommon.messages.updateElementAttributes;
                    msg.data = mediaSelectorInstanse.getElementAttributesAndValues(this);
                    mediaSelectorInstanse.broadcastMsgToProcessors(msg);
                    mediaSelectorInstanse.caller = this;
                    $(mediaSelectorInstanse.baseSettings.modalSelector).removeClass(mediaSelectorInstanse.baseSettings.hiddenClass);
                    $("body, html").addClass(mediaSelectorInstanse.baseSettings.preventBodyScrollClass);
                });
            },
            getElementAttributesAndValues:function(element){
                var result = [];
                $(element).each(function() {
                    $.each(element.attributes, function() {
                        result[this.name]=this.value;
                    });
                });
                return result;
            },
            settings: {
                mediaProcessors: [],
                onSave: function () { }
            },
            initTabs: function () {
                $.each(mediaSelectorInstanse.settings.mediaProcessors, function (index, item) {
                    item.init($(mediaSelectorInstanse.baseSettings.modalSelector));
                });

                $(mediaSelectorInstanse.baseSettings.modalSelector).find(mediaSelectorInstanse.baseSettings.modalTabsHeaderSelector + " div").on("click", function () {
                    mediaSelectorInstanse.setActiveTab(this);
                });
            },
            setActiveTab:function(tabSelector){
                $(mediaSelectorInstanse.baseSettings.modalSelector).find("div[tab-id]").addClass(mediaSelectorInstanse.baseSettings.hiddenClass);
                $(mediaSelectorInstanse.baseSettings.modalSelector).find(mediaSelectorInstanse.baseSettings.modalTabsHeaderSelector + " div").removeClass(mediaSelectorInstanse.baseSettings.selectedTabClass);

                var linkedTabId = $(tabSelector).attr("linked-tab-id");
                $(mediaSelectorInstanse.baseSettings.modalSelector).find("div[tab-id=" + linkedTabId + "]").removeClass(mediaSelectorInstanse.baseSettings.hiddenClass);
                $(tabSelector).addClass(mediaSelectorInstanse.baseSettings.selectedTabClass);
            },
            showLoader: function () {
                spinnerModule.Show();
            },
            hideLoader: function () {
                spinnerModule.Hide();
            },
            addProcessorListeners: function () {
                $.each(mediaSelectorInstanse.settings.mediaProcessors, function (index, item) {
                    item.settings.listeners.push(function (msg) { mediaSelectorInstanse.msgHandler(msg, mediaSelectorInstanse); });
                });
            },
            msgHandler: function (msg, currentInstance) {
                if (msg.event == mediaSelectorCommon.events.showLoader) {
                    currentInstance.showLoader();
                    return;
                }
                if (msg.event == mediaSelectorCommon.events.hideLoader) {
                    currentInstance.hideLoader();
                    return;
                }
                if (msg.event == mediaSelectorCommon.events.closeModal) {
                    currentInstance.closeModal();
                    return;
                }
                if (msg.event == mediaSelectorCommon.events.save) {
                    currentInstance.closeModal();
                    currentInstance.settings.onSave(mediaSelectorInstanse.caller, msg.data);
                    return;
                }
                if (msg.event == mediaSelectorCommon.events.setActiveTab)
                {
                    currentInstance.setActiveTab($(mediaSelectorInstanse.baseSettings.modalSelector).find("div[linked-tab-id='" + msg.data.linkedTabId + "']"));
                    return;
                }
            },
            broadcastMsgToProcessors: function (msg) {
                $.each(mediaSelectorInstanse.settings.mediaProcessors, function (index, item) {
                    item.msgHandler(msg);
                });
            }
        };
        return mediaSelectorInstanse;
    }
    
}









