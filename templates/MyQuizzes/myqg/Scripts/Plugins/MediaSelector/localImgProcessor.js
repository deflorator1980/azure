﻿localImgProcessor = {
    getNew: function (settings) {
        var uniquePart = GlobalUtils.uuidv4();
        var currentInstance = {
            context: {},
            elementAttributes: [],
            proccessorStates: {
                'initial': 'initial',
                'cropper': 'cropper'
            },
            currentProccessorState: 'initial',
            changeState: function (newState) {
                if (newState == currentInstance.currentProccessorState) { return; };
                currentInstance.currentProccessorState = newState;
                if (newState == currentInstance.proccessorStates.cropper) {
                    currentInstance.cropper.showCropper(currentInstance.context);
                    currentInstance.hideDropzone();
                    currentInstance.showSaveBtn();
                    currentInstance.showChangeDropzoneBtn();
                }
                if (newState == currentInstance.proccessorStates.initial) {
                    currentInstance.cropper.hideCropper(currentInstance.context);
                    currentInstance.showDropzone();
                    currentInstance.hideChangeDropzoneBtn();
                    currentInstance.hideSaveBtn();
                }
            },

            broadcastMsg: function (msg) {
                $.each(currentInstance.settings.listeners, function (index, item) {
                    item(msg);
                });
            },
            msgHandler: function (msg) {
                if (msg.event == mediaSelectorCommon.events.fileAdded) {
                    currentInstance.onDropZoneFileAdded(msg.data.file, msg.data.dropzone);
                    return true;
                };
                if (msg.event == mediaSelectorCommon.events.dropZoneFileUploaded) {
                    currentInstance.onDropZoneFileUploaded(msg.data.response);
                    return true;
                };
                if (msg.event == mediaSelectorCommon.events.dropZoneComplete) {
                    currentInstance.onDropZoneCompleted(msg.data.data, msg.data.dropzone);
                    return true;
                };
                if (msg.event == mediaSelectorCommon.events.closeModal) {
                    mediaSelectorCommon.error.hide(currentInstance.context, currentInstance.baseSettings.uniqueTabIdSelector);
                    currentInstance.changeState(currentInstance.proccessorStates.initial);
                    return true;
                };
                if (msg.event == mediaSelectorCommon.events.updateElementAttributes) {
                    currentInstance.elementAttributes = msg.data;
                    return true;
                }
            },

            baseSettings: {
                uniqueModalClass: 'mediaSelectorLocalImagesLoader',
                uniqueTabId: 'mediaSelectorLocalImagesLoaderTab',
                uniqueTabIdSelector: '.mediaSelectorLocalImagesLoaderTab',
                dropZoneClass: 'imgDropzone',
                dropZoneSelector: '.imgDropzone',
                dropZoneUniqueClass: 'imgDropzone_' + uniquePart,
                dropZoneUniqueSelector: '.imgDropzone_' + uniquePart,
                footerClass: 'localImgFooter',
                footerSelector: '.localImgFooter',
                hiddenClass: 'hidden',
                btnSelectors: {
                    newImgBtnClass: 'btn-newImg',
                    newImgBtnSelector: '.btn-newImg',
                    saveBtnClass: 'btn-save',
                    saveBtnSelector: '.btn-save',
                    cancelClass: 'btn-cancel',
                    cancelSelector: '.btn-cancel'
                }
            },
            settings: {
                dropzoneImgUrl: '/images/imgForDropzone.png',
                dropzoneSettings: {
                    method: "POST",
                    url: "/Image/Upload",
                    thumbnailWidth: null,
                    thumbnailHeight: null,
                    acceptedFiles: '.png,.jpg,.jpeg',
                    maxFileSize: 41943040,
                    maxFiles: 1,
                    rootUrl: '/Content/Uploads/',
                    autoProcessQueue: false,
                },
                listeners: []
            },

            getHeader: function () {
                return {
                    full: mediaSelectorCommon.localization.localImgProcessorHeader,
                    mobile: mediaSelectorCommon.localization.localImgProcessorHeaderMobile
                };
            },
            getView: function () {
                var result = '';
                result += '<div class="row ' + currentInstance.baseSettings.dropZoneClass + ' ' + currentInstance.baseSettings.dropZoneUniqueClass + '">';
                result += mediaSelectorCommon.error.getView(currentInstance.baseSettings.uniqueTabId);
                result += '<div class="col-md-12 col-sm-12 col-xs-12 dropzoneImgContainer">';
                result += '<img src="' + currentInstance.settings.dropzoneImgUrl + '" />';
                result += '<div class="row dropzoneMsg">';
                result += '<div class="col-md-12, col-sm-12 col-xs-12">';
                result += mediaSelectorCommon.localization.dropZoneText;
                result += '</div>';
                result += '</div>';
                result += '</div>';
                result += '</div>';
                result += currentInstance.cropper.getView();
                return result;
            },
            getFooterView: function () {
                var result = '<div class="croppQuizModal__buttonPlaceholder ' + currentInstance.baseSettings.footerClass + '">';
                result += '<a class="btn btn--cropp-submit ' + currentInstance.baseSettings.hiddenClass + ' ' + currentInstance.baseSettings.btnSelectors.newImgBtnClass + '">' + mediaSelectorCommon.localization.btnBrowse + '</a>';
                result += '<a class="btn btn--cropp-submit ' + currentInstance.baseSettings.hiddenClass + ' ' + currentInstance.baseSettings.btnSelectors.saveBtnClass + '">' + mediaSelectorCommon.localization.btnSave + '</a>';
                result += '<a class="btn btn--cropp-cancel ' + currentInstance.baseSettings.btnSelectors.cancelClass + '">' + mediaSelectorCommon.localization.btnCancel + '</a>';
                result += '</div>';
                return result;
            },

            init: function (context) {
                currentInstance.context = context;
                currentInstance.initDropZone(context);
                currentInstance.initBtns(context);
            },
            initDropZone: function (context) {
                currentInstance.settings.dropzoneSettings.init = function () {
                    var dropzoneInstance = this;
                    this.on("addedfile", function (file) {
                        currentInstance.onDropZoneFileAdded(file, dropzoneInstance);
                    });
                    this.on("success", function (response) { currentInstance.onDropZoneFileUploaded(response, dropzoneInstance); });
                    this.on("complete", function (data) { currentInstance.onDropZoneCompleted(data, dropzoneInstance); });
                };
                currentInstance.settings.dropzoneSettings.clickable = [currentInstance.baseSettings.dropZoneUniqueSelector, currentInstance.baseSettings.dropZoneUniqueSelector + ' .dropzoneImgContainer'];
                $(context).find(currentInstance.baseSettings.dropZoneSelector).dropzone(currentInstance.settings.dropzoneSettings);
            },
            initBtns: function (context) {
                $(context).find(currentInstance.baseSettings.footerSelector).find(currentInstance.baseSettings.btnSelectors.cancelSelector).click(function () {
                    currentInstance.changeState(currentInstance.proccessorStates.initial);
                    currentInstance.broadcastMsg(mediaSelectorCommon.messages.closeModal);
                });
                $(context).find(currentInstance.baseSettings.footerSelector).find(currentInstance.baseSettings.btnSelectors.newImgBtnSelector).click(function () {
                    $(currentInstance.context).find(currentInstance.baseSettings.dropZoneSelector).click();
                });
                $(context).find(currentInstance.baseSettings.footerSelector).find(currentInstance.baseSettings.btnSelectors.saveBtnSelector).click(function () {
                    mediaSelectorCommon.error.hide(currentInstance.context, currentInstance.baseSettings.uniqueTabIdSelector);
                    currentInstance.broadcastMsg(mediaSelectorCommon.messages.showLoader);
                    mediaSelectorService.uploadImgAndCrop(currentInstance.cropper,
                        function (result) {
                            currentInstance.changeState(currentInstance.proccessorStates.initial);
                            var saveMsg = mediaSelectorCommon.messages.save;
                            saveMsg.data = result;
                            saveMsg.data.mediaType = mediaSelectorCommon.mediaTypes.img;

                            currentInstance.broadcastMsg(saveMsg);
                            currentInstance.broadcastMsg(mediaSelectorCommon.messages.hideLoader);
                        },
                        function (data) {
                            var errorMsg = data.message ? data.message : JSON.stringify(data);
                            mediaSelectorCommon.error.show(currentInstance.context, currentInstance.baseSettings.uniqueTabIdSelector, errorMsg);
                            currentInstance.changeState(currentInstance.proccessorStates.initial);
                            currentInstance.broadcastMsg(mediaSelectorCommon.messages.hideLoader);
                        });
                });
            },

            onDropZoneFileAdded: function (dropZoneFile, dropzoneInstance) {
                var validationResult = currentInstance.validateFile(dropZoneFile);
                currentInstance.broadcastMsg(mediaSelectorCommon.messages.showLoader);
                if (!validationResult.isValid) {
                    dropzoneInstance.removeAllFiles();
                    //dropZoneFile.previewElement.remove();
                    currentInstance.broadcastMsg(mediaSelectorCommon.messages.hideLoader);
                    return;
                }

                $(dropZoneFile.previewElement).hide();

                mediaSelectorImgCompressor.transformFile(dropZoneFile, function (file) {
                    currentInstance.cropper.setImgSRC(currentInstance.context, '');
                    currentInstance.changeState(currentInstance.proccessorStates.cropper);

                    var cropperSettings = currentInstance.cropper.getCropperBaseSettings();
                    cropperSettings.aspectRatio = null;
                    currentInstance.cropper.setImgSRC(currentInstance.context, file, dropZoneFile.name);
                    currentInstance.cropper.init(currentInstance.context, cropperSettings);

                    dropzoneInstance.removeAllFiles();
                    console.log("transaction completed");
                    currentInstance.broadcastMsg(mediaSelectorCommon.messages.hideLoader);
                })

                    //dropzoneInstance.processQueue();
            },
            onDropZoneFileUploaded: function (response, dropzoneInstance) {
            },
            onDropZoneCompleted: function (data, dropzoneInstance) {
            },
            validateFile: function (file) {
                mediaSelectorCommon.error.hide(currentInstance.context, currentInstance.baseSettings.uniqueTabIdSelector);
                var result = { isValid: true };
                if (file.size > currentInstance.settings.dropzoneSettings.maxFileSize) {
                    currentInstance.changeState(currentInstance.proccessorStates.initial);
                    mediaSelectorCommon.error.show(currentInstance.context, currentInstance.baseSettings.uniqueTabIdSelector, mediaSelectorCommon.localization.dropzoneMaxFileSizeError);
                    return { isValid: false };
                };

                if (!Dropzone.isValidFile(file, currentInstance.settings.dropzoneSettings.acceptedFiles)) {
                    currentInstance.changeState(currentInstance.proccessorStates.initial);
                    mediaSelectorCommon.error.show(currentInstance.context, currentInstance.baseSettings.uniqueTabIdSelector, mediaSelectorCommon.localization.dropzoneMaxFileTypeError);
                    return { isValid: false };
                };
                return result;
            },

            cropper: mediaSelectorCropper.getNew('localImgCropper'),

            showDropzone: function () {
                $(currentInstance.context).find(currentInstance.baseSettings.dropZoneSelector).removeClass(currentInstance.baseSettings.hiddenClass);
            },
            hideDropzone: function () {
                $(currentInstance.context).find(currentInstance.baseSettings.dropZoneSelector).addClass(currentInstance.baseSettings.hiddenClass);
            },

            showChangeDropzoneBtn: function () {
                $(currentInstance.context).find(currentInstance.baseSettings.footerSelector).find(currentInstance.baseSettings.btnSelectors.newImgBtnSelector).removeClass(currentInstance.baseSettings.hiddenClass);
            },
            hideChangeDropzoneBtn: function () {
                $(currentInstance.context).find(currentInstance.baseSettings.footerSelector).find(currentInstance.baseSettings.btnSelectors.newImgBtnSelector).addClass(currentInstance.baseSettings.hiddenClass);
            },
            showSaveBtn: function () {
                $(currentInstance.context).find(currentInstance.baseSettings.footerSelector).find(currentInstance.baseSettings.btnSelectors.saveBtnSelector).removeClass(currentInstance.baseSettings.hiddenClass);
            },
            hideSaveBtn: function () {
                $(currentInstance.context).find(currentInstance.baseSettings.footerSelector).find(currentInstance.baseSettings.btnSelectors.saveBtnSelector).addClass(currentInstance.baseSettings.hiddenClass);
            },

        };
        if (settings) {
            currentInstance.settings = settings;
        }
        return currentInstance;
    }
}