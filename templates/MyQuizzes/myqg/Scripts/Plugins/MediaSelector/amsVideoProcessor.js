﻿amsVideoProcessor = {
    getNew: function(settings) {
        var uniquePart = GlobalUtils.uuidv4();
        var currentInstance = {
            context: {},
            elementAttributes: {},
            currentVideo: {},
            amPlayer: {},
            proccessorStates: {
                initial: 'initial',
                uploading: 'amsUploading',
                encoding: 'encoding',
                preview: 'amsPreview'
            },
            timerUpdate: {},
            currentProccessorState: 'initial',
            changeState: function (newState) {
                
                if (newState == currentInstance.currentProccessorState) { return; };

                currentInstance.currentProccessorState = newState;

                currentInstance.timerUpdateStop();

                if (newState == currentInstance.proccessorStates.initial) {
                    mediaSelectorCommon.error.hide(currentInstance.context, currentInstance.baseSettings.uniqueTabIdSelector);
                    
                    currentInstance.showInitialView();

                    currentInstance.stopAMPlayer();
                    currentInstance.currentVideo = {};

                    $(currentInstance.context).find(currentInstance.baseSettings.previewSelector).find(currentInstance.baseSettings.previewAutoPlaySelector).prop("checked", true);
                }
                
                if (newState == currentInstance.proccessorStates.uploading) {
                    currentInstance.showUploadingView();
                }

                if (newState == currentInstance.proccessorStates.encoding) {
                    currentInstance.timerUpdateStart(currentInstance.currentVideo.Id);
                    currentInstance.showPreviewView();
                }


                if (newState == currentInstance.proccessorStates.preview) {
                    currentInstance.setAMPlayer(currentInstance.currentVideo.ThumbnailUrl, currentInstance.currentVideo.URL);

                    currentInstance.showPreviewView();
                }
            },

            broadcastMsg: function (msg) {
                $.each(currentInstance.settings.listeners, function (index, item) {
                    item(msg);
                });
            },

            msgHandler: function (msg) {
                if (msg.event == mediaSelectorCommon.events.closeModal) {
                    mediaSelectorCommon.error.hide(currentInstance.context, currentInstance.baseSettings.uniqueTabIdSelector);
                    currentInstance.changeState(currentInstance.proccessorStates.initial);
                    return true;
                };
                if (msg.event == mediaSelectorCommon.events.updateElementAttributes) {
                    currentInstance.elementAttributes = msg.data;
                    currentInstance.restoreSelection();
                    return true;
                }

                return false;
            },
            timerUpdateStart: function (id) {
                currentInstance.timerUpdate = setInterval(function() {
                    mediaSelectorService.getVideoInfo(currentInstance.settings.getVideoInfoUrl,
                        id,
                        function(result) {
                            currentInstance.currentVideo = result[0];

                            if (currentInstance.currentVideo.URL) {
                                currentInstance.timerUpdateStop();

                                if (currentInstance.currentProccessorState == currentInstance.proccessorStates.encoding) {
                                    currentInstance.changeState(currentInstance.proccessorStates.preview);
                                }
                            };
                        },
                        function(data) {
                            console.log("amsVideoProcessor timerUpdate error ", data);
                        });
                }, 10000);
            },
            timerUpdateStop: function () {
                clearInterval(currentInstance.timerUpdate);
            },
            baseSettings: {
                uniqueModalClass: 'mediaSelectorAMSVideo',
                uniqueTabId: 'mediaSelectorAMSVideoTab',
                uniqueTabIdSelector: '.mediaSelectorAMSVideoTab',
                initialClass: 'mediaSelectorAMSVideoInitial',
                initialSelector: '.mediaSelectorAMSVideoInitial',
                uploadingClass: 'mediaSelectorAMSVideoUploading',
                uploadingSelector: '.mediaSelectorAMSVideoUploading',
                uploadingProgressClass: 'mediaSelectorAMSVideoUploadingProgress',
                uploadingProgressBarClass: 'mediaSelectorAMSVideoUploadingProgressBar',
                uploadingProgressBarSelector: '.mediaSelectorAMSVideoUploadingProgressBar',
                uploadingProgressBarFileNameClass: 'mediaSelectorAMSVideoUploadingProgressBarFileName',
                uploadingProgressBarFileNameSelector: '.mediaSelectorAMSVideoUploadingProgressBarFileName',
                uploadingProgressBarProgressLineClass: 'mediaSelectorAMSVideoUploadingProgressBarProgressLine',
                uploadingProgressBarProgressLineSelector: '.mediaSelectorAMSVideoUploadingProgressBarProgressLine',
                uploadingProgressBarBytesSentClass: 'mediaSelectorAMSVideoUploadingProgressBarBytesSent',
                uploadingProgressBarBytesSentSelector: '.mediaSelectorAMSVideoUploadingProgressBarBytesSent',
                uploadingProgressBarCancelClass: 'mediaSelectorAMSVideoUploadingProgressBarCancel',
                uploadingProgressBarCancelSelector: '.mediaSelectorAMSVideoUploadingProgressBarCancel',
                uploadingProgressBarCancelBtnClass: 'mediaSelectorAMSVideoUploadingProgressBarCancelBtn',
                uploadingProgressBarCancelBtnSelector: '.mediaSelectorAMSVideoUploadingProgressBarCancelBtn',
                uploadingSpinnerClass: 'spinnerFloatingBarsG',                
                uploadingMsgClass: 'mediaSelectorAMSVideoUploadingMsg',
                encodingProgressClass: 'mediaSelectorAMSVideoEncodingProgress',
                encodingProgressSelector: '.mediaSelectorAMSVideoEncodingProgress',
                encodingMsgClass: 'mediaSelectorAMSVideoEncodingMsg',
                previewClass: 'mediaSelectorAMSVideoPreview', 
                previewSelector: '.mediaSelectorAMSVideoPreview', 
                previewAttributesClass: 'mediaSelectorAMSVideoPreviewAttributes', 
                previewAutoPlayClass: 'mediaSelectorAMSVideoPreviewAutoPlay', 
                previewAutoPlaySelector: '.mediaSelectorAMSVideoPreviewAutoPlay', 
                ampViewClass: 'mediaSelectorAMSVideoAMPView',
                ampViewSelector: '.mediaSelectorAMSVideoAMPView',
                ampViewPanelBtnsClass: 'mediaSelectorAMSVideoAMPViewPanelBtns',
                ampViewPlayerClass: 'mediaSelectorAMSVideoAMPViewPlayer',
                ampViewPlayerSelector: '.mediaSelectorAMSVideoAMPViewPlayer',
                dropZoneClass: 'videoDropzone',
                dropZoneSelector: '.videoDropzone',
                dropZoneUniqueClass: 'videoDropzone_' + uniquePart,
                dropZoneUniqueSelector: '.videoDropzone_' + uniquePart,
                footerClass: 'amsVideoFooter',
                footerSelector: '.amsVideoFooter',
                hiddenClass: 'hidden',
                btnSelectors: {
                    saveBtnClass: 'btn-save',
                    saveBtnSelector: '.btn-save',
                    cancelClass: 'btn-cancel',
                    cancelSelector: '.btn-cancel',
                    changeBtnClass: 'btn-change',
                    changeBtnSelector: '.btn-change'
                }
            },
            settings: {
                language: 'end',
                //notSelectedVideoImgUrl: '/Images/imgForDropzoneVideo.png',
                //dropzoneVdeoUrl: '/images/imgForDropzoneVideo.png',
                //dropzoneSettings: {
                //    url: "http://localhost:49366/upload/video",
                //    method: "POST",
                //    thumbnailWidth: null,
                //    thumbnailHeight: null,
                //    acceptedFiles: 'video/mp4,video/x-msvideo,video/3gpp,video/mpeg,video/quicktime,video/x-flv', //todo
                //    maxFileSize: 104857600, //100MB
                //    maxFiles: 1,
                //    //rootUrl: '/Content/Uploads/',
                //    autoProcessQueue: true
                //},
                listeners: []
            },
            restoreSelection: function () {
                var attrMediaType = currentInstance.elementAttributes[mediaSelectorCommon.elementAttributes.mediaType];

                if (!attrMediaType || attrMediaType != mediaSelectorCommon.mediaTypes.videoSelfHosted) {
                    return;
                }

                currentInstance.currentVideo = {
                    Id : currentInstance.elementAttributes[mediaSelectorCommon.elementAttributes.imageId],
                    URL: currentInstance.elementAttributes[mediaSelectorCommon.elementAttributes.storedUrl],
                    MediaType: attrMediaType,
                    VideoAutoPlay: currentInstance.elementAttributes[mediaSelectorCommon.elementAttributes.videoAutoPlay],
                    ThumbnailUrl: currentInstance.elementAttributes[mediaSelectorCommon.elementAttributes.thumbnailUrl]
                }

                var autoplay = false;
                if (currentInstance.currentVideo.VideoAutoPlay) {
                    autoplay = currentInstance.currentVideo.VideoAutoPlay.toLowerCase() === "true";
                }

                $(currentInstance.context).find(currentInstance.baseSettings.previewSelector).find(currentInstance.baseSettings.previewAutoPlaySelector).prop("checked", autoplay);

                if (currentInstance.currentVideo.URL) {
                    currentInstance.changeState(currentInstance.proccessorStates.preview);
                } else {
                    currentInstance.changeState(currentInstance.proccessorStates.encoding);
                }

                currentInstance.broadcastMsg({
                    data: { "linkedTabId": currentInstance.baseSettings.uniqueTabId },
                    event: mediaSelectorCommon.events.setActiveTab
                });
            },

            getHeader: function () {
                return {
                    full: mediaSelectorCommon.localization.amsVideoProcessorHeader,
                    mobile: mediaSelectorCommon.localization.amsVideoProcessorHeaderMobile
                };
            },

            getView: function () {
                var result = '';
                result += '<div class="mediaSelectorAMSVideoBody">';
                result += currentInstance.getInitialView();
                result += currentInstance.getUploadingView();
                result += currentInstance.getPreviewView();
                result += '</div>';

                return result;
            },
            getFooterView: function () {
                var result = '<div class="croppQuizModal__buttonPlaceholder ' + currentInstance.baseSettings.footerClass + '">';
                result += '<a class="btn btn--cropp-submit ' + currentInstance.baseSettings.hiddenClass + ' ' + currentInstance.baseSettings.btnSelectors.saveBtnClass + '">' + mediaSelectorCommon.localization.btnSave + '</a>';
                result += '<a class="btn btn--cropp-cancel ' + currentInstance.baseSettings.btnSelectors.cancelClass + '">' + mediaSelectorCommon.localization.btnCancel + '</a>';
                result += '</div>';
                return result;
            },
            getInitialView: function() {
                var result = '';
                result += '<div class="' + currentInstance.baseSettings.initialClass + '">';
                result += '<div class="row ' + currentInstance.baseSettings.dropZoneClass + ' ' + currentInstance.baseSettings.dropZoneUniqueClass + '">';
                result += mediaSelectorCommon.error.getView(currentInstance.baseSettings.uniqueTabId);
                result += '<div class="col-md-12 col-sm-12 col-xs-12 dropzoneImgContainer">';
                result += '<img src="' + currentInstance.settings.dropzoneVdeoUrl + '" />';
                result += '<div class="row dropzoneMsg">';
                result += '<div class="col-md-12, col-sm-12 col-xs-12">';
                result += mediaSelectorCommon.localization.amsVideoProcessorDropZoneText;
                result += '</div>';
                result += '</div>';
                result += '</div>';
                result += '</div>';
                result += currentInstance.getDropzoneRequirement();
                result += '</div>';

                return result;
            },
            getDropzoneRequirement: function() {
                var result = '';
                result += '<div class="row videoDropzoneRequirements">';
                result += '<div class="col-md-12 col-sm-12 col-xs-12" >';
                result += '<ul>';
                result += ' <li>' + mediaSelectorCommon.localization.amsVideoProcessorSupportedFormatText +'</li>';
                result += ' <li>' + mediaSelectorCommon.localization.amsVideoProcessorMaxFileSizeText + '</li>';
                result += ' <li>' + mediaSelectorCommon.localization.amsVideoProcessorRecommendedVideoLengthText + '</li>';
                result += '</ul>';
                result += '</div>';
                result += '</div>';
                return result;
            },
            getUploadingView: function() {
                var result = '';
                result += '<div class="' + currentInstance.baseSettings.uploadingClass + '">';

                result += '<div class="row">';
                result += '<div class="col-md-12 col-sm-12 col-xs-12">';
                result += currentInstance.getUploadingProgressView();
                result += '</div>';
                result += '</div>';

                result += '<div class="row">';
                result += '<div class="col-md-12 col-sm-12 col-xs-12">';
                result += currentInstance.getUploadingProgressBarView();
                result += '</div>';
                result += '</div>';

                result += '</div>';

                return result;
            },
            getPreviewView: function() {
                var result = '';

                result += '<div class="' + currentInstance.baseSettings.previewClass + '">';

                result += '<div class="row">';
                result += '<div class="col-md-12 col-sm-12 col-xs-12">';
                result += currentInstance.getEncodingProgressView();
                result += '</div>';
                result += '</div>';

                result += '<div class="row">';
                result += '<div class="col-md-12 col-sm-12 col-xs-12">';
                result += currentInstance.getAMPView();
                result += '</div>';
                result += '</div>';

                result += '<div class="row ' + currentInstance.baseSettings.ampViewPanelBtnsClass + '">';
                result += '<div class="col-md-12 col-sm-12 col-xs-12">';
                result += '<a class="btn ' + currentInstance.baseSettings.btnSelectors.changeBtnClass + '">' + mediaSelectorCommon.localization.amsVideoProcessorChangeVideoBtnText + '</a>';
                result += '</div>';
                result += '</div>';

                result += '<div class="row">';
                result += '<div class="col-md-12 col-sm-12 col-xs-12 ' + currentInstance.baseSettings.previewAttributesClass + '">';
                result += '<input type="checkbox" id="' + currentInstance.baseSettings.previewAutoPlayClass + uniquePart + '" class="' + currentInstance.baseSettings.previewAutoPlayClass + '"/>';
                result += '<label for="' + currentInstance.baseSettings.previewAutoPlayClass + uniquePart + '">' + mediaSelectorCommon.localization.amsVideoProcessorAutoPlayLabel + '</label>';
                result += '</div>';
                result += '</div>';

                result += '</div>';

                return result;
            },

            getUploadingProgressView: function () {
                var result = '';
                result += '<div class="' + currentInstance.baseSettings.uploadingProgressClass + '">';

                result += '<div class="row">';
                result += '<div class="col-md-12 col-sm-12 col-xs-12">';
                result += currentInstance.getSpinnerView();
                result += '</div>';
                result += '</div>';

                result += '<div class="' + currentInstance.baseSettings.uploadingMsgClass + '">';
                result += '<div class="row ">';
                //result += '<div class="' + currentInstance.baseSettings.uploadingMsgClass +'">';
                result += '<div class="col-md-12 col-sm-12 col-xs-12">';
                result += mediaSelectorCommon.localization.amsVideoProcessorUploadingMsg;
                result += '</div>';
                result += '</div>';
                result += '</div>';

                result += '</div>';
                return result;

            },

            getEncodingProgressView: function () {
                var result = '';
                result += '<div class="' + currentInstance.baseSettings.encodingProgressClass + '">';

                result += '<div class="row">';
                result += '<div class="col-md-12 col-sm-12 col-xs-12">';
                result += currentInstance.getSpinnerView();
                result += '</div>';
                result += '</div>';

                result += '<div class="' + currentInstance.baseSettings.encodingMsgClass + '">';
                result += '<div class="row">';
                result += '<div class="col-md-12 col-sm-12 col-xs-12">';
                result += mediaSelectorCommon.localization.amsVideoProcessorEncodingMsg;
                result += '</div>';
                result += '</div>';
                result += '</div>';

                result += '</div>';
                return result;
            },

            getAMPView: function() {
                var result = '';
                result += '<div class="' + currentInstance.baseSettings.ampViewClass +'" >';

                result += '<div class="row">';
                result += '<div class="col-md-12 col-sm-12 col-xs-12">';
                result += '<div class="' + currentInstance.baseSettings.ampViewPlayerClass + '">';
                result += '<video class="azuremediaplayer amp-default-skin amp-big-play-centered"></video>';
                result += '</div>';
                result += '</div>';
                result += '</div>';

                result += '</div>';

                return result;
            },

            getSpinnerView: function () {
                var result = '';
                result += '<div class="' + currentInstance.baseSettings.uploadingSpinnerClass + '" >';
                result += '<div class="blockG" id="rotateG_01"></div>';
                result += '<div class="blockG" id="rotateG_02"></div>';
                result += '<div class="blockG" id="rotateG_03"></div>';
                result += '<div class="blockG" id="rotateG_04"></div>';
                result += '<div class="blockG" id="rotateG_05"></div>';
                result += '<div class="blockG" id="rotateG_06"></div>';
                result += '<div class="blockG" id="rotateG_07"></div>';
                result += '<div class="blockG" id="rotateG_08"></div>';
                result += '</div>';

                return result;
            },
            getUploadingProgressBarView: function() {
                var result = '';
                result += '<div class="' + currentInstance.baseSettings.uploadingProgressBarClass + '">';

                result += '<div class="row">';

                result += '<div class="col-md-11 col-sm-11 col-xs-11 vertical-center">';
                result += '<div class="' + currentInstance.baseSettings.uploadingProgressBarFileNameClass + '" ></div>';
                result += '<div class="' + currentInstance.baseSettings.uploadingProgressBarProgressLineClass + '" ></div>';
                result += '<div class="' + currentInstance.baseSettings.uploadingProgressBarBytesSentClass + '" ></div>';
                result += '</div>';

                result += '<div class="col-md-1 col-sm-1 col-xs-1 vertical-center ' + currentInstance.baseSettings.uploadingProgressBarCancelClass + '">';
                result += '<a class="' + currentInstance.baseSettings.uploadingProgressBarCancelBtnClass  +'" ><span>&times</span></a>';
                result += '</div>';

                result += '</div>';

                result += '</div>';

                return result;
            },
            init: function (context) {
                currentInstance.context = context;

                currentInstance.showInitialView();
                currentInstance.initDropZone(context);
                currentInstance.initBtns(context);
            },
            initDropZone: function (context) {
                currentInstance.settings.dropzoneSettings.init = function () {
                    var dropzoneInstance = this;
                    this.on("addedfile", function (file) {
                        currentInstance.onDropZoneFileAdded(file, dropzoneInstance);
                    });
                    this.on("success", function (response) { currentInstance.onDropZoneFileUploaded(response, dropzoneInstance); });
                    this.on("complete", function (data) { currentInstance.onDropZoneCompleted(data, dropzoneInstance); });
                    this.on("uploadprogress", function (file, progress, bytesSent) { currentInstance.onDropZoneUploadProgress(file, progress, bytesSent); });
                    this.on("error", function (file, response) { currentInstance.onDropZoneError(file, response); }); 
                };
                currentInstance.settings.dropzoneSettings.clickable = [currentInstance.baseSettings.dropZoneUniqueSelector, currentInstance.baseSettings.dropZoneUniqueSelector + ' .dropzoneImgContainer'];
                $(context).find(currentInstance.baseSettings.dropZoneSelector).dropzone(currentInstance.settings.dropzoneSettings);
            },
            initBtns: function (context) {
                $(context).find(currentInstance.baseSettings.footerSelector).find(currentInstance.baseSettings.btnSelectors.cancelSelector).click(function () {
                    currentInstance.changeState(currentInstance.proccessorStates.initial);
                    currentInstance.broadcastMsg(mediaSelectorCommon.messages.closeModal);
                });

                $(context).find(currentInstance.baseSettings.btnSelectors.changeBtnSelector).click(function() {
                    currentInstance.changeState(currentInstance.proccessorStates.initial);
                });

                $(context).find(currentInstance.baseSettings.footerSelector).find(currentInstance.baseSettings.btnSelectors.saveBtnSelector).click(function () {
                    mediaSelectorCommon.error.hide(context, currentInstance.baseSettings.uniqueTabIdSelector);
                    currentInstance.stopAMPlayer();
                    currentInstance.broadcastMsg(mediaSelectorCommon.messages.showLoader);
                    
                    var autoplay = $(currentInstance.context).find(currentInstance.baseSettings.previewSelector).find(currentInstance.baseSettings.previewAutoPlaySelector).prop("checked");
                    if (autoplay) {
                        currentInstance.currentVideo.VideoAutoPlay = true;
                    } else {
                        currentInstance.currentVideo.VideoAutoPlay = false;
                    }

                    mediaSelectorService.updateVideoSelfHosted(currentInstance.currentVideo.Id, currentInstance.currentVideo.VideoAutoPlay,
                        function (result) {
                            var saveMsg = mediaSelectorCommon.messages.save;
                            saveMsg.data = result;
                            saveMsg.data.mediaType = mediaSelectorCommon.mediaTypes.videoSelfHosted;

                            currentInstance.broadcastMsg(saveMsg);
                            currentInstance.broadcastMsg(mediaSelectorCommon.messages.hideLoader);
                            currentInstance.changeState(currentInstance.proccessorStates.initial);
                        },
                        function (data) {
                            mediaSelectorCommon.error.show(currentInstance.context, currentInstance.baseSettings.uniqueTabIdSelector, data);
                            currentInstance.broadcastMsg(mediaSelectorCommon.messages.hideLoader);
                        }, false);
                });
            },

            setAMPlayer: function (poster, url) {
                var elId = $(currentInstance.context).find('.azuremediaplayer');

                if (!poster && !url) {
                    return;
                }

                currentInstance.amPlayer = amp($(elId)[0],
                    {
                        techOrder: ["azureHtml5JS", "flashSS", "html5FairPlayHLS", "silverlightSS", "html5"],
                        inactivityTimeout: 2e3,
                        nativeControlsForTouch: false,
                        logo: { enabled: false },
                        language: currentInstance.settings.language,
                        autoplay: false,
                        controls: true,
                        fluid: true,
                        poster: poster
                    }, function () {
                        var self = this;
                        this.addEventListener('error', function() {
                                self.updateStyleEl_();
                            });
                    });

                currentInstance.amPlayer.src([
                    {
                        src: url,
                        type: "application/vnd.ms-sstr+xml"
                    }
                ]);
            },
            stopAMPlayer: function () {
                
                var el = (currentInstance.context)
                    .find(currentInstance.baseSettings.ampViewSelector)
                    .find(currentInstance.baseSettings.ampViewPlayerSelector);
                
                el.empty();
                el.append('<video class="azuremediaplayer amp-default-skin amp-big-play-centered"></video>');
            },

            onDropZoneFileAdded: function (dropZoneFile, dropzoneInstance) {
                var validationResult = currentInstance.validateFile(dropZoneFile);

                (currentInstance.context).find(currentInstance.baseSettings.uploadingProgressBarCancelBtnSelector)
                    .click(function(e) {
                        e.preventDefault();
                        dropzoneInstance.cancelUpload(dropZoneFile);
                        dropzoneInstance.removeAllFiles();
                        currentInstance.changeState(currentInstance.proccessorStates.initial);
                    });

                if (!validationResult.isValid) {
                    dropzoneInstance.removeAllFiles();
                    return;
                }
                
                currentInstance.changeState(currentInstance.proccessorStates.uploading);
            },
            onDropZoneUploadProgress: function (file, progress, bytesSent) {

                (currentInstance.context).find(currentInstance.baseSettings.uploadingProgressBarFileNameSelector).html(file.name);

                (currentInstance.context).find(currentInstance.baseSettings.uploadingProgressBarProgressLineSelector).width(progress + '%');

                (currentInstance.context).find(currentInstance.baseSettings.uploadingProgressBarBytesSentSelector).html(jQuery.validator.format(mediaSelectorCommon.localization.amsVideoProcessorUploadingBytesSent, Math.round(bytesSent/1024), Math.round(file.size/1024)));

            },
            onDropZoneFileUploaded: function (response, dropzoneInstance) {
                currentInstance.currentVideo = JSON.parse(response.xhr.response)[0];
                currentInstance.changeState(currentInstance.proccessorStates.encoding);
            },
            onDropZoneCompleted: function (data, dropzoneInstance) {
                dropzoneInstance.removeAllFiles();
            },
            onDropZoneError: function (file, response) {
                currentInstance.changeState(currentInstance.proccessorStates.initial);

                if (file.status != "canceled") {
                    mediaSelectorCommon.error.show(currentInstance.context,
                        currentInstance.baseSettings.uniqueTabIdSelector,
                        mediaSelectorCommon.localization.amsVideoProcessorUploadingError);
                }
            },

            validateFile: function (file) {
                mediaSelectorCommon.error.hide(currentInstance.context, currentInstance.baseSettings.uniqueTabIdSelector);
                var result = { isValid: true };
                if (file.size > currentInstance.settings.dropzoneSettings.maxFileSize) {
                    currentInstance.changeState(currentInstance.proccessorStates.initial);
                    mediaSelectorCommon.error.show(currentInstance.context, currentInstance.baseSettings.uniqueTabIdSelector, mediaSelectorCommon.localization.amsVideoProcessorDropZoneFileSizeError);
                    return { isValid: false };
                };
                
                if (!Dropzone.isValidFile(file, currentInstance.settings.dropzoneSettings.acceptedFiles)) {
                    currentInstance.changeState(currentInstance.proccessorStates.initial);
                    mediaSelectorCommon.error.show(currentInstance.context, currentInstance.baseSettings.uniqueTabIdSelector, mediaSelectorCommon.localization.dropzoneMaxFileTypeError);
                    return { isValid: false };
                };
                return result;
            },

            showInitialView: function() {
                currentInstance.showView(currentInstance.baseSettings.initialSelector);
                currentInstance.hideView(currentInstance.baseSettings.uploadingSelector);
                currentInstance.hideView(currentInstance.baseSettings.previewSelector);
                currentInstance.hideSaveBtn();
            },
            
            showUploadingView: function() {
                currentInstance.hideView(currentInstance.baseSettings.initialSelector);
                currentInstance.showView(currentInstance.baseSettings.uploadingSelector);
                currentInstance.hideView(currentInstance.baseSettings.previewSelector);
            },
            showPreviewView: function() {
                currentInstance.hideView(currentInstance.baseSettings.initialSelector);
                currentInstance.hideView(currentInstance.baseSettings.uploadingSelector);
                currentInstance.showView(currentInstance.baseSettings.previewSelector);
                currentInstance.showSaveBtn();

                // hide/show encoding/AMP
                if (currentInstance.currentVideo.URL) {
                    currentInstance.hideView(currentInstance.baseSettings.encodingProgressSelector);
                    currentInstance.showView(currentInstance.baseSettings.ampViewSelector);
                } else {
                    currentInstance.showView(currentInstance.baseSettings.encodingProgressSelector);
                    currentInstance.hideView(currentInstance.baseSettings.ampViewSelector);                    
                }

            },
            showView: function(selector) {
                $(currentInstance.context).find(selector).removeClass(currentInstance.baseSettings.hiddenClass);
            },
            hideView: function(selector) {
                $(currentInstance.context).find(selector).addClass(currentInstance.baseSettings.hiddenClass);
            },
            showSaveBtn: function () {
                $(currentInstance.context).find(currentInstance.baseSettings.footerSelector).find(currentInstance.baseSettings.btnSelectors.saveBtnSelector).removeClass(currentInstance.baseSettings.hiddenClass);
            },
            hideSaveBtn: function () {
                $(currentInstance.context).find(currentInstance.baseSettings.footerSelector).find(currentInstance.baseSettings.btnSelectors.saveBtnSelector).addClass(currentInstance.baseSettings.hiddenClass);
            }

        };

        if (settings) {
            currentInstance.settings = settings;
        }

        return currentInstance;
    }
}