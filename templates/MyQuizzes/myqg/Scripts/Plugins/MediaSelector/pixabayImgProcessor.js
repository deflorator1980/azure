﻿pixabayImgProcessor = {
    getNew: function () {
        var currentInstance = {
            context: {},
            elementAttributes: [],
            proccessorStates: {
                'initial': 'initial',
                'cropper': 'cropper',
                'backToSearch': 'backToSearch'
            },
            currentProccessorState: 'initial',
            changeState: function (newState) {
                currentInstance.currentProccessorState = newState;
                if (newState == currentInstance.proccessorStates.cropper) {
                    currentInstance.cropper.showCropper(currentInstance.context);
                    currentInstance.search.hide();
                    currentInstance.showBackToSearchBtn();
                    currentInstance.showSaveBtn();
                }
                if (newState == currentInstance.proccessorStates.backToSearch) {
                    currentInstance.cropper.hideCropper(currentInstance.context);
                    currentInstance.search.show();
                    currentInstance.hideSaveBtn();
                    currentInstance.hideBackToSearchBtn();
                }
                if (newState == currentInstance.proccessorStates.initial) {
                    currentInstance.search.cleanSearchBody();
                    currentInstance.search.cleanSearchText();
                    currentInstance.cropper.hideCropper(currentInstance.context);
                    currentInstance.search.show();
                    currentInstance.hideSaveBtn();
                    currentInstance.hideBackToSearchBtn();
                }
            },

            broadcastMsg: function (msg) {
                $.each(currentInstance.settings.listeners, function (index, item) {
                    item(msg);
                });
            },
            msgHandler: function (msg) {
                if (msg.event == mediaSelectorCommon.events.closeModal) {
                    currentInstance.changeState(currentInstance.proccessorStates.initial);
                    return true;
                };
                if (msg.event == mediaSelectorCommon.events.updateElementAttributes) {
                    currentInstance.elementAttributes = msg.data;
                    return true;
                }
            },

            baseSettings: {
                uniqueModalClass: 'mediaSelectorPixabayImages',
                uniqueTabId: 'mediaSelectorPixabayImagesTab',
                uniqueTabIdSelector: '.mediaSelectorPixabayImagesTab',
                searchClass: 'pixabaySearch',
                searchSelector: '.pixabaySearch',
                searchInputWrapperClass: 'pixabaySearchInputWrapper',
                searchResultClass: 'pixabaySearchResult',
                searchResultHeaderClass: 'pixabaySearchResultHeader',
                searchResultFooterClass: 'pixabaySearchResultFooter',
                searchResultImagesClass: 'pixabaySearchResultImages',
                searchResultSelector: '.pixabaySearchResult',
                pixabayInfoClass: 'pixabayInfo',
                footerClass: 'pixabayImgFooter',
                footerSelector: '.pixabayImgFooter',
                imgResultClass: 'pixabayImgResult',
                imgResultSelector: '.pixabayImgResult',
                hiddenClass: 'hidden',
                btnSelectors: {
                    searchBtnNextPageClass: 'btn-search-next-page',
                    searchBtnNextPageSelector: '.btn-search-next-page',
                    searchBtnPrevPageClass: 'btn-search-prev-page',
                    searchBtnPrevPageSelector: '.btn-search-prev-page',
                    searchBtnClass: 'btn-search',
                    searchBtnSelector: '.btn-search',
                    newImgBtnClass: 'btn-newImg',
                    newImgBtnSelector: '.btn-newImg',
                    saveBtnClass: 'btn-save',
                    saveBtnSelector: '.btn-save',
                    cancelClass: 'btn-cancel',
                    cancelSelector: '.btn-cancel',
                    backToSearchClass: 'backToSearch',
                    backToSearchSelector: '.backToSearch',
                },
                errors: {
                    fileUploadErrors: {
                        'maxFileSize': 'maxFileSize',
                        'type': 'type',
                        '': '',
                    }
                }
            },
            settings: {
                searchSettings: {
                    lastRequest: {},
                    searchURL: "/Image/Search",
                    pageSize: 9
                },
                listeners: []
            },

            search: {
                currentPage: 1,
                lastSearchText: null,
                init: function () {
                    $(currentInstance.context).find(currentInstance.baseSettings.searchSelector).find(currentInstance.baseSettings.btnSelectors.searchBtnSelector).on("click", function (event) {
                        event.preventDefault();
                        currentInstance.search.search();
                    });
                    $(currentInstance.context).find(currentInstance.baseSettings.searchSelector).find("input").on("keypress", function (event) {
                        /* ENTER PRESSED*/
                        if (event.keyCode == 13) {
                            event.preventDefault();
                            currentInstance.search.search();
                        }
                    });
                    $(currentInstance.context).find(currentInstance.baseSettings.searchSelector).find("input").on("change", function (event) {
                        event.preventDefault();
                        currentInstance.search.search();
                    });
                },
                search: function (page) {
                    var searchText = $(currentInstance.context).find(currentInstance.baseSettings.searchSelector).find("input").val();
                    if (!page || page <= 1) { page = 1; }
                    if (searchText == currentInstance.search.lastSearchText&&page==currentInstance.search.currentPage) {
                        return;
                    }

                    currentInstance.search.lastSearchText = searchText;
                    currentInstance.search.currentPage = page;

                    mediaSelectorCommon.error.hide(currentInstance.context, currentInstance.baseSettings.uniqueTabIdSelector);
                    currentInstance.broadcastMsg(mediaSelectorCommon.messages.showLoader);
                    
                    $.get(currentInstance.settings.searchSettings.searchURL, { "searchText": searchText, "page": page, "pageSize": currentInstance.settings.searchSettings.pageSize })
                        .done(function (result) {
                            if (!result.IsSuccess) { mediaSelectorCommon.error.show(currentInstance.context, currentInstance.baseSettings.uniqueTabIdSelector, result.Error); return; }
                            var searchResultBody = $(currentInstance.context).find(currentInstance.baseSettings.searchResultSelector);
                            var resultForMediumDevises = currentInstance.search.getSearchResultsView(result.Images, 'col-md-4 hidden-sm hidden-xs', 3);
                            var resultForSmallDevises = currentInstance.search.getSearchResultsView(result.Images, 'col-sm-6 col-xs-6 hidden-md hidden-lg', 2);

                            currentInstance.search.cleanSearchBody();
                            $(searchResultBody).append(currentInstance.search.getSearchResultsHeaderView(result.TotalImages));
                            $(searchResultBody).append(resultForMediumDevises).append(resultForSmallDevises);
                            $(searchResultBody).append(currentInstance.search.getSearchResultsFooterView(result.TotalPages));
                            currentInstance.search.initSearchResult();
                        }).fail(function (result) {
                            console.log(result);
                            mediaSelectorCommon.error.show(currentInstance.context, currentInstance.baseSettings.uniqueTabIdSelector, result.statusText);
                        }).complete(function () {
                            currentInstance.broadcastMsg(mediaSelectorCommon.messages.hideLoader);
                        });
                },
                initSearchResult: function () {
                    $(currentInstance.context).find(currentInstance.baseSettings.searchResultSelector).find(currentInstance.baseSettings.imgResultSelector).on('click', function () {
                        currentInstance.cropper.setImgSRC(currentInstance.context, '');
                        currentInstance.changeState(currentInstance.proccessorStates.cropper);

                        var aspectRatio = Controls.ASPECT_RATIO_QUIZ;
                        if (currentInstance.elementAttributes && currentInstance.elementAttributes[mediaSelectorCommon.elementAttributes.aspectRatio]) {
                            aspectRatio = eval(currentInstance.elementAttributes[mediaSelectorCommon.elementAttributes.aspectRatio]);
                        }

                        var cropperSettings = currentInstance.cropper.getCropperBaseSettings();
                        cropperSettings.aspectRatio = aspectRatio;

                        currentInstance.cropper.setImgSRC(currentInstance.context, $(this).attr("img-url"));
                        currentInstance.cropper.init(currentInstance.context, cropperSettings);
                    });

                    $(currentInstance.context).find(currentInstance.baseSettings.searchResultSelector).find(currentInstance.baseSettings.btnSelectors.searchBtnNextPageSelector).on('click', function () {
                        currentInstance.search.search(currentInstance.search.currentPage + 1);
                    });

                    $(currentInstance.context).find(currentInstance.baseSettings.searchResultSelector).find(currentInstance.baseSettings.btnSelectors.searchBtnPrevPageSelector).on('click', function () {
                        currentInstance.search.search(currentInstance.search.currentPage - 1);
                    });
                },
                getSearchResultsHeaderView: function (foundItems) {
                    var view = '';
                    view += '<div class="row ' + currentInstance.baseSettings.searchResultHeaderClass + '">';
                    view += '<div class="col-md-6 col-sm-6 col-xs-6">';
                    view += mediaSelectorCommon.localization.pixabaySearchFoundImages + ' ' + foundItems;
                    view += '</div>';
                    view += '<div class="col-md-6 col-sm-6 col-xs-6">';
                    view += '<a href="https://pixabay.com" target="_blank" alt="pixabay"> <img src="/Images/pixabay_logo.png"  alt="pixabay"> </a>';
                    view += '</div>';
                    view += '</div>';
                    return view;
                },
                getSearchResultsView: function (images, itemClass, itemsPerRow) {
                    var resultColumns = [];

                    for (var counter = 0; counter < itemsPerRow; counter++) {
                        resultColumns[counter] = '<div class="' + itemClass + '">';
                    }
                    var counter = 0;
                    $.each(images, function (index, item) {
                        resultColumns[counter] += '<img src="' + item.URL + '" class="' + currentInstance.baseSettings.imgResultClass + '" img-url="' + item.URL + '" />';
                        counter++;
                        if (counter >= resultColumns.length) {
                            counter = 0;
                        };
                    });

                    var resultView = '<div class="row ' + currentInstance.baseSettings.searchResultImagesClass + '">';
                    for (var counter = 0; counter < itemsPerRow; counter++) {
                        resultView += resultColumns[counter];
                        resultView += '</div>';
                    }
                    resultView += '</div>';
                    return resultView;
                },
                getSearchResultsFooterView: function (maxPage) {
                    var view = '';
                    view += '<div class="row ' + currentInstance.baseSettings.searchResultFooterClass + '">';
                    view += '<div class="col-md-6 col-sm-6 col-xs-6 ' + currentInstance.baseSettings.btnSelectors.searchBtnPrevPageClass + '">';
                    if (currentInstance.search.currentPage != 1) {
                        view += mediaSelectorCommon.localization.pixabaySearchBtnPrevPage + '&nbsp;';
                    }
                    view += '</div>';
                    view += '<div class="col-md-6 col-sm-6 col-xs-6 ' + currentInstance.baseSettings.btnSelectors.searchBtnNextPageClass + '">';
                    if (currentInstance.search.currentPage < maxPage) {
                        view += '&nbsp;' + mediaSelectorCommon.localization.pixabaySearchBtnNextPage;
                    }
                    view += '</div>';
                    view += '</div>';
                    return view;
                },
                hide: function () {
                    $(currentInstance.context).find(currentInstance.baseSettings.searchSelector).addClass(currentInstance.baseSettings.hiddenClass);
                },
                show: function () {
                    $(currentInstance.context).find(currentInstance.baseSettings.searchSelector).removeClass(currentInstance.baseSettings.hiddenClass);
                },
                cleanSearchBody: function () {
                    $(currentInstance.context).find(currentInstance.baseSettings.searchResultSelector).empty();
                },
                cleanSearchText: function () {
                    currentInstance.search.lastSearchText = null;
                    $(currentInstance.context).find(currentInstance.baseSettings.searchSelector).find("input").val("");
                }
            },

            getHeader: function () {
                return {
                    full: mediaSelectorCommon.localization.pixabayProcessorHeader,
                    mobile: mediaSelectorCommon.localization.pixabayProcessorHeaderMobile
                };
            },
            getView: function () {
                var result = mediaSelectorCommon.error.getView(currentInstance.baseSettings.uniqueTabId);
                result += '<div class="row ' + currentInstance.baseSettings.searchClass + '">';
                result += '<a  href="#" class="' + currentInstance.baseSettings.btnSelectors.searchBtnClass + ' "><span class="icon-search"></span></a>';
                result += '<div class="' + currentInstance.baseSettings.searchInputWrapperClass + '">';
                result += '<input type="text" placeholder="' + mediaSelectorCommon.localization.pixabaySearchPlaceholder + '"/>';
                result += '</div>';
                result += '<div class="' + currentInstance.baseSettings.pixabayInfoClass + '">';
                result += mediaSelectorCommon.localization.pixabayInfo;
                result += '</div>';
                result += '<div class="' + currentInstance.baseSettings.searchResultClass + '">';
                result += '</div>';
                result += '</div>';
                result += currentInstance.cropper.getView();
                return result;
            },
            getFooterView: function () {
                var result = '<div class="croppQuizModal__buttonPlaceholder ' + currentInstance.baseSettings.footerClass + '">';
                result += '<a class="btn btn--cropp-submit ' + currentInstance.baseSettings.hiddenClass + ' ' + currentInstance.baseSettings.btnSelectors.saveBtnClass + '">' + mediaSelectorCommon.localization.btnSave + '</a>';
                result += '<a class="btn btn--cropp-submit ' + currentInstance.baseSettings.hiddenClass + ' ' + currentInstance.baseSettings.btnSelectors.backToSearchClass + '">' + mediaSelectorCommon.localization.btnBackToSearch + '</a>';
                result += '<a class="btn btn--cropp-cancel ' + currentInstance.baseSettings.btnSelectors.cancelClass + '">' + mediaSelectorCommon.localization.btnCancel + '</a>';
                result += '</div>';
                return result;
            },
            init: function (context) {
                currentInstance.context = context;
                currentInstance.initBtns();
                currentInstance.search.init();
            },
            initBtns: function () {
                $(currentInstance.context).find(currentInstance.baseSettings.footerSelector).find(currentInstance.baseSettings.btnSelectors.cancelSelector).on('click', function (event) {
                    event.preventDefault();
                    currentInstance.changeState(currentInstance.proccessorStates.initial);
                    currentInstance.broadcastMsg(mediaSelectorCommon.messages.closeModal);
                });
                $(currentInstance.context).find(currentInstance.baseSettings.footerSelector).find(currentInstance.baseSettings.btnSelectors.backToSearchSelector).on('click', function (event) {
                    event.preventDefault();
                    currentInstance.changeState(currentInstance.proccessorStates.backToSearch);
                });
                $(currentInstance.context).find(currentInstance.baseSettings.footerSelector).find(currentInstance.baseSettings.btnSelectors.saveBtnSelector).on('click', function (event) {
                    mediaSelectorCommon.error.hide(currentInstance.context, currentInstance.baseSettings.uniqueTabIdSelector);
                    event.preventDefault();
                    currentInstance.broadcastMsg(mediaSelectorCommon.messages.showLoader);
                    mediaSelectorService.saveCroppedImg(currentInstance.cropper,
                        function (result) {
                            var saveMsg = mediaSelectorCommon.messages.save;
                            saveMsg.data = result;
                            saveMsg.data.mediaType = mediaSelectorCommon.mediaTypes.img;

                            currentInstance.broadcastMsg(saveMsg);
                            currentInstance.broadcastMsg(mediaSelectorCommon.messages.hideLoader);
                            currentInstance.changeState(currentInstance.proccessorStates.initial);
                        },
                        function (data) {
                            mediaSelectorCommon.error.show(currentInstance.context, currentInstance.baseSettings.uniqueTabIdSelector, data);
                            currentInstance.broadcastMsg(mediaSelectorCommon.messages.hideLoader);
                        }, false);
                });
            },
            cropper: mediaSelectorCropper.getNew('pixabayCropper'),
            showBackToSearchBtn: function () {
                $(currentInstance.context).find(currentInstance.baseSettings.footerSelector).find(currentInstance.baseSettings.btnSelectors.backToSearchSelector).removeClass(currentInstance.baseSettings.hiddenClass);
            },
            hideBackToSearchBtn: function () {
                $(currentInstance.context).find(currentInstance.baseSettings.footerSelector).find(currentInstance.baseSettings.btnSelectors.backToSearchSelector).addClass(currentInstance.baseSettings.hiddenClass);
            },
            showSaveBtn: function () {
                $(currentInstance.context).find(currentInstance.baseSettings.footerSelector).find(currentInstance.baseSettings.btnSelectors.saveBtnSelector).removeClass(currentInstance.baseSettings.hiddenClass);
            },
            hideSaveBtn: function () {
                $(currentInstance.context).find(currentInstance.baseSettings.footerSelector).find(currentInstance.baseSettings.btnSelectors.saveBtnSelector).addClass(currentInstance.baseSettings.hiddenClass);
            },
        }
        return currentInstance;
    }
}