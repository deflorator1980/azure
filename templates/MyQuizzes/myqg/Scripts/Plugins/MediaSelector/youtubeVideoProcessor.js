﻿youtubeVideoProcessor = {
    getNew: function (settings) {
        var uniquePart = GlobalUtils.uuidv4();
        var currentInstance = {
            context: {},
            currentYoutubeLink: "",
            currentYoutubeTextBoxVal: "",
            elementAttributes: {},
            proccessorStates: {
                'initial': 'initial',
                'youtubeVideo': 'youtubeVideo'
            },
            currentProccessorState: 'initial',
            changeState: function (newState) {
                if (newState == currentInstance.currentProccessorState) { return; };
                currentInstance.currentProccessorState = newState;
                if (newState == currentInstance.proccessorStates.youtubeVideo) {
                    currentInstance.showSaveBtn();
                    currentInstance.showIframe();
                }
                if (newState == currentInstance.proccessorStates.initial) {
                    currentInstance.hideIframe();
                    currentInstance.hideSaveBtn();
                    $(currentInstance.context).find(currentInstance.baseSettings.bodySelector).find("#" + currentInstance.searchUrlId()).val("");
                    $(currentInstance.context).find(currentInstance.baseSettings.bodySelector).find("#" + currentInstance.startTimeId()).val("");
                    $(currentInstance.context).find(currentInstance.baseSettings.bodySelector).find("#" + currentInstance.endTimeId()).val("");
                    $(currentInstance.context).find(currentInstance.baseSettings.bodySelector).find("#" + currentInstance.autoPlayId()).prop("checked", false);
                    currentInstance.currentYoutubeTextBoxVal = "";
                }
            },

            broadcastMsg: function (msg) {
                $.each(currentInstance.settings.listeners, function (index, item) {
                    item(msg);
                });
            },
            msgHandler: function (msg) {
                if (msg.event == mediaSelectorCommon.events.closeModal) {
                    currentInstance.changeState(currentInstance.proccessorStates.initial);
                    return true;
                };

                if (msg.event == mediaSelectorCommon.events.updateElementAttributes) {
                    currentInstance.elementAttributes = msg.data;
                    currentInstance.restoreSelection();
                    return true;
                }
            },

            baseSettings: {
                uniqueModalClass: 'mediaSelectorYoutubeVideo',
                uniqueTabId: 'mediaSelectorYoutubeVideoTab',
                uniqueTabIdSelector: '.mediaSelectorYoutubeVideoTab',
                videoPreviewClass: 'youtubeVideoPreview',
                videoPreviewSelector: '.youtubeVideoPreview',
                bodyClass: 'mediaSelectorYoutubeBody',
                bodySelector: '.mediaSelectorYoutubeBody',
                footerClass: 'youtubeVideoFooter',
                footerSelector: '.youtubeVideoFooter',
                hiddenClass: 'hidden',
                youtubeEmbadingURL: 'https://www.youtube.com/embed/',
                youTubeAutoStartClass: 'youTubeAutoStart',
                youTubeAutoStartSelector: '.youTubeAutoStart',
                youTubeSearchUrlClass: 'youTubeSearchUrl',
                youTubeSearchUrlSelector: '.youTubeSearchUrl',
                youTubePeriodClass: 'youTubePeriod',
                youTubePeriodSelector: '.youTubePeriod',
                youTubeStartTimeClass: 'youTubeStartTime',
                youTubeStartTimeSelector: '.youTubeStartTime',
                youTubeEndTimeClass: 'youTubeEndTime',
                youTubeEndTimeSelector: '.youTubeEndTime',
                youTubeAttributesClass: 'youTubeAttributes',
                btnSelectors: {
                    saveBtnClass: 'btn-save',
                    saveBtnSelector: '.btn-save',
                    cancelClass: 'btn-cancel',
                    cancelSelector: '.btn-cancel'
                }
            },
            settings: {
                notSelectedVideoImgUrl: '/Images/imgForDropzone.png',
                searchSettings: {
                },
                listeners: []
            },

            getYoutubeId: function (url) {
                if (url) {
                    var regExp = /^.*(youtu.be\/|v\/|u\/\w\/|embed\/|watch\?v=|\&v=|\?v=)([^#\&\?]*).*/;
                    var match = url.match(regExp);
                    if (match && match[2].length == 11) {
                        return { 'isValid': true, 'id': match[2] };
                    }
                    else {
                        return { 'isValid': false };
                    }
                }
                return { 'isValid': false };
            },
            searchUrlId: function() {
                return currentInstance.baseSettings.youTubeSearchUrlClass + uniquePart;
            },
            autoPlayId : function() {
                return currentInstance.baseSettings.youTubeAutoStartClass + uniquePart;
            },
            startTimeId: function() {
                return currentInstance.baseSettings.youTubeStartTimeClass + uniquePart;
            },
            endTimeId : function() {
                return currentInstance.baseSettings.youTubeEndTimeClass + uniquePart;
            },

            getHeader: function () {
                return {
                    full: mediaSelectorCommon.localization.youtubeHeader,
                    mobile: mediaSelectorCommon.localization.youtubeHeaderMobile
                };
            },
            getView: function () {
                var result = '';
                result += '<div class="' + currentInstance.baseSettings.bodyClass + '">';
                result += mediaSelectorCommon.error.getView(currentInstance.baseSettings.uniqueTabId);
                result += '<div class="row">';
                result += '<div class="col-md-12 col-sm-12 col-xs-12">';
                result += '<input type="text" id="' + currentInstance.searchUrlId() + '" placeholder="' + mediaSelectorCommon.localization.youtubeSearchPlaceholder + '" />';
                result += '</div>';
                result += '</div>';
                result += '<div class="row ' + currentInstance.baseSettings.videoPreviewClass + ' ' + currentInstance.baseSettings.hiddenClass + '">';
                result += '<div class="col-md-12 col-sm-12 col-xs-12">';
                result += '<iframe frameborder="0" allowfullscreen></iframe>';
                result += '</div>';
                result += '</div>';

                result += '<div class="row ' + currentInstance.baseSettings.youTubePeriodClass + ' ' + currentInstance.baseSettings.hiddenClass + '">';
                
                result += '<label class="control-label col-md-2 col-sm-2 hidden-xs">' + mediaSelectorCommon.localization.youtubeIntervalLabel +'</label>';
                
                result += '<label for="' + currentInstance.startTimeId() + '" class="control-label col-md-1 col-sm-1 col-xs-1">' + mediaSelectorCommon.localization.youtubeStartTimeLabel + '</label>';
                result += '<div class="col-md-2 col-sm-2 col-xs-3">';
                result += '<input type="text" class="form-controll text--center" id="' + currentInstance.startTimeId() + '" name="' + currentInstance.startTimeId() + '" />';
                result += '</div>';
                
                result += '<label for="' + currentInstance.endTimeId() + '" class="control-label col-md-1 col-sm-1 col-xs-1">' + mediaSelectorCommon.localization.youtubeEndTimeLabel + '</label>';
                result += '<div class="col-md-2 col-sm-2 col-xs-3">';
                result += '<input type="text" class="form-controll text--center" id="' + currentInstance.endTimeId() + '" name="' + currentInstance.endTimeId() + '" />';
                result += '</div>';
                
                result += '</div>';

                result += '<div class="row">';
                result += '<div class="col-md-12 col-sm-12 col-xs-12 ' + currentInstance.baseSettings.youTubeAttributesClass + '">';
                result += '<input type="checkbox" id="' + currentInstance.autoPlayId() + '" class="' + currentInstance.baseSettings.youTubeAutoStartClass + '" />';
                result += '<label for="' + currentInstance.autoPlayId() + '">' + mediaSelectorCommon.localization.youtubeAutoPlayLabel + '</label>';
                result += '</div>';
                result += '</div>';


                result += '</div>';
                return result;
            },
            getFooterView: function () {
                var result = '<div class="croppQuizModal__buttonPlaceholder ' + currentInstance.baseSettings.footerClass + '">';
                result += '<a class="btn btn--cropp-submit ' + currentInstance.baseSettings.hiddenClass + ' ' + currentInstance.baseSettings.btnSelectors.saveBtnClass + '">' + mediaSelectorCommon.localization.btnSave + '</a>';
                result += '<a class="btn btn--cropp-cancel ' + currentInstance.baseSettings.btnSelectors.cancelClass + '">' + mediaSelectorCommon.localization.btnCancel + '</a>';
                result += '</div>';
                return result;
            },
            init: function (context) {
                currentInstance.context = context;
                currentInstance.initBtns();
                currentInstance.initInputField();
            },
            initBtns: function () {
                $(currentInstance.context).find(currentInstance.baseSettings.footerSelector).find(currentInstance.baseSettings.btnSelectors.cancelSelector).click(function () {
                    currentInstance.changeState(currentInstance.proccessorStates.initial);
                    currentInstance.stopVideo();
                    currentInstance.broadcastMsg(mediaSelectorCommon.messages.closeModal);
                });
                $(currentInstance.context).find(currentInstance.baseSettings.footerSelector).find(currentInstance.baseSettings.btnSelectors.saveBtnSelector).click(function () {
                    mediaSelectorCommon.error.hide(currentInstance.context, currentInstance.baseSettings.uniqueTabIdSelector);

                    if (!currentInstance.validateInputs()) {
                        currentInstance.broadcastMsg(mediaSelectorCommon.messages.hideLoader);
                        return;
                    }
                    currentInstance.stopVideo();
                    currentInstance.broadcastMsg(mediaSelectorCommon.messages.showLoader);

                    currentInstance.addYoutubeAttributes();

                    mediaSelectorService.saveVideo(currentInstance.currentYoutubeLink,
                        function (result) {
                            var saveMsg = mediaSelectorCommon.messages.save;
                            saveMsg.data = result;
                            saveMsg.data.mediaType = mediaSelectorCommon.mediaTypes.video;

                            currentInstance.broadcastMsg(saveMsg);
                            currentInstance.broadcastMsg(mediaSelectorCommon.messages.hideLoader);
                            currentInstance.changeState(currentInstance.proccessorStates.initial);
                        },
                        function (data) {
                            mediaSelectorCommon.error.show(currentInstance.context, currentInstance.baseSettings.uniqueTabIdSelector, data);
                            currentInstance.broadcastMsg(mediaSelectorCommon.messages.hideLoader);
                        }, false);
                });
            },
            addYoutubeAttributes: function () {
                var attrList = [], currentLink = currentInstance.currentYoutubeTextBoxVal;
                var playlistParam = mediaSelectorCommon.helpers.getUrlParam(currentLink, "list");
                if (playlistParam) {
                    attrList.push({ code: "list", value: playlistParam });
                }

                var startTimeParam = $(currentInstance.context).find(currentInstance.baseSettings.bodySelector).find("#" + currentInstance.startTimeId()).val();
                if (startTimeParam) {
                    attrList.push({ code: "start", value: startTimeParam });
                    }

                var endTimeParam = $(currentInstance.context).find(currentInstance.baseSettings.bodySelector).find("#" + currentInstance.endTimeId()).val();
                if (endTimeParam) {
                    attrList.push({ code: "end", value: endTimeParam });
                }

                var autoplay = $(currentInstance.context).find(currentInstance.baseSettings.bodySelector).find(currentInstance.baseSettings.youTubeAutoStartSelector).prop("checked");
                if (autoplay) {
                    attrList.push({ code: "autoplay", value: "1" });
                }

                attrList.push({ code: "rel", value: "0" });

                var urlSearchParams = "";
                $.each(attrList,
                    function(index, item) {
                    urlSearchParams += (urlSearchParams === "" ? "?" : "&") + item.code + "=" + item.value;
                    });

                currentInstance.currentYoutubeLink = currentInstance.currentYoutubeLink + urlSearchParams;
            },
            initInputField: function () {
                $(currentInstance.context).find(currentInstance.baseSettings.bodySelector).find("#" + currentInstance.searchUrlId() ).on("keyup paste change", function (event) {
                    currentInstance.onYoutubeLinkChange(this);
                });
                $(currentInstance.context).find(currentInstance.baseSettings.bodySelector).find("#" + currentInstance.searchUrlId()).on("paste", function (event) {
                    var context = this;
                    setTimeout(function () {
                        currentInstance.onYoutubeLinkChange(context);
                    }, 100);
                });

                $(currentInstance.context).find(currentInstance.baseSettings.bodySelector)
                    .find("#" + currentInstance.startTimeId()).on("keyup paste change", function() {
                            mediaSelectorCommon.error.hide(currentInstance.context, currentInstance.baseSettings.uniqueTabIdSelector);
                    });

                $(currentInstance.context).find(currentInstance.baseSettings.bodySelector)
                    .find("#" + currentInstance.endTimeId()).on("keyup paste change", function () {
                            mediaSelectorCommon.error.hide(currentInstance.context, currentInstance.baseSettings.uniqueTabIdSelector);
                        });
                var timeMask = new Inputmask({
                    alias: "datetime",
                    mask: "m:s:s",
                    placeholder: "00:00:00",
                    autoUnmask: true,
                    clearMaskOnLostFocus: false,
                    definitions: {
                        m: {
                            validator: "[0-9][0-9]",
                            cardinality: 2
                        }
                    },
                    onBeforeMask: function (value, opts) {
                        if (!value) {
                            return value;
                        }
                        var h = Math.floor(value / 3600);
                        var m = Math.floor((value - h*3600) / 60);
                        var s = value - h * 3600 - m * 60;

                        var res = currentInstance.timeUnitStr(h) +
                            currentInstance.timeUnitStr(m) +
                            currentInstance.timeUnitStr(s);
                        return res;
                    },
                    onUnMask: function(maskedValue, unmaskedValue) {
                        if (!unmaskedValue) {
                            return undefined;
                        }
                        var parsedTimeParts = maskedValue.split(":");
                        unmaskedValue = parseInt(parsedTimeParts[0] * 3600) + parseInt(parsedTimeParts[1] * 60) + parseInt(parsedTimeParts[2]);
                        return unmaskedValue;
                    }
                });

                timeMask.mask($(currentInstance.context).find(currentInstance.baseSettings.bodySelector)
                    .find("#" + currentInstance.startTimeId()));

                timeMask.mask($(currentInstance.context).find(currentInstance.baseSettings.bodySelector)
                    .find("#" + currentInstance.endTimeId()));


            },
            timeUnitStr: function(unit) {
                if (unit < 10) {
                    return "0" + unit.toString();
                }
                return unit.toString();
            },
            validateInputs: function() {

                var startTimeParam = $(currentInstance.context).find(currentInstance.baseSettings.bodySelector).find("#" + currentInstance.startTimeId()).val();
                var endTimeParam = $(currentInstance.context).find(currentInstance.baseSettings.bodySelector).find("#" + currentInstance.endTimeId()).val();

                if (startTimeParam && endTimeParam && startTimeParam >= endTimeParam) {
                    mediaSelectorCommon.error.show(currentInstance.context, currentInstance.baseSettings.uniqueTabIdSelector, mediaSelectorCommon.localization.youtubeTimeIntervalErrorMsg );
                    return false;
                }

                return true;
            },
            onYoutubeLinkChange: function (eventContext) {
                var textBoxVal = $(eventContext).val();
                if (textBoxVal == currentInstance.currentYoutubeTextBoxVal) {
                    return;
                } else {
                    currentInstance.currentYoutubeTextBoxVal = textBoxVal;
                }
                mediaSelectorCommon.error.hide(currentInstance.context, currentInstance.baseSettings.uniqueTabIdSelector);
                currentInstance.currentYoutubeLink = "";
                $(currentInstance.context).find(currentInstance.baseSettings.bodySelector).find("#" + currentInstance.startTimeId()).val("");
                $(currentInstance.context).find(currentInstance.baseSettings.bodySelector).find("#" + currentInstance.endTimeId()).val("");

                var youtubeId = currentInstance.getYoutubeId(textBoxVal);

                if (youtubeId.isValid) {
                    currentInstance.currentYoutubeLink = currentInstance.baseSettings.youtubeEmbadingURL + youtubeId.id;
                    currentInstance.changeState(currentInstance.proccessorStates.youtubeVideo);
                    currentInstance.setYoutubeIframeSRC(currentInstance.currentYoutubeLink);
                }
                else {
                    currentInstance.changeState(currentInstance.proccessorStates.initial);
                    mediaSelectorCommon.error.show(currentInstance.context, currentInstance.baseSettings.uniqueTabIdSelector, mediaSelectorCommon.localization.youtubeWrongUrlError);
                }
            },
            restoreSelection: function () {
                var attrStoredUrl = currentInstance.elementAttributes[mediaSelectorCommon.elementAttributes.storedUrl];
                var attrMediaType = currentInstance.elementAttributes[mediaSelectorCommon.elementAttributes.mediaType];
                if (attrMediaType && attrMediaType == mediaSelectorCommon.mediaTypes.video && attrStoredUrl) {
                    var urlTextBox = $(currentInstance.context).find(currentInstance.baseSettings.bodySelector).find("#" + currentInstance.searchUrlId());
                    urlTextBox.val(attrStoredUrl);
                    currentInstance.onYoutubeLinkChange(urlTextBox);
                    currentInstance.setYoutubeAttributesBasedOnUrl(attrStoredUrl);
                    currentInstance.broadcastMsg({
                        data: { "linkedTabId": currentInstance.baseSettings.uniqueTabId },
                        event: mediaSelectorCommon.events.setActiveTab
                    });
                }
            },
            setYoutubeAttributesBasedOnUrl: function (url) {
                if (url.indexOf("autoplay=1") != -1) {
                    $(currentInstance.context).find(currentInstance.baseSettings.bodySelector).find("#" + currentInstance.autoPlayId()).prop("checked", true);
                }

                var startTime = mediaSelectorCommon.helpers.getUrlParam(url, "t");
                if (!startTime) {
                    startTime = mediaSelectorCommon.helpers.getUrlParam(url, "start");
                }
                if (startTime) {
                    $(currentInstance.context).find(currentInstance.baseSettings.bodySelector).find("#" + currentInstance.startTimeId()).val(startTime);
                }

                var endTime = mediaSelectorCommon.helpers.getUrlParam(url, "end");
                if (endTime) {
                    $(currentInstance.context).find(currentInstance.baseSettings.bodySelector).find("#" + currentInstance.endTimeId()).val(endTime);
                }
            },

            setYoutubeIframeSRC: function (src) {
                $(currentInstance.context).find(currentInstance.baseSettings.bodySelector).find("iframe").attr("src", src);
            },
            showIframe: function () {
                $(currentInstance.context).find(currentInstance.baseSettings.bodySelector).find(currentInstance.baseSettings.videoPreviewSelector).removeClass(currentInstance.baseSettings.hiddenClass);
                $(currentInstance.context).find(currentInstance.baseSettings.bodySelector).find(currentInstance.baseSettings.youTubePeriodSelector).removeClass(currentInstance.baseSettings.hiddenClass);
            },
            hideIframe: function () {
                $(currentInstance.context).find(currentInstance.baseSettings.bodySelector).find(currentInstance.baseSettings.videoPreviewSelector).addClass(currentInstance.baseSettings.hiddenClass);
                $(currentInstance.context).find(currentInstance.baseSettings.bodySelector).find(currentInstance.baseSettings.youTubePeriodSelector).addClass(currentInstance.baseSettings.hiddenClass);
            },

            showSaveBtn: function () {
                $(currentInstance.context).find(currentInstance.baseSettings.footerSelector).find(currentInstance.baseSettings.btnSelectors.saveBtnSelector).removeClass(currentInstance.baseSettings.hiddenClass);
            },
            hideSaveBtn: function () {
                $(currentInstance.context).find(currentInstance.baseSettings.footerSelector).find(currentInstance.baseSettings.btnSelectors.saveBtnSelector).addClass(currentInstance.baseSettings.hiddenClass);
            },

            stopVideo: function () {
                var iframes = $(currentInstance.context).find(currentInstance.baseSettings.bodySelector).find("iframe");
                if (iframes && iframes.length) {
                    iframes.attr("src", "");
                }
            }
        };
        if (settings) {
            currentInstance.settings = settings;
        }
        return currentInstance;
    }
}