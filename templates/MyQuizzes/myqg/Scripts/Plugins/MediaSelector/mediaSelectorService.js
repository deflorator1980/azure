﻿var mediaSelectorService = (
    function(mediaSelectorService) {
        var $this = mediaSelectorService;
        $this.settings = {
            saveVideoUrl: '/Image/SaveVideo',
            createImageInfoUrl: '/Image/CreateEmptyStorageImage',
            updateVideoSelfHostedUrl: '/Image/UpdateVideoSelfHosted'
        };

        function createNewImageInfo() {
            return new Promise(function(resolve, reject) {
                $.post($this.settings.createImageInfoUrl,
                    {
                        'storageAccountName': mediaSelectorCommon.settings.storageAccountName
                    },
                    function(imageInfo) {
                        resolve(imageInfo);
                    });
            });
        }

        function uploadImageFromUrl(imageInfo, url, cropBoxData, imageData) {
            var uploader = new imageUploader(new uploadToStorageStrategy(mediaSelectorCommon.settings.storageUrl,
                mediaSelectorCommon.settings.storageToken,
                mediaSelectorCommon.settings.storageAccountName,
                imageInfo.StorageId));
            return uploader.uploadFromUrl(imageInfo, url, { cropBoxData: cropBoxData, imageData: imageData });
        }

        function uploadImageFromData(imageInfo, base64Data, cropBoxData, imageData) {
            var uploader = new imageUploader(new uploadToStorageStrategy(mediaSelectorCommon.settings.storageUrl,
                mediaSelectorCommon.settings.storageToken,
                mediaSelectorCommon.settings.storageAccountName,
                imageInfo.StorageId));
            return uploader.uploadFromInput(imageInfo, base64Data, { cropBoxData: cropBoxData, imageData: imageData });
        }


        $this.saveCroppedImg = function(cropperWrapper, onSuccess, onError, isLocal) {
            var cropBoxData = cropperWrapper.cropper.getData();
            var imageData = cropperWrapper.cropper.getImageData();

            var result;

            var imageInfoPromise = createNewImageInfo();
            imageInfoPromise
                .then(function(imageInfo) {
                    result = imageInfo;
                    return uploadImageFromUrl(imageInfo, cropperWrapper.currentImgSRC, cropBoxData, imageData)
                        .then(function(fileUrl) {
                                result.fileUrl = fileUrl;
                                onSuccess(result);
                            },
                            function(error) {
                                onError(error);
                            });
                });
        };
        $this.uploadImgAndCrop = function(cropperWrapper, onSuccess, onError) {
            var cropBoxData = cropperWrapper.cropper.getData();
            var imageData = cropperWrapper.cropper.getImageData();
            var result;

            var imageInfoPromise = createNewImageInfo();
            imageInfoPromise
                .then(function(imageInfo) {
                    result = imageInfo;
                    return uploadImageFromData(imageInfo, cropperWrapper.currentImgSRC, cropBoxData, imageData)
                        .then(function(fileUrl) {
                                result.fileUrl = fileUrl;
                                onSuccess(result);
                            },
                            function(error) {
                                onError(error);
                            });
                });
        };
        $this.saveVideo = function(videoURL, onSuccess, onError) {
            $.ajax({
                url: mediaSelectorService.settings.saveVideoUrl,
                method: 'POST',
                data: {
                    "url": videoURL
                },
                success: function(result) {
                    onSuccess(result);
                },
                error: function(data) {
                    onError(data);
                    console.log('Cropping failed');
                }
            });
        };
        $this.updateVideoSelfHosted = function(id, autoplay, onSuccess, onError) {
            $.ajax({
                url: mediaSelectorService.settings.updateVideoSelfHostedUrl,
                type: 'POST',
                data: {
                    "id": id,
                    "autoplay": autoplay
                },
                success: function(result) {
                    onSuccess(result);
                },
                error: function(data) {
                    onError(data);
                    console.log('Updating selfhosted video failed');
                }
            });
        };

        $this.getVideoInfo = function(url, id, onSuccess, onError) {
            $.ajax({
                url: url,
                type: 'POST',
                data: $.param({ '': id }, true),
                success: function(result) {
                    onSuccess(result);
                },
                error: function(data) {
                    onError(data);
                    console.log('getVideoInfo failed');
                }
            });
        };

        return mediaSelectorService;
    }(mediaSelectorService || {}));