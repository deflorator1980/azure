﻿mediaSelectorCropper = {
    getNew: function (cropperClass) {
        var cropperInstance = {
            cropper: null,
            currentImgSRC: '',
            currentImgName: '',
            getCropperBaseSettings: function () {
                return {
                    aspectRatio: 16 / 9,
                    viewMode: 3,
                    responsive: true,
                    dragMode: 'move',
                    autoCropArea: 1,
                    restore: false,
                    guides: false,
                    highlight: true,
                    cropBoxMovable: true,
                    cropBoxResizable: true,
                    zoomOnWheel: false,
                    zoomOnTouch: false,
                    movable: false,
                    zoomable: false,
                    rotatable: true,
                    scalable: true,
                    minContainerWidth: 1,
                    minContainerHeight: 1,
                };
            },
            baseSettings: {
                cropperClass: cropperClass,
                cropperSelector: '.' + cropperClass,
                hiddenClass: 'hidden'
            },
            getView: function () {
                var view = '<div class="cropperContainer">';
                view += '<div class="' + cropperInstance.baseSettings.cropperClass + ' ' + cropperInstance.baseSettings.hiddenClass + '">';
                view += '<img/>';
                view += '</div>';
                view += '</div>';
                return view;
            },
            init: function (context, settings) {
                if (cropperInstance.cropper) {
                    try {
                        cropperInstance.cropper.destroy();
                    }
                    catch (err) {
                        console.log('cropper destroy error');
                        console.log(err);
                    }
                }
                var img = $(context).find(cropperInstance.baseSettings.cropperSelector + ' img');
                if (img && img.length) {
                    cropperInstance.cropper = new Cropper(img[0], settings);
                };
            },
            showCropper: function (context) {
                $(context).find(cropperInstance.baseSettings.cropperSelector).removeClass(cropperInstance.baseSettings.hiddenClass);
            },
            hideCropper: function (context) {
                $(context).find(cropperInstance.baseSettings.cropperSelector).addClass(cropperInstance.baseSettings.hiddenClass);
            },
            setImgSRC: function (context, img_src, name) {
                cropperInstance.currentImgSRC = img_src;
                cropperInstance.currentImgName = name ? name : cropperInstance.getImgNameFromSRC(img_src);
                $(context).find('.' + cropperClass).find("img").attr('src', img_src);
            },
            getImgNameFromSRC: function (src) {
                var result = src.replace(/\\/g, '/');
                result = result.substring(result.lastIndexOf('/') + 1);
                return result;
            }
        };
        return cropperInstance;
    }
}
