﻿$(function () {
    var timeZone = GlobalUtils.getCurrentTimeZone();
    var href = $('#installQuizModal__button').attr('href') + '?timeZone=' + timeZone.timeZoneOffset;
    $('#installQuizModal__button').attr('href', href);

});