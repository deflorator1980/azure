﻿//FACEBOOK
window.fbAsyncInit = function () {
    FB.init({
        appId: global_facebookAppId,
        xfbml: true,
        version: 'v2.8'
    });
    FB.AppEvents.logPageView();
};

(function (d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) { return; }
    js = d.createElement(s); js.id = id;
    js.src = "//connect.facebook.net/en_US/sdk.js";
    fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));


function initShareButtonFacebook(selectorOfButton, url) {
    var $element = $(selectorOfButton);
    $element.unbind('click');
    $element.click(function () {
        FB.ui({
            method: 'share',
            display: 'popup',
            href: url,
        }, function (response) { });
    });
}


//TWITTER
window.twttr = (function (d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0],
      t = window.twttr || {};
    if (d.getElementById(id)) return t;
    js = d.createElement(s);
    js.id = id;
    js.src = "https://platform.twitter.com/widgets.js";
    fjs.parentNode.insertBefore(js, fjs);

    t._e = [];
    t.ready = function (f) {
        t._e.push(f);
    };

    return t;
}(document, "script", "twitter-wjs"));

function initShareButtonTwitter(selectorOfButton, url, text) {
    $(selectorOfButton).attr(
        'href',
        'https://twitter.com/intent/tweet?text=' + encodeURI(text) + '&url=' + encodeURI(url)
    );
}


function initShareButtonVK(selectorOfButtonPlaceholder, url) {
    var $placeholder = $(selectorOfButtonPlaceholder);
    var $link = $(selectorOfButtonPlaceholder + ' > a');
    var shareButton__vk_html = $link.html();
    shareButton__vk_html = VK.Share.button(
        url,
        {
            type: 'custom',
            text: shareButton__vk_html
        });
    $placeholder.html(shareButton__vk_html);
}

