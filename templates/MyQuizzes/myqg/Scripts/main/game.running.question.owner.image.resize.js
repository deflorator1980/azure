﻿var $this =$();
var oHeight = 0;
var oWidth = 0;
function resize() {
    $this = $(this);
    $this.hide();
    $this.parent().css('height', 'auto').css('width', 'auto');
    var oHeight = this.naturalHeight;
    var oWidth = this.naturalWidth;
    var pHeight = $this.parents('.centered:first').height();
    var pWidth = $this.parents('.Photobg:first').width() ;
    var hm = pHeight / oHeight;
    var wm = pWidth / oWidth;
    var m = wm > hm ? hm : wm;
    $this.parent().css({ height: oHeight * m - 20 + 'px', width: oWidth * m - 20 + 'px' });
    $this.show();
};
$(window).off('resize');
$(window).resize(function(){
    $('.the-photo img').each(function(){
        resize.call(this);
    });
});

function imageLoaded(){
    $this = $(this);
    oHeight = this.naturalHeight;
    oWidth = this.naturalWidth;
    resize.call(this);
    $(this).addClass('loaded');
}

var users = [];
var usersIds={};
var timeout = 0;
var currentQuestionIdLoadAnsweredGlobal = undefined;

function loadAnswered() {
    setTimeout(function () {
        var currentQuestionIdLoadAnswered = $('#Question_Id').val();
        if (isNaN(parseInt(currentQuestionIdLoadAnswered))) {
            loadAnswered();
        }
        else if (window.RestingTime) {
            users = [];
            usersIds = {};
            $('.answer-user-container').html('');
            loadAnswered();
        }
        else if (currentQuestionIdLoadAnswered != currentQuestionIdLoadAnsweredGlobal) {
            currentQuestionIdLoadAnsweredGlobal = currentQuestionIdLoadAnswered;
            users = [];
            usersIds = {};
            $('.answer-user-container').html('');
            loadAnswered();
        }
        else {
            $.ajax({
                url: answeredUsersUrl,
                dataType: "html",
                type: "get",
                success: function (data) {
                    var $response = $(data);
                    $response.find('.answeredUser').hide().each(function () {
                        users.push(this);
                        console.log([this, users]);
                    });
                    loadAnswered();
                },
                error: loadAnswered
            });
        }
    }, timeout);

timeout = 2000;
}
loadAnswered();
$(document).ready(function(){
    var answerUserContainer= $('.answer-user-container');
    var currentUser=null;
    var text=$('.the-time-text');
    var textContainer=text.parent().parent();
    function showCurrentUser() {
        if(currentUser){
            if(!users.length) {
                text.css('padding-right',10);
            }
            currentUser.fadeOut(500,function(){
                $(this).remove();
            });
        }
        if(!users.length) {
            return;
        }
        var padding=0;
        if($(window).width()==textContainer.width())
            padding=350;
        else{
            padding=340-($(window).width()-textContainer.width())/2;
        }
        text.css('padding-right',padding);
        answerUserContainer.css('top',text.offset().top+(text.height()-90)/2);
        while(users.length){
            currentUser=$(users[0]);
            var id=currentUser.find('#i_UserId').val();
            if(!usersIds[id]){
                usersIds[id]=true;
                break;
            }
            else{
                users.shift();
                currentUser=null;
            }
        }
        if(!currentUser)
            return;
        answerUserContainer.append(currentUser);
        currentUser.hide().fadeIn(1000);
        users.shift();
    }
    showCurrentUser();
    setInterval(showCurrentUser, 3000);
});