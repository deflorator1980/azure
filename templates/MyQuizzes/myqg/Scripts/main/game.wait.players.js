﻿$(document).ready(function() {
    Controls.onCheckInternetConnectionSpeed('/Images/dummy-image.png', 46423);
    setInterval(function() {
        Controls.onCheckInternetConnectionSpeed('/Images/dummy-image.png', 46423);
    },
        10000);
});

function resize() {
    $('.the-photo img').each(function () {
        var $this = $(this);
        $this.hide();
        var oHeight = this.naturalHeight;
        var oWidth = this.naturalWidth;
        $this.parent().css('height', 'auto').css('width', 'auto');
        setTimeout(function () {
            var pHeight = $this.parents('#imageContent').height() - 150;
            var pWidth = $this.parents('td').width();
            console.log("height:" + $this.parents('#imageContent').height());
            console.log("width:" + $this.parents('td').width());
            var hm = pHeight / oHeight;
            var wm = pWidth / oWidth;
            var m = wm > hm ? hm : wm;

            $this.parent().css({'padding-bottom': '20px',
                margin: '10px auto', height: oHeight * m - 20 + 'px', width: oWidth * m - 40 + 'px' });
            $this.show();
        });
    });
};
$(window).off("resize");
$(window).resize(resize);

ping();

function ping() {
    setTimeout(CheckQuizStatus, 9000);
}

function CheckQuizStatus(){
    $.ajax({
        type: 'POST',
        data: { hostId: global_host_id },
        url: '/Game/QuizeStatus',
        success: function (result) {
            if (result.success) {
                ping();
                $('.top-line').children('h1').children('span').text(result.playersCount);
            }
            else if (result.isNotStarted) {
                window.location.replace('/Lectures');
            }
            else {
                ping();
                console.error(result.message);
            }
        },
        error: function (error) {
            ping();
            console.log(error);
        }
    });
}

function showMessage() {
    var $this = $('#connectionSpeed');
    $this.children('span').text(global_localization_LowInternetSpeed);
    if ($this.hasClass('hide')) {
        $this.removeClass('hide').addClass('show');
    } else {
        $this.addClass('show');
    }
}

function hideMessage() {
    var $this = $('#connectionSpeed');
    if ($this.hasClass('show')) {
        $this.removeClass('show').addClass('hide');
    } else {
        $this.addClass('hide');
    }
}

function imageLoaded() {
    $this = $(this);
    resize();
    $(this).addClass('loaded');
}