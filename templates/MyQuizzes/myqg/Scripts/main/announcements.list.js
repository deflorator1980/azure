﻿var announcementsModule = (

    function (announcementsModule) {
        $(function () {

            $('.announcement__action').each(function () {
                this.addEventListener('click', announcementsModule.SetWinnerPrize);
            });

            $('#ModalContinueAnnounceAction')[0].addEventListener('click', announcementsModule.CreateOrUpdateAnnouncement);
        });

        announcementsModule.SetWinnerPrize = function (element) {
            var isQuizWinnerCodeEnabled = $(element.target).parents('.gridData__buttonsBlock').children('#IsQuizWinnerCodeEnabled').val();
            isQuizWinnerCodeEnabled = isQuizWinnerCodeEnabled.toLowerCase() === 'true' ? true : false;
            var quizId = $(element.target).parents('.gridData__buttonsBlock').children('#QuizId').val();
            var actionUrl = $(element.target).attr('href');

            if (isQuizWinnerCodeEnabled) {
                var prize = $(element.target).data('prize');
                $('#quizPrizeModal').toggleClass('modal--close');
                $('#quizPrizeModal').find('#ModalQuizId').val(quizId);
                $('#quizPrizeModal').find('#ModalQuizPrizeInput').val(prize);
                $('#quizPrizeModal').find('#ModalContinueAnnounceAction').attr('href', actionUrl);
                event.preventDefault();
                return false;
            }

            return true;
        }

        announcementsModule.CreateOrUpdateAnnouncement = function (element) {
            var actionUrl = $(element.target).attr('href');
	        var prize = $('#quizPrizeModal').find('#ModalQuizPrizeInput').val();
            $.ajax({
                url: actionUrl,
                type: 'POST',
				data: { prize: prize},
                success: function() {
                    window.location.href = '/Announcement/List';
                },
                error: function(result) {
                    console.log('error during create an announce');
                    console.log(result.message);
                }
			});
	        $(element.target).data('prize', prize);
	        $(element.target).attr('href', '#'); // need to prevent second request
        }
        
        announcementsModule.HideModal = function () {
            $('#quizPrizeModal').toggleClass('modal--close');
            $(document.body).removeClass('fixed');
            return false;
        }

        return announcementsModule;

    }(announcementsModule || {})
);