﻿function imageLoaded() {
    $this = $(this);
    oHeight = this.naturalHeight;
    oWidth = this.naturalWidth;
    $(window).off('resize');
    $(window).resize(resize);
    resize();
    $('.the-photo img').addClass('loaded');
}
function resize() {
    var isiPhone = /iphone/i.test(navigator.userAgent.toLowerCase());
    var isiPod = /ipod/i.test(navigator.userAgent.toLowerCase());

    //if (!$this || $this.hasClass('loaded')) return;
    if (!$this && $this.hasClass('loaded') && !isiPhone && !isiPod) return;
    $this.hide();
    $this.parent().css('height', 'auto').css('width', 'auto');
    setTimeout(function () {
        var pHeight = $this.parents('.centered:first').height();
        var pWidth = $this.parents('td:first').width();
        if (pWidth > 700)
            pWidth = pWidth / 2;
        var hm = pHeight / oHeight;
        var wm = pWidth / oWidth;
        var m = wm > hm ? hm : wm;
        $this.parent().css({ height: oHeight * m - 20 + 'px', width: oWidth * m - 20 + 'px' });
        $this.show();
    });
}

$(document).ready(function () {
    Controls.onCheckInternetConnectionSpeed('/Images/dummy-image.png', 46423);
    setInterval(function () {
        Controls.onCheckInternetConnectionSpeed('/Images/dummy-image.png', 46423);
    },
        10000);

    if (/iPad/i.test(navigator.userAgent)) {
        $('body #lecture-description').addClass('ipad');
    } else {
        $('body #lecture-description').addClass('desktop');
        if ($('body #lecture-description').text().trim().length < 50) {
            $('body #lecture-description').addClass('centered-description');
        }
    }
});

function showPopup($this, option) {
    $('#table').addClass('blur-filter');
    $('.mobile-game').addClass('blur-filter');
    var $description = $('#descriptionText').children('p').children('span');
    if (option === 'low') {
        $description.text(global_loc_Gamer_Internet_Trouble);
    } else {
        $description.text(global_loc_Gamer_Internet_Offline);
    }
    $('#connectionSpeedDescriptionWrap').addClass('show-popup');
    $('.homepage').addClass('hideContent');
    $('.mobile-game #imageContent').addClass('hideContent');
    $('#connectionSpeed').removeClass('show').addClass('hideContent');
}

function hidePopup($this) {
    $('#table').removeClass('blur-filter');
    $('.mobile-game').removeClass('blur-filter');
    $this.parents('#connectionSpeedDescriptionWrap').removeClass('show-popup');
    $('.homepage').removeClass('hideContent');
    $('.mobile-game #imageContent').removeClass('hideContent');
}

$(document).ready(function () {
    gameStart();

    function gameStart() {
        $('.ajaxLoader').remove();
        $.ajax({
            url: global_check_if_game_started_url,
            contentType: 'application/json; charset=utf-8',
            dataType: 'json',
            type: 'POST',
            cache: false,
            data: JSON.stringify({ hostId: global_host_id }),
            success: function (data) {
                if (data.IsSuccess) {
                    window.location.replace(global_current_question_url);
                    console.log("User updated");
                    return;
                } else {
                    if (data.IsNotStarted || data.IsPlayerKicked) {
                        window.location.replace(global_quiz_code_url);
                    }
                    else {
                        console.log("User updated");
                        gameStart();
                    }
                }
            },
            error: function (error) {
                console.log("User NOT updated");
                setTimeout(gameStart, 1000);
                console.log(error);
            }
        });
    }

    function showMessage() {
        var $this = $('#connectionSpeed');
        $this.children('span').text(global_loc_LowInternetSpeed);
        if ($this.hasClass('hideContent') || $this.hasClass('hide')) {
            $this.removeClass('hideContent').removeClass('hide').addClass('show');
        } else {
            $this.addClass('show');
        }
    }

    function hideMessage() {
        var $this = $('#connectionSpeed');
        if ($this.hasClass('show')) {
            $this.removeClass('show').addClass('hideContent');
        } else {
            $this.addClass('hideContent');
        }
    }
});