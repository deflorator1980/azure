﻿function GetUpdateWhenReady(time) {

    var IsRestingTime = $('#RestingTime').val() == 'True';
    if (!IsRestingTime) {
        autoSubmit();
        console.log("autoSubmit")
    }

    setTimeout(function () {
        AsyncControls.ActionSender.LoadAction(
                             currentQuestionUrl,
                             "html",
        { },
                 function (responceContent) {
                     isRaady = true;
                     if (!$(responceContent).find('#question-info-container-answers-container').length) {
                         $(document.body).append(responceContent);
                         return;
                     }
                     var currentQuestionConteiner = $("#currentQuestion-info-container");

                     currentQuestionConteiner.html(responceContent);
                     $('.the-time-text h4').text($("#questionText").text());
                     var histotyContainer = $("#history-container");
                     if (!histotyContainer.is(":visible")) {
                         histotyContainer.show();
                     }
                     AsyncControls.ActionSender.LoadAction(
                         leaderboardIndividualsChildUrl,
                         "html",
                     { },
                         function (responceLeaderbordContent) {
                             var leaderboardContainer = $('.topPlayersContainer');
                             leaderboardContainer.empty();
                             leaderboardContainer.append(responceLeaderbordContent);

                             AsyncControls.ContentManager.UpdateProfileInHeader();
                         }
                         );
                     AsyncControls.ContentManager.UpdateHistory();
                 },
                 function (err) {
                     setTimeout(GetUpdateWhenReady, 2000);
                 }
                             );
    }, IsRestingTime ? 100 : (time || 2000));
}

function autoSubmit() {
    var $checkeds = $("input[id^=AnswerIds]");
    var $btnAnswer = $('#answer-button');

    if (($btnAnswer.attr('disabled') !== 'disabled') && ($checkeds.is(':checked')) && !chosenAnswers()) {
        $('#answersForm').submit();
    }

    function chosenAnswers() {
        var isAllChoosen = true;
        var isCheckAny = false;
        $checkeds.each(function (i, el) {
            if (!$(this).is(":checked")) {
                isAllChoosen = false;
            }
            isCheckAny = isCheckAny || $(this).is(":checked");
        });
        return isAllChoosen;
    }
}