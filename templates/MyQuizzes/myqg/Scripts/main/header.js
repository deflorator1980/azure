﻿setTimeout(function () {
    $("input#generateTo").datepicker();
    $("input#gameStart").datepicker();
});

/*Menu animation*/
if ('ontouchstart' in window) { var click = 'touchstart'; }
else { var click = 'click'; }

$('div.burger').on(click, function () {

    if (!$(this).hasClass('open')) { openMenu(); }
    else { closeMenu(); }

});

function openMenu() {
    if (window.location.href.indexOf("/Wait") < 1) {
        $('#body').css('display', 'none');
    }
    $('div.circle').addClass('expand').css('z-index', '900');
    $('div.burger').addClass('open').css('z-index', '901');
    $('#menu-container').css({ 'position': 'absolute', 'z-index': '901' });
    $('div.x, div.y, div.z').addClass('collapse');
    $('#menu-container li').addClass('animate');

    setTimeout(function () {
        $('div.y').hide();
        $('div.x').addClass('rotate30');
        $('div.z').addClass('rotate150');
    }, 70);
    setTimeout(function () {
        $('div.x').addClass('rotate45');
        $('div.z').addClass('rotate135');
    }, 120);
}

function closeMenu() {

    if (window.location.href.indexOf("/Wait") < 1) {
        $('#body').css('display', 'block');
    }

    $('div.burger').removeClass('open').removeAttr('style');
    $('div.x').removeClass('rotate45').addClass('rotate30');
    $('div.z').removeClass('rotate135').addClass('rotate150');
    $('div.circle').removeClass('expand').removeAttr('style');
    $('#menu-container li').removeClass('animate');
    $('#menu-container').removeAttr('style');

    setTimeout(function () {
        $('div.x').removeClass('rotate30');
        $('div.z').removeClass('rotate150');
    }, 50);
    setTimeout(function () {
        $('div.y').show();
        $('div.x, div.y, div.z').removeClass('collapse');
    }, 70);
}