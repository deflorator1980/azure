﻿$(function () {
    $.cookie('user.timezone.id', moment.tz.guess(), {path: '/' });
});

function countdownStart(initMilliSeconds) {
    var countdownPosition = {
        quizDateTime: new Date((new Date()).getTime() + initMilliSeconds)
    };

    function process() {
        countdownPosition.totalSeconds = Math.floor((countdownPosition.quizDateTime - new Date()) / 1000);

        if (countdownPosition.totalSeconds > 0) {
            showCountDown();
        }
        else {
            showQuizExpired();
        }
    }

    function showCountDown() {
        $('#QuizExpiredBlock').addClass('hidden');
        $('#CountDownBlock').removeClass('hidden');
        updateCountdownPosition();
        drawDays(countdownPosition.totalDays);
        drawHours(countdownPosition.hours);
        drawMinutes(countdownPosition.minutes);
        drawSeconds(countdownPosition.seconds);

    }

    function showQuizExpired() {
        $('#QuizExpiredBlock').removeClass('hidden');
        $('#CountDownBlock').addClass('hidden');
    }

    function updateCountdownPosition() {

        countdownPosition.seconds = Math.floor(countdownPosition.totalSeconds % 60);
        countdownPosition.totalMinutes = Math.floor(countdownPosition.totalSeconds / 60);
        countdownPosition.minutes = Math.floor(countdownPosition.totalMinutes % 60);
        countdownPosition.totalHours = Math.floor(countdownPosition.totalMinutes / 60);
        countdownPosition.hours = Math.floor(countdownPosition.totalHours % 24);
        countdownPosition.totalDays = Math.floor(countdownPosition.totalHours / 24);
    }

    function drawDays(days) {
        if (days > 0) {
            $('#CountDownTimer__DaysBlock').show();
            var daysDigits = ('' + days).split('');

            for (var i = 0; i < daysDigits.length; i++) {
                var digitPlace = $('#CountDownTimer__Days > .CountDownTimer__Digit:nth-child(' + (i + 1) + ')');
                if (digitPlace.length === 0) {
                    digitPlace = $('<div class="CountDownTimer__Digit"></div>');
                    $('#CountDownTimer__Days').append(digitPlace);
                }
                digitPlace.text(daysDigits[i]);
            }


            $('#CountDownTimer__Days > .CountDownTimer__Digit:gt(' + (daysDigits.length - 1) + ')').remove();
        }
        else {
            $('#CountDownTimer__DaysBlock').hide();

        }
    }
    function drawHours(hours) {
        $('#CountDownTimer__Hours > .CountDownTimer__Digit:nth-child(1)').text(Math.floor(hours / 10));
        $('#CountDownTimer__Hours > .CountDownTimer__Digit:nth-child(2)').text(Math.floor(hours % 10));
    }
    function drawMinutes(minutes) {
        $('#CountDownTimer__Minutes > .CountDownTimer__Digit:nth-child(1)').text(Math.floor(minutes / 10));
        $('#CountDownTimer__Minutes > .CountDownTimer__Digit:nth-child(2)').text(Math.floor(minutes % 10));
    }
    function drawSeconds(seconds) {
        $('#CountDownTimer__Seconds > .CountDownTimer__Digit:nth-child(1)').text(Math.floor(seconds / 10));
        $('#CountDownTimer__Seconds > .CountDownTimer__Digit:nth-child(2)').text(Math.floor(seconds % 10));
    }

    setInterval(process, 1000);
    process();

    function checkRegistrationAvailability() {
        $.ajax({
            url: global_QuizCodeWaitStartCheck_url,
            success: function (result) {
                if (result.hostIsAvailable) {
                    $('#redirectToConnectForm').submit();
                }
            },
            error: function () { }
        });
    }

    setInterval(checkRegistrationAvailability, 10000);
};