﻿var Deferred1 = $.Deferred();
var Deferred2 = $.Deferred();
$(document).ready(function () {
    Deferred1.resolve();
});
var t;
function imageLoaded() {
    t = this;
    Deferred2.resolve();
};
$.when(Deferred1.promise(), Deferred2.promise()).done(function () {
    imageLoaded = function () {
        resize.call(this, true);
        $(this).addClass('loaded');
    }
    imageLoaded.call(t);
});
$(window).off('resize');
$(window)
    .resize(function () {
        resize(false);
    });
function resize(element) {
    if (!element) {
        $('.the-photo img.loaded')
            .each(function () {
                resize.call(this, true);
            });
    } else {
        var $this = $(this);
        var oHeight = this.naturalHeight;
        var oWidth = this.naturalWidth;
        setTimeout(function () {
            var pHeight = $this.parents('.current-game').height();
            var pWidth = $('.current-game-body.clearfix:first').width();
            if (pHeight > $(window).height() * 0.8)
                pHeight = $(window).height() * 0.8;

            var hm = pHeight / oHeight;
            var wm = pWidth / oWidth;
            var m = wm > hm ? hm : wm;
            $this.parent().css({ height: oHeight * m - 20 + 'px ', width: oWidth * m - 20 + 'px' });
            $this.show();
        });
    }
}