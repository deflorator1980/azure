﻿$(function () {
    $('#prizePlaceSendEmail__Button').click(function () {
        if ($('#prizePlaceSendEmail__Form').valid()) {

            $.ajax({
                url: '/Archive/SendQuizWinnerCode',
                type: 'POST',
                data: {
                    __RequestVerificationToken: $('#prizePlaceSendEmail__Form > input[name=\'__RequestVerificationToken\']').val(),
                    WinnerCode: $('#WinnerCode').val(),
                    Position: $('#Position').val(),
                    Email: $('#Email').val(),
                    ArhiveId: $('#ArchiveId').val()
                },
                success: function () {
                    $('#prizePlaceSendEmail__Info').hide();
                    $('#prizePlaceSendEmail__SuccessResult').show();
                }
            });
        }
    });


    $('#successResult__CloseButton').click(function () {
        $('#prizePlaceSendEmail__Info').show();
        $('#prizePlaceSendEmail__SuccessResult').hide();
    });
});