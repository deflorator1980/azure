﻿var testimonialModule = (

	function (testimonialModule) {

	    $(function () {

	        testimonialModule.setTestimonialCountLocalization();

	        $('#search').keydown(testimonialModule.search);
	        $('#search').keypress(testimonialModule.search);

	        $('#createButtonContainer').click(function () { testimonialModule.getTestimonialTemplate($(this), "/Testimonial/New") });

	        $('#testimonial-list-container').on('click', '*[action-testimonial-edit]', function () { testimonialModule.getTestimonialTemplate($(this), "/Testimonial/Edit") });
	        $('#testimonial-list-container').on('click', '*[action-testimonial-delete]', function () { testimonialModule.showInlineConfirmation($(this)) });
	        $('#testimonial-list-container').on('click', '*[action-testimonial-delete-confirm]', function () { testimonialModule.deleteTestimonial($(this)) });
	        $('#testimonial-list-container').on('click', '*[action-testimonial-delete-cancel]', function () { testimonialModule.hideInlineConfirmation($(this)) });
	    });

	    testimonialModule.search = function () {
	        $('#search').parent().addClass('search').removeClass('close');
	    };

	    testimonialModule.showPopup = function (element, result, action) {
	        $('#testimonialModal').html(result).toggleClass('modal--close');
	        $('#testimonialModal *[close-popup]').click(testimonialModule.hidePopup);
	        $('#saveTestimonial').click(function () { testimonialModule.saveOrUpdate(element, action) });
	    };

	    testimonialModule.hidePopup = function () {
	        $('#testimonialAction__Progress').hide();
	        $('#testimonialModal').toggleClass('modal--close');
	    }

	    testimonialModule.saveOrUpdate = function (element, action) {

	        $('#testimonialAction_Progress').show();
	        var $entityCount = $('.countContainer div');

	        var id = $(element).data('testimonial-id');
	        var isUpdate = id != undefined ? true : false;
	        var currentTestimonialListItem;

	        $.ajax({
	            type: "POST",
	            url: action,
	            data: JSON.stringify({
	                __RequestVerificationToken: $('#testimonial__Form > input[name=\'__RequestVerificationToken\']').val(),
	                Id: id,
	                UserName: $('#UserName').val(),
	                UserPosition: $('#UserPosition').val(),
	                Text: $('#Text').val(),
	                Language: $('input[name=Language]:checked').next('label').text()
	            }),
	            async: false,
	            contentType: "application/json; charset=utf-8",
	            dataType: 'html',
	            success: function (result) {

	                currentTestimonialListItem = $(element).parents('[testimonial-list-item]');
	                testimonialModule.insertOrUpdateRecord(currentTestimonialListItem, isUpdate, result);

	                if (!isUpdate) { $entityCount.each(function () { $(this).text(parseInt($(this).text()) + 1); }); }

	                testimonialModule.setTestimonialCountLocalization();
	                testimonialModule.hidePopup();
	            },
	            failure: function (result) {
	                console.log('failure: ', result);
	                testimonialModule.hidePopup();
	            },
	            error: function (result) {
	                console.log('error: ', result);
	                testimonialModule.hidePopup();
	            }
	        });
	    }

	    testimonialModule.insertOrUpdateRecord = function (currentTestimonialListItem, isUpdate, html) {
	        var $peviousTestimonialListItem = $(currentTestimonialListItem).prev('[testimonial-list-item]');
	        var $firstTestimonialListItem = $('#testimonial-list-container [testimonial-list-item]').first();


	        if (isUpdate) {
	            $(currentTestimonialListItem).remove();

	            if ($peviousTestimonialListItem.length !== 0) {
	                $($peviousTestimonialListItem).after(html);
	            }
	            else {
	                $('#testimonial-list-container [testimonial-list-item]').first().before(html);
	            }
	        }
	        else {
                if ($firstTestimonialListItem.length !== 0) {
                    $firstTestimonialListItem.before(html);
	            } else {
	                $('#testimonial-list-container').append(html);

	            }
	        }
	    }

	    testimonialModule.deleteTestimonial = function (element) {
	        var $deleteButton = $(element);
	        var $entityCount = $('.countContainer div');
	        var searchString = $('#search').val();
	        var id = $deleteButton.data('testimonial-id');
	        console.log($deleteButton);
	        console.log(id);
	        $.ajax({
	            url: '/Testimonial/Delete',
	            data: { id: id, searchString: searchString },
	            success: function (result) {
	                if (result.success) {
	                    $deleteButton.closest('.gridTimeLine_row').remove();
	                    $entityCount.text(result.testimonialCount);
	                    testimonialModule.setTestimonialCountLocalization();
	                } else
	                    console.log(result.message);
	            },
	            error: function () { }
	        });

	        return false;
	    }

	    testimonialModule.showInlineConfirmation = function (element) {
	        $('.gridData__colConfirm').addClass('gridData__col--hidden');
	        $('.gridData__colButtons').removeClass('gridData__col--hidden');
	        var row = $(element).parents('.gridData__row');
	        row.find('.gridData__colButtons').addClass('gridData__col--hidden');
	        row.find('.gridData__colConfirm').removeClass('gridData__col--hidden');
	    }

	    testimonialModule.hideInlineConfirmation = function (element) {
	        var row = $(element).parents('.gridData__row');
	        row.find('.gridData__colButtons').removeClass('gridData__col--hidden');
	        row.find('.gridData__colConfirm').addClass('gridData__col--hidden');
	    }

	    testimonialModule.getTestimonialTemplate = function (element, action) {
	        var id = $(element).data('testimonial-id');

	        $.ajax({
	            type: "GET",
	            url: action,
	            data: { "id": id || null },
	            async: false,
	            contentType: "application/json; charset=utf-8",
	            dataType: 'html',
	            success: function (result) {
	                testimonialModule.showPopup(element, result, action);
	            },
	            failure: function (result) {
	                console.log('failure: ', result);
	            },
	            error: function (result) {
	                console.log('error: ', result);
	            }
	        });
	    }

	    testimonialModule.setTestimonialCountLocalization = function () {
	        $('.countContainer div').each(function () {
	            var $this = $(this);
	            var titleDesktopContainer = $this.parent().next('td');
	            var titleMobileContainer = $this.next('span');
	            var iCount;
	            var count = $this.text() % 100;
	            if (count >= 11 && count <= 19) {
	                titleDesktopContainer.text('');
	                titleMobileContainer.text('');
	                titleDesktopContainer.append(global_localization_Testimonials);
	                titleMobileContainer.append(global_localization_Testimonials);
	            } else {
	                iCount = count % 10;
	                switch (iCount) {
	                    case (0):
	                        titleDesktopContainer.text('');
	                        titleMobileContainer.text('');
	                        titleDesktopContainer.append(global_localization_Testimonials);
	                        titleMobileContainer.append(global_localization_Testimonials);
	                        break;
	                    case (1):
	                        titleDesktopContainer.text('');
	                        titleMobileContainer.text('');
	                        titleDesktopContainer.append(global_localization_Testimonial);
	                        titleMobileContainer.append(global_localization_Testimonial);
	                        break;
	                    case (2):
	                    case (3):
	                    case (4):
	                        titleDesktopContainer.text('');
	                        titleMobileContainer.text('');
	                        titleDesktopContainer.append(global_localization_Many_Testimonials);
	                        titleMobileContainer.append(global_localization_Many_Testimonials);
	                        break;
	                    default:
	                        titleDesktopContainer.text('');
	                        titleMobileContainer.text('');
	                        titleDesktopContainer.append(global_localization_Testimonials);
	                        titleMobileContainer.append(global_localization_Testimonials);
	                        break;
	                }
	            }
	        });
	    }

	    return testimonialModule;

	}(testimonialModule || {})
);