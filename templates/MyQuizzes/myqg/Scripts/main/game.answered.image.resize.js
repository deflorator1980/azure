﻿function imageLoaded() {
    resize.call(this, true);
    $(this).addClass('loaded');
}

$(window).off('resize');
$(window)
    .resize(function () {
        resize(false);
    });
function resize(element) {
    if (!element) {
        $('.the-photo img.loaded')
            .each(function () {
                resize.call(this, true);
            });
    } else {
        var $this = $(this);
        var oHeight = this.naturalHeight;
        var oWidth = this.naturalWidth;
        setTimeout(function () {
            var pHeight = $this.parents('.current-game').height();
            var pWidth = $('.current-game-body.clearfix:first').width();
            if (pHeight > $(window).height() * 0.8)
                pHeight = $(window).height() * 0.8;

            var hm = pHeight / oHeight;
            var wm = pWidth / oWidth;
            var m = wm > hm ? hm : wm;
            $this.parent().css({ height: oHeight * m - 20 + 'px ', width: oWidth * m - 20 + 'px' });
            $this.show();
        });
    }
}