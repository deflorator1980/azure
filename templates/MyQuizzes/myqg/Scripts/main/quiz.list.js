﻿function onSearchChange() {
    $('.closeButton').parent().addClass('search').removeClass('close');
}

function importQuizFormSubmit() {
    $('#importQuizForm').submit();
}

function showInlineConfirmation(btn) {
    $('.gridData__colConfirm').addClass('gridData__col--hidden');
    $('.gridData__colButtons').removeClass('gridData__col--hidden');
    var row = $(btn).parents('.gridData__row');
    row.find('.gridData__colButtons').addClass('gridData__col--hidden');
    row.find('.gridData__colConfirm').removeClass('gridData__col--hidden');
}

function hideInlineConfirmation(btn) {
    var row = $(btn).parents('.gridData__row');
    row.find('.gridData__colButtons').removeClass('gridData__col--hidden');
    row.find('.gridData__colConfirm').addClass('gridData__col--hidden');
}


function showShareQuizModal(quizId) {

    $.ajax({
        url: '/Lectures/GenerateSharedLink',
        data: { id: quizId },
        success: function (result) {
            if (result.success) {
                var shareUrl = global_shareQuizUrl + result.quiz.ShareId;
                var shareText = result.quiz.Theme;
                $('#shareQuizModal__link').text(shareUrl);
                initShareButtonFacebook('#shareButton__faceBook', shareUrl);
                initShareButtonVK('#shareQuizModal__shareButtonPlaceholder__Vk', shareUrl);
                initShareButtonTwitter('#shareButton__twitter', shareUrl, shareText);

                $('#shareQuizModal').data('quiz', result.quiz);
                $('#shareQuizModal').toggleClass('modal--close');
            } else
                console.log(result.message);
        },
        error: function () { }
    });
    return false;
}

function showInviteQuizModal(quizId) {

    $.ajax({
        url: '/Lectures/GenerateInviteLink',
        data: { id: quizId },
        success: function (result) {
            if (result.success) {
                var shareUrl = global_inviteQuizUrl + result.quiz.Code;
                var shareText = result.quiz.Theme;
                $('#inviteQuizModal__link').text(shareUrl);
                initShareButtonFacebook('#inviteButton__faceBook', shareUrl);
                initShareButtonVK('#inviteQuizModal__inviteButtonPlaceholder__Vk', shareUrl);
                initShareButtonTwitter('#inviteButton__twitter', shareUrl, shareText);

                $('#inviteQuizModal').data('quiz', result.quiz);
                $('#inviteQuizModal').toggleClass('modal--close');
            } else
                console.log(result.message);
        },
        error: function () { }
    });
    return false;
}

function showVideoProcessingQuizModal(quizId) {
    $('#videoProcessingQuizModal .navigation-buttons-ok').attr('data-quizid', quizId);
    $('#videoProcessingQuizModal').toggleClass('modal--close');
}

function hideShareQuizModal(element) {
    $('#shareQuizModal').toggleClass('modal--close');
    $(document.body).removeClass('fixed');
    return false;
}

function hideInviteQuizModal(element) {
    $('#inviteQuizModal').toggleClass('modal--close');
    $(document.body).removeClass('fixed');
    return false;
}

function hideVideoProcessingQuizModal(element) {
    $('#videoProcessingQuizModal .navigation-buttons-ok').attr('data-quizid', '');
    $('#videoProcessingQuizModal').toggleClass('modal--close');
    $(document.body).removeClass('fixed');
    return false;
}

function deleteQuiz(deleteButton) {
    var $deleteButton = $(deleteButton);
    var $entityCount = $('.countContainer div');
    var searchString = $('#search').val();
    var id = $deleteButton.data('quiz-id');
    console.log($deleteButton);
    console.log(id);
    $.ajax({
        url: '/Lectures/DeleteQuiz',
        data: { id: id, searchString: searchString, stopQuiz: false },
        success: function (result) {
            if (result.success) {
                $deleteButton.closest('.gridTimeLine_row').remove();
                $entityCount.text(result.quizCount);
                SetQuizCountLocalization();
                $(window).resize(); //For footer css class position changing;
            } else
                console.log(result.message);
        },
        error: function () { }
    });

    return false;
}

function SetQuizCountLocalization() {
    $('.countContainer div').each(function () {
        var $this = $(this);
        var titleDesktopContainer = $this.parent().next('td');
        var titleMobileContainer = $this.next('span');
        var iCount;
        var count = $this.text() % 100;
        if (count >= 11 && count <= 19) {
            titleDesktopContainer.text('');
            titleMobileContainer.text('');
            titleDesktopContainer.append(global_localization_Quizzes);
            titleMobileContainer.append(global_localization_Quizzes);
        } else {
            iCount = count % 10;
            switch (iCount) {
                case (0):
                    titleDesktopContainer.text('');
                    titleMobileContainer.text('');
                    titleDesktopContainer.append(global_localization_Quizzes);
                    titleMobileContainer.append(global_localization_Quizzes);
                    break;
                case (1):
                    titleDesktopContainer.text('');
                    titleMobileContainer.text('');
                    titleDesktopContainer.append(global_localization_Quiz);
                    titleMobileContainer.append(global_localization_Quiz);
                    break;
                case (2):
                case (3):
                case (4):
                    titleDesktopContainer.text('');
                    titleMobileContainer.text('');
                    titleDesktopContainer.append(global_localization_Many_Quizzes);
                    titleMobileContainer.append(global_localization_Many_Quizzes);
                    break;
                default:
                    titleDesktopContainer.text('');
                    titleMobileContainer.text('');
                    titleDesktopContainer.append(global_localization_Quizzes);
                    titleMobileContainer.append(global_localization_Quizzes);
                    break;
            }
        }
    });
}

function onStartQuizInitClick(quizId, canPlay, element) {
    if (!canPlay) {
        ShowError(element);
        
    } else {
        CheckVideoProcessingQuiz(quizId);
    }

    return false;
}

function CheckVideoProcessingQuiz(quizId) {
    $.ajax({
        url: '/Lectures/CheckIsVideoProcessedQuiz',
        method : 'GET',
        data: { id: quizId },
        success: function (result) {
            if (result.processing) {
                showVideoProcessingQuizModal(quizId);
            } else {
                document.location = '/HostInstance/Init/' + quizId;
            }
        },
        error: function() {
            console.log("Error checking video in processing for quiz "  + quizId );
        }
    });
}

function onVideoProcessingQuizModalBtnOkClick(el) {
    var quizId = $(el).attr('data-quizid');
    if (quizId) {
        document.location = '/HostInstance/Init/' + quizId;
    }
}

function ShowError(element) {
    $(element).parent().find('span[errorType=\'noQuestionsError\']').fadeIn('fast');
};

$(document).ready(function () {
    SetQuizCountLocalization();
    var clipBoards = ['shareQuizModal', 'inviteQuizModal'];
    $.each(clipBoards, function (index, value) {
        var clipboard = new Clipboard('#' + value + '__linkCopy');

        clipboard.on('success', function (e) {
            $('#' + value + '__copyResult')
            .clearQueue()
            .stop(true, true)
            .fadeIn(1000)
            .delay(3000)
            .fadeOut(1000);
            e.clearSelection();
        });

        $('#' + value + '__link').click(function () {
            selectText(value + '__link');
        });
    });
});


function selectText(containerId) {
    if (document.selection) {
        var range = document.body.createTextRange();
        range.moveToElementText(document.getElementById(containerId));
        range.select();
    } else if (window.getSelection) {
        var range = document.createRange();
        range.selectNode(document.getElementById(containerId));
        window.getSelection().addRange(range);
    }
}
