﻿var GlobalUtils = (
    function () {
    
        self.getCurrentTimeZone = function() {
            var result = '';
            var currentDateTime = new Date();
            var timeZoneOffset = -1 * currentDateTime.getTimezoneOffset();
            var timeZoneName = moment.tz.guess();

            var timeZoneOffsetHours = '' + Math.abs(timeZoneOffset / 60);
            if (timeZoneOffsetHours.length === 1) {
                timeZoneOffsetHours = '0' + timeZoneOffsetHours;
            }

            var timeZoneOffsetMinutes = '' + Math.abs(timeZoneOffset % 60);
            if (timeZoneOffsetMinutes.length === 1) {
                timeZoneOffsetMinutes = '0' + timeZoneOffsetMinutes;
            }

            if (timeZoneOffset > 0) {
                result += '+';
            }
            else if (timeZoneOffset < 0) {
                result += '-';
            }

            result = {
                timeZoneOffset: result + timeZoneOffsetHours + ':' + timeZoneOffsetMinutes,
                timeZoneName: timeZoneName
            };


            return result;
        }

        self.uuidv4 = function() {
            return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
                var r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8);
                return v.toString(16);
            });
        }

        return self;
    }
)();