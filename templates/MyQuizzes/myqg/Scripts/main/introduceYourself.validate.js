﻿var introduceYourselfModule = (function (introduceYourselfModule) {

    introduceYourselfModule.validateForm = function () {
        var $form = $('form:last');

        var language = $.cookie('lang');

        $form.validate({
            lang: language
        });
    }

    $('#register').bind('click', introduceYourselfModule.validateForm);

    return introduceYourselfModule;

}(introduceYourselfModule || {}));