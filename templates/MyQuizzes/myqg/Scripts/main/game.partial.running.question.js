﻿function hideMessage() {
    $('#popup-wrap ~ table').animate({ 'top': '-520%' }).fadeOut().unbind("click", hideMessage);
    $('#popup-wrap ~ table').hide();
    $('#popup-wrap').hide();
}

function showMessage() {
    setTimeout(function () {
        $('#popup-wrap ~ table').attr('style', 'display:table !important');
        $('#popup-wrap ~ table').show().animate({ 'top': '0%' }).click(hideMessage);
        $('#popup-wrap').show();
        setTimeout(hideMessage, 5000);
    });
}

function showAnswerButtonLoader() {
    setTimeout(function () {
        $("#answer-button").attr('disabled', 'disabled');
        $("#answer-button").children('span').addClass('hide');
        $('.cssload-container').addClass('show');
    });
}
var $checkeds = undefined;
var isSingleChoiseQuestion = undefined;

function updateTextInTimer() {
    if ($('#questionText').length > 0) {
        $('.the-time-text h4').text($('#questionText').text());
    }
}

function bindHandlersToAnswerElements() {
    $checkeds = $("input[id^=AnswerIds]");
    isSingleChoiseQuestion = $('#isSingleChoise').val() == 'True';
    $("#answer-button").on("click", function (e) {
        if (checkPosibilityToChoose()) {
            return false;
        }

        if ($checkeds.is(":checked")) {
            $(this).css("background-color", "#B8B8B8");
        } else {
            e.preventDefault();
        }
    });

    $checkeds.on("change", function (e) {
        if (isSingleChoiseQuestion) {
            var changedElement = this;
            $checkeds.each(function (i, el) {
                if (changedElement != this) {
                    $(this).attr("checked", false);
                }
            });
        }

        checkPosibilityToChoose();
    });
}

function checkPosibilityToChoose() {
    var isAllChoosen = true;
    var isCheckAny = false;
    $checkeds.each(function (i, el) {
        if (!$(this).is(":checked")) {
            isAllChoosen = false;
        }
        isCheckAny = isCheckAny || $(this).is(":checked");
    });
    if (isCheckAny && !isAllChoosen) {
        $('#answer-button').addClass('active');
        $('#answer-button').removeAttr('disabled', 'disabled');
        $('[name~="ErrorMaximumAnswers"]').addClass('hide');

    } else {
        $('#answer-button').removeClass('active');
        $('#answer-button').attr('disabled', 'disabled');
    }


    if (isAllChoosen) {
        showDialog();
    }
    return isAllChoosen;
}

function showDialog() {
    $('[name~="ErrorMaximumAnswers"]').removeClass('hide');
}