﻿$(function () {
    var timeZone = GlobalUtils.getCurrentTimeZone();
    $('.PublicQuizBox__CopyButton').each(function () {
        var href = $(this).attr('href') + '?timeZone=' + timeZone.timeZoneOffset;
        $(this).attr('href', href);
    });

});