﻿// set the date we're counting down to


var CountdownTimer;

$(function () {
    CountdownTimer = function (countDownSelector, milliseconds, questionTimeLocView, restingTimeLocView, onelapsed) {
        var self = this;
        var config = {
            isRestingTime: undefined,
            totalTime: undefined,
            currentIntervalId: undefined,
            onElapsedHandler: undefined,
            secondsLeft: undefined,
            percentage: undefined,
            desktopView: {
                countDownElement: undefined
            },
            mobileView: {
                timeLeftProgressBarElement: undefined,
                timeDescriptionElement: undefined,
                loaderElement: undefined,
                loaderClass: undefined,
                timeLocViewText: undefined,
                questionTimeLocView: undefined,
                restingTimeLocView: undefined
            }
        };

        function _init(countDownSelector, questionTimeLocView, restingTimeLocView, onElapsedHandler) {
            config.onElapsedHandler = onElapsedHandler;

            config.desktopView.countDownElement = $(countDownSelector);

            config.mobileView.timeLeftProgressBarElement = $('#time-left-progress-bar');
            config.mobileView.loaderElement = config.mobileView.timeLeftProgressBarElement.find('#loader');
            config.mobileView.timeDescriptionElement = $('#mobile-time-left span');
            config.mobileView.questionTimeLocView = questionTimeLocView;
            config.mobileView.restingTimeLocView = restingTimeLocView;

            updateRestingTimeRelatedConfig();
        }

        function updateRestingTimeRelatedConfig() {
            config.isRestingTime = getIsRestingTime();
            config.totalTime = getTotalTime();
            config.mobileView.timeLocViewText = getTimeDescriptionText();
            config.mobileView.loaderClass = getLoaderClass();
        };

        function getIsRestingTime() {
            return $('#RestingTime').val() !== "False";
        };
        function getTotalTime() {
            return config.isRestingTime ? window.TotalRestingTime : window.TotalTime;
        };
        function getTimeDescriptionText() {
            return config.isRestingTime ? restingTimeLocView : questionTimeLocView;
        };
        function getLoaderClass() {
            return config.isRestingTime ? 'yellow-loader' : 'blue-loader';
        };
        function calcPercentage() {
            return 100 - config.secondsLeft * 100 / config.totalTime;
        };

        function timerStep(targetDate) {
            var currentDate = new Date().getTime();
            var milliSecondsLeft = (targetDate - currentDate);

            if ((milliSecondsLeft) < 0) {
                milliSecondsLeft = 0;
                self.Stop();
                if (config.onElapsedHandler) {
                    config.onElapsedHandler();
                }
            }
            config.secondsLeft = parseInt(milliSecondsLeft / 1000);
            config.percentage = calcPercentage();
            updateRestingTimeRelatedConfig();
            displayProgress();
        };


        function displayProgress() {
            config.desktopView.countDownElement.text(config.secondsLeft);
            var progressBarWidthOld = config.mobileView.loaderElement.width();
            var paddingInPercents = 6 * config.mobileView.timeLeftProgressBarElement.width() / 100;
            var progressBarWidthNew = config.percentage * (config.mobileView.timeLeftProgressBarElement.width() - paddingInPercents) / 100;

            if (progressBarWidthOld > progressBarWidthNew) {
                config.mobileView.loaderElement.stop(true, true).width(progressBarWidthNew);
            }
            var animationSpeed = 300;
            config.mobileView.loaderElement
                .stop(true, true)
                .animate({ width: progressBarWidthNew }, animationSpeed)
                .removeClass()
                .addClass(config.mobileView.loaderClass);
            config.mobileView.timeDescriptionElement.text(config.mobileView.timeLocViewText);
        };

        self.Start = function (miliseconds) {
            var target_date = new Date().getTime() + miliseconds;
            self.Stop();
            timerStep(target_date);
            start(miliseconds/1000);
            config.currentIntervalId = setInterval(function () {
                timerStep(target_date);
            }, 1000);
        };
        function start(timeleft) {
            setTimeout(function () {
                var item = $('.ani-timer');
                if (timeleft > getTotalTime()) {
                    timeleft = getTotalTime();
                }
                var time =  getTotalTime()-timeleft;
                var tmp = time * 100 / getTotalTime();
                var deg = 360 / 100 * tmp;
                var percent = (getTotalTime() / 2 - time) / timeleft * 100;
                if (tmp >= 50) {
                    $('.pie').css({ 'animation': '', 'opacity': '1' });
                    $('.mask').css({ 'animation': '', 'opacity': '0' });
                    $('.spinner').css('animation', 'rota ' + (timeleft) + 's linear infinite');
                }
                else {
                    $('.mask').css('animation', 'opa ' + (timeleft) + 's steps(1, end) infinite');
                    $('.filler').css('animation', 'opa2 ' + (timeleft) + 's steps(1, end) infinite ');
                    $('.spinner').css('animation', 'rota ' + (timeleft) + 's linear infinite');
                }
                $('#rotate').text('@keyframes rota {from {transform: rotate(' + deg + 'deg);}to{transform: rotate(360deg);}}'
                    + '@keyframes opa {0% {opacity: 1;}' + percent + '% {opacity: 0;}}'
                    + '@keyframes opa2 {0% {opacity: 0;}' + percent + '% {opacity: 1;}'
                    );
                setTimeout(function () {
                    $('.pie').css({ 'animation': '', 'opacity': '1' });
                    $('.mask').css({ 'animation': '', 'opacity': '0' });
                    $('#rotate').text('');
                }, timeleft * 1000);
            });
        }
        self.Stop = function () {
            clearInterval(config.currentIntervalId);
            $('.pie').css({ 'animation': '' });
            $('.mask').css({ 'animation': '' });
            $('#rotate').text('');
        };

        _init(countDownSelector, questionTimeLocView, restingTimeLocView, onelapsed);

        if (milliseconds) {
            self.Start(milliseconds);
        }
    }
    GameControl = function (timeLeftToAnswer, tomeLeftToViewAnswer, callback) {
        var self = this;
        self.StartTimer = function (time) {
            self.IsRestingTime = $('#RestingTime').val() == 'True';
            if (!self.IsRestingTime)
                time = time - 2000;
            self.timer.Stop();
            if (time < 0) {
                callback(Math.abs(time))
            }
            else
                self.timer.Start(time);         
        };       
        self.timer = new CountdownTimer(".the-time", 0, timeLeftToAnswer, tomeLeftToViewAnswer, callback);
    }
});
