$(function () {
    checkMobile();

    var carousel = $('.social-carousel').bxSlider({
        mode: 'horizontal'
    });

    var feedbackCarousel = $('.feedback-social-carousel').bxSlider({
        maxSlides: 1,
        mode: 'horizontal',
        pager: true,
        useCSS: false
    });

    function resizeCarousel() {
        var width = $(document).width(),
            slideItemWidth = ($('.social-carousel-wrapper').width() - 40) / 3,
            feedbackSlideItemWidth = ($('.feedback-social-carousel-wrapper').width() - 40) / 3;

        if (width >= 768) {
            carousel.reloadSlider({
                minSlides: 3,
                maxSlides: 3,
                slideWidth: slideItemWidth,
                moveSlides: 1,
                pager: false
            });
        }
        if (width < 767) {
            carousel.reloadSlider({
                minSlides: 1,
                maxSlides: 1,
                slideWidth: 0,
                moveSlides: 1,
                pager: false
            });
        }
    }
    $(window).on("orientationchange load resize", function () {
        resizeCarousel();
    });

    function checkMobile() {
        if (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) {
            $('html').addClass('touch');
            return true
        } else {
            $('html').addClass('no-touch');
            return false
        }
    };

    /*Menu animation*/
    if ('ontouchstart' in window) { var click = 'touchstart'; }
    else { var click = 'click'; }

    $('div.burger').on(click, function () {

        if (!$(this).hasClass('open')) { openMenu(); }
        else { closeMenu(); }

    });

    function openMenu() {
        $('section').hide();
        $('footer').hide();
        $('div.circle').addClass('expand').css('z-index', '900');
        $('div.burger').addClass('open').css('z-index', '901');
        $('#menu-container').css({ 'position': 'absolute', 'z-index': '901' });
        $('div.x, div.y, div.z').addClass('collapse');
        $('#menu-container li').addClass('animate');

        setTimeout(function () {
            $('div.y').hide();
            $('div.x').addClass('rotate30');
            $('div.z').addClass('rotate150');
        }, 70);
        setTimeout(function () {
            $('div.x').addClass('rotate45');
            $('div.z').addClass('rotate135');
        }, 120);
    }

    function closeMenu() {
        $('section').show();
        $('footer').show();
        $('div.burger').removeClass('open').removeAttr('style');
        $('div.x').removeClass('rotate45').addClass('rotate30');
        $('div.z').removeClass('rotate135').addClass('rotate150');
        $('div.circle').removeClass('expand').removeAttr('style');
        $('#menu-container li').removeClass('animate');
        $('#menu-container').removeAttr('style');

        setTimeout(function () {
            $('div.x').removeClass('rotate30');
            $('div.z').removeClass('rotate150');
        }, 50);
        setTimeout(function () {
            $('div.y').show();
            $('div.x, div.y, div.z').removeClass('collapse');
        }, 70);
    }

})