﻿var homeModule = (
    function (homeModule) {
        
        $(function () {
            $('#AnnouncementsCarousel .item:first-child').addClass('active');
            $('#TestimonialCarousel .item:first-child').addClass('active');
            $('#TestimonialCarousel__Indicators span:first-child').addClass('active');
            $("#code-form-container").find("button").first().click(homeModule.setUserDevice);
            $('.Connect__JoinButton').first().click(homeModule.setUserDevice);

            $('.Event').each(function () {
                var $eventElement = $(this);
                var $eventShowElement = $eventElement.find('.Event__Show');
                var $eventInfoBlock = $eventShowElement.parents('.Event__InfoBlock');
                var $eventDetailsBlock = $eventShowElement.parents('.Event__DetailsBlock');
                var $eventNameBlock = $eventDetailsBlock.find('.Event__NameBlock');
                var $eventImageBlock = $eventDetailsBlock.find('.Event__ImageBlock');
                var $eventTextDetailsBlock = $eventDetailsBlock.find('.Event__TextDetailsBlock');
                var $eventDescriptionBlock = $eventDetailsBlock.find('.Event__DescriptionBlock');
                var $span = $eventShowElement.find('span');


                $eventShowElement.on('click', function () {
                    $span.toggleClass('icon-rounded-plus');
                    $span.toggleClass('icon-rounded-minus');
                    $eventNameBlock.toggleClass('Event__NameBlock--top');
                    $eventDescriptionBlock.toggleClass('Event__DescriptionBlock--fill');
                    $eventTextDetailsBlock.toggleClass('Event__TextDetailsBlock--fill');
                    $eventImageBlock.toggleClass('Event__ImageBlock--fill');
                });
            });

            $('.body .item:first-child').addClass('active');

            $('.announcement').each(function() {
                var $ann = $(this);
                $ann.find('.details-button').click(function() {
                    $ann.find('.pointer')
                        .toggleClass('icon-rounded-plus')
                        .toggleClass('icon-rounded-minus');
                    $ann.find('.quiz-description').toggleClass('open');
                    $ann.find('.quiz-title').toggleClass('open');
                    $ann.find('.code').toggleClass('open');
                });
            });

            var currentPlayer;
            function setYoutubePlayer(){
                if((typeof YT !== "undefined") && YT && YT.Player) {
                    $('.carouselPlayerFrame').each(function() {
                        $(this).data("yPlayer", new YT.Player(this));
                    });
                }
                else {
                    setTimeout(setYoutubePlayer, 100);
                }
            };
            setYoutubePlayer();

            $('#mobileCarousel.carousel').carousel(announcementMobileActiveIndex || 0);

            $(".carousel").on("touchstart", function (event) {
                var xClick = event.originalEvent.touches[0].pageX;
                $(this).one("touchmove", function (event) {
                    var xMove = event.originalEvent.touches[0].pageX;
                    if (Math.floor(xClick - xMove) > 5) {
                        $(this).carousel('next');
                    }
                    else if (Math.floor(xClick - xMove) < -5) {
                        $(this).carousel('prev');
                    } else {
                        return;
                    }

                    if (currentPlayer) {
                        if (currentPlayer.stopVideo) {
                            currentPlayer.stopVideo();
                        }
                        currentPlayer = null;

                        $('.carouselPlayer').off("touchend");
                    }
                });
                $(".carousel").on("touchend", function () {
                    $(this).off("touchmove");
                });
            });

            $(".carouselPlayer").on("touchstart", function (event) {
                $(this).on("touchend", function () {
                    currentPlayer = $(this).parent().find('iframe').data("yPlayer");
                    if (currentPlayer && currentPlayer.playVideo) {
                        if (currentPlayer.getPlayerState() == 1) {
                            currentPlayer.pauseVideo();
                        }
                        else {
                            currentPlayer.playVideo();
                        }
                    }
                    $(this).off("touchend");
                });
            });

            $('#mobileCarousel.carousel, #AnnouncementsCarousel.carousel').on('slide.bs.carousel',
                function(ev) {
                    // custom videos pause
                    $.each(window.videojs.players,
                        function(index, item) {
                            item.pause();
                        });
                    // youtube videos pause
                    $('.carouselPlayerFrame').each(function() {
                        if ($(this).data("yPlayer").pauseVideo) {
                            $(this).data("yPlayer").pauseVideo();
                        };
                    });

                    fixAMPStyling();
                });

            $("video.azuremediaplayer").each(function() {
                var ampOptions = {
                    inactivityTimeout: 2e3,
                    logo: { enabled: false },
                    "nativeControlsForTouch": false, 
                    fluid: true, 
                    controls: true, 
                    autoplay: false,
                    language: $(this).attr('language'),
                    poster: $(this).attr('thumbnailSrc')
                }

                var myPlayer = amp($(this)[0], ampOptions);
                myPlayer.src([
                    {
                        src: $(this).attr('storedSrc'),
                        type: "application/vnd.ms-sstr+xml"
                    }
                ]);
            });
            fixAMPStyling();
        });

        function fixAMPStyling() {
            setTimeout(function () {
                //fix incorrect visualization azure media player
                window.dispatchEvent(new Event('resize'));
            }, 100);            
        }

        homeModule.setUserDevice = function() {
            var deviceInfo = [
            {
                "index": navigator.userAgent.toLowerCase().indexOf("ipad"),
                "deviceName": "iOS"
            },
            {
                "index": navigator.userAgent.toLowerCase().indexOf("iphone"),
                "deviceName": "iOS"
            },
            {
                "index": navigator.userAgent.toLowerCase().indexOf("ipod"),
                "deviceName": "iOS"
            },
            {
                "index": navigator.userAgent.toLowerCase().indexOf("android"),
                "deviceName": "Android"
            },
            {
                "index": navigator.userAgent.toLowerCase().indexOf("blackberry"),
                "deviceName": "Blackberry"
            },
            {
                "index": navigator.userAgent.toLowerCase().indexOf("webos"),
                "deviceName": "WebOs"
            },
            {
                "index": navigator.userAgent.toLowerCase().indexOf("windows phone"),
                "deviceName": "WindowsPhone"
            },
            {
                "index": navigator.userAgent.toLowerCase().indexOf("iemobile"),
                "deviceName": "WindowsPhone"
            },
            {
                "index": navigator.userAgent.toLowerCase().indexOf("wpdesktop"),
                "deviceName": "WindowsPhone"
            },
            {
                "index": navigator.userAgent.toLowerCase().indexOf("windows"),
                "deviceName": "Windows"
            },
            {
                "index": navigator.userAgent.toLowerCase().indexOf("mac"),
                "deviceName": "Mac"
            }];

            var firstBracketIndex = navigator.userAgent.toLowerCase().indexOf("(");
            var minIndex = 99999999;
            var arrayIndex;
            var userDevice = 'Сan not identify mobile device';
            for (var counter = 0; counter < deviceInfo.length; counter++) {
                if (deviceInfo[counter].index < minIndex && deviceInfo[counter].index > firstBracketIndex) {
                    minIndex = deviceInfo[counter].index;
                    userDevice = deviceInfo[counter].deviceName;
                }
            }

            if ($.cookie("UserDevice") !== null) {
                $.removeCookie("UserDevice");
            }
            $.cookie("UserDevice", userDevice, { expires: 1, path: '/' });
        };

        return homeModule;
    }(homeModule || {})
);