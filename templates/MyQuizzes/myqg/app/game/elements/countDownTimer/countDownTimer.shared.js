﻿var countDownTimerModule = (
    function (countDownTimerModule) {

        function countDownTimerLogic(id, idMobile) {
            var self = this;
			var intervalHandler = undefined;
	        var $timeInfo = $('.timeInfo');
			var $elementClock = $('#' + id + ' > .countDownTimer__Clock');
            var $elementSvg = $('#' + id + ' > .countDownTimer__Clock svg');
            var $elementCircleBg = $('#' + id + ' > .countDownTimer__Clock .countDownTimer__Circle--bg');
            var $elementCircleMask = $('#' + id + ' > .countDownTimer__Clock .countDownTimer__Circle--mask');
            var $elementCircleActive = $('#' + id + ' > .countDownTimer__Clock .countDownTimer__Circle--active');
			var $elementSeconds = $('#' + id + ' > .countDownTimer__Seconds');
			var $elementPauseButton = $('.countDownTimer__pause-layer.icon-pause-button');
			var $elementPlayButton = $('.countDownTimer__pause-layer.icon-play-button');
			var $elementPauseButtonBackground = $('.countDownTimer__pause-layer-mobile .icon-pause-background');
			var $elementPlayButtonBackground = $('.countDownTimer__pause-layer-mobile .icon-play-button-background');

            var $elementMobileLine = idMobile == undefined ? undefined : $('#' + idMobile);
            var countDownPosition = {
                animationInterval: 1000,

                totalTime: 1,
                remainingTime: 0,
                remainingTimeInSeconds: 0,

                radius: 0,
                cx: 0,
                cy: 0,
                strokeWidth: 0,

                initialDashOffset: 0,
                currentDashOffset: 0,
                nextDashOffset: 0,

                currentPercentCompleted: 0,
                nextPercentCompleted: 0,
			};

	        var mobileLineTimeout = null, desktopCircleTimeout = null;

            self.timerTick = new EventObserver();

            self.setRemainingTime = function (time) {
                countDownPosition.remainingTime = Math.round(time / 1000) * 1000;
            };

            self.setTotalTime = function (time) {
                countDownPosition.totalTime = Math.round(time / 1000) * 1000;
            };

            self.setColor = function (type) {
                $elementCircleActive.removeClass('countDownTimer__Circle--color1');
                $elementCircleActive.removeClass('countDownTimer__Circle--color2');
                $elementCircleActive.addClass('countDownTimer__Circle--' +type);

                if ($elementMobileLine != undefined) {
                    $elementMobileLine.removeClass('countDownTimerMobile--color1');
                    $elementMobileLine.removeClass('countDownTimerMobile--color2');
                    $elementMobileLine.addClass('countDownTimerMobile--' + type);
                }
            };

	        self.show = function() {
		        updateElementSizes();
		        remainingTimeChanged();

		        resetMobileLine();
		        resetDesktopCircle();
	        };

            self.start = function () {

                animateDesktopCircle();
                animateMobileLine();

                if (intervalHandler != undefined) {
                    clearInterval(intervalHandler);
                }
                intervalHandler = setInterval(doStep, countDownPosition.animationInterval);

			};

			function showPauseState() {
				$elementPauseButton.css('display', 'none');
				$elementPlayButton.css('display', 'flex');
				$elementClock.css('opacity', '0.5');
				$elementSeconds.css('color', 'rgba(166, 166, 166, 0.5)');
				$elementPauseButtonBackground.css('display', 'none');
				$elementPlayButtonBackground.css('display', 'inline-block');
			};

			function hidePauseState() {
				$elementClock.css('opacity', '1');
				$elementPlayButton.css('display', 'none');
				$elementPauseButton.css('display', 'none');
				$elementSeconds.css('color', 'rgb(0, 0, 0)');
				$elementPauseButtonBackground.css('display', 'inline-block');
				$elementPlayButtonBackground.css('display', 'none');
			};

			function disable() {
				$timeInfo.css('opacity', 0.5);
			};

	        function enable() {
		        $timeInfo.css('opacity', 1);
	        };

			self.pause = function () {
				console.log('timer paused');
				clearInterval(intervalHandler);
				disable();
				if (gameCoreModule.isOwner) {
					showPauseState();
				}
			};

			self.resume = function () {
				console.log('timer resumed');
				enable();
				self.start();
				if (gameCoreModule.isOwner) {
					hidePauseState();
				}
			};

	        self.setCurrentPauseState = function(isPaused) {
				if (isPaused) {
					showPauseState();
					disable();
				} else {
					hidePauseState();
					enable();
				}
	        };

            function doStep() {
                countDownPosition.remainingTime = countDownPosition.remainingTime - countDownPosition.animationInterval;

                if (countDownPosition.remainingTime <= 0) {
                    countDownPosition.remainingTime = 0;
                    clearInterval(intervalHandler);
                }

                updateElementSizes();
                remainingTimeChanged();
                animateDesktopCircle();
                animateMobileLine();
            }

            function updateElementSizes() {
                countDownPosition.cx = $elementSvg.width() / 2;
                countDownPosition.cy = $elementSvg.height() / 2;
                countDownPosition.r = $elementSvg.height() / 2;
                countDownPosition.strokeWidth = countDownPosition.r / 6;
                countDownPosition.r = countDownPosition.r - (countDownPosition.strokeWidth / 2);
                circleLength = 2 * Math.PI * countDownPosition.r;
                countDownPosition.initialDashOffset = circleLength;
                maskDashPeriod = circleLength / 12;
                maskDashPeriodStick = maskDashPeriod / 7;
                maskDashPeriodBar = maskDashPeriod - maskDashPeriodStick;
                maskDashPeriodStickOffset = maskDashPeriodStick / 2;

                $elementCircleBg
                    .attr('cx', countDownPosition.cx)
                    .attr('cy', countDownPosition.cy)
                    .attr('r', countDownPosition.r)
                    .css('stroke-width', countDownPosition.strokeWidth + 'px');
                $elementCircleMask
                    .attr('cx', countDownPosition.cx)
                    .attr('cy', countDownPosition.cy)
                    .attr('r', countDownPosition.r)
                    .css('stroke-width', countDownPosition.strokeWidth + 'px')
                    .css('stroke-dashoffset', maskDashPeriodStickOffset + 'px')
                    .css('stroke-dasharray', maskDashPeriodStick + 'px, ' + maskDashPeriodBar + 'px');
                $elementCircleActive
                    .attr('cx', countDownPosition.cx)
                    .attr('cy', countDownPosition.cy)
                    .attr('r', countDownPosition.r)
                    .attr('transform', "rotate(-90 " + countDownPosition.cx + " " + countDownPosition.cy + ")")
                    .css('stroke-width', countDownPosition.strokeWidth + 'px')
                    .css('stroke-dasharray', countDownPosition.initialDashOffset + 'px');
            }
            function remainingTimeChanged() {
                countDownPosition.remainingTimeInSeconds = Math.ceil(countDownPosition.remainingTime / 1000);
                countDownPosition.currentPercentCompleted =
                    ((countDownPosition.totalTime - countDownPosition.remainingTime) * 100) / countDownPosition.totalTime;
                if (countDownPosition.currentPercentCompleted > 100) {
                    countDownPosition.currentPercentCompleted = 100;
                }
                countDownPosition.nextPercentCompleted =
                    ((countDownPosition.totalTime - countDownPosition.remainingTime + countDownPosition.animationInterval) * 100) / countDownPosition.totalTime;
                if (countDownPosition.nextPercentCompleted > 100) {
                    countDownPosition.nextPercentCompleted = 100;
                }


                countDownPosition.currentDashOffset =
                    countDownPosition.initialDashOffset - ((countDownPosition.currentPercentCompleted * countDownPosition.initialDashOffset) / 100)
                if (countDownPosition.currentDashOffset < 0) {
                    countDownPosition.currentDashOffset = 0;
                }

                countDownPosition.nextDashOffset =
                    countDownPosition.initialDashOffset - ((countDownPosition.nextPercentCompleted * countDownPosition.initialDashOffset) / 100)
                if (countDownPosition.nextDashOffset < 0) {
                    countDownPosition.nextDashOffset = 0;
                }


                displayTimeAsText();

                self.timerTick.fire({
                    remainingTimeInSeconds: countDownPosition.remainingTimeInSeconds,
                    currentPercentCompleted: countDownPosition.currentPercentCompleted
                });
            }

            function displayTimeAsText() {
                $elementSeconds.text(countDownPosition.remainingTimeInSeconds);

            }

            function resetDesktopCircle() {
                $elementCircleActive.css('transition', '').css('stroke-dashoffset', countDownPosition.currentDashOffset + 'px');
                setTimeout(function () {
                    $elementCircleActive.css('transition', 'stroke-dashoffset 1000ms linear');
                }, 50);
            }

            function animateDesktopCircle() {
                desktopCircleTimeout = setTimeout(function () {
                    $elementCircleActive.css('stroke-dashoffset', countDownPosition.nextDashOffset + 'px');
                }, 100);
            }

            function resetMobileLine() {
                if ($elementMobileLine != undefined) {
                    $elementMobileLine.css('transition', '').css('width', countDownPosition.currentPercentCompleted + '%');
                    setTimeout(function () {
                        $elementMobileLine.css('transition', 'width 1000ms linear');
                    }, 50);
                }
            }

            function animateMobileLine() {
                if ($elementMobileLine != undefined) {
                    mobileLineTimeout = setTimeout(function () {
                        $elementMobileLine.css('width', countDownPosition.nextPercentCompleted + '%');
                    }, 100);
                }
            }

            return self;
        }
        
        countDownTimerModule.createTimer = function (id, idMobile, timeRemaining, timeTotal) {
            var countDownTimer = $('#' + id).data('countDownTimer');
            if (countDownTimer == undefined) {
                countDownTimer = countDownTimerLogic(id, idMobile);
            }

            countDownTimer.setRemainingTime(timeRemaining);
            countDownTimer.setTotalTime(timeTotal);

            $('#' + id).data('countDownTimer', countDownTimer);

            return countDownTimer;
		};

        return countDownTimerModule;
    }(countDownTimerModule || {})
);