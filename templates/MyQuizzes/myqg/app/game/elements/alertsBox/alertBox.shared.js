﻿var alertBoxModule = (
    function (alertBoxModule) {

        $(function () {
            $('.alertItem').each(function () {
                var $alertItem = $(this);
                var $closeButton = $(this).find('.alertItem__Close');

                var $moreButton = $(this).find('.alertItem__More');
                var $detailsText = $(this).find('.alertItem__Details');
                
                $closeButton.click(function () {
                    $alertItem.hide();
                });
                $moreButton.click(function () {
                    $detailsText.toggle();
                });
            });
        });
                
        alertBoxModule.connectionSlowShow = function () {
            $('#connectionSlowAlert').show();
        };
        alertBoxModule.connectionSlowHide = function () {
            $('#connectionSlowAlert').hide();
        };

        alertBoxModule.connectionLostShow = function () {
            $('#connectionLostAlert').show();
        };

        alertBoxModule.connectionLostHide = function () {
            $('#connectionLostAlert').hide();
		};

		alertBoxModule.gamePausedShow = function () {
			console.log('pause notification showed');
			$('#gamePause').show();
	    };

		alertBoxModule.gamePausedHide = function () {
			$('#gamePause').hide();
		};

        return alertBoxModule;
    }(alertBoxModule || {})
);