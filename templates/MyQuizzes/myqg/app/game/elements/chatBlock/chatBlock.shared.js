﻿var chatBlockModule = (
    function (chatBlockModule) {
        var notificationTemplate = '\
<div class="chatNotification" data-chat-notification="true">\
    <div class="chatNotification__UserFotoCol">\
        <div class="chatNotification__UserFoto"##STYLE_FOTO##></div>\
    </div>\
    <div class="chatNotification__TextDataCol">\
        <div class="chatNotification__UserNameCol">\
            <div class="chatNotification__UserName">##USERNAME##:</div>\
        </div>\
        <div class="chatNotification__TextCol">\
            <div class="chatNotification__Text">##TEXT##</div>\
        </div>\
    </div>\
    <div class="chatNotification__CloseBlock">\
        <a class="icon-close" data-chat-notification-close="true"></a>\
    </div>\
</div>';
        ///$('body').css('overflow', 'hidden');
        function initChat(chatBlockSelector, chatIconSelectors) {

            var self = this;
            var $mainElement = $(chatBlockSelector);
            var $chatCloseElement = $mainElement.find('[data-chat-close]');
            var $messagesContainerElement = $mainElement.find('[data-chat-messages-container]');
            var $messagesContentElement = $mainElement.find('[data-chat-messages-content]');
            var $messagesPlaceholderElement = $mainElement.find('[data-chat-messages-placeholder]');
            var perfectScrollbarMessages = new PerfectScrollbar($messagesContainerElement[0], {
                suppressScrollX: true
            });

            var $messageTemplateElement = $mainElement.find('[data-chat-message-template]');
            var $inputBlockElement = $mainElement.find('[data-chat-input-block]');
            var $inputElement = $mainElement.find('[data-chat-input]');
            var $sendElement = $mainElement.find('[data-chat-send]');
            if ($inputBlockElement.length > 0) {
                var perfectScrollbarInputBlock = new PerfectScrollbar($inputBlockElement[0], {
                    suppressScrollX: true
                });
            }
            console.log($chatCloseElement)
            $chatCloseElement.on('click', function () {
                self.hide();
            });

            $sendElement.on('click', validateAndSendMessage);
            $inputElement.on('keydown', function (e) {
                if (e.which == 13 && !e.shiftKey) {
                    validateAndSendMessage();
                    return false;
                }
            });
            function validateAndSendMessage() {
                var message = $inputElement.val();
                if (message.length != 0) {
                    $inputElement.val('');
                    chatServiceModule.send(message);
                    setHeightAndUpdateScroll($inputElement[0]);
                }
            }

            chatServiceModule.receiveChatMessage.subscribe(receiveChatMessage);
            function receiveChatMessage(message) {
                console.log(message);
                $messagesPlaceholderElement.remove();
                var $messageElement = $messageTemplateElement.clone();
                var formattedText = message.Text
                $messageElement.find('[data-chat-message-text]').text(formattedText);
                var momentDateTime = moment(message.DateTime);
                var formattedDateTime = momentDateTime.lang(global_Lang).format(global_TimeFormat);
                $messageElement.find('[data-chat-message-datetime]').text(formattedDateTime);
                $messageElement.hide();
                $messagesContentElement.prepend($messageElement);
                $messageElement.fadeIn();
                perfectScrollbarMessages.update();

                $('[data-chat-notifications]').each(function () {
                    $(this).empty();
                    var notificationHtml = notificationTemplate;
                    notificationHtml = notificationHtml.replace(new RegExp('##USERNAME##', 'g'), message.FromName);
                    notificationHtml = notificationHtml.replace(new RegExp('##TEXT##', 'g'), message.Text);

                    var styleForFoto = '';
                    var userPhotoDefault = '/app/images/userprofile/user_photo.png';
                    if (message.FromImagePath != undefined) {
                        styleForFoto = ' style="background-image:url(' + message.FromImagePath + ')"';
                    } else {
                        styleForFoto = ' style="background-image:url(' + userPhotoDefault + ')"';
                    }
                    notificationHtml = notificationHtml.replace(new RegExp('##STYLE_FOTO##', 'g'), styleForFoto);


                    var $alertItem = $(notificationHtml);
                    $alertItem.hide();
                    $(this).append($alertItem);

                    $alertItem.find('[data-chat-notification-close]').on('click', function () {
                        $alertItem.stop().fadeOut();
                    });

                    $alertItem.fadeIn().delay(15000).fadeOut();
                });
            }

            function initAutoSize() {
                $inputElement.each(function () {
                    setHeightAndUpdateScroll(this);
                }).on('input', function () {
                    setHeightAndUpdateScroll(this)
                });
            }

            function setHeightAndUpdateScroll(elm) {
                elm.style.height = 'auto';
                elm.style.height = (elm.scrollHeight) + 'px';
                perfectScrollbarInputBlock.update();
            }

            self.update = function () {
                perfectScrollbarMessages.update();
                initAutoSize();
            };

            self.toggle = function () {
                $mainElement.slideToggle('fast', function () {
                    self.update();
                }).css('display', 'flex');
            };

            self.show = function () {
                $mainElement.slideDown('fast', function () {
                    self.update();
                }).css('display', 'flex');
            };

            self.hide = function () {
                $mainElement.slideUp('fast', function () {
                    self.update();
                });
            };


            $(function () {
                initAutoSize();

                for (var i in chatIconSelectors) {
                    var chatIconSelector = chatIconSelectors[i];
                    $(chatIconSelector).on('click', function () {
                        self.toggle();
                    });
                }
            });

            return self;

        }
        
        chatBlockModule.init = function (chatBlockSelector, chatIconSelectors) {
            var chatApi = initChat(chatBlockSelector, chatIconSelectors);
            chatServiceModule.loadChatHistory();

            return chatApi;
        };

        return chatBlockModule;
    }(chatBlockModule || {})
);