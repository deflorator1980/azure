﻿/// <reference path="core/event.observer.shared.js" />
/// <reference path="elements/imageBlock/imageBlock.shared.js" />
/// <reference path="elements/alertsBox/alertBox.shared.js" />
/// <reference path="core/localization.shared.js" />

var gameModule = (
    function (gameModule) {
        gameCoreModule.waitStageStarted.subscribe(waitStageStarted);
        function waitStageStarted(position) {
            //remove youtube ads
            document.cookie = "VISITOR_INFO1_LIVE=oKckVSqvaGw; path=/; domain=.youtube.com";
            hideAll();
            waitModule.show();
            waitModule.displayPosition(position);
            resetYoutubeVideo(false, false, true);
            resetVideoSelfHosted();
        }

        gameCoreModule.countdownStageStarted.subscribe(countdownStageStarted);
        function countdownStageStarted(position) {
            hideAll();
            countdownModule.show();
            countdownModule.start(position.CountdownTimerInSeconds * 1000);
            resetYoutubeVideo(false, false, false);
            resetVideoSelfHosted();
        }

        gameCoreModule.questionsAnswerStageStarted.subscribe(questionsAnswerStageStarted);
        function questionsAnswerStageStarted(position) {
            $('body').scrollTop(300);
            hideAll();
            questionAnswerModule.show();
            questionAnswerModule.displayPosition(position);
            resetYoutubeVideo(false, false, true);
            resetVideoSelfHosted();
        }

        gameCoreModule.answeredUpdate.subscribe(answeredUpdate);
        function answeredUpdate(answersState) {
            questionAnswerModule.displayAnswersState(answersState);
		}

        gameCoreModule.questionsReviewStageStarted.subscribe(questionsReviewStageStarted);
        function questionsReviewStageStarted(position) {
            hideAll();
            questionReviewModule.show();
            questionReviewModule.displayPosition(position);
        }

        gameCoreModule.leadersStageStarted.subscribe(leadersStageStarted);
        function leadersStageStarted(position) {
            hideAll();
            leadersModule.show();
            leadersModule.displayPosition(position);
            resetYoutubeVideo(false, false, false);
            resetVideoSelfHosted();
        }

        gameCoreModule.autoStartDisabled.subscribe(autoStartDisabled);
        function autoStartDisabled() {
            waitModule.autoStartDisabled();
        }


        function hideAll() {
            connectModule.hide();
            waitModule.hide();
            countdownModule.hide();
            questionAnswerModule.hide();
            questionReviewModule.hide();
			leadersModule.hide();
	        alertBoxModule.gamePausedHide();
        }

        function resetYoutubeVideo(hide, removeAttributes, resetVideo) {
            $.each($("iframe"), function (index, item) {
                var src = $(item).attr("src");
                if (!hide && $(item).is(":visible") && src && !resetVideo) {
                    return;
                }
                if (!src)
                {
                    src = $(item).attr("storedSRC");
                }
                $(item).attr("storedSRC", src);
                $(item).attr("src", "");

                var isChrome = !!window.chrome && !!window.chrome.webstore; //chrome browser
                
                if (isChrome && src.split("?")[1]) {
                    // for Chrome autoplay policy changes: Muted autoplay is always allowed 
                    //https://developers.google.com/web/updates/2017/09/autoplay-policy-changes
                    var domain = src.split("?")[0];
                    var params = $.parseParams( src.split("?")[1] || '');
                    
                    if (params.autoplay && params.autoplay == '1') {
                        params.mute = '1';
                        src = domain + '?' + $.param(params);
                    }
                }

                if (removeAttributes)
                {
                    src = src.split("?")[0];
                }
    
                if (!hide&& $(item).is(":visible")) {
                    $(item).attr("src", src);
                }
            });
        }

        function resetVideoSelfHosted() {
            $.each($(".azuremediaplayer"),
                function(index, item) {

                    var isHidden = $(item).parents(".layout__Content").is(":hidden");
                    var autoplay = $(item).attr('storedAutoPlay') &&
                        $(item).attr('storedAutoPlay').toLowerCase() == 'true' &&
                        !isHidden;

                    var src = $(item).attr('storedSrc');
                    var poster = $(item).attr('thumbnailSrc');
                    var lang = $(item).attr('language');

                    if (!isHidden && src) {
                        var amPlayer = amp($(item)[0],
                            {
                                techOrder: ["azureHtml5JS", "flashSS", "html5FairPlayHLS", "silverlightSS", "html5"],
                                nativeControlsForTouch: false,
                                language: lang,
                                autoplay: autoplay,
                                controls: true,
                                inactivityTimeout: 2e3,
                                //fluid: true,
                                width: "100%",
                                heigth: "100%",
                                poster: poster,
                                logo: { enabled: false}
                            }, function () {
                                var self = this;
                                this.addEventListener('error', function () {
                                    self.updateStyleEl_();
                                });
                            });

                        amPlayer.src([
                            {
                                src: src,
                                type: "application/vnd.ms-sstr+xml"
                            }
                        ]);

                        
                        if ($(amPlayer.el_).parents('.promoBlockWait__Image').length > 0 ||
                            $(amPlayer.el_).parents('.promoLeaders__Image').length > 0 ||
                            $(amPlayer.el_).parents('.promoQuestion__Video').length > 0 ) {
                            amPlayer.muted(true);
                            $(amPlayer.el_).find('.vjs-play-control').addClass('hidden');
                        };

                    } else {
                        $(item).empty();
                    }
                }
            );
        }

        return gameModule;
    }(gameModule || {})
);