﻿/// <reference path="game.core.shared.js"/>

var chatServiceModule = (
    function (chatServiceModule) {

        chatServiceModule.receiveChatMessage = gameCoreModule.receiveChatMessage;
        chatServiceModule.loadChatHistory = function () {
            console.log('chatServiceModule.loadChatHistory');
            $.connection.hub.start({ transport: ['webSockets', 'longPolling'] }).done(function () {
                gameCoreModule.hostInstance.server
                .loadChatHistory(gameCoreModule.hostId)
                .done(function (messages) {
                        messages.forEach(function(message) {
                            message = mapper.toChatMessage(message);
                            chatServiceModule.receiveChatMessage.fire(message);
                        });
                    console.log('Done - chatServiceModule.loadChatHistory');
                })
                .fail(function () {
                    console.log('Fail - chatServiceModule.loadChatHistory');
                });
            });
        };

        return chatServiceModule;
    }(chatServiceModule || {})
);

