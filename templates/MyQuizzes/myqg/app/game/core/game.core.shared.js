﻿/// <reference path="event.observer.shared.js" />
/// <reference path="mapper.shared.js" />

var gameCoreModule = (
    function (gameCoreModule, hostId) {
        var firstQustion = true;
        gameCoreModule.hostId = hostId;
		gameCoreModule.hostInstance = $.connection.hostInstanceHub;

		function checkPauseState(position) {
			if (position.IsPaused !== gameCoreModule.isPaused) {
				if (position.IsPaused) {
					gameCoreModule.pause();
					alertBoxModule.gamePausedShow();
				} else {
					gameCoreModule.resume();
					alertBoxModule.gamePausedHide();
				}
			}
		};

        gameCoreModule.waitStageStarted = new EventObserver();
        gameCoreModule.hostInstance.client.waitStageStarted = function (position) {
            position = mapper.toPosition(position);
            console.log(position);
            $('.countDownTimer__pause-layer-mobile').removeClass('block-element-mobile');
            gameCoreModule.waitStageStarted.fire(position);
        };

        gameCoreModule.countdownStageStarted = new EventObserver();
        gameCoreModule.hostInstance.client.countdownStageStarted = function (position) {
            console.log('gameCoreModule.countdownStageStartedFire');
            position = mapper.toPosition(position);

            $('.countDownTimer__pause-layer-mobile').removeClass('block-element-mobile');
            gameCoreModule.countdownStageStarted.fire(position);
        };

        gameCoreModule.questionsAnswerStageStarted = new EventObserver();
        gameCoreModule.hostInstance.client.questionsAnswerStageStarted = function (position) {
            console.log('gameCoreModule.questionsAnswerStageStarted.fire');

            position = mapper.toPosition(position);
            console.log(position);
            $('.countDownTimer__pause-layer-mobile').addClass('block-element-mobile');
	        checkPauseState(position);
            gameCoreModule.questionsAnswerStageStarted.fire(position);
            if (firstQustion) {
                firstQustion = false;
                quiz_push_messages.showGamePushMessage(quiz_push_messages.gameStates.OnGameStart);
            }
            else {
                quiz_push_messages.showGamePushMessage(quiz_push_messages.gameStates.OnNewGameQuestion);
            }
        };

        gameCoreModule.answeredUpdate = new EventObserver();
        gameCoreModule.hostInstance.client.answeredUpdate = function (answersState) {
            answersState = mapper.toAnswerState(answersState);
            gameCoreModule.answeredUpdate.fire(answersState);
        };

        gameCoreModule.questionsReviewStageStarted = new EventObserver();
        gameCoreModule.hostInstance.client.questionsReviewStageStarted = function (position) {
            $('.countDownTimer__pause-layer-mobile').addClass('block-element-mobile');

            position = mapper.toPosition(position);
			checkPauseState(position);
            gameCoreModule.fillPosition(position);
            gameCoreModule.questionsReviewStageStarted.fire(position);
        };

        gameCoreModule.leadersStageStarted = new EventObserver();
		gameCoreModule.hostInstance.client.leadersStageStarted = function (position) {

		    position = mapper.toPosition(position);
            checkPauseState(position);
		    $('.countDownTimer__pause-layer-mobile').removeClass('block-element-mobile');
            gameCoreModule.leadersStageStarted.fire(position);
            quiz_push_messages.showGamePushMessage(quiz_push_messages.gameStates.OnGameEnd);
        };

        gameCoreModule.hostStopped = new EventObserver();
        gameCoreModule.hostInstance.client.hostStopped = function (position) {

            position = mapper.toPosition(position);
            gameCoreModule.hostStopped.fire(position);
        };

		gameCoreModule.setPauseState = new EventObserver();
		gameCoreModule.hostInstance.client.setPauseState = function(isPaused) {
			gameCoreModule.setPauseState.fire(isPaused);
	    };

        gameCoreModule.autoStartDisabled = new EventObserver();
        gameCoreModule.hostInstance.client.autoStartDisabled = function () {
            gameCoreModule.autoStartDisabled.fire();
        };

        gameCoreModule.receiveChatMessage = new EventObserver();
        gameCoreModule.hostInstance.client.receiveChatMessage = function (message) {
            console.log('gameCoreModule.receiveChatMessage');
            
            message = mapper.toChatMessage(message);
            gameCoreModule.receiveChatMessage.fire(message);
        };
		gameCoreModule.pause = function () {
			gameCoreModule.isPaused = true;
			questionReviewModule.pause();
			questionAnswerModule.pause();
	    };

		gameCoreModule.resume = function () {
			gameCoreModule.isPaused = false;
			questionReviewModule.resume();
		    questionAnswerModule.resume();
	    };

        $.connection.hub.connectionSlow(function () {
            console.log('connectionSlow');
            alertBoxModule.connectionSlowShow();
        });
        $.connection.hub.reconnected(function () {
            console.log('reconnected');
            alertBoxModule.connectionLostHide();
            alertBoxModule.connectionSlowHide();
        });
        $.connection.hub.reconnecting(function () {
            console.log('reconnecting');
            alertBoxModule.connectionLostShow();
        });

        return gameCoreModule;
    }(gameCoreModule || {}, global_hostId)
);