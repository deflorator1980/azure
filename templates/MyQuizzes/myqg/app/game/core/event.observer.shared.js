﻿function EventObserver() {
    this.handlers = [];  // observers
}

EventObserver.prototype = {

    subscribe: function (fn) {
        this.handlers.push(fn);
    },

    unsubscribe: function (fn) {
        this.handlers = this.handlers.filter(
            function (item) {
                if (item !== fn) {
                    return item;
                }
            }
        );
    },

    fire: function (args) {
        this.handlers.forEach(function (item) {
            item.call(this, args);
        });
    }
};