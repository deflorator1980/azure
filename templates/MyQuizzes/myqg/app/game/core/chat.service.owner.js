﻿/// <reference path="chat.service.shared.js"/>

chatServiceModule = (
    function (chatServiceModule) {
        chatServiceModule.send = function (message) {
            console.log('chatServiceModule.send');
            gameCoreModule.hostInstance.server
                .sendChatMessage(gameCoreModule.hostId, message)
                .done(function () {
                    console.log('Done - chatServiceModule.send');
                })
                .fail(function () {
                    console.log('Fail - chatServiceModule.send');
                });
        };

        return chatServiceModule;
    }(chatServiceModule || {})
);

