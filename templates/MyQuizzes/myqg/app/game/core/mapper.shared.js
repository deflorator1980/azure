﻿var mapper = (
    function (mapper) {

        var positionContract = {
            a : "AnswerIds",
            b : "CurrentQuestion",
            c : "HostState",
            d : "PlayersCount",
            e : "QuestionsCount",
            f : "TimeForAnswerInSeconds",
            g : "TimeForReviewAnswerInSeconds",
            h : "TimeLeft",
            i : "TopScores",
            k : "CountdownTimerInSeconds",
            l : "IsPaused"
        }

        var quizQuestionContract = {
            a : "Answers",
            b : "ArchiveQuestionId",
            c : "Id",
            d : "ImagePath",
            e : "ImageMediaType",
            f : "IsSingleChoise",
            g : "Number",
            h : "PromoEnabled",
            i : "PromoImagePath",
            k : "PromoImageMediaType",
            l : "PromoText",
            m : "Text",
            n : "IsNeedCorrectAnswerNote",
            o : "CorrectAnswerNote",
            p : "ImageThumbnailUrl",
            q : "ImageVideoAutoPlay",
            r : "PromoImageThumbnailUrl",
            s : "PromoImageVideoAutoPlay"
        }

        var topScoreContract = {
            a : "Position",
            b : "Score",
            c : "User",
            d : "ShowWinnerCode",
            e : "WinnerCode",
            f : "Email"
        }

        var quizQuestionAnswerContract = {
            a : "ArchiveAnswerId",
            b : "Id",
            c : "IsCorrect",
            d : "Text"
        }

        var answerStateContract = {
            a : "LastAnsweredPlayers",
            b : "LastAnsweredPlayersTotalCount"
        }

        var userContract = {
            a : "DisplayName",
            b : "FullName",
            c : "ImagePath",
            d : "UserId"
        }

        var chatMessageContract = {
            a : "FromImagePath",
            b : "FromName",
            c : "Text",
            d : "DateTime"
        }

        function reMap(smallObject, contract) {
            if (!smallObject) {
                return null;
            }

            var largeObject = {};
            for (var smallProperty in contract) {
                largeObject[contract[smallProperty]] = smallObject[smallProperty];
            }
            return largeObject;
        }
        
        mapper.toPosition = function(position) {
            if (!position) {
                return null;
            }

            var result = reMap(position, positionContract);
            result.CurrentQuestion = reMap(result.CurrentQuestion, quizQuestionContract);

            if (result.CurrentQuestion && result.CurrentQuestion.Answers) {
                for (var i = 0; i < result.CurrentQuestion.Answers.length; i++) {
                    result.CurrentQuestion.Answers[i] = reMap(result.CurrentQuestion.Answers[i], quizQuestionAnswerContract);
                }
            }

            if (result.TopScores) {
                for (var i = 0; i < result.TopScores.length; i++) {
                    result.TopScores[i] = this.toScore(result.TopScores[i]);
                }
            }

            return result;
        }

        mapper.toScore = function(score) {
            if (!score) {
                return null;
            }

            var result = reMap(score, topScoreContract);
            result.User = this.toUser(result.User);

            return result;
        }

        mapper.toUser = function(user) {
            return reMap(user, userContract);
        }

        mapper.toAnswerState = function(answersState) {
            if (!answersState) {
                return null;
            }

            var result = reMap(answersState, answerStateContract);

            if (result.LastAnsweredPlayers) {
                for (var i = 0; i < result.LastAnsweredPlayers.length; i++) {
                    result.LastAnsweredPlayers[i] = this.toUser(result.LastAnsweredPlayers[i]);
                }
            }

            return result;
        }

        mapper.toChatMessage = function(message) {
            return reMap(message, chatMessageContract);
        }


        return mapper;
    }(mapper || {})
);