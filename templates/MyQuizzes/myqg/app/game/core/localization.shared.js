﻿var localizationModule = (
    function (localizationModule, lang) {
        localizationModule.lang = lang;

        localizationModule.GetSuffix = function (number, suffixes) {
            var suffix = '';

            var ones = number % 10;
            var tens = Math.floor(number / 10);

            if (tens == 1)
            {
                suffixes.other;
            }
            else
            {
                switch (ones)
                {
                    case 1:
                        suffixes.first;
                        break;

                    case 2:
                        suffixes.second;
                        break;

                    case 3:
                        suffixes.third;
                        break;

                    default:
                        suffixes.other;
                        break;
                }
            }
            return suffix;
            
        }

        localizationModule.GetDeclension = function(number, words)
        {

            number = number % 100;
            if (number >= 11 && number <= 19)
            {
                return words.plural;
            }

            var i = number % 10;
            switch (i)
            {
                case 1:
                    return words.nominativ;
                case 2:
                case 3:
                case 4:
                    return words.genetiv;
                default:
                    return words.plural;
            }
        }

        localizationModule.GetAppealToTheCurrenUser = function(appealToTheUser) {
            return appealToTheUser;
        }

        return localizationModule;
    }(localizationModule || {}, global_Lang)
);