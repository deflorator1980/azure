﻿/// <reference path="game.core.shared.js" />

gameCoreModule = (
    function (gameCoreModule) {
        var questionAnswers = {};

        gameCoreModule.join = function () {
            $.connection.hub.start({ transport: ['webSockets', 'longPolling'] }).done(function () {
                gameCoreModule.hostInstance.server
                    .join(gameCoreModule.hostId)
                    .done(function () {
                        console.log('You are joined!');
                        alertBoxModule.connectionLostHide();
                        alertBoxModule.connectionSlowHide();
                    })
                    .fail(function () {
                        console.log('Fail - gameCoreModule.connect');
                    });
            });
        };

        gameCoreModule.answer = function (questionId, answerIds) {
            gameCoreModule.hostInstance.server
                .answer(gameCoreModule.hostId, questionId, answerIds)
                .done(function () {
                    questionAnswers[questionId] = answerIds;
                    console.log('You are answered!');
                })
                .fail(function (error) {
					console.log('Fail - gameCoreModule.answer');
		            console.log(error);
	            });
        };

        gameCoreModule.fillPosition = function (position) {
            if (position.AnswerIds == null || position.AnswerIds.length == 0) {
                if (questionAnswers[position.CurrentQuestion.Id] != undefined) {
                    position.AnswerIds = questionAnswers[position.CurrentQuestion.Id];
                }
            }
        };

        gameCoreModule.playerResultGet = function () {
            console.log('Starting playerResult!');
            gameCoreModule.hostInstance.server
                .playerResultGet(gameCoreModule.hostId)
                .done(function () {
                    console.log('You are requested playerResult!');
                })
                .fail(function (err) {
                    console.log('Fail - gameCoreModule.playerResultGet');
                    console.log(err);
                });
        };

        gameCoreModule.playerResult = new EventObserver();
        gameCoreModule.hostInstance.client.playerResult = function (score) {
            score = mapper.toScore(score);
            gameCoreModule.playerResult.fire(score);
        };


        gameCoreModule.sendQuizWinnerCode = function (quizWinnerCodeInfo, callback) {
            console.log(quizWinnerCodeInfo);
            gameCoreModule.hostInstance.server
                .sendQuizWinnerCode(quizWinnerCodeInfo)
                .done(function () {
                    console.log('You are requested send winner code!');
                    if (callback != undefined) {
                        callback({success:true});
                    }
                })
                .fail(function () {
                    console.log('Fail - gameCoreModule.sendQuizWinnerCode');
                    callback({ success: false });
                });
        };

        $.connection.hub.disconnected(function () {
            console.log('disconnected.player');
            setTimeout(function () {
                gameCoreModule.join();
            }, 5000);
        });


        return gameCoreModule;
    }(gameCoreModule, global_hostId)
);

