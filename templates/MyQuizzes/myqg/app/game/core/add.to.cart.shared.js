﻿var addToCartModule = (function (addToCartModule) {
    addToCartModule.addedToCart = false;
    addToCartModule.init = function (blockSelector, cartFlyToSelector, cartSelector, filledCartClass, sendRequestWithoutRedirect, onClickShowElementSelector, notificationSelector) {
        var block = $(blockSelector);
        $.each(block.find("a"), function (index, item) {
            if (!$(item).attr("addToCartModuleAttached")) {
                $(item).attr("addToCartModuleAttached", true);
                $(item).on('click', function (event) {
                    var notification = $(notificationSelector);
                    console.log(notificationSelector)
                    var cart = null;
                    addToCartModule.addedToCart = true;
                    $(onClickShowElementSelector).show();
                    $.each(cartFlyToSelector.split(','), function (index, selector) {
                        if ($(selector).is(":visible")) {
                            cart = $(selector);
                        };
                    });

                    if (!cart) { return; };

                    if (sendRequestWithoutRedirect) {
                        addToCartModule.sendRequestWithoutRedirect(this, blockSelector, event);
                    }

                    notification.stop().fadeIn().delay(8000).fadeOut();

                    var itemToDrag = $(blockSelector);
                    if (itemToDrag) {
                        var itemClone = itemToDrag.clone()
                            .offset({
                                top: itemToDrag.offset().top,
                                left: itemToDrag.offset().left
                            })
                            .css({
                                'opacity': '0.5',
                                'position': 'absolute',
                                'width': itemToDrag.width(),
                                'z-index': '100'
                            })
                            .appendTo($('body'))
                            .animate({
                                'top': cart.offset().top + 10,
                                'left': cart.offset().left + 10,
                                'width': 75,
                                'height': 75
                            }, 1000);

                        itemClone.animate({
                            'width': 0,
                            'height': 0
                        }, function () {
                            $(this).detach()
                        });

                        setTimeout(function () {
                            $(cartSelector).addClass(filledCartClass);
                        }, 200);
                    }
                });
            }
        });
    };

    addToCartModule.sendRequestWithoutRedirect = function (context, container, event) {
        event.preventDefault();

        var urlParams = {}, match,
            pl = /\+/g,  // Regex for replacing addition symbol with a space
            search = /([^&=]+)=?([^&]*)/g,
            decode = function (s) { return decodeURIComponent(s.replace(pl, " ")); },
            urlWithoutParams = $(context).attr("href").split("?")[0],
            query = $(context).attr("href").split("?")[1];

        while (match = search.exec(query)) {
            urlParams[decode(match[1])] = decode(match[2])
        };

        var formData = new FormData();
        $.each(urlParams, function (index, item) {
            formData.append(index.replace("?",""), item);
        });
        formData.append('blockHTML', encodeURIComponent($(container).html()));
        formData.append('save', true);

        $.ajax({
            type: 'POST',
            url: urlWithoutParams,
            data: formData,
            cache: false,
            contentType: false,
            processData: false,
            success: function (data) { },
            error: function (data) { console.log(data); }
        }, 'json');
    }

    addToCartModule.initAll = function (blocksSelector) {
        $.each($(blocksSelector), function (index, item) {
            console.log(blocksSelector);
            var itemData = $(item).data();
            if ($(item) && itemData.cartFlyToSelector && itemData.cartSelector && itemData.filledCartClass && itemData.onClickShowElementSelector)
            {
                addToCartModule.init(
                    item,
                    itemData.cartFlyToSelector,
                    itemData.cartSelector,
                    itemData.filledCartClass,
                    true,
                    itemData.onClickShowElementSelector,
                    itemData.notificationSelector);
            }
        });
    };
        return addToCartModule;
    }(addToCartModule || {})
);
