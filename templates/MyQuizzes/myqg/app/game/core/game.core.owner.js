﻿/// <reference path="game.core.shared.js"/>

gameCoreModule = (
    function (gameCoreModule) {
        gameCoreModule.manage = function () {
            console.log('gameCoreModule.manage');
            $.connection.hub.start({ transport: ['webSockets', 'longPolling'] }).done(function () {
                gameCoreModule.hostInstance.server
                    .manage(gameCoreModule.hostId)
                    .done(function () {
                        console.log('You are manage!');
                        alertBoxModule.connectionLostHide();
                        alertBoxModule.connectionSlowHide();
                    })
                    .fail(function () {
                        console.log('Fail - gameCoreModule.connect');
                    });
            });
        };

        gameCoreModule.start = function () {
            console.log('gameCoreModule.start');
            gameCoreModule.hostInstance.server
                .start(gameCoreModule.hostId)
                .done(function () {
                    console.log('game is started!');
                })
                .fail(function () {
                    console.log('Fail - gameCoreModule.start');
                });
        };

        gameCoreModule.switchToManualMode = function () {
            console.log('gameCoreModule.switchToManualMode');
            gameCoreModule.hostInstance.server
                .switchToManualMode(gameCoreModule.hostId)
                .done(function () {
                    console.log('Done - gameCoreModule.switchToManualMode');
                })
                .fail(function () {
                    console.log('Fail - gameCoreModule.switchToManualMode');
                });
        };

        gameCoreModule.stop = function () {
            console.log('gameCoreModule.stop');
            gameCoreModule.hostInstance.server
                .stop(gameCoreModule.hostId)
                .done(function () {
                    console.log('game is stopped!');
                })
                .fail(function () {
                    console.log('Fail - gameCoreModule.stop');
                });
		};

	    gameCoreModule.changePauseState = function () {
			console.log('gameCoreModule.pauseStart');
		    gameCoreModule.hostInstance.server
				.changePauseState(gameCoreModule.hostId)
			    .done(function () {
				    console.log('game pause started!');
			    })
			    .fail(function () {
					console.log('Fail - gameCoreModule.pauseStart');
			    });
		};

        gameCoreModule.playerJoined = new EventObserver();
        gameCoreModule.hostInstance.client.playerJoined = function (count) {
            gameCoreModule.playerJoined.fire(count);
        };

        gameCoreModule.fillPosition = function (position) {
        };

        $.connection.hub.disconnected(function () {
            console.log('disconnected.owner');
            setTimeout(function () {
                gameCoreModule.manage();
            }, 5000);
        });

        return gameCoreModule;
    }(gameCoreModule)
);

