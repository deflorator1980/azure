﻿/// <reference path="core/game.core.owner.js" />
/// <reference path="game.shared.js" />
/// <reference path="elements/chatBlock/chatBlock.owner.js" />
/// <reference path="stages/connect/connect.shared.js" />
/// <reference path="stages/wait/wait.owner.js" />
/// <reference path="stages/countdown/countdown.shared.js" />
/// <reference path="stages/questions/questionAnswer.owner.js" />
/// <reference path="stages/questions/questionReview.owner.js" />
/// <reference path="stages/leaders/leaders.owner.js" />

gameModule = (
    function (gameModule) {

        gameModule.connect = function () {
            gameCoreModule.manage();
        };

        gameCoreModule.playerJoined.subscribe(playerJoined);
        function playerJoined(count) {
            waitModule.updatePlayersCount(count);
        }

        gameCoreModule.hostStopped.subscribe(hostStopped);
        function hostStopped(position) {
            document.location = '/Lectures';
        }

        return gameModule;
    }(gameModule)
);

