﻿/// <reference path="questionAnswer.shared.js" />

var questionAnswerModule = (
    function (questionAnswerModule) {
        questionAnswerModule.displayInternalContent = function (position) {
            questionAnswerModule.displayAnswers(position.CurrentQuestion.Answers);
            questionAnswerModule.setTimeInfoText();
            questionAnswerModule.displayPromo(position.CurrentQuestion);
        };

        questionAnswerModule.displayAnswers = function (answers) {
            $('#questionDetails__Answers').empty();
            for (var i = 0; i < answers.length; i++) {
                var newAnswerHtml = questionTemplatesModule.answerTemplateNotEditable;
                newAnswerHtml = newAnswerHtml.replace(new RegExp('##TEXT##', 'g'), answers[i].Text.replace(/\r\n/g, '<br/>'));
                var $newAnswerObject = $(newAnswerHtml);
                $newAnswerObject.click(function () {
                    $('#validationError_NeedToConnect').show().delay(5000).fadeOut(2000);
                });
                $('#questionDetails__Answers').append($newAnswerObject);
            }
        };

        questionAnswerModule.setTimeInfoText = function () {
            questionAnswerModule.setTimeInfoTextByPrefix('timeInfo');
            questionAnswerModule.setTimeInfoTextByPrefix('timeInfoMobile');
        };

        questionAnswerModule.setTimeInfoTextByPrefix = function (prefix) {
            $('#' + prefix + '__Text > span').hide();
            $('#' + prefix + '__TimeForAnswerText').show();
        };

        return questionAnswerModule;
    }(questionAnswerModule)
);

