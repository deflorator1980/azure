﻿/// <reference path="questionTemplates.shared.js" />

var questionAnswerModule = (
    function (questionAnswerModule, gameCoreModule) {
        var countDownTimer;
        $(function () {
            $('#leadersList__ToggleIcon').click(questionAnswerModule.toggleLeadersList);
        });
        questionAnswerModule.show = function () {
            $('#stage__Questions').show();
        };

        questionAnswerModule.hide = function () {
            $('#stage__Questions').hide();
        };

        function disable() {
            if (typeof questionAnswerModule.validateVariantsOfAnswers == 'function') {
                if (questionAnswerModule.validateVariantsOfAnswers()) {
                    $('.questionDetails__Actions').css('opacity', 0.65);
                } else {
                    $('.questionDetails__Actions').css('opacity', 0.35);
                }
            }
            $('.questionDetails__QuestionCol').css('opacity', 0.5);
            $('.resultState').css('opacity', 0.5);
            $('.answerState').css('opacity', 0.5);
            $('.leadersList').css('opacity', 0.5);
            $('#timeInfoMobile__Seconds').css('opacity', 0.5);
            $('#timeInfoMobile__Text').css('opacity', 0.5);
            $('#question__countDownTimerMobile').css('opacity', 0.5);
            $('.questionDetails__lock-layer').show();
            
        };

        function enable() {
            $('.questionDetails__Actions').css('opacity', 1);
            $('.resultState').css('opacity', 1);
            $('.answerState').css('opacity', 1);
            $('.leadersList').css('opacity', 1);
            $('#timeInfoMobile__Seconds').css('opacity', 1);
            $('#timeInfoMobile__Text').css('opacity', 1);
            $('#question__countDownTimerMobile').css('opacity', 1);
            $('.questionDetails__QuestionCol').css('opacity', 1);
            $('.questionDetails__lock-layer').hide();
        };

        questionAnswerModule.pause = function () {
            if (countDownTimer != undefined) {
                disable();
                countDownTimer.pause();
            }
        };

        questionAnswerModule.resume = function () {
            if (countDownTimer != undefined) {
                enable();
                countDownTimer.resume();
            }
        };

        var currentQuestionId = undefined;
        questionAnswerModule.displayPosition = function (position) {
            if (position.CurrentQuestion.Id != currentQuestionId) {
                currentQuestionId = position.CurrentQuestion.Id;
                questionAnswerModule.displayStaticContent(position);
                questionAnswerModule.scrollToTop();
            }
            questionAnswerModule.displayInternalContent(position);

            countDownTimer = countDownTimerModule.createTimer(
                'question__countDownTimer',
                'question__countDownTimerMobile',
                (position.TimeLeft - 2000),
                (position.TimeForAnswerInSeconds * 1000)
            );
            countDownTimer.timerTick.subscribe(function (tickStatus) {
                $('#timeInfoMobile__Seconds').text(tickStatus.remainingTimeInSeconds);
                if (tickStatus.remainingTimeInSeconds == 0 && questionAnswerModule.answerQuestion != undefined) {
                    questionAnswerModule.answerQuestion();
                }
            });
            countDownTimer.setColor('color1');
            countDownTimer.show();
            if (position.IsPaused) {
                disable();
                if(!gameCoreModule.isOwner) {
			        alertBoxModule.gamePausedShow();
			    }
			    countDownTimer.pause();
			} else {
                enable();
			    if (!gameCoreModule.isOwner) {
			        alertBoxModule.gamePausedHide();
			    }
			    countDownTimer.resume();
			}
//	        if (gameCoreModule.isOwner) {
//		        countDownTimer.setCurrentPauseState(gameCoreModule.isPaused);
//	        }
            if (!gameCoreModule.isPaused) {
                countDownTimer.start();
            }
        };

        questionAnswerModule.displayStaticContent = function (position) {
            $('#questionDetails__CorrectAnswerNote').hide();
            questionAnswerModule.displayQuestionImage(position.CurrentQuestion.ImagePath,
                position.CurrentQuestion.ImageMediaType,
                position.CurrentQuestion.ImageThumbnailUrl,
                position.CurrentQuestion.ImageVideoAutoPlay);
            questionAnswerModule.displayQuestionText(position.CurrentQuestion.Text);
            questionAnswerModule.displayProgress(position.CurrentQuestion.Number, position.QuestionsCount);
            questionAnswerModule.displayLeaders(position.TopScores);
            questionAnswerModule.startAnsweredUsersRoutine();
        };


        questionAnswerModule.displayQuestionImage = function (imagePath, mediaType, thumbnailSrc, videoAutoPlay ) {
            var imageBlock = imageBlockModule.init('questionDetails__ImageBlock');
            if (imagePath != null || (mediaType == 'VideoSelfHosted')) {
                $('#questionDetails__ImageBlock').show();
                imageBlock.setElement(imagePath, mediaType, thumbnailSrc, videoAutoPlay, localizationModule.lang);
            }
            else {
                $('#questionDetails__ImageBlock').hide();
            }
        };

        questionAnswerModule.displayQuestionText = function (text) {
            $('#questionDetails__Text').html(text.replace(/\r\n/g, '<br/>'));
        };

        questionAnswerModule.displayProgress = function (currentQuestionNumber, totalQuestionsCount) {
            $('#gameProgress__QuestionNumber').text(currentQuestionNumber);
            $('#gameProgress__QuestionsCount').text(totalQuestionsCount);
            $('#gameProgressMobile__QuestionNumber').text(currentQuestionNumber);
            $('#gameProgressMobile__QuestionsCount').text(totalQuestionsCount);
        };

        questionAnswerModule.displayLeaders = function (topScores) {
            $('#leadersList__Users').empty();

            if (topScores.length > 0) {
                $('#leadersList').show();

                var topScoresLimit = Math.min(topScores.length, 5);
                for (var i = 0; i < topScoresLimit; i++) {
                    questionAnswerModule.displayLeader(topScores[i]);
                }
            }
            else {
                $('#leadersList').hide();
            }
        };

        questionAnswerModule.displayLeader = function (topScore) {
            var leaderInfoHtml = questionAnswerModule.getLeaderHtml(topScore);

            $('#leadersList__Users').append($(leaderInfoHtml));
        };

        questionAnswerModule.getLeaderHtml = function (topScore) {
            var isYou = topScore.User.UserId === global_userId;
            var leaderInfoHtml = questionTemplatesModule.leaderInfoTemplate;
            leaderInfoHtml = leaderInfoHtml.replace(new RegExp('##USERNAME##', 'g'), topScore.User.FullName);
            leaderInfoHtml = leaderInfoHtml.replace(new RegExp('##SCORE##', 'g'), topScore.Score);
            leaderInfoHtml = leaderInfoHtml.replace(new RegExp('##PLACE##', 'g'), topScore.Position);
            leaderInfoHtml = leaderInfoHtml.replace(new RegExp('##ISYOU##', 'g'), isYou ? '(' + global_Localization.you + ')' : '');

            var styleForFoto = '';
            var userPhotoDefault = '/app/images/userprofile/user_photo.png';
            if (topScore.User.ImagePath != undefined) {
                styleForFoto = ' style="background-image:url(' + topScore.User.ImagePath + ')"';
            } else {
                styleForFoto = ' style="background-image:url(' + userPhotoDefault + ')"';
            }
            leaderInfoHtml = leaderInfoHtml.replace(new RegExp('##STYLE_FOTO##', 'g'), styleForFoto);

            return leaderInfoHtml;
        };

        questionAnswerModule.toggleLeadersList = function () {
            $('#leadersList__ToggleIcon').toggleClass('icon-arrow-down-t2');
            $('#leadersList__Users').slideToggle();
        };

        questionAnswerModule.hidePromo = function (question) {
            $('#infoBlockPromo > #promoBlockPlaceHolder').empty();
            $('#infoBlockPromo').hide();
            $('#infoBlockPromo').removeData('questionId');
        };

        questionAnswerModule.displayPromo = function (question) {
            if (question.PromoEnabled) {
                if ($('#infoBlockPromo').data('questionId') == currentQuestionId) {
                    return;
                }
                else {
                    $('#infoBlockPromo').data('questionId', currentQuestionId);
                }

                var promoQuestionHtml = questionTemplatesModule.promoTemplate;

                if (question.PromoImageMediaType == 'VideoSelfHosted' && question.PromoImagePath != null) {
                    promoQuestionHtml = questionTemplatesModule.promoTemplateVideoSelfHostedAutoplay;
                    promoQuestionHtml = promoQuestionHtml.replace(new RegExp('##THUMBNAIL##', 'g'), question.PromoImageThumbnailUrl ? question.PromoImagePath : '');
                    promoQuestionHtml = promoQuestionHtml.replace(new RegExp('##VIDEOAUTOPLAY##', 'g'), question.PromoImageVideoAutoPlay);
                }

                if (question.PromoImageMediaType == 'VideoSelfHosted' && question.PromoImagePath == null) {
                    promoQuestionHtml = promoQuestionHtml.replace(new RegExp('##IMAGE##', 'g'), '/app/images/home/video_processing_' + localizationModule.lang + '_square.png');
                } else {
                    promoQuestionHtml = promoQuestionHtml.replace(new RegExp('##IMAGE##', 'g'), question.PromoImagePath ? question.PromoImagePath : '#');
                }

                promoQuestionHtml = promoQuestionHtml.replace(new RegExp('##TEXT##', 'g'), question.PromoText);
                $('#infoBlockPromo > #promoBlockPlaceHolder').empty();
                $('#infoBlockPromo > #promoBlockPlaceHolder').append(promoQuestionHtml);

                var promoQuestionNotificationHtml = questionTemplatesModule.promoNotificationTemplate;
                promoQuestionNotificationHtml = promoQuestionNotificationHtml.replace(new RegExp('##TEXT##', 'g'), global_Localization.linksCollectionNotification);
                $('#infoBlockPromo > #promoNotificationPlaceHolder').empty();
                $('#infoBlockPromo > #promoNotificationPlaceHolder').append(promoQuestionNotificationHtml);

                if (question.PromoImagePath == null && ( question.PromoImageMediaType == null || question.PromoImageMediaType != 'VideoSelfHosted')) {
                    $('#infoBlockPromo').find('img').remove();
                }
                $('#infoBlockPromo').show();
            }
            else {
                $('#infoBlockPromo > #promoBlockPlaceHolder').empty();
                $('#infoBlockPromo').hide();
            }
        };

        var answeredUsersArray = undefined;
        var answeredUsersRoutineRef = undefined;
        questionAnswerModule.startAnsweredUsersRoutine = function () {
            questionAnswerModule.stopAnsweredUsersRoutine();
            answeredUsersRoutineRef = setInterval(questionAnswerModule.displayAnswersStateRoutine, 1000);
        };
        questionAnswerModule.stopAnsweredUsersRoutine = function () {
            $('#answersState').hide();
            $('#answersState__UsersRow').empty();
            $('#mobileNotifications').empty();
            answeredUsersArray = [];
            clearInterval(answeredUsersRoutineRef);
        };

        questionAnswerModule.displayAnswersState = function (answersState) {
            $('#answersState').show();
            $('#answersState__ReceivedAnswers').text(answersState.LastAnsweredPlayersTotalCount);

            for (var i = 0; i < answersState.LastAnsweredPlayers.length; i++) {
                answeredUsersArray.push(answersState.LastAnsweredPlayers[i]);
            }
        };

        questionAnswerModule.displayAnswersStateRoutine = function () {
            var answeredPlayer = answeredUsersArray.shift();
            if (answeredPlayer != undefined) {
                questionAnswerModule.displayAnsweredPlayer(answeredPlayer);
                questionAnswerModule.displayAnsweredPlayerMobile(answeredPlayer);
            }
        };

        questionAnswerModule.displayAnsweredPlayer = function (lastAnsweredPlayer) {
            var userColumnsCount = $('#answersState *[user-col]').length;

            if (userColumnsCount >= 3) {
                questionAnswerModule.hideAnswerPlayerBlock();
            }
            var answeredUserHtml = questionAnswerModule.renderAnsweredPlayerHtml(
                questionTemplatesModule.answeredUserTemplate, lastAnsweredPlayer);

            var $answerUserElement = $(answeredUserHtml);
            $('#answersState__UsersRow').append($answerUserElement);

            console.log("New answered user item: ", $answerUserElement);
            console.log("New answered user item class name", $answerUserElement.attr('class'));
            console.log("New answered user item class attribute left", $answerUserElement.css('left'));

            setTimeout(function () {
                $answerUserElement.removeClass('answersState__UserCol--New');
            }, 10);
        };

        questionAnswerModule.renderAnsweredPlayerHtml = function (template, lastAnsweredPlayer) {
            var answeredUserHtml = template;
            answeredUserHtml = answeredUserHtml.replace(new RegExp('##USERNAME##', 'g'), lastAnsweredPlayer.DisplayName);

            var styleForFoto = '';
            var userPhotoDefault = '/app/images/userprofile/user_photo.png';
            if (lastAnsweredPlayer.ImagePath != undefined) {
                styleForFoto = ' style="background-image:url(' + lastAnsweredPlayer.ImagePath + ')"';
            }
            else {
                styleForFoto = ' style="background-image:url(' + userPhotoDefault + ')"';
            }
            answeredUserHtml = answeredUserHtml.replace(new RegExp('##STYLE_FOTO##', 'g'), styleForFoto);

            return answeredUserHtml;
        }

        questionAnswerModule.hideAnswerPlayerBlock = function () {
            var block = $('#answersState *[user-col]:nth-of-type(1)');
            setTimeout(function () {
                $(block).remove();
            }, 500);
        };

        questionAnswerModule.displayAnsweredPlayerMobile = function (lastAnsweredPlayer) {
            var answeredUserHtml = questionAnswerModule.renderAnsweredPlayerHtml(
                questionTemplatesModule.answeredUserTemplateMobile, lastAnsweredPlayer);
            answeredUserHtml = answeredUserHtml.replace(new RegExp('##ALREADYANSWERED##', 'g'), global_Localization.alreadyAnsweredLowercase);

            var $answerUserElement = $(answeredUserHtml);
            $answerUserElement.hide();

            $('#mobileNotifications').empty();
            $('#mobileNotifications').append($answerUserElement);

            $answerUserElement.fadeIn();
        };

        questionAnswerModule.scrollToTop = function () {
            $('body,html').animate({ scrollTop: 0 }, 800);
        }

        return questionAnswerModule;
    }(questionAnswerModule || {}, gameCoreModule)
);

