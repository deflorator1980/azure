﻿/*jshint multistr: true */

var questionTemplatesModule = (
    function (questionTemplatesModule) {
        questionTemplatesModule.answerTemplateEditable = '\
<div class="answerButton" wa-type="answerVariantButton">\
    <div class="answerButton__LeftLine">\
        <input id="answer__##ID##" name="answer" type="##TYPE##" data-answer-id="##ID##" class="answerButton__Input" />\
        <label class="answerButton__Text" for="answer__##ID##">##TEXT##</label>\
    </div>\
</div>';
        questionTemplatesModule.answerTemplateNotEditable = '\
<div class="answerButton">\
    <div class="answerButton__LeftLine">\
        <label class="answerButton__Text">##TEXT##</label>\
    </div>\
</div>';
        questionTemplatesModule.leaderInfoTemplate = '\
<div class="leaderInfo">\
    <div class="leaderInfo__PlaceCol">\
        <div class="leaderInfo__Place">##PLACE##</div>\
    </div>\
    <div class="leaderInfo__FotoCol">\
        <div class="leaderInfo__Foto"##STYLE_FOTO##></div>\
        <div class="leaderInfo__Score">##SCORE##</div>\
    </div>\
    <div class="leaderInfo__NameCol">\
        <div class="leaderInfo__Name">##USERNAME## <span class="hidden-lg">##ISYOU##</span></div>\
    </div>\
</div>';
        questionTemplatesModule.answeredUserTemplate = '\
<div class="answersState__UserCol answersState__UserCol--New" user-col="true">\
    <div class="answersState__UserFotoCol">\
        <div class="answersState__UserFoto"##STYLE_FOTO##></div>\
    </div>\
    <div class="answersState__UserNameCol">\
        <div class="answersState__UserName">##USERNAME##</div>\
    </div>\
</div>';
        questionTemplatesModule.promoTemplate = '\
<div class="promoQuestion">\
    <div class="promoQuestion__Image"><img src="##IMAGE##" /></div>\
    <div class="promoQuestion__Text">##TEXT##</div>\
</div>';

        questionTemplatesModule.promoTemplateVideoSelfHostedStopped = '\
<div class="promoQuestion promoQuestion--video">\
    <div class="promoQuestion__Video">\
        <video muted playsinline>\
            <source src= "##IMAGE##" />\
        </video>\
    </div>\
    <div class="promoQuestion__Text">##TEXT##</div>\
</div>';

        questionTemplatesModule.promoTemplateVideoSelfHostedAutoplay = '\
<div class="promoQuestion promoQuestion--video">\
    <div class="promoQuestion__Video">\
        <video class="azuremediaplayer amp-default-skin amp-big-play-centered" storedSrc="##IMAGE##" thumbnailSrc="##THUMBNAIL##" storedAutoPlay="##VIDEOAUTOPLAY##" >\
        </video>\
    </div>\
    <div class="promoQuestion__Text">##TEXT##</div>\
</div>';

        questionTemplatesModule.promoNotificationTemplate = '\
<div class="promoQuestionNotification">\
    ##TEXT##\
</div>';

        questionTemplatesModule.answeredUserTemplateMobile = '\
<div class="mobileNotification">\
    <div class="mobileNotification__TitleCol">\
        <div class="mobileNotification__Title">##ALREADYANSWERED##</div>\
    </div>\
    <div class="mobileNotification__UserFotoCol">\
        <div class="mobileNotification__UserFoto"##STYLE_FOTO##></div>\
    </div>\
    <div class="mobileNotification__UserNameCol">\
        <div class="mobileNotification__UserName"><div>##USERNAME##</div></div>\
    </div>\
</div>';

        return questionTemplatesModule;
    }(questionTemplatesModule || {})
);

