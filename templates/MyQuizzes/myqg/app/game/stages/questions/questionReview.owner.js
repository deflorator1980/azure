﻿/// <reference path="questionReview.shared.js" />

var questionReviewModule = (
    function (questionReviewModule) {
        var currentPosition = undefined;

        questionReviewModule.displayInternalContent = function (position) {
            currentPosition = position;
            questionReviewModule.displayAnswers();
            questionReviewModule.setTimeInfoText();
            questionAnswerModule.displayPromo(position.CurrentQuestion);
            questionAnswerModule.stopAnsweredUsersRoutine();
        };


        questionReviewModule.displayAnswers = function () {
            var answers = currentPosition.CurrentQuestion.Answers;

            $('#questionDetails__Answers').empty();
            for (var i = 0; i < answers.length; i++) {
                var newAnswerHtml = questionTemplatesModule.answerTemplateNotEditable;
                newAnswerHtml = newAnswerHtml.replace(new RegExp('##TEXT##', 'g'), answers[i].Text.replace(/\r\n/g, '<br/>'));

                var $answerElement = $(newAnswerHtml);

                if (answers[i].IsCorrect) {
                    $answerElement.find('label').append($('<span class="answerButton__Icon icon-checked"></span>'));
                }
                else {
                    $answerElement.find('label').append($('<span class="answerButton__Icon icon-close"></span>'));
                }

                $('#questionDetails__Answers').append($answerElement);
            }

        };

        questionReviewModule.setTimeInfoText = function () {
            questionReviewModule.setTimeInfoTextByPrefix('timeInfo');
            questionReviewModule.setTimeInfoTextByPrefix('timeInfoMobile');
        };

        questionReviewModule.setTimeInfoTextByPrefix = function (prefix) {
            $('#' + prefix + '__Text > span').hide();
            $('#' + prefix + '__NextQuestionAfter').show();
        };

        return questionReviewModule;
    }(questionReviewModule)
);

