﻿/// <reference path="questionAnswer.shared.js" />

var questionAnswerModule = (
    function (questionAnswerModule) {
        var currentPosition = undefined;

        questionAnswerModule.displayInternalContent = function (position) {
            currentPosition = position;

            currentPosition.IsAnswered = false;
            if (position.AnswerIds != null && position.AnswerIds.length > 0) {
                currentPosition.IsAnswered = true;
                questionAnswerModule.displayPromo(position.CurrentQuestion);
            }
            else {
                questionAnswerModule.hidePromo(position.CurrentQuestion);
            }
            questionAnswerModule.setTimeInfoText(currentPosition.IsAnswered);
            questionAnswerModule.setAnswerButtonState(currentPosition.IsAnswered ? 'answered' : 'noanswer');
            questionAnswerModule.displayAnswers();
            questionAnswerModule.validateVariantsOfAnswers();

            chatModuleAPI.hide();

            if (currentPosition.IsAnswered && currentPosition.CurrentQuestion.PromoEnabled) {
                questionAnswerModule.scrollToPromoBlock();
            }
        };

        questionAnswerModule.displayAnswers = function () {
            var answers = currentPosition.CurrentQuestion.Answers;
            var answerIds = currentPosition.AnswerIds;

            $('#questionDetails__Answers').empty();
            var answerTemplateToUse = questionTemplatesModule.answerTemplateEditable;
            if (currentPosition.IsAnswered) {
                answerTemplateToUse = questionTemplatesModule.answerTemplateNotEditable;
            }


            for (var i = 0; i < answers.length; i++) {
                var newAnswerHtml = answerTemplateToUse;
                newAnswerHtml = newAnswerHtml.replace(new RegExp('##ID##', 'g'), answers[i].Id);
                newAnswerHtml = newAnswerHtml.replace(new RegExp('##TEXT##', 'g'), answers[i].Text.replace(/\r\n/g, '<br/>'));
                newAnswerHtml = newAnswerHtml.replace(new RegExp('##TYPE##', 'g'), currentPosition.CurrentQuestion.IsSingleChoise ? 'radio' : 'checkbox');

                var $answerElement = $(newAnswerHtml);
                if (currentPosition.IsAnswered && answerIds.indexOf(answers[i].Id) > -1) {
                    $answerElement.addClass('answerButton--selected');
                }
                else {
                    var $answerCheckBox = $answerElement.find('.answerButton__Input');
                    $answerCheckBox.change(questionAnswerModule.validateVariantsOfAnswers);
                }
                $('#questionDetails__Answers').append($answerElement);

            }
        };

        questionAnswerModule.answerQuestion = function () {
            var isValid = questionAnswerModule.validateVariantsOfAnswers();
            if (!currentPosition.IsAnswered && isValid) {
                currentPosition.IsAnswered = true;
                var selectedAnswerIds = questionAnswerModule.getSelectedAnswerIds();

                questionAnswerModule.setAnswerButtonState('progress');
                gameCoreModule.answer(currentPosition.CurrentQuestion.Id, selectedAnswerIds);
            }
        };

        questionAnswerModule.scrollToPromoBlock = function () {
            $(function () {
                setTimeout(function () {
                    $('html, body').animate({
                        scrollTop: $("#promoBlockPlaceHolder").offset().top + $("#promoBlockPlaceHolder").height() + 20 - $(window).height()
                    }, 400);
                }, 400);

            });
        };

        questionAnswerModule.setAnswerButtonState = function (state) {
            $('.sticky-button-layout').show();
            $('#answerAction').unbind('click');
            $('#answerAction > b').hide();
            if (state == 'progress') {
                $('.sticky-button-layout.sticky').removeClass('sticky');
                $('#answerAction').attr('disabled', true);
                $('#answerAction__ProgressText').show();

            }
            else if (state == 'answered') {
                $('.sticky-button-layout.sticky').removeClass('sticky');
                $('#answerAction').attr('disabled', true);
                $('#answerAction__AnsweredText').show();

            }
            else {
                $('.sticky-button-layout').addClass('sticky');
                $('#answerAction').removeAttr('disabled');
                $('#answerAction__AnswerText').show();
                $('#answerAction').bind('click', questionAnswerModule.answerQuestion);
            }
        };

        questionAnswerModule.setTimeInfoText = function (isAnswered) {
            questionAnswerModule.setTimeInfoTextByPrefix(isAnswered, 'timeInfo');
            questionAnswerModule.setTimeInfoTextByPrefix(isAnswered, 'timeInfoMobile');
        };

        questionAnswerModule.setTimeInfoTextByPrefix = function (isAnswered, prefix) {
            $('#' + prefix + '__Text > span').hide();
            if (isAnswered) {
                $('#' + prefix + '__AnswerAfter').show();
            }
            else {
                $('#' + prefix + '__TimeForAnswerText').show();
            }
        };

        questionAnswerModule.getSelectedAnswerIds = function () {
            var selectedAnswerIds = [];

            $('input[data-answer-id]:checked').each(function (index, elm) {
                selectedAnswerIds.push($(elm).data('answer-id'));
            });

            return selectedAnswerIds;
        };

        questionAnswerModule.displayPlayerResult = function (score) {
            var leaderInfoHtml = questionAnswerModule.getLeaderHtml(score);
            $('#resultState__User').empty();
            $('#resultState__User').append($(leaderInfoHtml));
        }

        questionAnswerModule.validateVariantsOfAnswers = function () {
            var result = false;
            $('#validationError_AllVariantsChoosen').hide();
            if (!currentPosition.IsAnswered) {
                var countOfCheckboxesChecked = $('.answerButton__Input:checked').length;
                var countOfCheckboxesTotal = $('.answerButton__Input').length;
                if (countOfCheckboxesChecked == 0) {
                    $('#answerAction').attr('disabled', true);
                }
                else if (countOfCheckboxesChecked == countOfCheckboxesTotal) {
                    $('#answerAction').attr('disabled', true);
                    $('#validationError_AllVariantsChoosen').show();
                }
                else {
                    $('#answerAction').removeAttr('disabled');
                    result = true;
                }
            }

            return result;
        }

        return questionAnswerModule;
    }(questionAnswerModule)
);

