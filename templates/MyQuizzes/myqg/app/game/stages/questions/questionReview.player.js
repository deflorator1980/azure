﻿/// <reference path="questionReview.shared.js" />

var questionReviewModule = (
    function (questionReviewModule) {
        var currentPosition = undefined;

        questionReviewModule.displayInternalContent = function (position) {
            currentPosition = position;

            questionReviewModule.hideAnswerButton();
            questionReviewModule.hideValidationMessages();
            questionReviewModule.displayAnswers();
            questionReviewModule.setTimeInfoText();
            questionAnswerModule.displayPromo(position.CurrentQuestion);
            questionAnswerModule.stopAnsweredUsersRoutine();
        };

        questionReviewModule.displayAnswers = function () {
            var answers = currentPosition.CurrentQuestion.Answers;
            var answerIds = currentPosition.AnswerIds;

            $('#questionDetails__Answers').empty();
            for (var i = 0; i < answers.length; i++) {
                var newAnswerHtml = questionTemplatesModule.answerTemplateNotEditable;
                newAnswerHtml = newAnswerHtml.replace(new RegExp('##TEXT##', 'g'), answers[i].Text.replace(/\r\n/g, '<br/>'));

                var $answerElement = $(newAnswerHtml);

                if (answerIds != null && answerIds.indexOf(answers[i].Id) > -1) {
                    $answerElement.addClass('answerButton--selected');
                }

                if (answers[i].IsCorrect) {
                    $answerElement.find('label').append($('<span class="answerButton__Icon icon-checked"></span>'));
                }
                else {
                    $answerElement.find('label').append($('<span class="answerButton__Icon icon-close"></span>'));
                }

                $('#questionDetails__Answers').append($answerElement);
            }

        };

        questionReviewModule.setTimeInfoText = function () {
            questionReviewModule.setTimeInfoTextByPrefix('timeInfo');
            questionReviewModule.setTimeInfoTextByPrefix('timeInfoMobile');
        };

        questionReviewModule.setTimeInfoTextByPrefix = function (prefix) {
            $('#' + prefix + '__Text > span').hide();
            $('#' + prefix + '__NextQuestionAfter').show();
        };



        questionReviewModule.hideAnswerButton = function () {
            $('.sticky-button-layout').hide();
        };

        questionReviewModule.hideValidationMessages = function () {
            $('#validationError_AllVariantsChoosen').hide();
        };


        return questionReviewModule;
    }(questionReviewModule)
);

