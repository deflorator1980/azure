﻿/// <reference path="questionTemplates.shared.js" />

var questionReviewModule = (
	function (questionReviewModule, gameCoreModule) {

		var countDownTimer;

        questionReviewModule.show = function () {
            $('#stage__Questions').show();
        };

        questionReviewModule.hide = function () {
            $('#stage__Questions').hide();
        };

	    function disable() {
	        $('.questionDetails__QuestionCol').css('opacity', 0.5);
	        $('.resultState').css('opacity', 0.5);
	        $('.answerState').css('opacity', 0.5);
	        $('.leadersList').css('opacity', 0.5);
	        $('#timeInfoMobile__Seconds').css('opacity', 0.5);
	        $('#timeInfoMobile__Text').css('opacity', 0.5);
	        $('#question__countDownTimerMobile').css('opacity', 0.5);
	        $('.questionDetails__lock-layer').show();

	    };

	    function enable() {
	        $('.resultState').css('opacity', 1);
	        $('.answerState').css('opacity', 1);
	        $('.leadersList').css('opacity', 1);
	        $('#timeInfoMobile__Seconds').css('opacity', 1);
	        $('#timeInfoMobile__Text').css('opacity', 1);
	        $('#question__countDownTimerMobile').css('opacity', 1);
	        $('.questionDetails__QuestionCol').css('opacity', 1);
	        $('.questionDetails__lock-layer').hide();
	    };

		questionReviewModule.pause = function () {
            if (countDownTimer != undefined) {
                disable();
				countDownTimer.pause();
			}
		};

		questionReviewModule.resume = function () {
            if (countDownTimer != undefined) {
                enable();
				countDownTimer.resume();
			}
		};

        questionReviewModule.displayPosition = function (position) {
            questionAnswerModule.displayStaticContent(position);
            questionReviewModule.displayInternalContent(position);
            countDownTimer = countDownTimerModule.createTimer(
                'question__countDownTimer',
                'question__countDownTimerMobile',
                position.TimeLeft,
                position.TimeForReviewAnswerInSeconds * 1000
            );
            countDownTimer.timerTick.subscribe(function (tickStatus) {
                $('#timeInfoMobile__Seconds').text(tickStatus.remainingTimeInSeconds);
            });
            countDownTimer.setColor('color2');
			countDownTimer.show();
//	        if (gameCoreModule.isOwner) {
//		        countDownTimer.setCurrentPauseState(gameCoreModule.isPaused);
//	        }
            if (position.IsPaused) {
                disable();
                if (!gameCoreModule.isOwner) {
                    alertBoxModule.gamePausedShow();
                }
                countDownTimer.pause();
            } else {
                enable();
                if (!gameCoreModule.isOwner) {
                    alertBoxModule.gamePausedHide();
                }
                countDownTimer.resume();
            }
	        if (!gameCoreModule.isPaused) {
		        countDownTimer.start();
            }
            var note = position.CurrentQuestion.CorrectAnswerNote;
            if (position.CurrentQuestion != undefined &&
                position.CurrentQuestion != null &&
                position.CurrentQuestion.IsNeedCorrectAnswerNote &&
                note != undefined) {
                $('#questionDetails__CorrectAnswerNote').show();
                $('#questionDetails__CorrectAnswerNote .note-text')
                    .html(note.replace(/\r\n/g, '<br/>'));
            }
        };


        return questionReviewModule;
    }(questionReviewModule || {}, gameCoreModule)
);

