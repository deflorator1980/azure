﻿/// <reference path="../../elements/countDownTimer/countDownTimer.shared.js" />

var countdownModule = (
    function (countdownModule) {
	    var countDownTimer;
        countdownModule.show = function () {
            $('#stage__Countdown').show();
        };

        countdownModule.hide = function () {
            $('#stage__Countdown').hide();
		};

		countdownModule.pause = function () {
			if (countDownTimer != undefined) {
				countDownTimer.pause();
			}
		};

		countdownModule.resume = function () {
			if (countDownTimer != undefined) {
				countDownTimer.resume();
			}
		};

        countdownModule.start = function (time) {
            countDownTimer = countDownTimerModule.createTimer(
                'countDownTimerStart',
                undefined,
                time,
                time
            );

            countDownTimer.setColor('color1');

			countDownTimer.show();
	        if (!gameCoreModule.isPaused) {
		        countDownTimer.start();
	        }
        };

        return countdownModule;
    }(countdownModule || {})
);