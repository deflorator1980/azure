﻿/// <reference path="leaders.shared.js" />
var leadersModule = (
    function (leadersModule) {

        leadersModule.displayInternalContent = function (position) {
            console.log(position);

            chatModuleAPI.hide();

        };

        leadersModule.displayPlayerResult = function (playerResult) {
            $('#resultHeader').hide();
            $('#prizeHeader').hide();
            if (playerResult.ShowWinnerCode) {
                $('#prizeHeader').show();
                $('#prizeSendEmail__Place').text(playerResult.Position);
                $('#prizeSendEmail__PlaceSuffix').text(
                    localizationModule.GetSuffix(
                        playerResult.Position,
                        global_Localization.suffixesForPlaces
                    )
                );
                $('#prizeSendEmail__WinnerCode').text(playerResult.WinnerCode);
                $('#prizeSendEmail__EmailInput').val(playerResult.Email);
                $('#prizeSendEmail__SendButton').click(function () {
                    leadersModule.SendEmail({
                        Position: playerResult.Position,
                        WinnerCode: playerResult.WinnerCode,
                        Email: $('#prizeSendEmail__EmailInput').val()
                    });
                });
            }
            else {
                $('#resultHeader').show();

                if (playerResult.Score == 0) {
                    $('#yourResult__PlaceScore').hide();
                    $('#yourResult__Not__Answer').show();
                } else {
                    $('#yourResult__Not__Answer').hide();
                    $('#yourResult__PlaceScore').show();
                    $('#yourResult__Place').text(playerResult.Position);
                    $('#yourResult__Score').text(playerResult.Score);
                    $('#yourResult__ScoreText').text(
                        localizationModule.GetDeclension(
                            playerResult.Score,
                            global_Localization.wordsForPoints
                        )
                    );
                }

                $('#yourResult__Name').text(playerResult.User.FullName);

                var userPhotoDefault = '/app/images/userprofile/user_photo.png';
                if (playerResult.User.ImagePath != undefined) {
                    $('#yourResult__Foto').css('background-image', 'url(' + playerResult.User.ImagePath + ')');
                } else {
                    $('#yourResult__Foto').css('background-image', 'url(' + userPhotoDefault + ')');
                }
            }

        };

        leadersModule.SendEmail = function (quizWinnerCodeInfo) {
            var emailIsValid = leadersModule.EmailInputValidate($('#prizeSendEmail__EmailInput'));
            if (emailIsValid) {
                $('#prizeSendEmail__EmailInput').attr('disabled', true);
                $('#prizeSendEmail__SendButton').attr('disabled', true);

                gameCoreModule.sendQuizWinnerCode(quizWinnerCodeInfo, function (result) {
                    console.log(result);
                    if (result.success) {
                        $('#prizeSendEmail__Info').hide();
                        $('#prizeSendEmail__SuccessResult').show();
                    }
                });
            }
        };

        leadersModule.EmailInputValidate = function (emailInput) {
            var emailRegEx = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
            if ($(emailInput).val() === '' || !emailRegEx.test($(emailInput).val())) {
                $(emailInput).addClass('input-validation-error');
                return false;
            } else {
                $(emailInput).removeClass('input-validation-error');
                return true;
            }
        };

        return leadersModule;
    }(leadersModule)
);

