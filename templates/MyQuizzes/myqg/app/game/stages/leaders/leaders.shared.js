﻿/*jshint multistr: true */
var leadersModule = (
    function (leadersModule, gameCoreModule) {
        var leaderPositionInfoTemplate = '\
<div class="leaderPositionInfo">\
    <div class="leaderPositionInfo__PositionRow">\
        <div class="leaderPositionInfo__PositionCol">\
            <div class="leaderPositionInfo__Position">##POSITION##</div>\
        </div>\
        <div class="leaderPositionInfo__FotoCol">\
            <div class="leaderPositionInfo__Foto"##STYLE_FOTO##></div>\
        </div>\
        <div class="leaderPositionInfo__NameCol">\
            <div class="leaderPositionInfo__Name">##USERNAME##</div>\
        </div>\
        <div class="leaderPositionInfo__BadgeCol">\
            <div class="leaderPositionInfo__Badge" data-icon></div>\
        </div>\
        <div class="leaderPositionInfo__ScoreCol">\
            <div class="leaderPositionInfo__Score">##SCORE##</div>\
        </div>\
    </div>\
</div>';

        
        leadersModule.show = function () {
            $('#stage__Leaders').show();
        };

        leadersModule.hide = function () {
            $('#stage__Leaders').hide();
        };


        leadersModule.displayPosition = function (position) {
            leadersModule.displayStaticContent(position);
            leadersModule.displayInternalContent(position);
        };

        leadersModule.displayStaticContent = function (position) {
            console.log(position);
            leadersModule.displayLeaders(position.TopScores);
        };

        leadersModule.displayLeaders = function (topScores) {
            $('#leaderBoard__LeftCol').empty();
            $('#leaderBoard__RightCol').empty();

            if (topScores.length > 0) {
                for (var i = 0; i < topScores.length; i++) {
                    leadersModule.displayLeader(i, topScores[i]);
                }
            }
            else {
                $('#leaderBoard__List').hide();
                $('#leaderBoard__NoLeaders').show();
            }
        };

        leadersModule.displayLeader = function (index, topScore) {
            var leaderPositionInfoHtml = leadersModule.getLeaderHtml(topScore);

            if (index < 5) {
                $('#leaderBoard__LeftCol').append($(leaderPositionInfoHtml));
            }
            else {
                $('#leaderBoard__RightCol').append($(leaderPositionInfoHtml));
            }
        };

        leadersModule.getLeaderHtml = function (topScore) {
            var leaderPositionInfoHtml = leaderPositionInfoTemplate;
            leaderPositionInfoHtml = leaderPositionInfoHtml.replace(new RegExp('##USERNAME##', 'g'), topScore.User.FullName);
            leaderPositionInfoHtml = leaderPositionInfoHtml.replace(new RegExp('##SCORE##', 'g'), topScore.Score);
            leaderPositionInfoHtml = leaderPositionInfoHtml.replace(new RegExp('##POSITION##', 'g'), topScore.Position);

            var styleForFoto = '';
            var userPhotoDefault = '/app/images/userprofile/user_photo.png';
            if (topScore.User.ImagePath != undefined) {
                styleForFoto = ' style="background-image:url(' + topScore.User.ImagePath + ')"';
            } else {
                styleForFoto = ' style="background-image:url(' + userPhotoDefault + ')"';
            }

            leaderPositionInfoHtml = leaderPositionInfoHtml.replace(new RegExp('##STYLE_FOTO##', 'g'), styleForFoto);

            return leaderPositionInfoHtml;
        };


        return leadersModule;
    }(leadersModule || {}, gameCoreModule)
);

