﻿var connectModule = (
    function (connectModule, gameCoreModule) {
        
        connectModule.show = function () {
            $('#stage__Connect').show();
        };

        connectModule.hide = function () {
            $('#stage__Connect').hide();
        };

        return connectModule;
    }(connectModule || {}, gameCoreModule)
);

