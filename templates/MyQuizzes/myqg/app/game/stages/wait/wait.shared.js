﻿var waitModule = (
    function (waitModule) {

        waitModule.show = function () {
            $('#stage__Wait').show();
        };

        waitModule.hide = function () {
            $('#stage__Wait').hide();
        };

        $(function () {
            var imageBlock = imageBlockModule.init('gameDetails__ImageBlock');
        });

        return waitModule;
    }(waitModule || {})
);

