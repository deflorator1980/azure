﻿/// <reference path="wait.shared.js" />

waitModule = (
    function (waitModule) {
        $(function () {
            $('#gameActions__StartGame').click(function () {
                gameCoreModule.start();
            });

            $('#gameActions__SwitchToManualMode').click(function () {
                gameCoreModule.switchToManualMode();
            });

            $('#gameActions__StopGame').click(function () {
                gameCoreModule.stop();
            });

        });

        waitModule.displayPosition = function (position) {
            waitModule.updatePlayersCount(position.PlayersCount);
        };


        waitModule.updatePlayersCount = function (count) {
            $('#usersCount').text(count);
            $('#usersCountMobile').text(count);
        }

        waitModule.autoStartDisabled = function () {
            $('#gameActions__StartGame').show();
            $('#gameActions__SwitchToManualMode').hide();
            $('#gameDetails__StartTime').hide();
        }

        return waitModule;
    }(waitModule)
);

