﻿/// <reference path="wait.shared.js" />
(function () {
    function Clock(clockContainer) {
        var $this = this;
        init();
        function init() {
            $this.clock = {
                days: clockContainer.find("[time-clock-element-type='days']"),
                hours: clockContainer.find('[time-clock-element-type="hours"]'),
                minutes: clockContainer.find('[time-clock-element-type="minutes"]'),
                seconds: clockContainer.find('[time-clock-element-type="seconds"]')
            };
            $this.clockValue = {
                days: parseInt($this.clock.days.text()),
                hours: parseInt($this.clock.hours.text()),
                minutes: parseInt($this.clock.minutes.text()),
                seconds: parseInt($this.clock.seconds.text())
            };
            $this.tickTimeout = setTimeout(tick, 1000);
        }

        function updateClock() {
            var clock = $this.clock;
            var clockValue = $this.clockValue;
            clock.seconds.text(clockValue.seconds);
            clock.minutes.text(clockValue.minutes);
            clock.hours.text(clockValue.hours);
            clock.days.text(clockValue.days);
        }

        function stopClock() {
            var clock = $this.clock;
            clock.seconds.css('color', '#959494');
            clock.minutes.css('color', '#959494');
            clock.hours.css('color', '#959494');
            clock.days.css('color', '#959494');

            initLoader();
        }

        function initLoader() {
            $('.time-layout').after($("<div class='loaderContainer'>" +
                                                    "<div class='loaderContainer__loader'>" +
                                                        "<div class='loader--dot'></div>" +
                                                        "<div class='loader--dot'></div>" +
                                                        "<div class='loader--dot'></div" +
                                                        "><div class='loader--dot'></div>" +
                                                        "<div class='loader--dot'></div>" +
                                                        "<div class='loader--dot'></div>" +
                                                        "<div class='loader--text'>" + global_Localization.waitingMessage+ "</div>" +
                                                      "</div>" +
                                                 "</div>"));
        }

        function tick() {
            if (decreaseSeconds()) {
                updateClock();
                $this.tickTimeout = setTimeout(tick, 1000);
            }
        }

        function decreaseSeconds() {
            if ($this.clockValue.seconds === 0) {
                if (decreaseMinutes()) {
                    $this.clockValue.seconds = 59;
                } else {
                    stopClock();
                    return false;
                }
            } else {
                $this.clockValue.seconds -= 1;
            }
            return true;
        }

        function decreaseMinutes() {
            if ($this.clockValue.minutes === 0) {
                if (decreaseHours()) {
                    $this.clockValue.minutes = 59;
                } else {
                    return false;
                }
            } else {
                $this.clockValue.minutes -= 1;
            }
            return true;
        }

        function decreaseHours() {
            if ($this.clockValue.hours === 0) {
                if (decreaseDays()) {
                    $this.clockValue.hours = 23;
                } else {
                    return false;
                }
            } else {
                $this.clockValue.hours -= 1;
            }
            return true;
        }

        function decreaseDays() {
            if ($this.clockValue.days > 0) {
                $this.clockValue.days -= 1;
                return true;
            }
            return false;
        }
    }

    if (typeof jQuery !== "undefined" && jQuery !== null) {
        jQuery.fn.countDownClock = function () {
            return new Clock(this);
        };
    }
    else {
        console.error("JQuery not found");
    }
}).call(this);

waitModule = (
    function (waitModule) {
        waitModule.displayPosition = function (position) {
        };

        waitModule.autoStartDisabled = function () {
            $('#gameDetails__StartTime').hide();
        }

        $(function () {
            $('.time-clock-layout').first().countDownClock();
        });

        return waitModule;
    }(waitModule)
);