﻿(function () {
    function Clock(clockContainer) {
        var $this = this;
        init();
        function init() {
            $this.clock = {
                days : clockContainer.find("[time-clock-element-type='days']"),
                hours : clockContainer.find('[time-clock-element-type="hours"]'),
                minutes : clockContainer.find('[time-clock-element-type="minutes"]'),
                seconds : clockContainer.find('[time-clock-element-type="seconds"]')
            };
            $this.clockValue = {
                days : parseInt($this.clock.days.text()),
                hours : parseInt($this.clock.hours.text()),
                minutes : parseInt($this.clock.minutes.text()),
                seconds : parseInt($this.clock.seconds.text())
            };
            $this.tickTimeout = setTimeout(tick, 1000);
        }

        function updateClock() {
            var clock = $this.clock;
            var clockValue = $this.clockValue;
            clock.seconds.text(clockValue.seconds);
            clock.minutes.text(clockValue.minutes);
            clock.hours.text(clockValue.hours);
            clock.days.text(clockValue.days);
        }

        function tick() {
            decreaseSeconds();
            updateClock();
            $this.tickTimeout = setTimeout(tick, 1000);
        }

        function decreaseSeconds() {
            if ($this.clockValue.seconds === 0) {
                if (decreaseMinutes()) {
                    $this.clockValue.seconds = 59;
                }
            } else {
                $this.clockValue.seconds -= 1;
            }
        }

        function decreaseMinutes() {
            if ($this.clockValue.minutes === 0) {
                if (decreaseHours()) {
                    $this.clockValue.minutes = 59;
                } else {
                    return false;
                }
            } else {
                $this.clockValue.minutes -= 1;
            }
            return true;
        }

        function decreaseHours() {
            if ($this.clockValue.hours === 0) {
                if (decreaseDays()) {
                    $this.clockValue.hours = 23;
                } else {
                    return false;
                }
            } else {
                $this.clockValue.hours -= 1;
            }
            return true;
        }

        function decreaseDays() {
            if ($this.clockValue.days > 0) {
                $this.clockValue.days -= 1;
                return true;
            }
            return false;
        }
    }

    if (typeof jQuery !== "undefined" && jQuery !== null) {
        jQuery.fn.countDownClock = function () {
            return new Clock(this);
        };
        jQuery.fn.setStartTime = function() {
            var calendar = this;
            var startDateData = calendar.attr('start-date-data');
            var locale = calendar.attr('locale-data');
            moment.locale(locale);
            var startDate = moment.utc(startDateData).local();
            var dayAndMonth = startDate.format('D MMMM');
            var dayAndMonthParts = dayAndMonth.split(' ');
            calendar.find('.day').first().text(dayAndMonthParts[0]);
            calendar.find('.month').first().text(dayAndMonthParts[1]);
            if (locale == 'en') {
                calendar.find('.hour').first().text(startDate.format('h'));
                calendar.find('.minute').first().text(startDate.format('mm'));
                calendar.find('.hour_am_pm').first().text(startDate.format('A'));
            } else {
                calendar.find('.hour').first().text(startDate.format('H'));
                calendar.find('.minute').first().text(startDate.format('mm'));
            }
            //calendar.find('.time-zone').first().text('GMT ' + startDate.format('Z'));
        };
    }
    else {
        console.error("JQuery not found");
    }
}).call(this);

var gameLandingModule = (
    function (gameLandingModule) {
        function checkRegistrationAvailability() {
            $.ajax({
                url: global_QuizCodeWaitStartCheck_url,
                success: function (result) {
                    if (result.hostIsAvailable) {
                        $('#redirectToConnectForm').submit();
                    }
                },
                error: function () { }
            });
        }

        $(function () {
            $('.time-clock-layout').first().countDownClock();
        });

        setInterval(checkRegistrationAvailability, 5000);


        return gameLandingModule;
    }(gameLandingModule || {})
);



$(document).ready(function () {
    $.cookie('user.timezone.id', moment.tz.guess(), { path: '/' });
    $('.start-time').first().setStartTime();
});