﻿/// <reference path="core/game.core.player.js" />
/// <reference path="game.shared.js" />
/// <reference path="elements/chatBlock/chatBlock.player.js" />
/// <reference path="stages/connect/connect.shared.js" />
/// <reference path="stages/wait/wait.player.js" />
/// <reference path="stages/countdown/countdown.shared.js" />
/// <reference path="stages/questions/questionAnswer.player.js" />
/// <reference path="stages/questions/questionReview.player.js" />
/// <reference path="stages/leaders/leaders.player.js" />
/// <reference path="core/add.to.cart.shared.js" />

gameModule = (
    function (gameModule, gameCoreModule) {

        gameModule.connect = function () {
            gameCoreModule.join();
        };

        var cartSelector = ".blockForCart";

        gameCoreModule.waitStageStarted.subscribe(waitStageStarted);
        function waitStageStarted(position) {
            addToCartModule.initAll(cartSelector);
        }

        gameCoreModule.questionsAnswerStageStarted.subscribe(questionsAnswerStageStarted);
        function questionsAnswerStageStarted(position) {
            gameCoreModule.playerResultGet();
            addToCartModule.initAll(cartSelector);
        }

        gameCoreModule.questionsReviewStageStarted.subscribe(questionsReviewStageStarted);
        function questionsReviewStageStarted(position) {
            gameCoreModule.playerResultGet();
            addToCartModule.initAll(cartSelector);
        }

        gameCoreModule.leadersStageStarted.subscribe(leadersStageStarted);
        function leadersStageStarted(position) {
            gameCoreModule.playerResultGet();
            addToCartModule.initAll(cartSelector);
        }

        gameCoreModule.hostStopped.subscribe(hostStopped);
        function hostStopped(position) {
            document.location = '/Game/QuizCode?quizCode=' + global_quizCode;
        }

        gameCoreModule.setPauseState.subscribe(setPauseState);
        function setPauseState(isPaused) {
            gameCoreModule.isPaused = isPaused;
            if (isPaused) {
                gameCoreModule.pause();
                alertBoxModule.gamePausedShow();
            } else {			    
                gameCoreModule.resume();
                alertBoxModule.gamePausedHide();
            }
        };

        gameCoreModule.playerResult.subscribe(playerResult);
        function playerResult(score) {
            questionAnswerModule.displayPlayerResult(score);
            leadersModule.displayPlayerResult(score);
        }


        return gameModule;
    }(gameModule, gameCoreModule)
);