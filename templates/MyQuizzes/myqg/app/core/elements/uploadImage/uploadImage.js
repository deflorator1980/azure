﻿var uploadImageModule = (
    function (uploadImageModule) {
        uploadImageModule.InitAll = function () {
            $(function () {
                $('[data-upload-image]').each(function (index, elm) {
                    uploadImageModule.Init(elm);
                });
            });
        };


        uploadImageModule.Init = function (elm, uploadStrategy, fileUrl, isPhotoChangedElement) {
            var uploadImage = $(elm).data('uploadImageAPI');
            if (uploadImage === undefined) {
                uploadImage = uploadImageAPI(elm, uploadStrategy, fileUrl, isPhotoChangedElement);
            }

            return uploadImage;
        };


        function uploadImageAPI(elm, uploadStrategy, fileUrl, isPhotoChangedElement) {
            var self = this;
            var supportedMimeTypes = 'image/png,image/jpg,image/jpeg';
            var imageForOperations = undefined;
            var imageForDisplay = undefined;
            var imageName = undefined;
            var imageWidth = undefined;
            var imageHeight = undefined;
            var currentAngle = 0;
            var canvas = undefined;
            var ctx = undefined;
            var $elementMain = $(elm);
            var $elementImageIdField = $elementMain.find('input');
            var $elementImage= $elementMain.find('img');
            var $elementForm = $('<form></form>');
            var $elementInputField = $('<input type="file" style="display: none;" accept="' + supportedMimeTypes + '"/>');

            self.isPhotoChangedElement = isPhotoChangedElement;

            $elementForm.append($elementInputField);
            $elementMain.after($elementForm);

            $elementMain.on('click', thumbClicked);
            $elementInputField.on('change', fileSelected);

            readSettings();
            initThumb();

            self.uploadStrategy = uploadStrategy;

            
            if (fileUrl) {
                toDataUrl(fileUrl,
                    function(data) {
                        var e = new Object();
                        e.target = new Object();
                        e.target.result = data;
                        fileReadCompleted(e);
                    });
            }


            //self.UploadCompleted = new EventObserver();

            function readSettings() {
                $elementMain.data('default-thumb-path', $elementImage.attr('src'));

                self.settings = {
                    aspectRatio: 1,
                    cropForm: 'rectangle',
                    compressionType: 'image/jpeg',
                    compressionQuality: 0.8,
                    thumbWidth: 800
                };
                if ($elementMain.data('aspect-ratio') !== undefined) {
                    self.settings.aspectRatio = eval($elementMain.data('aspect-ratio'));
                }
                if ($elementMain.data('crop-form') != undefined) {
                    self.settings.cropForm = $elementMain.data('crop-form');
                }
                if ($elementMain.data('compression-type') != undefined) {
                    self.settings.compressionType = $elementMain.data('compression-type');
                }
                if ($elementMain.data('compression-quality') != undefined) {
                    self.settings.compressionQuality = eval($elementMain.data('compression-quality'));
                }
                if ($elementMain.data('thumb-width') != undefined) {
                    self.settings.thumbWidth = eval($elementMain.data('thumb-width'));
                }
            }

            function toDataUrl(url, callback) {
                try{
                var xhr = new XMLHttpRequest();
                xhr.onload = function () {
                    var reader = new FileReader();
                    reader.onloadend = function () {
                        callback(reader.result);
                    }
                    reader.readAsDataURL(xhr.response);
                };
                xhr.onerror = function() {
                    console.log('SN image is not accessible');
                };
                xhr.open('GET', url);
                xhr.responseType = 'blob';
                xhr.send();
                } catch (error) {
                    console.log(error);
                }
            }

            function initThumb() {
                if ($elementMain.data('thumb-path') != undefined && $elementMain.data('thumb-path').length > 0) {
                    $elementImage.attr('src', $elementMain.data('thumb-path'));
                }
            }

            function thumbClicked() {
                $elementInputField.click();
            }

            function browseButtonClicked() {
                $elementInputField.click();
            }

            function fileSelected() {
                $('#cropImageModal__ErrorModeration').hide();
                $('#cropImageModal__ErrorUnknown').hide();
                $('#cropImageModal__ErrorUnsupportedMimeType').hide();

                console.log('fileSelected');
                spinnerModule.Show();
                self.startTime = +new Date();
                if (this.files.length > 0) {
                    var file = this.files[0];

                    setTimeout(function () {
                        if (fileValidate(file)) {
                            fileRead(file);
                        }
                        else {
                            spinnerModule.Hide();
                            showCropModal();
                        }
                    }, 100);
                }
                else {
                    spinnerModule.Hide();
                }
            }

            function fileValidate(file) {
                console.log(file);
                $('#cropImageModal__ErrorUnsupportedMimeType').hide();
                if (!supportedMimeTypes.match(file.type)) {
                    $('#cropImageModal__ErrorUnsupportedMimeType').show();
                    return false;
                }
                return true;
            }

            function fileRead(file) {
                imageName = file.name;
                var reader = new FileReader();                
                reader.onload = fileReadCompleted;
                reader.readAsDataURL(file);
            }

            function fileReadCompleted(e) {
                console.log('fileReadCompleted-1');
                imageForDisplay = new Image();
                imageForOperations = new Image();
                imageForOperations.onload = imageLoaded;
                imageForOperations.src = e.target.result;
                $elementForm[0].reset();
            }

            function imageLoaded(e) {
                imageForOperations.onload = undefined;
                console.log('imageLoaded-1');
                copyImageToCanvas();
                console.log('imageLoaded-2');
                copyDrawToImgForOperations();
                copyDrawToImgForDisplay();
                console.log('imageLoaded-3');
                spinnerModule.Hide();
                showCropModal();
                
                console.log('ready for:' + (+new Date() - self.startTime));
            }

            function copyImageToCanvas() {
                imageWidth = 518;
                oldWidth = imageForOperations.width;
                oldHeight = imageForOperations.height;
                imageHeight = Math.floor(oldHeight / oldWidth * imageWidth);

                canvas = document.createElement('canvas');
                canvas.width = imageWidth;
                canvas.height = imageHeight;

                currentAngle = 0;
                ctx = canvas.getContext('2d');
                ctx.drawImage(imageForOperations, 0, 0, imageWidth, imageHeight);
            }

            function canvasRotateLeft() {
                currentAngle -= 90;
                canvasRotate();
                
            }

            function canvasRotateRight() {
                currentAngle += 90;
                canvasRotate();
            }

            function canvasRotate() {
                ctx.clearRect(0, 0, canvas.width, canvas.height);
                newWidth = canvas.height;
                newHeight = canvas.width;
                canvas.width = newWidth;
                canvas.height = newHeight;
                console.log([canvas.width, canvas.height]);
                ctx.translate(newWidth / 2, newHeight / 2);
                ctx.rotate(currentAngle * Math.PI / 180);
                ctx.drawImage(
                    imageForOperations,
                    -imageWidth / 2,
                    -imageHeight / 2,
                    imageWidth,
                    imageHeight
                );
            }

            function copyDrawToImgForOperations() {
                imageForOperations.src = canvas.toDataURL(
                    self.settings.compressionType,
                    self.settings.compressionQuality);
            }

            function copyDrawToImgForDisplay() {
                imageForDisplay.src = canvas.toDataURL(
                    self.settings.compressionType,
                    self.settings.compressionQuality);
            }


            function showCropModal() {
                var modalIsShown = ($('#cropImageModal').data('bs.modal') || {}).isShown;
                if (!modalIsShown) {
                    $('#cropImageModal').modal('show');
                }
                $('#cropImageModal__Cropper').empty()
                    .append($(imageForDisplay))
                    .removeClass()
                    .addClass('cropImageModal__Cropper cropImageModal__Cropper--' + self.settings.cropForm);
                $('#cropImageModal__ButtonUpload')
                    .off('click')
                    .on('click', browseButtonClicked);
                $('#cropImageModal__ButtonReady')
                    .off('click')
                    .on('click', uploadImage);
                $('#cropImageModal__ButtonRotateRight')
                    .off('click')
                    .on('click', rotateRight);
                $('#cropImageModal__ButtonDelete')
                    .off('click')
                    .on('click', deleteImage);

                console.log(modalIsShown)
                var showCropperDelay = modalIsShown ? 0 : 300;
                setTimeout(showCropper, showCropperDelay);

            }

            function showCropper() {
                if (self.cropper != undefined) {
                    try {
                        self.cropper.destroy();
                    }
                    catch (err) {
                        console.log('cropper destroy error');
                        console.log(err);
                    }
                }
                self.cropper = new Cropper(imageForDisplay, {
                    aspectRatio: self.settings.aspectRatio,
                    viewMode: 3,
                    dragMode: 'none',
                    autoCropArea: 1,
                    restore: false,
                    guides: false,
                    highlight: true,
                    cropBoxMovable: true,
                    cropBoxResizable: true,
                    zoomOnWheel: false,
                    zoomOnTouch: false,
                    rotatable: false,
                    responsive: true,
                    minContainerWidth: 1,
                    minContainerHeight: 1,
                    ready: function () {
                        //$quizImageForCropp[0].cropper.setCropBoxData(result.AdditionalInfo);
                    }
                });
            }

            function uploadImage() {
                $('#cropImageModal__ErrorModeration').hide();
                $('#cropImageModal__ErrorUnknown').hide();
                spinnerModule.Show();
                self.uploadStrategy.uploadProfilePhoto(self.cropper, function(result) {
                    spinnerModule.Hide();
                    $('#cropImageModal').modal('hide');
                    self.isPhotoChangedElement.val(true);
                }, function(error){
                    spinnerModule.Hide();
                    $('#cropImageModal__ErrorUnknown').show();
                }, imageForDisplay, imageName);
            }

            function deleteImage() {
                $elementImageIdField.val('');
                $elementMain.data('original-path', '');
                $elementMain.data('thumb-path', $elementMain.data('default-thumb-path'));
                initThumb();
                $('#cropImageModal').modal('hide');
                if (self.uploadStrategy.isDownloadedHiddenElement !== undefined) {
                    self.uploadStrategy.isDownloadedHiddenElement.val(false);
                }
                self.isPhotoChangedElement.val(true);
            }

            function rotateLeft() {
                canvasRotateLeft();
                copyDrawToImgForDisplay();
                showCropper();
                self.isPhotoChangedElement.val(true);
            }

            function rotateRight() {
                canvasRotateRight();
                copyDrawToImgForDisplay();
                showCropper();
                self.isPhotoChangedElement.val(true);
            }

            return self;
        }


        

        return uploadImageModule;
    }(uploadImageModule || {})
);