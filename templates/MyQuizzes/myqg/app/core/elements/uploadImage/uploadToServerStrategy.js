﻿function uploadToServerStrategy($elementImageIdField, $elementMain, initThumbFunc, $settings) {
    var $this = this;
    $this.elementImageIdField = $elementImageIdField;
    $this.elementMain = $elementMain;
    $this.initThumbFunc = initThumbFunc;
    $this.settings = $settings;

    $this.upload = function(cropper, success, fail, imageForDisplay, imageName) {
        $('#cropImageModal__ErrorModeration').hide();
        $('#cropImageModal__ErrorUnknown').hide();
        spinnerModule.Show();
        var cropBoxData = cropper.getData();
        var dataToSend = {
            ImageData: imageForDisplay.src,
            ImageName: imageName + '.jpg',
            ReturnThumbWidth: $this.settings.thumbWidth,
            Crop: {
                Width: cropBoxData.width,
                Height: cropBoxData.height,
                X: cropBoxData.x,
                Y: cropBoxData.y
            },
            Rotate: {
                Angle: cropBoxData.rotate
            }
        };
        $.ajax({
            type: 'POST',
            url: "/Image/UploadAsync",
            data: JSON.stringify(dataToSend),
            contentType: 'application/json; charset=utf-8',
            dataType: 'json',
            success: function(data) {
                if (data.success) {
                    $this.elementImageIdField.val(data.image.Id);
                    $this.elementMain.data('original-path', data.image.OriginalPath);
                    $this.elementMain.data('thumb-path', data.image.ThumbPath);
                    $this.initThumbFunc();
                    $('#cropImageModal').modal('hide');
                } else {
                    if (data.isAdultImage) {
                        $('#cropImageModal__ErrorModeration').show();
                    } else {
                        $('#cropImageModal__ErrorUnknown').show();
                    }
                }
                success(data);
            },
            error: function(data) {
                fail(data);
            }
        });
    }
};