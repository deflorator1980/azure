﻿var spinnerModule = (
    function (spinnerModule) {
        spinnerModule.Show = function () {
            $('#fullScreenSpinner').show();
        };
        
        spinnerModule.Hide = function () {
            $('#fullScreenSpinner').hide();
        };

        return spinnerModule;
    }(spinnerModule || {})
);