﻿var menuModule = (
    function (menuModule) {

        $(function () {
            $('#menuLang__RU').click(function () {
                menuModule.setLanguage('ru');
            });
            $('#menuLang__EN').click(function () {
                menuModule.setLanguage('en');
            });

            $('#burger').click(function () {

                if ($('#topMenu').is('.topMenu--open')) {
                    menuModule.closeMobileMenu();
                }
                else {
                    menuModule.openMobileMenu();
                }
            });
        });

        menuModule.setLanguage = function (lang) {
            $.cookie('lang', lang, { expires: 100, path: '/' });
            location.reload();
        };

        menuModule.openMobileMenu = function () {
            console.log('menuModule.openMobileMenu');
            $("body").css("overflow", "hidden");
            $('#burger').css('z-index', '1003');
            $('#burger > .burger__Stick').addClass('burger__Stick--collapse');

            $('#topMenu').addClass('topMenu--open');
            $('#topMenu__Items').css({ 'position': 'absolute', 'z-index': '1002' });
            $('#topMenu__Items').show();
            //$('#topMenu li').addClass('animate');

            $('#burger__Circle').addClass('burger__Circle--expand').css('z-index', '1001');

            setTimeout(function () {
                $('#burger > .burger__Stick--y').hide();
                $('#burger > .burger__Stick--x').addClass('burger__Stick--rotate30');
                $('#burger > .burger__Stick--z').addClass('burger__Stick--rotate150');
            }, 70);
            setTimeout(function () {
                $('#burger > .burger__Stick--x').addClass('burger__Stick--rotate45');
                $('#burger > .burger__Stick--z').addClass('burger__Stick--rotate135');
            }, 120);
        }

        menuModule.closeMobileMenu = function() {
            console.log('menuModule.closeMobileMenu');
            $("body").css("overflow", "auto");
            $('#burger').removeAttr('style');

            $('#topMenu').removeClass('topMenu--open');
            $('#topMenu__Items').removeAttr('style');
            $('#topMenu__Items').hide();
            
            $('#burger__Circle').removeClass('burger__Circle--expand').removeAttr('style');

            $('#burger > .burger__Stick--x')
                .removeClass('burger__Stick--rotate45')
                .addClass('burger__Stick--rotate30');
            $('#burger > .burger__Stick--z')
                .removeClass('burger__Stick--rotate135')
                .addClass('burger__Stick--rotate150');

            setTimeout(function () {
                $('#burger > .burger__Stick--x').removeClass('burger__Stick--rotate30');
                $('#burger > .burger__Stick--z').removeClass('burger__Stick--rotate150');
            }, 50);
            setTimeout(function () {
                $('#burger > .burger__Stick--y').show();
                $('#burger > .burger__Stick').removeClass('burger__Stick--collapse');
            }, 70);
        }

        return menuModule;
    }(menuModule || {})
);