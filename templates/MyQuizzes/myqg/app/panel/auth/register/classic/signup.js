﻿var signUpModule = (
    function (signUpModule) {
        signUpModule.Init = function (isRecaptchaEnabled) {
            uploadImageModule.InitAll();
            $(function () {
                if (isRecaptchaEnabled) {
                    var $form = $('#registrationForm');
                    $form.on('submit', function (event) {
                        event.preventDefault();
                        $form.validate();
                        if ($form.valid()) {
                            grecaptcha.execute();
                        }
                    });
                }
            });
        };

        signUpModule.OnRecaptchaValidated = function (token) {
            var $form = $('#registrationForm');
            $form[0].submit();
        };

        return signUpModule;
    }(signUpModule || {})
);