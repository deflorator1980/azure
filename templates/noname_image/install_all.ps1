# web server
Install-WindowsFeature Web-Server -IncludeManagementTools 

Install-WindowsFeature Web-Http-Redirect
Install-WindowsFeature Web-Custom-Logging 
Install-WindowsFeature Web-Request-Monitor 
Install-WindowsFeature Web-Http-Tracing 

Install-WindowsFeature Web-CertProvider
Install-WindowsFeature Web-Net-Ext45
Install-WindowsFeature Web-Asp-Net45
Install-WindowsFeature Web-ISAPI-Ext
Install-WindowsFeature Web-ISAPI-Filter
Install-WindowsFeature Web-Includes
Install-WindowsFeature Web-WebSockets

# ASP.NET 4.6
Get-WindowsFeature *ASP*
Add-WindowsFeature NET-Framework-45-ASPNET

# порты
New-NetFirewallRule -DisplayName "Https" -Direction Inbound -Action Allow -EdgeTraversalPolicy Allow -Protocol TCP -LocalPort 80,443,8088 -Program "C:\Program Files (x86)\TestIPv6App.exe"

netsh int ipv4 set dynamicport tcp start=10000 num=55536

# choco
Set-ExecutionPolicy Bypass -Scope Process -Force; iex ((New-Object System.Net.WebClient).DownloadString('https://chocolatey.org/install.ps1'))