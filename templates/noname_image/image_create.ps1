$vmName = "noname"
$rgName = "myquiz-move"
$location = "EastUS"
$imageName = "noname"

Stop-AzVM -ResourceGroupName $rgName -Name $vmName -Force

Set-AzVm -ResourceGroupName $rgName -Name $vmName -Generalized

$vm = Get-AzVm -Name $vmName -ResourceGroupName $rgName

$image = New-AzImageConfig -Location $location -SourceVirtualMachineId $vm.Id

New-AzImage -Image $image -ImageName $imageName -ResourceGroupName $rgName

# Create an image from a managed disk using PowerShell  https://clck.ru/HaC2J