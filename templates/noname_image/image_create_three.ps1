$vmName = "noname"
$rgName = "myquiz-move"
$location = "EastUS"
$imageName = "noname"

# create snapshot of vm before do it
$snapshot = Get-AzSnapshot -ResourceGroupName $rgName -SnapshotName $snapshotName

$imageConfig = New-AzImageConfig -Location $location

$imageConfig = Set-AzImageOsDisk -Image $imageConfig -OsState Generalized -OsType Windows -SnapshotId $snapshot.Id

New-AzImage -ImageName $imageName -ResourceGroupName $rgName -Image $imageConfig

# Create an image from a snapshot using Powershell https://clck.ru/HaC2J