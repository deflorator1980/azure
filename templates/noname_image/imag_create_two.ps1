$vmName = "noname"
$rgName = "myquiz-move"
$location = "EastUS"
$imageName = "noname"

$vm = Get-AzVm -Name $vmName -ResourceGroupName $rgName

$diskID = $vm.StorageProfile.OsDisk.ManagedDisk.Id

$imageConfig = New-AzImageConfig -Location $location

$imageConfig = Set-AzImageOsDisk -Image $imageConfig -OsState Generalized -OsType Windows -ManagedDiskId $diskID

New-AzImage -ImageName $imageName -ResourceGroupName $rgName -Image $imageConfig

# Create an image from a managed disk using PowerShell https://clck.ru/HaC2J