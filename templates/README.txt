az group deployment create --resource-group myquiz-move --template-file azuredeploy_vmss_win.json --parameters @azuredeploy_vmss_win.parameters.json
az group deployment create --resource-group myquiz-move --template-file templates/arm/vmss_create/azuredeploy_vmss_win.json --parameters @templates/arm/vmss_create/azuredeploy_vmss_win.parameters.json

az group deployment create --resource-group myquiz-move --template-file azuredeploy_vmss_win.json --parameters @azuredeploy_vmss_win.parameters.json --parameters '{"vmssName": { "value": "yyy"}}'

az group deployment create --resource-group myquiz-move --template-file azuredeploy_vmss_win.json --parameters @azuredeploy_vmss_win.parameters.json --parameters '{"vmssName": { "value": "foo"}}'

az group deployment create --resource-group myquiz-move --template-file templates/arm/vmss_create/azuredeploy_vmss_win.json --parameters @templates/arm/vmss_create/azuredeploy_vmss_win.parameters.json --parameters '{"instanceCount": { "value": 2}}'

az group deployment create --resource-group myquiz-move --template-file azuredeploy_vmss_win_noautoscale_two.json --parameters azuredeploy_vmss_win.parameters.json 

az group deployment create --resource-group myquiz-move --template-file azuredeploy_vmss_win_noautoscale.json --parameters azuredeploy_vmss_win.parameters.json 